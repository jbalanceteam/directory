﻿drop  table if exists ProductImage CASCADE;
drop table if exists Product CASCADE;
drop table if exists Packing CASCADE;
drop table if exists Measure CASCADE;
drop table if exists Juridical CASCADE;
drop table if exists TradeMark CASCADE;
drop table if exists AgeBracket CASCADE;
drop table if exists ColdProcessingLossPercent CASCADE;
drop table if exists FlowChart CASCADE;
drop table if exists FlowChart_FlowChartImage CASCADE;
drop table if exists FlowChartImage CASCADE;
drop table if exists GrossNorm CASCADE;
drop table if exists NetNorm CASCADE;
drop table if exists Stay CASCADE;
drop table if exists ProductCategory CASCADE;
drop table if exists FlowChartImage_ColdProcessingLossPercent CASCADE;
drop table if exists Contractor CASCADE;
drop table if exists EducationalInstitution CASCADE;
drop table if exists Netnorm_Grossnorm CASCADE;
drop table if exists Product_Packing CASCADE;
drop table if exists Trademark_Productimage CASCADE;

