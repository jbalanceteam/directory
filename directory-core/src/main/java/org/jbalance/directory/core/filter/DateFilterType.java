package org.jbalance.directory.core.filter;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.jbalance.directory.core.util.DateUtil;
import org.jbalance.directory.core.util.Str;

public enum DateFilterType implements FilterType {
    /**
     * <p>Equals or equal-to.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> = <i>rvalue</i></tt></p>
     */
    EQUALS("Equals", ArgumentsExpected.ONE),
    /**
     * <p>Equals or equal-to a date without time.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> &gt;= convert(datetime, cast(<i>rvalue</i> as int)) AND <i>lvalue</i> &lt; convert(datetime, cast(<i>rvalue</i> as int) + 1)</tt></p>
     */
    SAME_DAY("Equals Date", ArgumentsExpected.ONE),
    /**
     * <p>Between (date comparison).  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> BETWEEN <i>rvalue1</i> AND <i>rvalue2</i></tt> <i>(inclusive)</i></p>
     */
    BETWEEN("Between", ArgumentsExpected.TWO),
    /**
     * <p>Greater than.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> &gt; <i>rvalue</i></tt></p>
     */
    GT("Greater Than", ArgumentsExpected.ONE),
    /**
     * <p>Less than.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> &lt; <i>rvalue</i></tt></p>
     */
    LT("Less Than", ArgumentsExpected.ONE),
    /**
     * <p>Greater than or equal to.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> &gt;= <i>rvalue</i></tt></p>
     */
    GTE("Greater Than or Equal To", ArgumentsExpected.ONE),
    /**
     * <p>Less than or equal to.  Equivalent to:</p>
     * <p><tt><i>lvalue</i> &lt;= <i>rvalue</i></tt></p>
     */
    LTE("Less Than or Equal To", ArgumentsExpected.ONE),
    /**
     * <p>Is null.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> IS NULL}.</tt></p>
     */
    NULL("Is Null", ArgumentsExpected.NONE),
    /**
     * <p>In.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> IN (<i>rvalue</i>[0],
     * <i>rvalue</i>[1], ...)</tt></p>
     */
    IN("In", ArgumentsExpected.ONE_OR_MORE),
    /**
     * <p>Greater than or is null.  Equivalent to:</p>
     *
     * <p><tt>(<i>lvalue</i> &gt; <i>rvalue</i> OR <i>lvalue</i> IS NULL)</tt></p>
     */
    GTN("Greater Than or Is Null", ArgumentsExpected.ONE),
    /**
     * <p>Less than or is null.  Equivalent to:</p>
     *
     * <p><tt>(<i>lvalue</i> &lt; <i>rvalue</i> OR <i>lvalue</i> IS NULL)</tt></p>
     */
    LTN("Less Than or Is Null", ArgumentsExpected.ONE),
    /**
     * <p>Between (date comparison) or null.  Equivalent to:</p>
     *
     * <p><tt>(<i>lvalue</i> BETWEEN <i>rvalue1</i> AND <i>rvalue2</i> OR <i>lvalue</i> IS NULL)</tt> <i>(inclusive)</i></p>
     */
    BETWEEN_OR_NULL("Between or Is Null", ArgumentsExpected.TWO);


    private final String description;
    private final ArgumentsExpected argumentsExpected;

    /**
     * Returns a user-friendly description of the comparison operator
     *
     * @return a user-friendly description of the comparison operator
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * Returns the number of arguments expected by this filter type
     *
     * @return number of arguments expected by this filter type
     */
    @Override
    public ArgumentsExpected getArgumentsExpected() {
        return argumentsExpected;
    }

    /**
     * Creates a new comparison with the specified description (internal use
     * only)
     *
     * @param description A user-friendly description of the comparison
     * operator
     */
    private DateFilterType(String description, ArgumentsExpected argumentsExpected) {
        this.description = description;
        this.argumentsExpected = argumentsExpected;
    }

    /**
     * Returns the user-friendly description of the comparison.  This returns
     * the same value as {@link #getDescription()}
     *
     * @return the user-friendly description of the comparison
     *
     * @see #getDescription()
     */
    @Override
    public String toString() {
        return description;
    }

    @Override
    public void appendFilter(StringBuilder query, FilterParams params, String fieldName, List<?> filterValues) {
        if(query == null) {
            throw new IllegalArgumentException("Query string builder is required");
        }

        if(params == null) {
            throw new IllegalArgumentException("Filter parameters are required");
        }

        if(Str.empty(fieldName)) {
            throw new IllegalArgumentException("Field name is required");
        }

        String paramName1 = null;
        String paramName2 = null;
        if(this == NULL) {
            query.append(" ");
            query.append(fieldName);
            query.append(" is null");
        } else if(this == BETWEEN) {
            if(filterValues == null || filterValues.size() != 2) {
                throw new IllegalArgumentException(this + " filter type requires exactly two filter values");
            }
            paramName1 = params.addParam(filterValues.get(0));
            paramName2 = params.addParam(filterValues.get(1));

            query.append(" ");
            query.append(fieldName);
            query.append(" between ");
            query.append(" :").append(paramName1);
            query.append(" and :").append(paramName2);
        } else if(this == SAME_DAY) {
            if(filterValues == null || filterValues.size() != 1) {
                throw new IllegalArgumentException(this + " filter type requires exactly one filter value");
            }
            Date filterValue = (Date)filterValues.get(0);
            Date date1 = DateUtil.trunc(filterValue, "yyyy-MM-dd");
            Calendar cal = Calendar.getInstance();
            cal.setTime(date1);
            cal.add(Calendar.DATE, 1);
            Date date2 = cal.getTime();

            paramName1 = params.addParam(date1);
            paramName2 = params.addParam(date2);

            query.append(" (");
            query.append(fieldName);
            query.append(" >= :");
            query.append(paramName1);
            query.append(" and ");
            query.append(fieldName);
            query.append(" < :");
            query.append(paramName2);
            query.append(")");
        } else if(this == IN) {
            if(filterValues == null || filterValues.isEmpty()) {
                // IN (empty list) will always evaluate to false, but we will
                // get a query syntax exception if the parameter list is
                // missing.  To avoid this, substitute with an expression that
                // will always evaluate to false.
                query.append(" 1=0");
            } else {
                paramName1 = params.addParam(filterValues);
                query.append(" ");
                query.append(fieldName);
                query.append(" in (:");
                query.append(paramName1);
                query.append(")");
            }
        } else if(this == GTN) {
            if(filterValues == null || filterValues.size() != 1) {
                throw new IllegalArgumentException(this + " filter type requires exactly one filter value");
            }

            paramName1 = params.addParam(filterValues.get(0));

            query.append(" (");
            query.append(fieldName);
            query.append(" >");
            query.append(" :").append(paramName1);
            query.append(" or ");
            query.append(fieldName);
            query.append(" is null)");
        } else if(this == LTN) {
            if(filterValues == null || filterValues.size() != 1) {
                throw new IllegalArgumentException(this + " filter type requires exactly one filter value");
            }

            paramName1 = params.addParam(filterValues.get(0));

            query.append(" (");
            query.append(fieldName);
            query.append(" <");
            query.append(" :").append(paramName1);
            query.append(" or ");
            query.append(fieldName);
            query.append(" is null)");
        } else if(this == BETWEEN_OR_NULL) {
            if(filterValues == null || filterValues.size() != 2) {
                throw new IllegalArgumentException(this + " filter type requires exactly two filter values");
            }
            paramName1 = params.addParam(filterValues.get(0));
            paramName2 = params.addParam(filterValues.get(1));

            query.append(" ((");
            query.append(fieldName);
            query.append(" between ");
            query.append(" :").append(paramName1);
            query.append(" and :").append(paramName2);
            query.append(") or ").append(fieldName);
            query.append(" is null)");
        } else {
            if(filterValues == null || filterValues.size() != 1) {
                throw new IllegalArgumentException(this + " filter type requires exactly one filter value");
            }

            paramName1 = params.addParam(filterValues.get(0));

            query.append(" ");
            query.append(fieldName);
            query.append(" ");

            switch(this) {
                case EQUALS:
                    query.append(" =");
                    break;
                case GT:
                    query.append(" >");
                    break;
                case GTE:
                    query.append(" >=");
                    break;
                case LT:
                    query.append(" <");
                    break;
                case LTE:
                    query.append(" <=");
                    break;
                default:
                    // Should never happen, but Sonar will complain if we don't
                    // have a default case
                    throw new UnsupportedOperationException("Unsupported filter type: " + this);
            }

            query.append(" :").append(paramName1);
        }
    }
}
