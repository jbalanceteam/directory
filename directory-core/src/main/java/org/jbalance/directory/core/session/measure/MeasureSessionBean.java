package org.jbalance.directory.core.session.measure;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import org.jbalance.directory.core.model.measure.Measure;
import org.jbalance.directory.core.query.helper.measure.GetMeasuresRequest;
import org.jbalance.directory.core.query.helper.measure.GetMeasuresRequestHelper;
import org.jbalance.directory.core.query.helper.measure.GetMeasuresResult;
import org.jbalance.directory.core.session.GenericSessionBean;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@Stateless
public class MeasureSessionBean extends GenericSessionBean<Measure, GetMeasuresRequest> implements MeasureSession {

    @Override
    public List<Measure> list(GetMeasuresRequest request) {
        
        GetMeasuresResult result = new GetMeasuresRequestHelper(request).createResult(manager);
                
        return result.getMeasures();
    }

    @Override
    @PostConstruct
    public void init() {
        beanClass = Measure.class;
    }
}
