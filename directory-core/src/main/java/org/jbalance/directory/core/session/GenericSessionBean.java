package org.jbalance.directory.core.session;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.jbalance.directory.core.model.BaseEntity;
import org.jbalance.directory.core.query.GetRequest;

import com.google.common.base.Preconditions;


/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 * @param <T> - Реализация класса справочника, наследующая от {@link BaseEntity}
 * @param <S> - Реализация {@link GetRequest}, для конкретного класса справочника <tt>T</tt>
 */
public abstract class GenericSessionBean<T extends BaseEntity, S extends GetRequest<?>> extends BaseSessionBean implements GenericSession<T, S>
{
    protected Class<T> beanClass;

    /**
     * Метод инициализации бина справочника.<br/>
     * Реализация должна присваивать переменной {@link GenericSessionBean#beanClass} конкретное значение класса экземпляра справочника.
     */
    public abstract void init();
    
    @Override
    public T add(T bean) {
        bean.setEnteredDate(new Date());
        bean.setModifiedDate(bean.getEnteredDate());
        manager.persist(bean);
        return bean;
    }

    @Override
    public T delete(Long id) {
    	Preconditions.checkNotNull(id, "Id must not be null.");
        T bean = getById(id);
        bean.delete();
        bean.setModifiedDate(new Date());
        return manager.merge(bean);
    }

    @Override
    public T getById(Long id) {
    	Preconditions.checkNotNull(id, "Id must not be null.");
        return (T) manager.find(beanClass, id);
    }

    @Override
    public T edit(T o) {
    	if(!o.isPersistent()){
    		throw new RuntimeException("Use add method to save record initially. Edit method is only for updating.");
    	}
    	o.setModifiedDate(new Date());
        return manager.merge(o);
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<T> getUpdates(Date date) {
        CriteriaBuilder cb = manager.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(beanClass);
        Root<T> r = cq.from(beanClass);
        cq.select(r);
        Predicate p = cb.greaterThan(r.<Date>get("modifiedDate"), date);
        cq.where(p);
        Query query = manager.createQuery(cq);
        return query.getResultList();
    }



    @Override
    public abstract List<T> list(S r);

}
