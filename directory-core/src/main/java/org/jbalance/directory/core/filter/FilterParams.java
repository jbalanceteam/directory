package org.jbalance.directory.core.filter;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

public class FilterParams implements Serializable {
    private static final long serialVersionUID = 1L;

    private Map<String, Object> paramMap = new LinkedHashMap<String, Object>();

    public FilterParams() {

    }

    public Map<String, Object> getParamMap() {
        // allow the param map to be modifiable so that QueryHelper
        // can use the same map without rewriting it (for now)
//        return Collections.unmodifiableMap(paramMap);
        return paramMap;
    }

    public String addParam(Object paramValue) {
        String paramName = nextParamName();
        paramMap.put(paramName, paramValue);
        return paramName;
    }

    public String addParam(String paramName, Object paramValue) {
        paramMap.put(paramName, paramValue);
        return paramName;
    }

    private String nextParamName() {
        final String paramNameBase = "p";
        String paramName = null;
        int index = 0;
        do {
            paramName = new StringBuilder(paramNameBase).append(index).toString();
            index++;
        } while (paramMap.containsKey(paramName));

        return paramName;
    }
}
