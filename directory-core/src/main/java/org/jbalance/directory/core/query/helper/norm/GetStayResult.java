package org.jbalance.directory.core.query.helper.norm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import org.jbalance.directory.core.model.norm.Stay;
import org.jbalance.directory.core.query.GetResult;

/**
 *
 * @author Mihail
 */
public class GetStayResult  extends GetResult{
    private static final long serialVersionUID = 1L;
    
    private List<Stay> stayList;
    
    @XmlElement(name = "stayList")
    public List<Stay> getStays()
    {
        if (stayList == null)
        {
            stayList = new ArrayList<Stay>();
        }
        return stayList;
    }
    
    public void setStayList(List<Stay> stayList)
    {
        this.stayList = stayList;
    }

    public GetStayResult(GetStayRequest request)
    {
        super(request);
    }
    
}
