package org.jbalance.directory.core.model;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;

/**
 *
 * @author roman
 */
@Entity
public class JuridicalGroup extends BaseEntity {

    private String name;

    @ManyToMany
    private List<Juridical> juridicals = new LinkedList<Juridical>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Juridical> getJuridicals() {
        return juridicals;
    }

    public void setJuridicals(List<Juridical> juridicals) {
        this.juridicals = juridicals;
    }
}
