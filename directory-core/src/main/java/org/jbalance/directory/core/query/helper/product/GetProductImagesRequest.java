package org.jbalance.directory.core.query.helper.product;

import javax.xml.bind.annotation.XmlType;
import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.query.GetRequest;

/**
 * @author Komov Roman
 */
@XmlType(propOrder = {
            "idFilter",
            "nameFilter",
            "productImageOkpFilter",
        })
public class GetProductImagesRequest extends GetRequest<GetProductImagesResult> {

    private static final long serialVersionUID = 1L;

    private StringFilter productImageOkpFilter;
    private LongFilter idFilter;
    private StringFilter nameFilter;

    public StringFilter getNameFilter() {
        return nameFilter;
    }

    public void setNameFilter(StringFilter nameFilter) {
        this.nameFilter = nameFilter;
    }

    @Override
    public GetProductImagesResult createResult() {
        return new GetProductImagesResult(this);
    }

    public StringFilter getProductImageOkpFilter() {
        return productImageOkpFilter;
    }

    public void setProductImageOkpFilter(StringFilter productImageOkpFilter) {
        this.productImageOkpFilter = productImageOkpFilter;
    }

    public LongFilter getIdFilter() {
        return idFilter;
    }

    public void setIdFilter(LongFilter idFilter) {
        this.idFilter = idFilter;
    }

}
