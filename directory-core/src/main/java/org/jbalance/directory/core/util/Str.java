package org.jbalance.directory.core.util;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.geom.Rectangle2D;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IllegalFormatException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * <p>
 * Common string manipulation shortcuts. The methods are static and are as terse
 * as possible to facilitate in-line use.
 * </p>
 *
  */
public class Str {
    private static Log log = new Log(Str.class);

    /**
     * <p>
     * Returns {@code true} if the specified object is {@code null} or can be
     * cast to a String that contains nothing but whitespace characters (no
     * matter how many whitespace characters it contains.)
     * </p>
     *
     * @param obj
     *            The object to check
     * @return {@code true} if the specified object is {@code null} or is a
     *         {@code String} that contains no non-whitespace characters
     */
    public static boolean empty(Object obj) {
        return obj instanceof String ? empty((String) obj) : obj == null;
    }

    /**
     * <p>
     * Returns {@code true} if the specified string is {@code null} or contains
     * nothing but whitespace characters (no matter how many whitespace
     * characters it contains.)
     * </p>
     *
     * @param str
     *            The string to check
     * @return {@code true} if the specified string contains no non-whitespace
     *         characters
     */
    public static boolean empty(String str) {
        return str == null || str.trim().isEmpty();
    }

    /**
     * <p>Returns the {@code len} left most characters of {@code str}</p>
     *
     * @param str The string to truncate
     * @param len The maximum length
     * @param trim Whether to trim the string before truncating
     *
     * @return The {@code len} left most characters of {@code str}
     *
     * @deprecated Use {@link #ltrunc2(java.lang.String, int, boolean)}.  (This
     * method was renamed to {@code ltrunc2} when the {@link
     * #rtrunc2(java.lang.String, int, boolean)} method was introduced to be
     * consistent and avoid confusion.)
     */
    @Deprecated
    public static String trunc2(String str, int len, boolean trim) {
        return ltrunc2(str, len, trim);
    }

    /**
     * <p>Shortcut for {@code ltrunc2(str, len, false)}</p>
     *
     * @param str The string to truncate
     * @param len The maximum length
     *
     * @return The specified string truncated to be no longer than the specified
     *         number of characters
     *
     * @see #ltrunc2(java.lang.String, int, boolean)
     *
     * @deprecated Use {@link #left(java.lang.String, int)}.  (This method was
     * renamed to {@code ltrunc} when the {@link
     * #right(java.lang.String, int, boolean)} method was introduced to be
     * consistent and avoid confusion.)
     */
    @Deprecated
    public static String trunc(String str, int len) {
        return ltrunc2(str, len, false);
    }

    /**
     * <p>Returns the {@code len} left-most characters of {@code str}</p>
     *
     * @param str The string to truncate
     * @param len The maximum length
     * @param trim Whether to trim the string before truncating
     *
     * @return The {@code len} left most characters of {@code str}
     */
    public static String ltrunc2(String str, int len, boolean trim) {
        if (str != null) {
            if (trim) {
                str = str.trim();
            }
            if (str.length() > len) {
                str = str.substring(0, len);
            }
        }
        return str;
    }

    /**
     * <p>Shortcut for {@code ltrunc2(str, len, false)}</p>
     *
     * @param str The string to truncate
     * @param len The maximum length
     *
     * @return The specified string truncated to be no longer than the specified
     *         number of characters
     *
     * @see #ltrunc(java.lang.String, int, boolean)
     */
    public static String ltrunc(String str, int len) {
        return ltrunc2(str, len, false);
    }

    /**
     * <p>Returns the {@code len} right-most characters in the specified
     * string</p>
     *
     * @param str The string to truncate
     * @param len The maximum length
     * @param trim Whether to trim the string before truncating
     *
     * @return The {@code len} right-most characters in the specified string
     */
    public static String rtrunc2(String str, int len, boolean trim) {
        if (str != null) {
            if (trim) {
                str = str.trim();
            }
            if (str.length() > len) {
                str = str.substring(str.length() - len);
            }
        }
        return str;
    }

    /**
     * <p>
     * Shortcut for {@code rtrunc2(str, len, false)}
     * </p>
     *
     * @param str The string to truncate
     * @param len The maximum length
     * @return The specified string truncated to be no longer than the specified
     *         number of characters
     * @see #right(java.lang.String, int, boolean)
     */
    public static String rtrunc(String str, int len) {
        return rtrunc2(str, len, false);
    }

    /**
     * <p>
     * Determines whether two strings are equivalent using a broader set of
     * conditions than the character-by-character comparison in {@code
     * String.equals}.
     * </p>
     *
     * @param str1
     *            The first string
     * @param str2
     *            The second string
     * @param trim
     *            Whether to trim both strings before comparing them
     * @param ignoreCase
     *            Whether the comparison should be case insensitive
     * @param nullsEqual
     *            Whether two null values should be considered equal
     * @return {@code true} if the two strings are equivalent according to the
     *         specified conditions
     */
    public static boolean eq(String str1, String str2, boolean trim, boolean ignoreCase, boolean nullsEqual) {
        if (trim) {
            str1 = str1 == null ? null : str1.trim();
            str2 = str2 == null ? null : str2.trim();
        }
        if (str1 == null || str2 == null) {
            return str1 == null && str2 == null && nullsEqual;
        }
        return ignoreCase ? str1.equalsIgnoreCase(str2) : str1.equals(str2);
    }

    /**
     * <p>
     * Shortcut for {@code eq(str1, str2, true, false, false)}
     * </p>
     *
     * @param str1
     *            The first string
     * @param str2
     *            The second string
     * @return {@code true} if the trimmed versions of the strings are equal
     * @see #eq(java.lang.String, java.lang.String, boolean, boolean, boolean)
     */
    public static boolean tEq(String str1, String str2) {
        return eq(str1, str2, true, false, false);
    }

    /**
     * <p>
     * Shortcut for {@code eq(str1, str2, false, true, false)}
     * </p>
     *
     * @param str1
     *            The first string
     * @param str2
     *            The second string
     * @return {@code true} if the strings are equal ignoring case
     * @see #eq(java.lang.String, java.lang.String, boolean, boolean, boolean)
     */
    public static boolean iEq(String str1, String str2) {
        return eq(str1, str2, true, true, false);
    }

    /**
     * <p>
     * Shortcut for {@code eq(str1, str2, true, true, false)}
     * </p>
     *
     * @param str1
     *            The first string
     * @param str2
     *            The second string
     * @return {@code true} if the trimmed versions of the strings are equal
     *         ignoring case
     * @see #eq(java.lang.String, java.lang.String, boolean, boolean, boolean)
     */
    public static boolean tiEq(String str1, String str2) {
        return eq(str1, str2, true, true, false);
    }

    /**
     * <p>
     * Shortcut for {@code eq(str1, str2, true, true, true)}
     * </p>
     *
     * @param str1
     *            The first string
     * @param str2
     *            The second string
     * @return {@code true} if the trimmed versions of the strings are equal
     *         ignoring case or are both {@code null}
     * @see #eq(java.lang.String, java.lang.String, boolean, boolean, boolean)
     */
    public static boolean tinEq(String str1, String str2) {
        return eq(str1, str2, true, true, true);
    }

    /**
     * <p>
     * Compares two strings using a broader set of conditions than the
     * character-by-character comparison in {@code String.equals}.
     * </p>
     *
     * @param str1
     *            The first string
     * @param str2
     *            The second string
     * @param trim
     *            Whether to trim both strings before comparing them
     * @param ignoreCase
     *            Whether the comparison should be case insensitive
     * @param nullsEqual
     *            Whether two null values should be considered equal
     * @param nullsFirst
     *            Whether null values should be considered "less than" non-null
     *            values. Only applies if {@code nullsEqual} is {@code false}.
     * @return A negative integer, a positive integer, or zero if {@code str1}
     *         is less than, greater than, or equal to {@code str2} according to
     *         the specified conditions
     */
    public static int cmp(String str1, String str2, boolean trim, boolean ignoreCase, boolean nullsEqual, boolean nullsFirst) {
        if (trim) {
            str1 = str1 == null ? null : str1.trim();
            str2 = str2 == null ? null : str2.trim();
        }
        if (str1 == null || str2 == null) {
            if (str1 == null && str2 == null && nullsEqual) {
                return 0;
            } else if (str1 == null) {
                return nullsFirst ? -1 : 1;
            } else {
                return nullsFirst ? 1 : -1;
            }
        }
        return ignoreCase ? str1.compareToIgnoreCase(str2) : str1.compareTo(str2);
    }

    /**
     * <p>
     * Shortcut for {@code cmp(str1, str2, true, false, false, false)}
     * </p>
     *
     * @param str1
     *            The first string
     * @param str2
     *            The second string
     * @return A negative integer, a positive integer, or zero if {@code str1}
     *         is less than, greater than, or equal to {@code str2} after
     *         leading and trailing whitespace has been removed
     * @see #eq(java.lang.String, java.lang.String, boolean, boolean, boolean)
     */
    public static int tCmp(String str1, String str2) {
        return cmp(str1, str2, true, false, false, false);
    }

    /**
     * <p>
     * Shortcut for {@code cmp(str1, str2, false, true, false, false)}
     * </p>
     *
     * @param str1
     *            The first string
     * @param str2
     *            The second string
     * @return A negative integer, a positive integer, or zero if {@code str1}
     *         is less than, greater than, or equal to {@code str2} regardless
     *         of case
     * @see #eq(java.lang.String, java.lang.String, boolean, boolean, boolean)
     */
    public static int iCmp(String str1, String str2) {
        return cmp(str1, str2, false, true, false, false);
    }

    /**
     * <p>
     * Shortcut for {@code cmp(str1, str2, false, true, false, false)}
     * </p>
     *
     * @param str1
     *            The first string
     * @param str2
     *            The second string
     * @return A negative integer, a positive integer, or zero if {@code str1}
     *         is less than, greater than, or equal to {@code str2} regardless
     *         of case or leading and trailing whitespace
     * @see #eq(java.lang.String, java.lang.String, boolean, boolean, boolean)
     */
    public static int tiCmp(String str1, String str2) {
        return cmp(str1, str2, true, true, false, false);
    }

    /**
     * <p>
     * Returns a string of the specified length filled with the specified
     * character.
     * </p>
     *
     * @param len
     *            The desired length
     * @param c
     *            The fill character
     * @return a string of the specified length filled with the specified
     *         character
     */
    public static String fill(int len, char c) {
        char[] str = new char[len];
        Arrays.fill(str, c);
        return new String(str);
    }

    /**
     * <p>
     * Pads the specified string on the left with the specified character so
     * that the result is the specified length. If the length of specified
     * string is greater than the specified length then the original string is
     * returned.
     * </p>
     *
     * @param str
     *            The string to pad
     * @param c
     *            The character to pad with
     * @param len
     *            The resulting length
     * @return The specified string left-padded with the specified character to
     *         form a new string of the specified length
     */
    public static String lpad(String str, char c, int len) {
        if(str == null) {
            return null;
        }

        int padLen = len - str.length();
        if (padLen > 0) {
            String pad = fill(padLen, c);
            str = new StringBuilder(pad).append(str).toString();
        }
        return str;
    }

    /**
     * <p>
     * Pads the specified string on the right with the specified character so
     * that the result is the specified length. If the length of specified
     * string is greater than the specified length then the original string is
     * returned.
     * </p>
     *
     * @param str
     *            The string to pad
     * @param c
     *            The character to pad with
     * @param len
     *            The resulting length
     * @return The specified string right-padded with the specified character to
     *         form a new string of the specified length
     */
    public static String rpad(String str, char c, int len) {
        if(str == null) {
            return null;
        }
        int padLen = len - str.length();
        if (padLen > 0) {
            String pad = fill(padLen, c);
            str = new StringBuilder(str).append(pad).toString();
        }
        return str;
    }

    /**
     * <p>
     * Generates a comma-separated list with or without an "and" separator
     * preceding the final item in the list. Standard English rules apply:
     * </p>
     * <ul>
     * <li>If there is only one item specified, that item is returned with no
     * additional punctuation.</li>
     * <li>If two items "A" and "B" are specified, and {@code conjunction} is
     * not empty, then the result will be "A and B"</li>
     * <li>If {@code conjunction} is empty and there is more than one item in
     * the collection, then each item will be separated by a comma followed by a
     * single space (e.g. "A, B, C")</li>
     * <li>If more than two items is specified and {@code conjuction} is not,
     * empty, then the following rules apply:
     * <ul>
     * <li>If {@code commaBeforeConjunction} is {@code true}, then all items
     * except the first and last will be preceded by a comma followed by a
     * single space. The last item will be preceded by a comma followed by a
     * space, followed by the conjunction and another space (e.g.
     * "A, B, and C").</li>
     * <li>If {@code commaBeforeConjunction} is {@code false}, then all items
     * except the first and last will be preceded by a comma followed by a
     * single space. The last item will be preceded by a space followed by the
     * conjunction and another space (e.g. "A, B and C").</li>
     * </ul>
     * </li>
     * </ul>
     * <p>
     * The objects in the collection will be converted into strings individually
     * using their {@code toString} methods. If this is not the form correct
     * form, then they should be converted into {@code String}s prior to adding
     * them to the list. {@code null} or empty objects will be omitted from the
     * list.
     * </p>
     *
     * @param objs
     *            The list of objects to format as a list
     * @param conjunction
     *            (Optional) The conjuction to use to separate the
     *            second-to-last and last item in the list (i.e. "and", "or",
     *            "not", etc.)
     * @param commaBeforeConjunction
     *            Whether to insert a comma before the conjunction if the list
     *            is more than 2 items long.
     * @return A comma-separated list
     */
    public static String list(Collection<?> objs, String conjunction, boolean commaBeforeConjunction) {
        List<String> strings = new ArrayList<String>();
        for (Object obj : objs) {
            if (!empty(obj)) {
//                if (obj instanceof UiElement) {
//                    strings.add(((UiElement) obj).getShortDescription());
//                } else {
                    strings.add(obj.toString());
//                }
            }
        }
        StringBuilder list = new StringBuilder();
        int last = strings.size() - 1;
        if (last >= 0) {
            list.append(strings.get(0));
            for (int i = 1; i < last; i++) {
                list.append(", ").append(strings.get(i));
            }
            if (last > 0) {
                if (empty(conjunction) || last > 1 && commaBeforeConjunction) {
                    list.append(", ");
                } else {
                    list.append(" ");
                }
                if (!empty(conjunction)) {
                    list.append(conjunction).append(" ");
                }
                list.append(strings.get(last));
            }
        }
        return list.toString();
    }

    /**
     * <p>
     * Same as the {@code list} method that operates on collections, but for
     * arrays. Same as calling {@code list(Arrays.asList(objs), conjunction,
     * commaBeforeConjunction)}.
     * </p>
     *
     * @param objs
     *            An array of objects
     * @param conjunction
     *            (Optional) The conjuction to use to separate the
     *            second-to-last and last item in the list (i.e. "and", "or",
     *            "not", etc.)
     * @param commaBeforeConjunction
     *            Whether to insert a comma before the conjunction if the list
     *            is more than 2 items long.
     * @return A comma-separated list
     */
    public static String list(Object[] objs, String conjunction, boolean commaBeforeConjunction) {
        return list(Arrays.asList(objs), conjunction, commaBeforeConjunction);
    }

    /**
     * Converts an ASCII String to a Hexadecimal value
     *
     * @param ascii
     * @return A hexadecmal value
     */
    public static String asciiToHex(String ascii) {
        if (empty(ascii)) {
            return null;
        }
        StringBuilder hex = new StringBuilder();
        for (int i = 0; i < ascii.length(); i++) {
            hex.append('%').append(Integer.toHexString(ascii.charAt(i)));
        }
        return hex.toString();
    }

    /**
     * @param string
     * @return trimmed string or empty string if input is null
     */
    public static String trim(String string) {
        if (string == null) {
            return "";
        }
        return string.trim();
    }

    /**
     * Returns a duration string. startTime is required, endTime is optional. if
     * endTime is null, the current system time is used Samples: 1.5 days - if
     * duration has days and hours between 12 and 18 23.25 hours - if duration
     * has 0 days 14 mins - if no hours or days
     *
     * @param startDate
     * @param endDate
     * @return friendly duration string or Unknown if startTime is null
     * @deprecated Use {@link DateUtil#duration(java.util.Date,java.util.Date)}
     *             instead
     */
//    @Deprecated
//    public static String duration(java.util.Date startDate, java.util.Date endDate) {
//        return DateUtil.duration(startDate, endDate);
//    }

    /**
     * Capitalizes each word based upon the StringTokenizer break points.
     *
     * @param str
     *            String - Returns itself if null or empty
     * @return String with each word capitalized based upon standard break
     *         points
     */
//    public static String capWords(String str) {
//        if (empty(str)) {
//            return str;
//        }
//        // previous implementation threw PatternSyntaxException on symbols
//        return WordUtils.capitalize(str);
//    }

    /**
     * Checks to see if the String is alphanumeric based upon the regex
     * [A-Za-z0-9] This is a wrapper to save a few lines of code for a common
     * check, it also is usable by calling #{Str.isAlphaNumeric(value)} from EL
     * expressions.
     *
     * @param str
     *            String - returns false if null or empty
     * @return True if its alphanumeric
     */
    public static boolean isAlphaNumeric(String str) {
        if (empty(str)) {
            return false;
        }
        return str.matches("[A-Za-z0-9]+");
    }

    /**
     * Calculates the width in pixels that the string will occupy given the current font settings.
     * @param str
     * @param font
     * @return
     */
    public static int getWidthInPixels(String str, Font font) {
        if (empty(str)) {
            return 0;
        }
        if (font == null) {
            font = new Font("Arial", Font.PLAIN, 10);
        }

        FontMetrics metrics = new FontMetrics(font) {private static final long serialVersionUID = 1L;};
        Rectangle2D bounds = metrics.getStringBounds(str, null);
        return (int) bounds.getWidth();
    }

    protected Str() {
        // hide default constructor for classes containing only static methods
    }

    public static String initials(String... nameParts) {
        StringBuilder initials = new StringBuilder();
        for(String namePart : nameParts) {
            if(!Str.empty(namePart)) {
                initials.append(namePart.trim().charAt(0));
            }
        }
        return initials.toString().toUpperCase();
    }

    /**
     * Used by {@link Log} and exception constructors to generate pattern-based
     * messages
     *
     * @param pattern A string consisting of {@link
     * java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     * and/or {@link String#format(java.lang.String, java.lang.Object[]) }
     * markup
     * @param arguments Replacement values for the tokens in {@code pattern}
     *
     * @return The formatted string
     */
    public static String format(String pattern, Object... arguments) {
        if(Str.empty(pattern) || arguments == null || arguments.length == 0) {
            return pattern;
        }

        pattern = escapeSpecialCharacters(pattern);
        try {
            pattern = MessageFormat.format(pattern, arguments);
        } catch (IllegalArgumentException iae) {
        }

        try {
            pattern = String.format(pattern, arguments);
        } catch (IllegalFormatException ife) {
        }

        return pattern;
    }

    private static String escapeSpecialCharacters(String messagePattern) {
        // Single quotes are special characters used by MessageFormat to denote
        // literal values. It is not expected that the caller will use them for
        // this purpose. Since the caller probably intends for the single
        // quote to be displayed, we'll use other single quotes to encapsulate
        // them.
        messagePattern = messagePattern.replaceAll("'", "''");
        // Escape EL expressions to avoid their being interpreted as message
        // format elements
        messagePattern = messagePattern.replaceAll("(\\#|\\$)\\{", "$1'{'");
        return messagePattern;
    }

    /**
     * Creates a string by concatenating an array of strings, each separated
     * by the specified delimiter.
     *
     * @param delimiter (Optional) If null or empty then the {@code parts} will
     * be concatenated with no separation
     * @param parts The items to concatenate
     *
     * @return The concatenation of the specified {@code parts} separated by
     * the specified {@code delimiter}
     */
    public static String concatd(String delimiter, Object... parts) {
        return concatd2(delimiter, Arrays.asList(parts));
    }

    /**
     * Creates a string by concatenating an array of strings, each separated
     * by the specified delimiter.
     *
     * @param delimiter (Optional) If null or empty then the {@code parts} will
     * be concatenated with no separation
     * @param parts The items to concatenate
     *
     * @return The concatenation of the specified {@code parts} separated by
     * the specified {@code delimiter}
     */
    public static String concatd2(String delimiter, Collection<?> parts) {
        StringBuilder result = new StringBuilder();
        for(Object part: parts) {
            if(!empty(part)) {
                if(result.length() > 0 && delimiter != null) {
                    result.append(delimiter);
                }
                result.append(Obj.coerceToString(part));
            }
        }
        return result.toString();
    }

    public static int length(String string) {
        return string == null ? 0 : string.length();
    }

    /**
     * {@link String#trim() Trims} string then returns its length
     *
     * @param string
     * @return Returns the trimmed string's length
     */
    public static int tlength(String string) {
        return string == null ? 0 : string.trim().length();
    }

    public static String[] split(String string, String delimiterRegex) {
        if(string == null) {
            return null;
        }
        return string.split(delimiterRegex);
    }

    public static String substring(String string, int start, int end) {
        String result = null;
        if(!Str.empty(string)) {
            end = Math.min(end, string.length());
            result = string.substring(start, end);

        }
        log.trace("substring(string={0}, start={1}, end={2}) [1] result={3}", string, start, end, result);
        return result;
    }

    public static String replaceAll(String string, String regex, String replacement) {
        String result = null;
        if(!Str.empty(string)) {
            result = string.replaceAll(regex, replacement);
        }
        log.trace("replaceAll(string={0}, regex={1}, replacement={2}) [1] result={3}", string, regex, replacement, result);
        return result;
    }

    public static String toUpperCase(String string) {
        String result = null;
        if(!Str.empty(string)) {
            result = string.toUpperCase();
        }
        log.trace("toUpperCase(string={0}) [1] result={1}", string, result);
        return result;
    }

    public static String toLowerCase(String string) {
        String result = null;
        if(!Str.empty(string)) {
            result = string.toLowerCase();
        }
        log.trace("toLowerCase(string={0}) [1] result={1}", string, result);
        return result;
    }

    public static String concat(Object... parts) {
        String result = Str.concatd(null, parts);
        log.trace("concat(string={0}) [1] result={1}", parts, result);
        return result;
    }

    public static String nullIfEmpty(String string) {
        if(empty(string)) {
            return null;
        }
        return string;
    }

//    public static String keypadDigits(String string) {
//        return KeypadDigit.forString(string);
//    }

    /**
     * Helper method to convert the local transient set of email address into
     * a comma-delimited string for persistence.
     *
     * @param emailAddresses The transient set of email addresses
     *
     * @return A comma-delimited string of email addresses
     */
    public static String serializeEmailAddresses(Set<String> emailAddresses) {
        StringBuilder sb = new StringBuilder();
        for(Iterator<String> i = emailAddresses.iterator(); i.hasNext();) {
            sb.append(i.next());
            if(i.hasNext()) {
                sb.append(", ");
            }
        }
        return sb.toString();
    }

    /**
     * Helper method for converting a comma-delimited string of email addresses
     * into a set.
     *
     * @param emailAddresses The comma-separated string of email addresses
     *
     * @return A set of email address strings
     * @throws AddressException
     */
//    public static Set<String> deserializeEmailAddresses(String emailAddresses) throws AddressException {
//        Set<String> set = new TreeSet<String>();
//        if(!Str.empty(emailAddresses)) {
//            InternetAddress[] addresses = InternetAddress.parse(emailAddresses,true);
//            for(InternetAddress address:addresses){
//                set.add(address.getAddress());
//            }
//        }
//        return set;
//    }

    public static String serializeMapToString(Map<String, String> map) {
        StringBuilder sb = new StringBuilder();
        for (Iterator<String> i = map.keySet().iterator(); i.hasNext();) {
            String code = i.next();
            String value = map.get(code);
            sb.append(code).append("=").append(value);
            if (i.hasNext()) {
                sb.append(", ");
            }
        }
        return sb.toString();
    }

    public static Map<String, String> deserializeMapFromString(String string) {
        Map<String, String> map = new HashMap<String, String>();
        if (!Str.empty(string)) {
            String[] splittedAttributeValues = string.split("\\s*,\\s*");
            for (String attributeSerializeData : splittedAttributeValues) {
                String[] attributeData = attributeSerializeData.split("\\s*=\\s*");
                if (attributeData.length == 2) {
                    map.put(attributeData[0], attributeData[1]);
                }
            }
        }
        return map;
    }

    public static String serializeSetToString(Set<String> set) {
        StringBuilder sb = new StringBuilder();
        for (Iterator<String> i = set.iterator(); i.hasNext();) {
            sb.append(i.next());
            if (i.hasNext()) {
                sb.append(", ");
            }
        }
        return sb.toString();
    }

    public static Set<String> deserializeSetFromString(String deserialsizeString) {
        Set<String> set = new HashSet<String>();
        if (!Str.empty(deserialsizeString)) {
            String[] splittedValues = deserialsizeString.split("\\s*,\\s*");
            set.addAll(Arrays.asList(splittedValues));
        }
        return set;
    }
    /**
     * Checks if the passed string contains digits only
     * @param s string to be checked
     */
    public static boolean isNumeric(String s) {  
        return s.matches("[-+]?\\d*\\.?\\d+");  
    }
}
