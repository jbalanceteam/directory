package org.jbalance.directory.core.model.address;

import java.io.Serializable;

import java.util.UUID;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class City extends AddressObject implements ICity, Serializable {

	private static final long serialVersionUID = 1L;

	public City() {      
    }

    public City(UUID uuid)
    {
        super(uuid);
    }
}
