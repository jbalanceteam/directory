package org.jbalance.directory.core.query.helper.norm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import org.jbalance.directory.core.model.norm.NormGroup;
import org.jbalance.directory.core.query.GetResult;

/**
 *
 * @author Mihail
 */
public class GetNormGroupResult extends GetResult{
    private static final long serialVersionUID = 1L;

    private List<NormGroup> normGroups;

    public GetNormGroupResult()
    {
    }

    @XmlElement(name = "normGroup")
    public List<NormGroup> getNormGroups()
    {
        if (normGroups == null)
        {
            normGroups = new ArrayList<NormGroup>();
        }
        return normGroups;
    }

    public void setNormGroups(List<NormGroup> normGroups)
    {
        this.normGroups = normGroups;
    }

    public GetNormGroupResult(GetNormGroupRequest request)
    {
        super(request);
    }
}
