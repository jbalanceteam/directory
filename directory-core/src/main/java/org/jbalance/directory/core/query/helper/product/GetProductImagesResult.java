
package org.jbalance.directory.core.query.helper.product;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.jbalance.directory.core.model.product.ProductImage;
import org.jbalance.directory.core.query.GetResult;

/**
 * @author Komov Roman
 */
@XmlRootElement
public class GetProductImagesResult extends GetResult
{
    private static final long serialVersionUID = 1L;

    private List<ProductImage> productImages;

    public GetProductImagesResult()
    {
    }

    @XmlElement(name = "productImages")
    public List<ProductImage> getProductImages()
    {
        if (productImages == null)
        {
            productImages = new ArrayList<ProductImage>();
        }
        return productImages;
    }

    public void setProductImages(List<ProductImage> productImages)
    {
        this.productImages = productImages;
    }

    public GetProductImagesResult(GetProductImagesRequest request)
    {
        super(request);
    }
}
