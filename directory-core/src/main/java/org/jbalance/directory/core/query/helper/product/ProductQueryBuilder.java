
package org.jbalance.directory.core.query.helper.product;

import org.jbalance.directory.core.model.product.Product;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class ProductQueryBuilder extends AbstractQueryBuilder<ProductQueryBuilder>{
     public ProductQueryBuilder()
    {
        super(null, Product.class, "product");
    }


    @Override
    public StringBuilder buildJoins()
    {
        StringBuilder joins = new StringBuilder();
        return joins;
    }

    @Override
    protected ProductQueryBuilder get()
    {
        return this;
    }
}