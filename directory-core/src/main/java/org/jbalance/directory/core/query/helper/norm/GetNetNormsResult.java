package org.jbalance.directory.core.query.helper.norm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.jbalance.directory.core.model.norm.NetNorm;
import org.jbalance.directory.core.query.GetResult;

/**
 *
 * @author mihail
 */
@XmlRootElement
public class GetNetNormsResult extends GetResult{
    private static final long serialVersionUID = 1L;

    private List<NetNorm> netNorms;

    public GetNetNormsResult()
    {
    }

    @XmlElement(name = "netNorms")
    public List<NetNorm> getNetNorms()
    {
        if (netNorms == null)
        {
            netNorms = new ArrayList<NetNorm>();
        }
        return netNorms;
    }

    public void setNetNorms(List<NetNorm> netNorms)
    {
        this.netNorms = netNorms;
    }

    public GetNetNormsResult(GetNetNormsRequest request)
    {
        super(request);
    }
}
