package org.jbalance.directory.core.filter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class BigDecimalFilter extends Filter<NumberFilterType, BigDecimal> {
    private static final long serialVersionUID = 1L;

    private NumberFilterType filterType;
    private List<BigDecimal> filterValues;

    public BigDecimalFilter() {}

    public BigDecimalFilter(NumberFilterType filterType) {
        this.filterType = filterType;
    }

    public BigDecimalFilter(NumberFilterType filterType, List<BigDecimal> filterValues) {
        this.filterType = filterType;
        this.filterValues = filterValues;
    }

    public BigDecimalFilter(NumberFilterType filterType, BigDecimal... filterValues) {
        setFilterType(filterType);
        setFilterValues(Arrays.asList(filterValues));
    }

    @XmlAttribute(required=true)
    @Override
    public NumberFilterType getFilterType() {
        return filterType;
    }

    @Override
    public void setFilterType(NumberFilterType filterType) {
        this.filterType = filterType;
    }

    @XmlElement(name="filterValue")
    @Override
    public List<BigDecimal> getFilterValues() {
        if (filterValues == null) {
            filterValues = new ArrayList<BigDecimal>();
        }
        return filterValues;
    }

    @Override
    public void setFilterValues(List<BigDecimal> filterValues) {
        this.filterValues = filterValues;
    }
}
