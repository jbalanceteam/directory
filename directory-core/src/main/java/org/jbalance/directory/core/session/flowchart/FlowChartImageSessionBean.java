package org.jbalance.directory.core.session.flowchart;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import org.jbalance.directory.core.model.flowchart.FlowChartProductImage;
import org.jbalance.directory.core.query.helper.flowchart.GetFlowChartImagesRequest;
import org.jbalance.directory.core.session.GenericSessionBean;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@Stateless
public class FlowChartImageSessionBean extends GenericSessionBean<FlowChartProductImage, GetFlowChartImagesRequest>
{

    @Override
    public List<FlowChartProductImage> list(GetFlowChartImagesRequest request)
    {
        //TODO: Implement
        throw new UnsupportedOperationException("Еще не реализовано!");
    }

    @Override
    @PostConstruct
    public void init() {
        beanClass = FlowChartProductImage.class;
    }
}
