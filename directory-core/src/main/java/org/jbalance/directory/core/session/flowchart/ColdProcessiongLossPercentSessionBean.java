package org.jbalance.directory.core.session.flowchart;

import java.util.List;
import javax.annotation.PostConstruct;
import org.jbalance.directory.core.model.flowchart.ColdProcessingLossPercent;
import org.jbalance.directory.core.query.helper.flowchart.GetColdProcessingLossPercentRequest;
import org.jbalance.directory.core.session.GenericSessionBean;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class ColdProcessiongLossPercentSessionBean
        extends GenericSessionBean<ColdProcessingLossPercent, GetColdProcessingLossPercentRequest>
{

    @Override
    public List<ColdProcessingLossPercent> list(GetColdProcessingLossPercentRequest request)
    {
        //TODO: Implement
        throw new UnsupportedOperationException("Еще не реализовано!");
    }

    @Override
    @PostConstruct
    public void init()
    {
        beanClass = ColdProcessingLossPercent.class;
    }
}
