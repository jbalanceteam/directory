package org.jbalance.directory.core.session.norm;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.jbalance.directory.core.model.norm.GrossNorm;
import org.jbalance.directory.core.model.norm.NetNorm;
import org.jbalance.directory.core.query.helper.norm.GetNetNormsRequest;
import org.jbalance.directory.core.query.helper.norm.GetNetNormsRequestHelper;
import org.jbalance.directory.core.query.helper.norm.GetNetNormsResult;
import org.jbalance.directory.core.session.GenericSessionBean;
/**
 *
 * @author mihail
 */
@Stateless
public class NetNormSessionBean extends GenericSessionBean <NetNorm, GetNetNormsRequest> implements NetNormSession{
    
    @Inject
    GrossNormSession grossNormSession;
    
    @Override
    public List<NetNorm> list(GetNetNormsRequest request)
    {
        GetNetNormsResult result = new GetNetNormsRequestHelper(request).createResult(manager);
        return result.getNetNorms();
    }

    @Override
    @PostConstruct
    public void init() {
        beanClass = NetNorm.class;
    }
    
    @Override
    public NetNorm add(NetNorm netNorm){
        
        NetNorm addedNetNorm = super.add(netNorm);
        
        GrossNorm defaultGrossNorm = new GrossNorm();
        defaultGrossNorm.setStartMonth(new Short("1"));
        defaultGrossNorm.setEndMonth(new Short("12"));
        defaultGrossNorm.setNetNorm(addedNetNorm);
        defaultGrossNorm.setQuantity(addedNetNorm.getQuantity());
        grossNormSession.add(defaultGrossNorm);
        return addedNetNorm;
    }
}