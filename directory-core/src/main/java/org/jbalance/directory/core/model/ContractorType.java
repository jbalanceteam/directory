package org.jbalance.directory.core.model;

/**
 * Тип контрагента
 * Для каждого из типов, имеется подкласс 
 * @author Petr Aleksandrov apv@jbalance.org
 */
public enum ContractorType {
	
	EDUCATIONA_LINSTITUTION("Образовательное учреждение"),
	JURIDICAL("Юр. лицо"),
	/**
	 * Физлица не представленны в проекте Directory
	 */
	INDIVIDUAL("Физлицо");
	
	private final String description;
	
	private ContractorType(String description){
		this.description = description;
	}

	public String getDescription() {
		return description;
	}
	
	public static final int FIELD_LENGTH = 25; 
}
