package org.jbalance.directory.core.query;

/**
 * Enumerates the directions in which a sort can occur (ascending or descending
 * order)
 *
 */
public enum SortDirection {

    /**
     * Ascending order
     */
    ASC("Ascending"),

    /**
     * Descending order
     */
    DESC("Descending");

    private final String description;

    /**
     * Returns a user-friendly description of the sort direction
     *
     * @return a user-friendly description of the sort direction
     */
    public String getDescription() {
        return description;
    }

    /**
     * Creates a new sort direction with the specified description
     *
     * @param description A user-friendly description
     */
    private SortDirection(String description) {
        this.description = description;
    }

    /**
     * Returns a string representation of the sort direction suitable for
     * presentation or logging
     *
     * @return the user-friendly description
     *
     * @see #getDescription()
     */
    @Override
    public String toString() {
        return description;
    }
}
