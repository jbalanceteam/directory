package org.jbalance.directory.core.query;

import java.util.List;

/**
 * {@link Request Requests} that support sorting should implement this
 * interface.
 */
public interface Sortable {

    /**
     *
     * @return a {@link List} of {@link Sort Sorts} that are to be applied to
     *         the {@link Result results}.
     */
    public List<? extends Sort> getSorts();
}
