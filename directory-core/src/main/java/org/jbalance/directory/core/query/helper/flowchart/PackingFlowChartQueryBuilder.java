package org.jbalance.directory.core.query.helper.flowchart;

import org.jbalance.directory.core.model.flowchart.PackingFlowChart;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;

/**
 *
 * @author Mihail
 */
public class PackingFlowChartQueryBuilder extends AbstractQueryBuilder<PackingFlowChartQueryBuilder>{
    public PackingFlowChartQueryBuilder()
    {
        super(null, PackingFlowChart.class, "packingFlowChart");
    }


    @Override
    public StringBuilder buildJoins()
    {
        StringBuilder joins = new StringBuilder();
        return joins;
    }

    @Override
    protected PackingFlowChartQueryBuilder get()
    {
        return this;
    }
	
}
