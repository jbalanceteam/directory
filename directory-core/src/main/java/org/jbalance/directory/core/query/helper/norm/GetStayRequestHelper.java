package org.jbalance.directory.core.query.helper.norm;

import java.util.List;
import javax.persistence.EntityManager;
import org.jbalance.directory.core.model.norm.Stay;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;
import org.jbalance.directory.core.query.helper.GetRequestHelper;
import org.jbalance.directory.core.util.CollectionUtil;

/**
 *
 * @author Mihail
 */
public class GetStayRequestHelper extends GetRequestHelper<GetStayRequest, GetStayResult>{

    public GetStayRequestHelper(GetStayRequest request) {
        super(request);
    }

    @Override
    protected GetStayResult createResult(GetStayResult result, List<?> records, EntityManager em) {
        @SuppressWarnings("unchecked")
		List<Stay> stayList = (List<Stay>) records;
        stayList = CollectionUtil.filterDuplicates(stayList);
        result.setStayList(stayList);
        return result;
    }

    @Override
    protected AbstractQueryBuilder<?> initQuery() {
        StayQueryBuilder stayQueryBuilder = new StayQueryBuilder();
        stayQueryBuilder.appendFilter(request.getIdFilter(), "stay.id");
//        stayQueryBuilder.appendFilter(request.getTimeFilter(), "stay.time");
        stayQueryBuilder.appendFilter(request.getNameFilter(), "stay.name");
        stayQueryBuilder.orderBy().append("order by stay.enteredDate desc");
        return stayQueryBuilder;
    }
    
}
