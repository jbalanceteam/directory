package org.jbalance.directory.core.query;

public abstract class GetResult extends Result {

    private static final long serialVersionUID = 1L;

    private boolean moreRecordsAvailable;
    private Long totalRecordsAvailable;
    private Long pageSize;
    private Long pageNumber;

    public boolean isMoreRecordsAvailable() {
        return moreRecordsAvailable;
    }

    public void setMoreRecordsAvailable(boolean moreRecordsAvailable) {
        this.moreRecordsAvailable = moreRecordsAvailable;
    }

    public GetResult() {
    }

    public GetResult(GetRequest<?> request) {
        super(request);
    }

    public Long getTotalRecordsAvailable() {
        return totalRecordsAvailable;
    }

    public void setTotalRecordsAvailable(Long totalRecordsAvailable) {
        this.totalRecordsAvailable = totalRecordsAvailable;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public Long getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Long pageNumber) {
        this.pageNumber = pageNumber;
    }
}
