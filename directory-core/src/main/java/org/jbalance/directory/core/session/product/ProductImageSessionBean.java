package org.jbalance.directory.core.session.product;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import org.jbalance.directory.core.model.product.ProductImage;
import org.jbalance.directory.core.query.helper.product.GetProductImagesRequest;
import org.jbalance.directory.core.query.helper.product.GetProductImagesRequestHelper;
import org.jbalance.directory.core.query.helper.product.GetProductImagesResult;
import org.jbalance.directory.core.session.GenericSessionBean;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@Stateless
public class ProductImageSessionBean
        extends GenericSessionBean<ProductImage, GetProductImagesRequest>
        implements ProductImageSession
{

    @Override
    public List<ProductImage> list(GetProductImagesRequest request)
    {
        GetProductImagesResult result = new GetProductImagesRequestHelper(request).createResult(manager);
        return result.getProductImages();
    }

    @Override
    @PostConstruct
    public void init() {
        beanClass = ProductImage.class;
    }
}
