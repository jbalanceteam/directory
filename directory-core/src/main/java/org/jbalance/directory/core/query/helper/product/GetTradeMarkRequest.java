package org.jbalance.directory.core.query.helper.product;

import javax.xml.bind.annotation.XmlType;
import org.jbalance.directory.core.filter.DateFilter;
import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.query.GetRequest;


@XmlType(propOrder ={
    "idFilter",
    "tradeMarkNameFilter",
    "modifiedFilter"
})
public class GetTradeMarkRequest extends GetRequest<GetTradeMarkResult>
{

    private static final long serialVersionUID = 1L;

    private LongFilter idFilter;
    private StringFilter tradeMarkNameFilter;
    private DateFilter modifiedFilter;

    @Override
    public GetTradeMarkResult createResult()
    {
        return new GetTradeMarkResult(this);
    }

    public LongFilter getIdFilter()
    {
        return idFilter;
    }

    public void setIdFilter(LongFilter idFilter)
    {
        this.idFilter = idFilter;
    }

    public StringFilter getTradeMarkNameFilter() {
        return tradeMarkNameFilter;
    }

    public void setTradeMarkNameFilter(StringFilter tradeMarkNameFilter) {
        this.tradeMarkNameFilter = tradeMarkNameFilter;
    }

    public DateFilter getModifiedFilter()
    {
        return modifiedFilter;
    }

    public void setModifiedFilter(DateFilter modifiedFilter)
    {
        this.modifiedFilter = modifiedFilter;
    }

}
