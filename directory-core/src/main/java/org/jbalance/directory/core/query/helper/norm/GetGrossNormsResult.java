package org.jbalance.directory.core.query.helper.norm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import org.jbalance.directory.core.model.norm.GrossNorm;
import org.jbalance.directory.core.query.GetResult;

/**
 *
 * @author Mihail
 */
public class GetGrossNormsResult extends GetResult{
    private static final long serialVersionUID = 1L;

    private List<GrossNorm> grossNorms;

    public GetGrossNormsResult()
    {
    }

    @XmlElement(name = "grossNorms")
    public List<GrossNorm> getGrossNorms()
    {
        if (grossNorms == null)
        {
            grossNorms = new ArrayList<GrossNorm>();
        }
        return grossNorms;
    }

    public void setGrossNorms(List<GrossNorm> grossNorms)
    {
        this.grossNorms = grossNorms;
    }

    public GetGrossNormsResult(GetGrossNormsRequest request)
    {
        super(request);
    }
}