package org.jbalance.directory.core.model.norm;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import org.jbalance.directory.core.model.BaseEntity;
import org.jbalance.directory.core.model.measure.Measure;

/**
 * @author Mihail
 * сущность описывает нормы брутто
 */
@Entity
public class GrossNorm extends BaseEntity{
    
    public GrossNorm(){
        
    }

    /**
     * соответствующая норма нетто
     */
    @ManyToOne
    @NotNull
    private NetNorm netNorm;
    
    /**
     * начальная дата периода действия нормы
     */
    @Column
    private Short startMonth;
    
    /**
     * конечная дата периода действия нормы
     */
    @Column
    private Short endMonth;
    
//    /**
//     * единица измерения
//     */
//    @ManyToOne
//    private Measure measure;
    
    /**
     * количество
     */
    @Column
    private Float quantity;
    
    
	@Deprecated
    public GrossNorm(Long id, Date enteredDate, Date deletedDate, Float quantity){
        super(id, enteredDate, deletedDate);
        this.quantity = quantity;
    }
    
    public NetNorm getNetNorm(){
        return netNorm;
    }
    
    
    public void setNetNorm(NetNorm netNorm){
        this.netNorm = netNorm;
    }
    
    public Short getStartMonth(){
        return startMonth;
    }
    
    public void setStartMonth(Short startMonth){
        this.startMonth = startMonth;
    }
    
    public Short getEndMonth(){
        return endMonth;
    }
    
    public void setEndMonth(Short endMonth){
        this.endMonth = endMonth;
    }
    
//    public Measure getMeasure(){
//        return measure;
//    }
//    
//    public void setMeasure(Measure measure){
//        this.measure = measure;
//    }
    
    public Float getQuantity(){
        return quantity;
    }
    
    public void setQuantity(Float quantity){
        this.quantity = quantity;
    }
}
