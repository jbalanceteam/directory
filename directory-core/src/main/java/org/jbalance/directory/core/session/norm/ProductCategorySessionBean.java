package org.jbalance.directory.core.session.norm;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import org.jbalance.directory.core.model.norm.ProductCategory;
import org.jbalance.directory.core.query.helper.norm.GetProductCategoryRequest;
import org.jbalance.directory.core.query.helper.norm.GetProductCategoryRequestHelper;
import org.jbalance.directory.core.query.helper.norm.GetProductCategoryResult;
import org.jbalance.directory.core.session.GenericSessionBean;

/**
 *
 * @author Mihail
 */
@Stateless
public class ProductCategorySessionBean extends GenericSessionBean<ProductCategory, GetProductCategoryRequest> implements ProductCategorySession{

    @Override
    @PostConstruct
    public void init() {
        beanClass = ProductCategory.class;
    }

    @Override
    public List<ProductCategory> list(GetProductCategoryRequest request) {
        GetProductCategoryResult result = new GetProductCategoryRequestHelper(request).createResult(manager);
        return result.getProductCategories();
    }

    @Override
    public List<ProductCategory> getAll() {
        GetProductCategoryResult result = new GetProductCategoryRequestHelper(new GetProductCategoryRequest()).createResult(manager);
        return result.getProductCategories();
    }
}
