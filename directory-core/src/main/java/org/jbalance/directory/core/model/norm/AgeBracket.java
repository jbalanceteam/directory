package org.jbalance.directory.core.model.norm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.jbalance.directory.core.model.BaseEntity;

/**
 * 
 * @author Mihail
 * сущность, описывающая возрастную категорию
 */
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "deleteddate"})})
public class AgeBracket extends BaseEntity{
    
	private static final long serialVersionUID = 1L;

	@Column(nullable = false)
    protected String name;
    
    @Column
    protected String description;
    
    /**
     * минимальный возраст для возрастной группы в месяцах
     */
    @Column(nullable=false)
    private short minimumAge;
    
    /**
     * максимальный возраст для возрастной группы в месяцах
     */
    @Column(nullable=false)
    private short maximumAge;
    
    public short getMinimumAge(){
    	return minimumAge;
    }
    
    public float getMinimumAgeInYears(){
        float ageInYears = (float)(minimumAge * 10 / 12);
        ageInYears /= 10;
    	return ageInYears;
    }

    public void setMinimumAge(short minimumAge){
        this.minimumAge = minimumAge;
    }
    
    public short getMaximumAge(){
        return maximumAge;
    }
    
    public float getMaximumAgeInYears(){
        float ageInYears = maximumAge * 10 / 12;
        ageInYears /= 10;
    	return ageInYears;
    }
    
    public void setMaximumAge(short maximumAge){
        this.maximumAge = maximumAge;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
    
    
}
