package org.jbalance.directory.core.model;

import java.util.UUID;

public class UuidGenerator {

    public static String generate() {
        return UUID.randomUUID().toString().toUpperCase();
    }
}
