
package org.jbalance.directory.core.session.product;

import org.jbalance.directory.core.model.product.Product;
import org.jbalance.directory.core.query.helper.product.GetProductsRequest;
import org.jbalance.directory.core.session.GenericSession;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public interface ProductSession extends GenericSession<Product, GetProductsRequest>
{
}
