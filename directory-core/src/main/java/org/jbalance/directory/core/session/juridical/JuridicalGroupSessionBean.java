package org.jbalance.directory.core.session.juridical;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import org.jbalance.directory.core.model.JuridicalGroup;
import org.jbalance.directory.core.query.helper.juridical.GetJuridicalGroupsRequest;
import org.jbalance.directory.core.query.helper.juridical.GetJuridicalGroupsRequestHelper;
import org.jbalance.directory.core.query.helper.juridical.GetJuridicalGroupsResult;
import org.jbalance.directory.core.session.GenericSessionBean;

/**
 *
 * @author roman
 */
@Stateless
public class JuridicalGroupSessionBean extends GenericSessionBean<JuridicalGroup, GetJuridicalGroupsRequest> implements JuridicalGroupSession {

    @Override
    public JuridicalGroup getById(Long id) {
        JuridicalGroup jg = super.getById(id);
        jg.getJuridicals().size();
        return jg;
    }

    @Override
    public List<JuridicalGroup> list(GetJuridicalGroupsRequest request) {
        GetJuridicalGroupsResult result = new GetJuridicalGroupsRequestHelper(request).createResult(manager);
        return result.getJuridicalGroups();
    }

    @Override
    @PostConstruct
    public void init() {
        beanClass = JuridicalGroup.class;
    }
}
