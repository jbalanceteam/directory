package org.jbalance.directory.core.query.helper.norm;

import org.jbalance.directory.core.filter.IntegerFilter;
import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.query.GetRequest;

/**
 *
 * @author Mihail
 */
public class GetStayRequest extends GetRequest<GetStayResult> {
    private static final long serialVersionUID = 1L;
    
    private LongFilter idFilter;
//    private IntegerFilter timeFilter;
    private StringFilter nameFilter;
    
    @Override
    public GetStayResult createResult() {
        return new GetStayResult(this);
    }
    
    public LongFilter getIdFilter(){
        return idFilter;
    }
    
    public void setIdFilter(LongFilter idFilter){
        this.idFilter = idFilter;
    }
    
//    public IntegerFilter getTimeFilter(){
//        return timeFilter;
//    }
//    
//    public void setTimeFilter(IntegerFilter timeFilter){
//        this.timeFilter = timeFilter;
//    }
    
    public StringFilter getNameFilter(){
        return nameFilter;
    }
    
    public void setNameFilter(StringFilter nameFilter){
        this.nameFilter = nameFilter;
    }
    
}
