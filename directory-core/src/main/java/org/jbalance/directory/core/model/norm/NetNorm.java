package org.jbalance.directory.core.model.norm;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import org.jbalance.directory.core.model.BaseEntity;
import org.jbalance.directory.core.model.measure.Measure;

/**
 * @author Mihail
 * сущность описывает нормы нетто
 */
@Entity
@XmlAccessorType(XmlAccessType.FIELD)
public class NetNorm extends BaseEntity{
    
    public NetNorm(){}
    
    /**
     * группа норм, которой принадлежит данная норма
     */
    @ManyToOne
    private NormGroup normGroup;
    
    /**
     * категория продуктов, для которых устанавливается норма
     */
    @ManyToOne
    private ProductCategory productCategory;
    
    /**
     * единицы измерения
     */
    @ManyToOne
    private Measure measure;
    
    /**
     * количество
     */
    private Float quantity;
    
    @XmlTransient
    @OneToMany(mappedBy = "netNorm")
    private Set<GrossNorm> grossNorm;
    
    public void setNormGroup(NormGroup normGroup){
        this.normGroup = normGroup;
    }
    
    public NormGroup getNormGroup(){
        return normGroup;
    }
    
    public ProductCategory getProductCategory(){
        return productCategory;
    }
    public void setProductCategory(ProductCategory productCategory){
        this.productCategory = productCategory;
    }
    
    public Measure getMeasure(){
        return measure;
    }
    
    public void setMeasure(Measure measure){
        this.measure = measure;
    }
    
    public Float getQuantity(){
        return quantity;
    }
    
    public void setQuantity(Float quantity){
        this.quantity = quantity;
    }
    
    public Set<GrossNorm> getGrossNorms(){
        return grossNorm;
    }
    
//    @Override
//    public String toString(){
//        return "категория: " + getProductCategory().getName() + ", возрастная группа: " + getAgeBracket().getName() + ", режим пребывания: " + getStay().getHours() + "ч.";
//    }
}
