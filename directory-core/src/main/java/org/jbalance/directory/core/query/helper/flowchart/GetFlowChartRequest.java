package org.jbalance.directory.core.query.helper.flowchart;

import javax.xml.bind.annotation.XmlType;
import org.jbalance.directory.core.filter.DateFilter;
import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.query.GetRequest;


@XmlType(propOrder ={
    "idFilter",
    "flowChartNameFilter",
    "modifiedFilter"
})
public class GetFlowChartRequest extends GetRequest<GetFlowChartResult>
{

    private static final long serialVersionUID = 1L;

    private LongFilter idFilter;
    private StringFilter flowChartNameFilter;
    private DateFilter modifiedFilter;

    @Override
    public GetFlowChartResult createResult()
    {
        return new GetFlowChartResult(this);
    }

    public LongFilter getIdFilter()
    {
        return idFilter;
    }

    public void setIdFilter(LongFilter idFilter)
    {
        this.idFilter = idFilter;
    }

    public StringFilter getFlowChartNameFilter() {
        return flowChartNameFilter;
    }

    public void setFlowChartNameFilter(StringFilter flowChartNameFilter) {
        this.flowChartNameFilter = flowChartNameFilter;
    }

    public DateFilter getModifiedFilter()
    {
        return modifiedFilter;
    }

    public void setModifiedFilter(DateFilter modifiedFilter)
    {
        this.modifiedFilter = modifiedFilter;
    }
}
