package org.jbalance.directory.core.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class LongFilter extends Filter<NumberFilterType, Long>
{

    private static final long serialVersionUID = 1L;

    private NumberFilterType filterType;
    private List<Long> filterValues;

    public LongFilter()
    {
    }

    public LongFilter(NumberFilterType filterType)
    {
        this.filterType = filterType;
    }

    public LongFilter(NumberFilterType filterType, List<Long> filterValues)
    {
        this.filterType = filterType;
        this.filterValues = filterValues;
    }

    public LongFilter(NumberFilterType filterType, Long... filterValues)
    {
        setFilterType(filterType);
        setFilterValues(Arrays.asList(filterValues));
    }

    /**
     * Convenience constructor that sets the filterType to
     * {@link NumberFilterType#EQUALS}.
     *
     * @param filterValue
     */
    public LongFilter(Long filterValue)
    {
        setFilterType(NumberFilterType.EQUALS);
        setFilterValue(filterValue);
    }

    /**
     * Convenience constructor that sets the filterType to
     * {@link NumberFilterType#IN}.
     *
     * @param filterValues
     */
    public LongFilter(List<Long> filterValues)
    {
        setFilterType(NumberFilterType.IN);
        setFilterValues(filterValues);
    }

    @Override
    public NumberFilterType getFilterType()
    {
        return filterType;
    }

    @Override
    public void setFilterType(NumberFilterType filterType)
    {
        this.filterType = filterType;
    }

    @XmlElement(name = "filterValues")
    @Override
    public List<Long> getFilterValues()
    {
        if (filterValues == null)
        {
            filterValues = new ArrayList<Long>();
        }
        return filterValues;
    }

    @Override
    public void setFilterValues(List<Long> filterValues)
    {
        this.filterValues = filterValues;
    }
}
