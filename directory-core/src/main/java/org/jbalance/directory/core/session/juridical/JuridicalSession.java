package org.jbalance.directory.core.session.juridical;

import org.jbalance.directory.core.model.Juridical;
import org.jbalance.directory.core.query.helper.juridical.GetJuridicalsRequest;
import org.jbalance.directory.core.session.GenericSession;

/**
 * Интерфейс сессионных бинов 
 * @author Petr Aleksandrov apv@jbalance.org
 */
public interface JuridicalSession extends GenericSession<Juridical, GetJuridicalsRequest> {
    /***
     * Путь привязки сессии в JNDI.
     */
    public static final String LOCAL_EJB_JNDI_BINDING = "java:global/directory/directory-core/JuridicalSessionBean!org.jbalance.directory.core.session.juridical.JuridicalSession";   

}
