package org.jbalance.directory.core.query.helper.flowchart;

import java.util.List;
import javax.persistence.EntityManager;
import org.jbalance.directory.core.model.flowchart.PackingFlowChart;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;
import org.jbalance.directory.core.query.helper.GetRequestHelper;
import org.jbalance.directory.core.util.CollectionUtil;

/**
 *
 * @author Mihail
 */
public class GetPackingFlowChartRequestHelper extends GetRequestHelper<GetPackingFlowChartRequest, GetPackingFlowChartResult>{

    public GetPackingFlowChartRequestHelper(GetPackingFlowChartRequest request)
    {
        super(request);
    }

    @Override
    protected AbstractQueryBuilder<?> initQuery()
    {
        PackingFlowChartQueryBuilder packingFlowChartQueryBuilder = new PackingFlowChartQueryBuilder();
        packingFlowChartQueryBuilder.appendFilter(request.getIdFilter(), "packingFlowChart.id");      
        packingFlowChartQueryBuilder.appendFilter(request.getPackingFilter(), "packingFlowChart.targetPacking.id");
        packingFlowChartQueryBuilder.appendFilter(request.getImageFilter(), "packingFlowChart.targetPacking.image.id");
        packingFlowChartQueryBuilder.appendFilter(request.getNameFilter(), "packingFlowChart.name");
        packingFlowChartQueryBuilder.orderBy().append("order by packingFlowChart.targetPacking.image.name asc, packingFlowChart.targetPacking.name asc, packingFlowChart.name asc");
        return packingFlowChartQueryBuilder;
    }

    @Override
    protected GetPackingFlowChartResult createResult(GetPackingFlowChartResult result, List<?> records, EntityManager em)
    {
        List<PackingFlowChart> l = (List<PackingFlowChart>) records;
        l = CollectionUtil.filterDuplicates(l);
        result.getPackingFlowCharts().addAll(l);
        return result;
    }
}
