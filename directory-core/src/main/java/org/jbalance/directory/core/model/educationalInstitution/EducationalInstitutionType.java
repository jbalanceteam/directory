package org.jbalance.directory.core.model.educationalInstitution;

/**
 * Тип образовательного учреждение
 * @author Petr Aleksandrov apv@jbalance.org
 */
public enum EducationalInstitutionType {
	
	NURSERY("ДОУ"),
	SCHOOL("Школа");
	
	private final String shortName;
	
	private EducationalInstitutionType(String shortName){
		this.shortName = shortName;
	}

	public String getShortName() {
		return shortName;
	}
}
