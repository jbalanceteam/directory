package org.jbalance.directory.core.session.flowchart;

import org.jbalance.directory.core.model.flowchart.PackingFlowChartPacks;
import org.jbalance.directory.core.query.helper.flowchart.GetPackingFlowChartPacksRequest;
import org.jbalance.directory.core.session.GenericSession;

/**
 *
 * @author Mihail
 */
public interface PackingFlowChartPacksSession extends GenericSession<PackingFlowChartPacks, GetPackingFlowChartPacksRequest>{
	
}
