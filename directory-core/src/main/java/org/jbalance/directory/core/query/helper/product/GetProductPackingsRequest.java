package org.jbalance.directory.core.query.helper.product;

import javax.xml.bind.annotation.XmlType;

import org.jbalance.directory.core.filter.DateFilter;
import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.query.GetRequest;

@XmlType(propOrder
        = {
            "idFilter",
            "productNameFilter",
            "productFilter",
            "packingFilter",
            "modifiedFilter"
        })
public class GetProductPackingsRequest extends GetRequest<GetProductPackingsResult> {

    private static final long serialVersionUID = 1L;

    private LongFilter idFilter;

    private StringFilter productNameFilter;
	
    private LongFilter productFilter;
	
    private LongFilter packingFilter;

    private DateFilter modifiedFilter;

    @Override
    public GetProductPackingsResult createResult() {
        return new GetProductPackingsResult(this);
    }

    public LongFilter getIdFilter() {
        return idFilter;
    }

    public void setIdFilter(LongFilter idFilter) {
        this.idFilter = idFilter;
    }

    public StringFilter getProductNameFilter() {
        return productNameFilter;
    }

    public void setProductNameFilter(StringFilter name) {
        this.productNameFilter = name;
    }

    public LongFilter getProductFilter() {
        return productFilter;
    }

    public void setProductFilter(LongFilter productFilter) {
        this.productFilter = productFilter;
    }

    public LongFilter getPackingFilter() {
        return packingFilter;
    }

    public void setPackingFilter(LongFilter packingFilter) {
        this.packingFilter = packingFilter;
    }

    public DateFilter getModifiedFilter() {
        return modifiedFilter;
    }

    public void setModifiedFilter(DateFilter modifiedFilter) {
        this.modifiedFilter = modifiedFilter;
    }
//
//    @Override
//    public String toString() {
//        return "idFilter: " + (idFilter == null ? "null" : idFilter.toString()) +
//                " productNameFilter:  " + (productNameFilter == null ? "null" : productNameFilter.toString()) + 
//                " modifiedFilter:  " + (modifiedFilter == null ? "null" : modifiedFilter.toString());
//
//    }

}
