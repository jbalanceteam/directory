package org.jbalance.directory.core.query.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.jbalance.directory.core.model.BaseEntity;
import org.jbalance.directory.core.query.GetRequest;
import org.jbalance.directory.core.query.GetResult;
import org.jbalance.directory.core.query.Sortable;
import org.jbalance.directory.core.util.Str;

public abstract class GetRequestHelper<R extends GetRequest<S>, S extends GetResult> {

    protected R request;
    protected boolean nativeQuery = false;
    protected AbstractQueryBuilder<?> queryBuilder;

    protected Date effectiveDate;

    public GetRequestHelper(R request) {
        this(request, false);
    }

    public GetRequestHelper(R request, boolean nativeQuery) {
        this.request = request;

        effectiveDate = request.getEffectiveDate();
        if (effectiveDate == null && request.getCreatedDateFilter() == null) {
            effectiveDate = new Date();
        }
        this.nativeQuery = nativeQuery;
    }

    protected List getRecords(EntityManager em) {
        this.queryBuilder = initQuery();
        this.queryBuilder.setSelectCount(request.isCountOnly());
        queryBuilder.appendEffectiveDateFilters(effectiveDate, queryBuilder.primaryAlias());
        queryBuilder.appendFilter(request.getCreatedDateFilter(), queryBuilder.primaryAlias()+".createdDate");
        queryBuilder.appendFilter(request.getDeletedDateFilter(), queryBuilder.primaryAlias()+".deletedDate");
        if(!Str.empty(request.getUuid())){
            queryBuilder.and().where().append(" (").append(queryBuilder.primaryAlias()).append(".uuid = :uuid)");
            queryBuilder.params().addParam("uuid", request.getUuid());
        }
        if (Sortable.class.isAssignableFrom(request.getClass())) {
            Sortable sortableRequest = (Sortable) request;
            queryBuilder.appendSortables(sortableRequest.getSorts());
        }
        queryBuilder.setPaginatedRequest(request.getFirstRecord() != null && request.getMaxRecords() != null);

        String queryString = queryBuilder.toString();
//        new Log(getClass()).debug(queryString);
        Query query = nativeQuery ? em.createNativeQuery(queryString) : em.createQuery(queryString);
        for(Entry<String, Object> param: queryBuilder.params().getParamMap().entrySet()) {
            query.setParameter(param.getKey(), param.getValue());
        }

        if (request.getMaxRecords() != null) {
            if (request.getFirstRecord() != null) {
                query.setMaxResults(request.getMaxRecords());
            } else {
                query.setMaxResults(request.getMaxRecords()+1);
            }
        }
        if (request.getFirstRecord() != null) {
            query.setFirstResult(request.getFirstRecord());
        }

        return query.getResultList();
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public S createResult(EntityManager em) {
        List<?> records = getRecords(em);
        S result = request.createResult();
        if(request.getMaxRecords() != null) {
            if (request.getFirstRecord() == null && records.size() > request.getMaxRecords() ) {
                result.setMoreRecordsAvailable(true);
                records = new ArrayList(records.subList(0, request.getMaxRecords()));
            }
        }


        if (!request.isCountOnly()) {
            result = createResult(result, records, em);
            result.setTotalRecordsAvailable(new Long(records.size()));
        } else {
            result.setTotalRecordsAvailable((Long)records.get(0));
        }

        //Evaluation of pagination performed only if maxRecords and firstRecord has been specified
        if (request.getMaxRecords() != null && request.getFirstRecord() != null) {
            result.setPageSize(request.getMaxRecords().longValue());
            Integer firstResult = request.getFirstRecord();
            double pageNumber = Math.floor(request.getFirstRecord()/request.getMaxRecords()) + 1;
            result.setPageNumber(new Double(pageNumber).longValue());
            //if knownTotalRecordCount is specified, there is no need to count total available records
            if (request.getKnownTotalRecordCount() == null) {
                request.setCountOnly(true);
                request.setFirstRecord(null);
                request.setMaxRecords(null);
                List<?> totalRecords = getRecords(em);
                if (totalRecords != null) {
                    result.setTotalRecordsAvailable((Long)totalRecords.get(0));
                }
            } else {
                result.setTotalRecordsAvailable(request.getKnownTotalRecordCount());
            }
            if (result.getTotalRecordsAvailable() > (firstResult + records.size())) {
                result.setMoreRecordsAvailable(true);
            }
        }
        return result;
    }

    protected <E extends BaseEntity> Map<Long, E> buildResultIndexMap(Collection<E> results) {
        return BaseEntityUtilities.newIdMap(results);
    }

    protected abstract S createResult(S result, List<?> records, EntityManager em);

    protected abstract AbstractQueryBuilder<?> initQuery();

}
