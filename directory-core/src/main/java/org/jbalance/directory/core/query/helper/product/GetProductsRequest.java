package org.jbalance.directory.core.query.helper.product;

import javax.xml.bind.annotation.XmlType;
import org.jbalance.directory.core.filter.DateFilter;
import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.query.GetRequest;


@XmlType(propOrder ={
    "idFilter",
    "productNameFilter",
    "modifiedFilter"
})
public class GetProductsRequest extends GetRequest<GetProductsResult>
{

    private static final long serialVersionUID = 1L;

    private LongFilter idFilter;
    private StringFilter productNameFilter;
    private DateFilter modifiedFilter;

    @Override
    public GetProductsResult createResult()
    {
        return new GetProductsResult(this);
    }

    public LongFilter getIdFilter()
    {
        return idFilter;
    }

    public void setIdFilter(LongFilter idFilter)
    {
        this.idFilter = idFilter;
    }

    public StringFilter getProductNameFilter() {
        return productNameFilter;
    }

    public void setProductNameFilter(StringFilter productNameFilter) {
        this.productNameFilter = productNameFilter;
    }

    public DateFilter getModifiedFilter()
    {
        return modifiedFilter;
    }

    public void setModifiedFilter(DateFilter modifiedFilter)
    {
        this.modifiedFilter = modifiedFilter;
    }
}
