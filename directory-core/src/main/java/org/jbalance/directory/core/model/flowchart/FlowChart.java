package org.jbalance.directory.core.model.flowchart;

import java.util.*;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import org.jbalance.directory.core.model.BaseEntity;
import org.jbalance.directory.core.model.product.ProductImage;

/**
 * Технологическая карта
 */
@Entity
public class FlowChart extends BaseEntity
{

    /**
     * Список результатов
     */
    @OneToMany
    private Set<FlowChartProductImage> target;

    /**
     * Список компонентов
     */
    @OneToMany
    private Set<FlowChartProductImage> source;
    
    
    
    private String description;


    public FlowChart()
    {
    }

    public Set<FlowChartProductImage> getTarget() {
        return target;
    }

    public void setTarget(Set<FlowChartProductImage> target) {
        this.target = target;
    }

    public Set<FlowChartProductImage> getSource() {
        return source;
    }

    public void setSource(Set<FlowChartProductImage> source) {
        this.source = source;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

   

}
