package org.jbalance.directory.core.session.address;

import java.util.List;
import java.util.UUID;

import org.jbalance.directory.core.model.address.*;


/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public interface IAddressFinder {
	/**
	 * Максимальное кол-во по умолчанию строк в выборке городов
	 */
    int DEFAULT_LIMIT_SELECT_REGION = 20;
    int DEFAULT_LIMIT_SELECT_CITY = 20;
    int DEFAULT_LIMIT_SELECT_STREET = 20;
    int DEFAULT_LIMIT_SELECT_HOUSE = 20;

    List<Region> findRegion(String term);

    Region findRegion(UUID uuid);

    List<City> findCity(String term, Integer region);

    City findCity(UUID uuid);

    List<House> findHouse(String term, UUID parent);

    List<HouseInterval> findHouseInterval(UUID parent);

    List<Street> findStreet(String term, UUID parent);

    Street findStreet(UUID uuid);

    String getFullAddressForAddressObject(UUID uuid);

    String getFullAddressForHouse(UUID uuid, int regionCode);

    String getFullAddressForHouseInterval(UUID uuid, String houseNum);

    /**
     * Задать максимальный объем выборки
     */
	void setLimitSelectRegion(int recordsAmountLimit);
	void setLimitSelectCity(int recordsAmountLimit);
	void setLimitSelectStreet(int recordsAmountLimit);
	void setLimitSelectHouse(int recordsAmountLimit);

}
