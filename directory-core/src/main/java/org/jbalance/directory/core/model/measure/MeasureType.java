package org.jbalance.directory.core.model.measure;

/**
 *
 * @author Mihail
 */
public enum MeasureType {
	IntIncludedInESKK, NatIncludedInESKK, IntNotIncludedInESKK;
	public String getStringValue(){
		String val = null;
		switch(this){
			case IntIncludedInESKK:
				val="Международные единицы измерения, включенные в ЕСКК";
				break;
			case NatIncludedInESKK:
				val="Национальные единицы измерения, включенные в ЕСКК";
				break;
			case IntNotIncludedInESKK:
				val="Международные единицы измерения, не включенные в ЕСКК";
				break;
		}
		return val;
	}
}
