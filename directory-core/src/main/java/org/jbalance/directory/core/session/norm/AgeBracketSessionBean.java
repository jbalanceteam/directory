package org.jbalance.directory.core.session.norm;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import org.jbalance.directory.core.model.norm.AgeBracket;
import org.jbalance.directory.core.query.helper.norm.GetAgeBracketRequest;
import org.jbalance.directory.core.query.helper.norm.GetAgeBracketRequestHelper;
import org.jbalance.directory.core.query.helper.norm.GetAgeBracketResult;
import org.jbalance.directory.core.session.GenericSessionBean;

/**
 *
 * @author Mihail
 */
@Stateless
public class AgeBracketSessionBean extends GenericSessionBean<AgeBracket, GetAgeBracketRequest> implements AgeBracketSession{

    @Override
    @PostConstruct
    public void init() {
        beanClass = AgeBracket.class;
    }

    @Override
    public List<AgeBracket> list(GetAgeBracketRequest request) {
        GetAgeBracketResult result = new GetAgeBracketRequestHelper(request).createResult(manager);
        return result.getAgeBrackets();
    }
    @Override
    public List<AgeBracket> getAll(){
        GetAgeBracketResult result = new GetAgeBracketRequestHelper(new GetAgeBracketRequest()).createResult(manager);
        return result.getAgeBrackets();
    }
}
