package org.jbalance.directory.core.query.helper.norm;

import org.jbalance.directory.core.filter.IntegerFilter;
import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.query.GetRequest;

/**
 *
 * @author Mihail
 */
public class GetAgeBracketRequest  extends GetRequest<GetAgeBracketResult>{
    private static final long serialVersionUID = 1L;

    private LongFilter idFilter;
    private IntegerFilter ageFilter;

    @Override
    public GetAgeBracketResult createResult()
    {
        return new GetAgeBracketResult(this);
    }

    public LongFilter getIdFilter() {
        return idFilter;
    }

    public void setIdFilter(LongFilter idFilter) {
        this.idFilter = idFilter;
    }
    
    public IntegerFilter getAgeFilter(){
        return ageFilter;
    }
    
    public void setAgeFilter(IntegerFilter ageFilter){
        this.ageFilter = ageFilter;
    }
}