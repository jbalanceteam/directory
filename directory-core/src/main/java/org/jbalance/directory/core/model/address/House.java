package org.jbalance.directory.core.model.address;


import java.util.UUID;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class House implements IHouse
{

    protected Street street;
    /**
     * Номер дома
     */
    protected String houseNum;

    /**
     * Признак владения
     */
    protected Integer estStatus;

    /**
     * Номер корпуса
     */
    protected String buildNum;

    /**
     * Номер строения
     */
    protected String structNum;

    /**
     * Признак строения
     */
    protected Integer strStatus;

    /**
     * Уникальный идентификатор записи дома
     */
    protected UUID houseId;

    /**
     * Глобальный уникальный идентификатор дома
     */
    protected UUID houseGuid;

    /**
     * Состояние дома
     */
    protected Integer statStatus;

    /**
     * Счётчик записей домов для КЛАДР 4
     */
    protected Integer counter;

    /**
     * Guid записи родительского объекта (улицы, города, населенного пункта и
     * т.п.)
     */
    protected UUID aoGuid;
    
    protected String postalCode;
    

    @Override
    public UUID getAoGuid()
    {
        return aoGuid;
    }

    @Override
    public void setAoGuid(UUID aoGuid)
    {
        this.aoGuid = aoGuid;
    }

    /**
     * Код региона
     */
    protected int regionCode;

    public House()
    {
    }

    public House(UUID id, int regionCode)
    {
        this.houseGuid = id;
        this.regionCode = regionCode;
    }

    @Override
    public String getHouseNum()
    {
        return houseNum;
    }

    @Override
    public void setHouseNum(String houseNum)
    {
        this.houseNum = houseNum;
    }

    @Override
    public Integer getEstStatus()
    {
        return estStatus;
    }

    @Override
    public void setEstStatus(Integer estStatus)
    {
        this.estStatus = estStatus;
    }

    @Override
    public String getBuildNum()
    {
        return buildNum;
    }

    @Override
    public void setBuildNum(String buildNum)
    {
        this.buildNum = buildNum;
    }

    @Override
    public String getStructNum()
    {
        return structNum;
    }

    @Override
    public void setStructNum(String structNum)
    {
        this.structNum = structNum;
    }

    @Override
    public Integer getStrStatus()
    {
        return strStatus;
    }

    @Override
    public void setStrStatus(Integer strStatus)
    {
        this.strStatus = strStatus;
    }

    @Override
    public UUID getHouseId()
    {
        return houseId;
    }

    @Override
    public void setHouseId(UUID houseId)
    {
        this.houseId = houseId;
    }

    @Override
    public UUID getHouseGuid()
    {
        return houseGuid;
    }

    @Override
    public void setHouseGuid(UUID houseGuid)
    {
        this.houseGuid = houseGuid;
    }

    @Override
    public Integer getStatStatus()
    {
        return statStatus;
    }

    @Override
    public void setStatStatus(Integer statStatus)
    {
        this.statStatus = statStatus;
    }

    @Override
    public Integer getCounter()
    {
        return counter;
    }

    @Override
    public void setCounter(Integer counter)
    {
        this.counter = counter;
    }

    @Override
    public int getRegionCode()
    {
        return regionCode;
    }

    @Override
    public void setRegionCode(int regionCode)
    {
        this.regionCode = regionCode;
    }

    @Override
    public String toString()
    {
        return this.houseNum;
    }

	@Override
	public String getPostalCode() {
		return postalCode;
	}

	@Override
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

}
