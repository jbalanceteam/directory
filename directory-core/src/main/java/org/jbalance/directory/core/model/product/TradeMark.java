package org.jbalance.directory.core.model.product;

import java.util.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import org.jbalance.directory.core.model.BaseEntity;

/**
 * Торговая марка
 */
@Entity
@XmlAccessorType(XmlAccessType.FIELD)
public class TradeMark extends BaseEntity
{

    /**
     * 
     * Название
     */
    @Column
    private String name;
    
    
    /**
     * Список продуктов этой тогровой марки
     */
    @OneToMany(mappedBy = "tradeMark")
    @XmlTransient
    private Set<Product> products;

    public TradeMark()
    {
    }

   
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }
    
    
}
