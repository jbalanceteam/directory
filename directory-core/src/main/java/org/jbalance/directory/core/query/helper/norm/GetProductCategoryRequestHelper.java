package org.jbalance.directory.core.query.helper.norm;

import java.util.List;
import javax.persistence.EntityManager;
import org.jbalance.directory.core.model.norm.ProductCategory;
import org.jbalance.directory.core.query.GetResult;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;
import org.jbalance.directory.core.query.helper.GetRequestHelper;
import org.jbalance.directory.core.util.CollectionUtil;

/**
 *
 * @author Mihail
 */
public class GetProductCategoryRequestHelper extends GetRequestHelper<GetProductCategoryRequest, GetProductCategoryResult>{

    public GetProductCategoryRequestHelper(GetProductCategoryRequest request) {
        super(request);
    }

    @Override
    protected GetProductCategoryResult createResult(GetProductCategoryResult result, List<?> records, EntityManager em) {
        List<ProductCategory> productCategories = (List<ProductCategory>) records;
        productCategories = CollectionUtil.filterDuplicates(productCategories);
        result.setProductCategories(productCategories);
        return result;
    }

    @Override
    protected AbstractQueryBuilder<?> initQuery() {
        ProductCategoryQueryBuilder productCategoryQueryBuilder = new ProductCategoryQueryBuilder();
        productCategoryQueryBuilder.appendFilter(request.getIdFilter(), "productCategory.id");
        productCategoryQueryBuilder.appendFilter(request.getCategoryNameFilter(), "productCategory.name");
        productCategoryQueryBuilder.orderBy().append("order by productCategory.enteredDate desc");
        return productCategoryQueryBuilder;
    }
    
}
