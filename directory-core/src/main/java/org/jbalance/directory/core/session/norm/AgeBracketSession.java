package org.jbalance.directory.core.session.norm;

import java.util.List;
import org.jbalance.directory.core.model.norm.AgeBracket;
import org.jbalance.directory.core.query.helper.norm.GetAgeBracketRequest;
import org.jbalance.directory.core.session.GenericSession;

/**
 *
 * @author Mihail
 */
public interface AgeBracketSession extends GenericSession<AgeBracket, GetAgeBracketRequest>{
    public List<AgeBracket> getAll();
}
