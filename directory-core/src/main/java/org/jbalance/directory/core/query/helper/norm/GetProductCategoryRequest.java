package org.jbalance.directory.core.query.helper.norm;

import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.query.GetRequest;

/**
 *
 * @author Mihail
 */
public class GetProductCategoryRequest  extends GetRequest<GetProductCategoryResult>
{

    private static final long serialVersionUID = 1L;

    private LongFilter idFilter;
    private StringFilter categoryNameFilter;

    @Override
    public GetProductCategoryResult createResult()
    {
        return new GetProductCategoryResult(this);
    }

    public StringFilter getCategoryNameFilter()
    {
        return categoryNameFilter;
    }

    public void setCategoryNameFilter(StringFilter categoryNameFilter)
    {
        this.categoryNameFilter = categoryNameFilter;
    }

    public LongFilter getIdFilter() {
        return idFilter;
    }

    public void setIdFilter(LongFilter idFilter) {
        this.idFilter = idFilter;
    }
    
}
