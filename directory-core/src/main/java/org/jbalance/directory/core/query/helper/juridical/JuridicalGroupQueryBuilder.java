package org.jbalance.directory.core.query.helper.juridical;

import org.jbalance.directory.core.model.Juridical;
import org.jbalance.directory.core.model.JuridicalGroup;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;

/**
 * Построитель запросов для {@link Juridical}
 *
 * @author Alexandr Chubenko
 */
public class JuridicalGroupQueryBuilder extends AbstractQueryBuilder<JuridicalGroupQueryBuilder> {

    public JuridicalGroupQueryBuilder() {
        super(null, JuridicalGroup.class, "group");
    }


    @Override
    public StringBuilder buildJoins() {
        StringBuilder joins = new StringBuilder();
//        joins.append(" LEFT JOIN FETCH juridicals ");
        return joins;
    }

    @Override
    protected JuridicalGroupQueryBuilder get() {
        return this;
    }

}
