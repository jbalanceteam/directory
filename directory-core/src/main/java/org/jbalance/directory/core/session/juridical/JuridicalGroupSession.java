
package org.jbalance.directory.core.session.juridical;

import org.jbalance.directory.core.model.JuridicalGroup;
import org.jbalance.directory.core.query.helper.juridical.GetJuridicalGroupsRequest;
import org.jbalance.directory.core.session.GenericSession;

/**
 *
 * @author roman
 */
public interface JuridicalGroupSession extends GenericSession<JuridicalGroup, GetJuridicalGroupsRequest> {
    
}
