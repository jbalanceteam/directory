package org.jbalance.directory.core.session.norm;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import org.jbalance.directory.core.model.norm.Stay;
import org.jbalance.directory.core.query.helper.norm.GetStayRequest;
import org.jbalance.directory.core.query.helper.norm.GetStayRequestHelper;
import org.jbalance.directory.core.query.helper.norm.GetStayResult;
import org.jbalance.directory.core.session.GenericSessionBean;

/**
 *
 * @author Mihail
 */
@Stateless
public class StaySessionBean extends GenericSessionBean<Stay, GetStayRequest>implements StaySession{
    @Override
    @PostConstruct
    public void init() {
        beanClass = Stay.class;
    }

    @Override
    public List<Stay> list(GetStayRequest request) {
        GetStayResult result = new GetStayRequestHelper(request).createResult(manager);
        return result.getStays();
    }
    @Override
    public List<Stay> getAll(){
        GetStayResult result = new GetStayRequestHelper(new GetStayRequest()).createResult(manager);
        return result.getStays();
    }
}
