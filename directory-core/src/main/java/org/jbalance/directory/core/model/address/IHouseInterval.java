package org.jbalance.directory.core.model.address;

import java.util.UUID;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public interface IHouseInterval
{

    UUID getAoGuid();

    UUID getHouseIntId();

    Integer getIntEnd();

    UUID getIntGuid();

    Integer getIntStart();

    Integer getIntStatus();

    void setAoGuid(UUID aoGuid);

    void setAoGuid(String aoGuid);

    void setHouseIntId(UUID houseIntId);

    void setHouseIntId(String houseIntId);

    void setIntEnd(Integer intEnd);

    void setIntGuid(UUID intGuid);

    void setIntGuid(String intGuid);

    void setIntStart(Integer intStart);

    void setIntStatus(Integer intStatus);

    String toString();

}
