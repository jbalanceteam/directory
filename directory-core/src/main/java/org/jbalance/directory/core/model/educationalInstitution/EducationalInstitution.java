package org.jbalance.directory.core.model.educationalInstitution;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.jbalance.directory.core.model.ContractorType;
import org.jbalance.directory.core.model.Juridical;
/**
 * Образовательное учреждение  
 * @author Petr Aleksandrov apv@jbalance.org
 */
@Entity
public class EducationalInstitution extends Juridical {

	private static final long serialVersionUID = 1L;
	
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private EducationalInstitutionType type;
	/**
	 * Номер может содердать не только цифры, но и любые символы. 
	 * Наиболее распространенные: / - 
	 */
	@Column
	private String number;
	
	
	
	public EducationalInstitution() {
		super();
		contractorType = ContractorType.EDUCATIONA_LINSTITUTION;
	}
	public EducationalInstitutionType getType() {
		return type;
	}
	public void setType(EducationalInstitutionType type) {
		this.type = type;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	

}
