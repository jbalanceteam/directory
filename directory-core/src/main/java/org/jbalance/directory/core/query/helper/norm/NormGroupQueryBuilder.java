package org.jbalance.directory.core.query.helper.norm;

import org.jbalance.directory.core.model.norm.NormGroup;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;

/**
 *
 * @author Mihail
 */
public class NormGroupQueryBuilder extends AbstractQueryBuilder<NormGroupQueryBuilder>{
    public NormGroupQueryBuilder() {
        super(null, NormGroup.class, "normGroup");
    }

    @Override
    public StringBuilder buildJoins() {
        StringBuilder joins = new StringBuilder();
        return joins;
    }

    @Override
    protected NormGroupQueryBuilder get() {
        return this;
    }
}
