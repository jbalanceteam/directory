package org.jbalance.directory.core.query.helper.norm;

import java.util.List;
import javax.persistence.EntityManager;
import org.jbalance.directory.core.model.norm.NetNorm;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;
import org.jbalance.directory.core.query.helper.GetRequestHelper;
import org.jbalance.directory.core.util.CollectionUtil;

/**
 *
 * @author mihail
 */
public class GetNetNormsRequestHelper extends GetRequestHelper<GetNetNormsRequest, GetNetNormsResult>{
    
    public GetNetNormsRequestHelper(GetNetNormsRequest request) {
        super(request);
    }

    @Override
    protected AbstractQueryBuilder<?> initQuery() {
        NetNormQueryBuilder netNormQueryBuilder = new NetNormQueryBuilder();
        netNormQueryBuilder.appendFilter(request.getIdFilter(), "netNorm.id");
        netNormQueryBuilder.appendFilter(request.getNormGroupFilter(), "netNorm.normGroup.id");
        netNormQueryBuilder.orderBy().append("order by netNorm.enteredDate desc");
        return netNormQueryBuilder;
    }


    @Override
    protected GetNetNormsResult createResult(GetNetNormsResult result, List<?> records, EntityManager em) {
        List<NetNorm> netNorm = (List<NetNorm>) records;
        netNorm = CollectionUtil.filterDuplicates(netNorm);
        result.setNetNorms(netNorm);
        return result;
    }

}
