// TODO: Придумать имя класса

package org.jbalance.directory.core.model.norm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.jbalance.directory.core.model.BaseEntity;
import org.jbalance.directory.core.model.measure.Measure;

/**
 * Время пребывания в ДОУ
 *
 * Например:
 * "Группа краткосрочного пребывания"  (3)
 * "Группа полного дня"   (10, 12)
 * "Группа круглосуточного пребывания"
 * 
 * @author Mihail
 */
@Entity
public class Stay extends BaseEntity{
    
	private static final long serialVersionUID = 1L;

	@Column(nullable=false)
	private String name;
	
	@Column
    protected String description;
	
    /**
     * время пребывания
     * Например: 3, 10, 12, 24
     */
    private int time;
    
	/**
     * единица измерения
     */
	@ManyToOne
	private Measure measure;
    
    public int getTime(){
        return time;
    }
    
    public void setTime(int time){
        this.time = time;
    }

	public Measure getMeasure(){
		return measure;
	}
	
	public void setMeasure(Measure measure){
		this.measure = measure;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
