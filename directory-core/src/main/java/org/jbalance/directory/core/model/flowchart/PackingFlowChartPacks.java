package org.jbalance.directory.core.model.flowchart;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import org.jbalance.directory.core.model.BaseEntity;
import org.jbalance.directory.core.model.product.Packing;

/**
 *
 * @author Mihail
 */
@Entity
public class PackingFlowChartPacks extends BaseEntity{
	
	@ManyToOne
	private Packing packing;
	
	private Float quantity;
	
	@ManyToOne
	private PackingFlowChart packingFlowChart;
	
	public Packing getPacking(){
		return packing;
	}
	
	public void setPacking(Packing packing){
		this.packing = packing;
	}
	
	public Float getQuantity(){
		return quantity;
	}
	
	public void setQuantity(Float quantity){
		this.quantity = quantity;
	}
	
	public PackingFlowChart getPackingFlowChart(){
		return packingFlowChart;
	}
	
	public void setPacking(PackingFlowChart packingFlowChart){
		this.packingFlowChart = packingFlowChart;
	}
}
