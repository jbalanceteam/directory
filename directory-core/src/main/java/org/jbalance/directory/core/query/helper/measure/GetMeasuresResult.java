
package org.jbalance.directory.core.query.helper.measure;


import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.jbalance.directory.core.model.measure.Measure;
import org.jbalance.directory.core.query.GetResult;


@XmlRootElement
public class GetMeasuresResult extends GetResult
{
    private static final long serialVersionUID = 1L;

    private List<Measure> measures;

    public GetMeasuresResult()
    {
    }

    @XmlElement(name = "measures")
    public List<Measure> getMeasures()
    {
        if (measures == null)
        {
            measures = new ArrayList<Measure>();
        }
        return measures;
    }


    public GetMeasuresResult(GetMeasuresRequest request)
    {
        super(request);
    }
}
