package org.jbalance.directory.core.session.product;

import java.util.Iterator;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import org.jbalance.directory.core.model.product.ProductPacking;
import org.jbalance.directory.core.model.product.Product;
import org.jbalance.directory.core.query.helper.product.GetProductsRequest;
import org.jbalance.directory.core.query.helper.product.GetProductsRequestHelper;
import org.jbalance.directory.core.query.helper.product.GetProductsResult;
import org.jbalance.directory.core.session.GenericSessionBean;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@Stateless
public class ProductSessionBean extends GenericSessionBean<Product, GetProductsRequest> implements ProductSession{

    @Override
    public List<Product> list(GetProductsRequest request) {
         GetProductsResult result = new GetProductsRequestHelper(request).createResult(manager);
        return result.getProducts();
    }

    @Override
    @PostConstruct
    public void init() {
        beanClass = Product.class;
    }

    @Override
    public Product edit(Product o) {
        Product pr = super.edit(o);
        Iterator<ProductPacking> i = pr.getPackings().iterator();

        while (i.hasNext()) {
            ProductPacking p = i.next();

            p.setModifiedDate(pr.getModifiedDate());
        }
        return  pr;
    }

}
