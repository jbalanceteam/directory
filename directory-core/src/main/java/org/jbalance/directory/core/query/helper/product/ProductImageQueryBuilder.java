package org.jbalance.directory.core.query.helper.product;

import org.jbalance.directory.core.model.product.ProductImage;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;

/**
 * Построитель запросов для {@link ProductImage}
 *
 * @author Komov Roman
 */
public class ProductImageQueryBuilder extends AbstractQueryBuilder<ProductImageQueryBuilder>
{

    public ProductImageQueryBuilder()
    {
        super(null, ProductImage.class, "productImage");
    }

//    @Override
//    public StringBuilder buildSelectArgs()
//    {
//        StringBuilder sb = new StringBuilder()
//                .append(primaryAlias()).append(".id, ")
//                .append(primaryAlias()).append(".enteredDate, ")
//                .append(primaryAlias()).append(".deletedDate, ")
//                .append(primaryAlias()).append(".okp ");
//        return sb;
//    }

    @Override
    public StringBuilder buildJoins()
    {
        StringBuilder joins = new StringBuilder();
        return joins;
    }

    @Override
    protected ProductImageQueryBuilder get()
    {
        return this;
    }

}
