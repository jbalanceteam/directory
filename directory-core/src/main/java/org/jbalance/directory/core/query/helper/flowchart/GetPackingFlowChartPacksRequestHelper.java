package org.jbalance.directory.core.query.helper.flowchart;

import java.util.List;
import javax.persistence.EntityManager;
import org.jbalance.directory.core.model.flowchart.PackingFlowChartPacks;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;
import org.jbalance.directory.core.query.helper.GetRequestHelper;
import org.jbalance.directory.core.util.CollectionUtil;

/**
 *
 * @author Mihail
 */
public class GetPackingFlowChartPacksRequestHelper extends GetRequestHelper<GetPackingFlowChartPacksRequest, GetPackingFlowChartPacksResult>{

    public GetPackingFlowChartPacksRequestHelper(GetPackingFlowChartPacksRequest request)
    {
        super(request);
    }

    @Override
    protected AbstractQueryBuilder<?> initQuery()
    {
        PackingFlowChartPacksQueryBuilder packingFlowChartPacksQueryBuilder = new PackingFlowChartPacksQueryBuilder();
        packingFlowChartPacksQueryBuilder.appendFilter(request.getIdFilter(), "packingFlowChartPacks.id");      
        packingFlowChartPacksQueryBuilder.appendFilter(request.getPackingFlowChartFilter(), "packingFlowChartPacks.packingFlowChart.id");
        packingFlowChartPacksQueryBuilder.orderBy().append("order by packingFlowChartPacks.enteredDate asc");
        return packingFlowChartPacksQueryBuilder;
    }

    @Override
    protected GetPackingFlowChartPacksResult createResult(GetPackingFlowChartPacksResult result, List<?> records, EntityManager em)
    {
        List<PackingFlowChartPacks> l = (List<PackingFlowChartPacks>) records;
        l = CollectionUtil.filterDuplicates(l);
        result.getPackingFlowChartPacksList().addAll(l);
        return result;
    }
}
