package org.jbalance.directory.core.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;

/**
 * Сущность описывающая субъект, имеющий право заключать сделки.
 * @author Alexandr Chubenko
 */
@MappedSuperclass
public class Contractor extends BaseEntity{
    
	private static final long serialVersionUID = 1L;
	
	@Column(nullable=false)
	protected String name;
    /**
     * Идентификационный номер налогоплательщика.
     */
    @Column(unique = true)
    protected Long inn;
    
    /**
     * Почтовый адрес
     */
    @Column
    protected String mailingAdress;
    
    /**
     * Контактная информация
     */
    @Column
    protected String contactInfo;
    
    /**
     * Номер телефона
     */
    @Column
    protected String phone;
    
    /**
     * Адрес электронной почты
     */
    @Column
    protected String email;

    /**
     * Адрес электронной почты
     */
    @Column(length = 5000)
    protected String description;
    
    /**
     * Тип контрагента
     */
    @Enumerated(EnumType.STRING)
    @Column(length = ContractorType.FIELD_LENGTH, nullable = false, updatable = false)
    protected ContractorType contractorType;
    

    public Long getInn() {
        return inn;
    }

    public void setInn(Long inn) {
        this.inn = inn;
    }
    
    public String getMailingAdress() {
        return mailingAdress;
    }

    public void setMailingAdress(String mailingAdress) {
        this.mailingAdress = mailingAdress;
    }
    
    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public ContractorType getContractorType() {
		return contractorType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
