package org.jbalance.directory.core.query.helper.flowchart;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import org.jbalance.directory.core.model.flowchart.PackingFlowChart;
import org.jbalance.directory.core.query.GetResult;
/**
 *
 * @author Mihail
 */
public class GetPackingFlowChartResult extends GetResult
{
    private static final long serialVersionUID = 1L;

    private List<PackingFlowChart> packingFlowCharts;

    public GetPackingFlowChartResult()
    {
    }

    @XmlElement(name = "packingFlowCharts")
    public List<PackingFlowChart> getPackingFlowCharts()
    {
        if (packingFlowCharts == null)
        {
            packingFlowCharts = new ArrayList<PackingFlowChart>();
        }
        return packingFlowCharts;
    }


    public GetPackingFlowChartResult(GetPackingFlowChartRequest request)
    {
        super(request);
    }
}
