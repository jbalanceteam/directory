package org.jbalance.directory.core.query.helper.juridical;

import java.util.List;
import javax.persistence.EntityManager;
import org.jbalance.directory.core.model.JuridicalGroup;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;
import org.jbalance.directory.core.query.helper.GetRequestHelper;
import org.jbalance.directory.core.util.CollectionUtil;

/**
 *
 */
public class GetJuridicalGroupsRequestHelper extends GetRequestHelper<GetJuridicalGroupsRequest, GetJuridicalGroupsResult> {

    public GetJuridicalGroupsRequestHelper(GetJuridicalGroupsRequest request) {
        super(request);
    }

    @Override
    protected AbstractQueryBuilder<?> initQuery() {
        JuridicalGroupQueryBuilder juridicalQueryBuilder = new JuridicalGroupQueryBuilder();
        juridicalQueryBuilder.appendFilter(request.getIdFilter(), "group.id");

        return juridicalQueryBuilder;
    }

    @Override
    protected GetJuridicalGroupsResult createResult(GetJuridicalGroupsResult result, List<?> records, EntityManager em) {
        List<JuridicalGroup> juridicals = (List<JuridicalGroup>) records;
        juridicals = CollectionUtil.filterDuplicates(juridicals);
        result.setJuridicalGroups(juridicals);
        return result;
    }
}
