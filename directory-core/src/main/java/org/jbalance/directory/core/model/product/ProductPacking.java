package org.jbalance.directory.core.model.product;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import org.jbalance.directory.core.model.BaseEntity;

/**
 * Конкретный продукт в упакове
 */
@Entity
public class ProductPacking extends BaseEntity {

    @ManyToOne(fetch = FetchType.EAGER)
    private Packing packing;

    @ManyToOne
    private Product product;
	
	private String description;
	
//	@ManyToOne
//	private Measure measure;

    public ProductPacking() {
    }

   
    public ProductPacking(Long id, Date enteredDate, Date deletedDate) {
        super(id, enteredDate, deletedDate);
    }

    public void setPacking(Packing newVar) {
        packing = newVar;
    }

    public Packing getPacking() {
        return packing;
    }

    /**
     * Set the value of product
     *
     * @param newVar the new value of product
     */
    public void setProduct(Product newVar) {
        product = newVar;
    }

    /**
     * Get the value of product
     *
     * @return the value of product
     */
    public Product getProduct() {
        return product;
    }
	
	public String getDescription(){
		return description;
	}
	
	public void setDescription(String description){
		this.description = description;
	}
//	
//	public Measure getMeasure() {
//        return measure;
//    }
//
//    public void setMeasure(Measure measure) {
//        this.measure = measure;
//    }
	

}
