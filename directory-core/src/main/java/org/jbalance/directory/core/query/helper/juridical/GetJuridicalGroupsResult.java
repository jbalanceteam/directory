
package org.jbalance.directory.core.query.helper.juridical;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.jbalance.directory.core.model.JuridicalGroup;
import org.jbalance.directory.core.query.GetResult;

/**
 * @author Alexandr Chubenko
 */
@XmlRootElement
public class GetJuridicalGroupsResult extends GetResult {
    private static final long serialVersionUID = 1L;

    private List<JuridicalGroup> groups;

    public GetJuridicalGroupsResult() {
    }

    @XmlElement(name="juridicals")
    public List<JuridicalGroup> getJuridicalGroups() {
        if (groups == null) {
        	groups = new ArrayList<JuridicalGroup>();
        }
        return groups;
    }

    public void setJuridicalGroups(List<JuridicalGroup> juridicals) {
        this.groups = juridicals;
    }

    public GetJuridicalGroupsResult(GetJuridicalGroupsRequest request) {
        super(request);
    }
}
