package org.jbalance.directory.core.session.product;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import org.jbalance.directory.core.model.product.Packing;
import org.jbalance.directory.core.query.helper.product.GetPackingsRequestHelper;
import org.jbalance.directory.core.query.helper.product.GetPackingsResult;
import org.jbalance.directory.core.query.helper.product.GetPackingsRequest;
import org.jbalance.directory.core.session.GenericSessionBean;

/**
 *
 * @author Mihail
 */
@Stateless
public class PackingSessionBean extends GenericSessionBean<Packing, GetPackingsRequest> implements PackingSession {
	
	@Override
    public List<Packing> list(GetPackingsRequest request) {
        
        GetPackingsResult result = new GetPackingsRequestHelper(request).createResult(manager);
                
        return result.getProductImagePackings();
    }

    @Override
    @PostConstruct
    public void init() {
        beanClass = Packing.class;
    }
}
