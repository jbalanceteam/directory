
package org.jbalance.directory.core.query.helper.product;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.jbalance.directory.core.model.product.TradeMark;
import org.jbalance.directory.core.query.GetResult;

@XmlRootElement
public class GetTradeMarkResult extends GetResult
{
    private static final long serialVersionUID = 1L;

    private List<TradeMark> tradeMarks;

    public GetTradeMarkResult()
    {
    }

    @XmlElement(name = "tradeMarks")
    public List<TradeMark> getTradeMarks()
    {
        if (tradeMarks == null)
        {
            tradeMarks = new ArrayList<TradeMark>();
        }
        return tradeMarks;
    }


    public GetTradeMarkResult(GetTradeMarkRequest request)
    {
        super(request);
    }

    public void setTradeMarks(List<TradeMark> tradeMarks) {
        this.tradeMarks = tradeMarks;
    }

}
