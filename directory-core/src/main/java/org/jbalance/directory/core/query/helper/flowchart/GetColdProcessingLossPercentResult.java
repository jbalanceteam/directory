package org.jbalance.directory.core.query.helper.flowchart;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import org.jbalance.directory.core.model.flowchart.ColdProcessingLossPercent;
import org.jbalance.directory.core.model.flowchart.FlowChartProductImage;
import org.jbalance.directory.core.query.GetResult;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class GetColdProcessingLossPercentResult extends GetResult
{

    private static final long serialVersionUID = 1L;

    private List<ColdProcessingLossPercent> coldProcessingLossPercents;

    public GetColdProcessingLossPercentResult()
    {
    }

    @XmlElement(name = "coldProcessingLossPercent")
    public List<ColdProcessingLossPercent> getColdProcessingLossPercents()
    {
        if (coldProcessingLossPercents == null)
        {
            setColdProcessingLossPercents(new ArrayList<ColdProcessingLossPercent>());
        }
        return coldProcessingLossPercents;
    }

    public GetColdProcessingLossPercentResult(GetColdProcessingLossPercentRequest request)
    {
        super(request);
    }

    public void setColdProcessingLossPercents(List<ColdProcessingLossPercent> entities)
    {
        this.coldProcessingLossPercents = entities;
    }
}
