package org.jbalance.directory.core.model.flowchart;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import org.jbalance.directory.core.model.BaseEntity;
import org.jbalance.directory.core.model.product.ProductImage;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@Entity
public class PackingFlowChartProductImage extends BaseEntity {

    /**
     *
     */
    @ManyToOne
    private ProductImage image;

    @ManyToOne
    private PackingFlowChart flowChart;

    @Column
    private BigDecimal quanity;

    public PackingFlowChartProductImage() {
    }

    public ProductImage getImage() {
        return image;
    }

    public void setImage(ProductImage image) {
        this.image = image;
    }

    public PackingFlowChart getFlowChart() {
        return flowChart;
    }

    public void setFlowChart(PackingFlowChart flowChart) {
        this.flowChart = flowChart;
    }

    public BigDecimal getQuanity() {
        return quanity;
    }

    public void setQuanity(BigDecimal quanity) {
        this.quanity = quanity;
    }

}
