package org.jbalance.directory.core.session.measure;

import org.jbalance.directory.core.model.measure.Measure;
import org.jbalance.directory.core.query.helper.measure.GetMeasuresRequest;
import org.jbalance.directory.core.session.GenericSession;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public interface MeasureSession extends GenericSession<Measure, GetMeasuresRequest>{
    
}
