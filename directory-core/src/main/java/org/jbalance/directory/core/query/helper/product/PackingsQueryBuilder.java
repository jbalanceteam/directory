package org.jbalance.directory.core.query.helper.product;

import org.jbalance.directory.core.model.product.Packing;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;

/**
 *
 * @author Mihail
 */
public class PackingsQueryBuilder extends AbstractQueryBuilder<PackingsQueryBuilder>{
	
	@Override
    public StringBuilder buildJoins()
    {
        StringBuilder joins = new StringBuilder();
        return joins;
    }

	public PackingsQueryBuilder()
    {
        super(null, Packing.class, "productImagePacking");
    }
	
	
    @Override
    protected PackingsQueryBuilder get()
    {
        return this;
    }
}
