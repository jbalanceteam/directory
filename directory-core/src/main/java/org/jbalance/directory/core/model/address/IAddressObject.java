
package org.jbalance.directory.core.model.address;

import java.util.UUID;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public interface IAddressObject
{

    Integer getActStatus();

    UUID getAoGuid();

    UUID getAoId();

    Integer getAoLevel();

    String getAreaCode();

    String getAutoCode();

    Integer getCentStatus();

    String getCityCode();

    String getCode();

    String getCtarCode();

    Integer getCurrStatus();

    String getExtrCode();

    String getFormalName();

    Byte getLiveStatus();

    UUID getNextId();

    String getOffName();

    Integer getOperStatus();

    UUID getParentGuid();

    String getPlaceCode();

    String getPlainCode();

    UUID getPrevId();

    int getRegionCode();

    String getSextCode();

    String getShortName();

    String getStreetCode();

    public String getFullAddressString();

    void setActStatus(Integer actStatus);

    void setAoGuid(UUID aoGuid);

    void setAoId(UUID aoId);

    void setAoLevel(Integer aoLevel);

    void setAreaCode(String areaCode);

    void setAutoCode(String autoCode);

    void setCentStatus(Integer centStatus);

    void setCityCode(String cityCode);

    void setCode(String code);

    void setCtarCode(String ctarCode);

    void setCurrStatus(Integer currStatus);

    void setExtrCode(String extrCode);

    void setFormalName(String formalName);

    void setLiveStatus(Byte liveStatus);

    void setNextId(UUID nextId);

    void setOffName(String offName);

    void setOperStatus(Integer operStatus);

    void setParentGuid(UUID parentGuid);

    void setPlaceCode(String placeCode);

    void setPlainCode(String plainCode);

    void setPrevId(UUID prevId);

    void setRegionCode(int regionCode);

    void setSextCode(String sextCode);

    void setShortName(String shortName);

    void setStreetCode(String streetCode);

    public void setFullAddressString(String fullAddressString);

}
