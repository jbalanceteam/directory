package org.jbalance.directory.core.util;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Common methods for managing {@code Collections}, {@code Maps}, and arrays.
 * 
 */
public class CollectionUtil {
    /**
     * Adds all of the items from the specified source collection to the
     * specified target collection, filtering null and duplicates as specified.
     * Used to sanitize collection arguments to methods that do not expect
     * {@code null} or duplicate values.
     * 
     * @param target
     *            The target collection
     * @param source
     *            The source collection
     * @param filterNulls
     *            Whether to omit {@code null} values from the source collection
     * @param filterDuplicates
     *            Whether to omit duplicate values from the source collection
     * @return {@code target}
     */
    public static <T, C extends Collection<T>> C addAll(C target, Collection<T> source, boolean filterNulls,
            boolean filterDuplicates) {
        if (source != null) {
            for (T item : source) {
                if ((!filterNulls || item != null) && (!filterDuplicates || !target.contains(item))) {
                    target.add(item);
                }
            }
        }
        return target;
    }

    /**
     * Shortcut for {@code addAll(target, Arrays.asList(source), filterNulls,
     * filterDuplicates)}
     * 
     * @param target
     *            The target collection
     * @param source
     *            The source collection (array)
     * @return {@code target}
     * @see #addAll(java.util.Collection, java.util.Collection, boolean, boolean)
     */
    public static <T, C extends Collection<T>> C addAll(C target, T[] source, boolean filterNulls, boolean filterDuplicates) {
        return addAll(target, Arrays.asList(source), filterNulls, filterDuplicates);
    }

    /**
     * Shortcut for {@code addAll(target, source, true, false)}
     * 
     * @param target
     *            The target collection
     * @param source
     *            The source collection
     * @return {@code target}
     * @see #addAll(java.util.Collection, java.util.Collection, boolean, boolean)
     */
    public static <T, C extends Collection<T>> C addAll(C target, Collection<T> source) {
        return addAll(target, source, true, false);
    }

    /**
     * Shortcut for {@code addAll(target, source, true, false)}
     * 
     * @param target
     *            The target collection
     * @param source
     *            The source collection (array)
     * @return {@code target}
     * @see #addAll(java.util.Collection, java.util.Collection, boolean, boolean)
     */
    public static <T, C extends Collection<T>> C addAll(C target, T[] source) {
        return addAll(target, source, true, false);
    }

    /**
     * Creates a list out of several individual items of the same type
     * 
     * @param <T>
     *            The object type
     * @param objects
     *            The individual objects (or array of objects)
     * @return A {@code List} consisting of the individual objects
     * @see #addAll(java.util.Collection, java.util.Collection, boolean, boolean)
     */
    public static <T> List<T> listOf(T... objects) {
        List<T> list = new ArrayList<T>();
        for (T object : objects) {
            list.add(object);
        }
        return list;
    }

    public static <T> List<T> filterDuplicates(Collection<T> collection) {
        return new ArrayList<T>(new LinkedHashSet<T>(collection));
    }

    /**
     * Checks the list to see if it is null or empty
     * 
     * @param collection
     * @return True if List == null || empty
     */
    public static boolean empty(List<?> collection) {
        return collection == null ? true : collection.isEmpty();
    }

    /**
     * Merges a set of arrays into a single array. The arrays should be of the
     * same type, otherwise you will recieve a class cast exception
     * 
     * @param <T>
     * @param arrays
     * @return An array of whatever object is passed
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] mergeArrays(T[]... arrays) throws ClassCastException {
        List<T> list = new ArrayList<T>();
        for (T[] array : arrays) {
            list.addAll(Arrays.asList(array));
        }
        return list.toArray((T[]) Array.newInstance(arrays[0][0].getClass(), list.size()));
    }
    
    public static <T> List<List<T>> split(List<T> list, int maxElements) {
        if(list == null || list.isEmpty()) {
            return Collections.emptyList();
        }
        
        int start = 0;
        int end = 0;
        List<List<T>> listList = new ArrayList<List<T>>((int)Math.ceil((double)list.size() / (double)maxElements));
        while(start < list.size()) {
            end = Math.min(start + maxElements, list.size());
            listList.add(list.subList(start, end));
            start = end;
        }
        
        return listList;
    }
}
