package org.jbalance.directory.core.query.helper.product;

import java.util.List;
import javax.persistence.EntityManager;
import org.jbalance.directory.core.model.product.ProductPacking;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;
import org.jbalance.directory.core.query.helper.GetRequestHelper;
import org.jbalance.directory.core.util.CollectionUtil;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class GetProductPackingsRequestHelper   extends GetRequestHelper<GetProductPackingsRequest, GetProductPackingsResult>
{

    public GetProductPackingsRequestHelper(GetProductPackingsRequest request)
    {
        super(request);
    }

    @Override
    protected AbstractQueryBuilder<?> initQuery()
    {
        ProductPackingQueryBuilder packingQueryBuilder = new ProductPackingQueryBuilder();
        packingQueryBuilder.appendFilter(request.getIdFilter(), "productPacking.id");
        packingQueryBuilder.appendFilter(request.getProductFilter(), "productPacking.product.id");
        packingQueryBuilder.appendFilter(request.getProductNameFilter(), "productPacking.product.name");
        packingQueryBuilder.appendFilter(request.getPackingFilter(), "productPacking.packing.id");
        packingQueryBuilder.appendFilter(request.getModifiedFilter(), "productPacking.modifiedDate");

        packingQueryBuilder.orderBy().append("order by productPacking.enteredDate desc");
        return packingQueryBuilder;
    }

    @Override
    protected GetProductPackingsResult createResult(GetProductPackingsResult result, List<?> records, EntityManager em)
    {
        List<ProductPacking> productImages = (List<ProductPacking>) records;
        productImages = CollectionUtil.filterDuplicates(productImages);
        result.setPackings(productImages);
        return result;
    }
}