package org.jbalance.directory.core.query.helper.product;

import java.util.List;
import javax.persistence.EntityManager;
import org.jbalance.directory.core.model.product.Packing;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;
import org.jbalance.directory.core.query.helper.GetRequestHelper;
import org.jbalance.directory.core.util.CollectionUtil;

/**
 *
 * @author Mihail
 */
public class GetPackingsRequestHelper extends GetRequestHelper<GetPackingsRequest, GetPackingsResult>{
	
	public GetPackingsRequestHelper(GetPackingsRequest request)
    {
        super(request);
    }
	
	@Override
    protected AbstractQueryBuilder<?> initQuery()
    {
        PackingsQueryBuilder qb = new PackingsQueryBuilder();
        qb.appendFilter(request.getIdFilter(), "productImagePacking.id");      
        qb.appendFilter(request.getNameFilter(), "productImagePacking.name");
        qb.appendFilter(request.getImageFilter(), "productImagePacking.image.id");
//        qb.appendFilter(request.getImagePackingFilter(), "productImagePacking.productImagePacking.id");
        
        qb.orderBy().append("order by productImagePacking.enteredDate desc");
        return qb;
    }

    @Override
    protected GetPackingsResult createResult(GetPackingsResult result, List<?> records, EntityManager em)
    {
        List<Packing> packings = (List<Packing>) records;
        packings = CollectionUtil.filterDuplicates(packings);
        result.setProductImagePackings(packings);
        return result;
    }
}
