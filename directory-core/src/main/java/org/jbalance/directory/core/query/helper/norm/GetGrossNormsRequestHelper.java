package org.jbalance.directory.core.query.helper.norm;

import java.util.List;
import javax.persistence.EntityManager;
import org.jbalance.directory.core.model.norm.GrossNorm;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;
import org.jbalance.directory.core.query.helper.GetRequestHelper;
import org.jbalance.directory.core.util.CollectionUtil;

/**
 *
 * @author Mihail
 */
public class GetGrossNormsRequestHelper extends GetRequestHelper<GetGrossNormsRequest, GetGrossNormsResult>{
    
    public GetGrossNormsRequestHelper(GetGrossNormsRequest request) {
        super(request);
    }

    @Override
    protected AbstractQueryBuilder<?> initQuery() {
        GrossNormQueryBuilder grossNormQueryBuilder = new GrossNormQueryBuilder();
        grossNormQueryBuilder.appendFilter(request.getIdFilter(), "grossNorm.id");
        grossNormQueryBuilder.appendFilter(request.getNetNormFilter(), "grossNorm.netNorm.id");
        grossNormQueryBuilder.orderBy().append("order by grossNorm.startMonth asc");
        return grossNormQueryBuilder;
    }


    @Override
    protected GetGrossNormsResult createResult(GetGrossNormsResult result, List<?> records, EntityManager em) {
        List<GrossNorm> grossNorm = (List<GrossNorm>) records;
        grossNorm = CollectionUtil.filterDuplicates(grossNorm);
        result.setGrossNorms(grossNorm);
        return result;
    }

}