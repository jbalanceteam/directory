package org.jbalance.directory.core.query.helper.norm;

import org.jbalance.directory.core.model.norm.AgeBracket;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;

/**
 *
 * @author Mihail
 */
public class AgeBracketQueryBuilder extends AbstractQueryBuilder<AgeBracketQueryBuilder>{
    public AgeBracketQueryBuilder() {
        super(null, AgeBracket.class, "ageBracket");
    }

    @Override
    protected AgeBracketQueryBuilder get() {
        return this;
    }
}
