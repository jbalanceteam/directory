package org.jbalance.directory.core.util;

import java.util.Date;

/**
 * <p>
 * Common object manipulation shortcuts. The methods are static and are as terse
 * as possible to facilitate in-line use.
 * 
 */
public class Obj {
    public static final int PRESERVE_ORDER = Integer.MIN_VALUE;

    /**
     * <p>
     * Null-safe equality check. Returns {@code true} if:
     * </p>
     * <ul>
     * <li>{@code obj1} and {@code obj2} are {@code null} and {@code nullsEqual}
     * is {@code true}</li>
     * <li>{@code obj1} and {@code obj2} are not null {@code null} and {@code
     * obj1.equals(obj2)}</li>
     * </ul>
     * 
     * @param obj1
     *            The first object
     * @param obj2
     *            The second object
     * @param nullsEqual
     *            Whether to tree two {@code null} objects as equal
     * @return {@code true} if the two objects are equal
     */
    public static boolean eq(Object obj1, Object obj2, boolean nullsEqual) {
        if (obj1 == null || obj2 == null) {
            return obj1 == obj2 && nullsEqual;
        }
        return obj1.equals(obj2);
    }

    /**
     * <p>
     * Null-safe comparison on the names of two enum values
     * </p>
     * 
     * @param <E>
     *            The type of enum
     * @param obj1
     *            The first enum value
     * @param obj2
     *            The second enum value
     * @param nullsEqual
     *            Whether two null values should be considered equal
     * @param nullsLast
     *            Whether {@code null} objects should come before or after
     *            non-null objects
     * @return A negative integer, 0, or a positive integer if {@code obj1} is
     *         less than, equal to, or greater than {@code obj2}. If both
     *         objects are {@code null} and {@code nullsEqual} is {@code false},
     *         then the outcome of this method in theory is undefined but in
     *         practice is a negative integer to preserve the original ordering.
     * @throws ClassCastException
     *             if {@code obj1} or {@code obj2} are not comparable (i.e.
     *             {@code obj1.compareTo} throws a {@code ClassCastException}).
     *             This might happen if the two objects share a common base
     *             class but are of different, incompatible subclasses (e.g.
     *             {@code obj1} is a {@code java.lang.Float} and {@code obj2} is
     *             a {@code java.lang.Integer}).
     */
    public static <E extends Enum<E>> int eCmp(E obj1, E obj2, boolean nullsEqual, boolean nullsLast) {
        if (obj1 == null && obj2 == null) {
            return nullsEqual ? 0 : -1;
        } else if (obj1 == null) {
            return nullsLast ? 1 : -1;
        } else if (obj2 == null) {
            return nullsLast ? -1 : 1;
        } else {
            return obj1.name().compareTo(obj2.name());
        }
    }

    /**
     * <p>
     * Null-safe object comparison between two like comparable objects
     * </p>
     * 
     * @param <T>
     *            The type of object
     * @param obj1
     *            The first object
     * @param obj2
     *            The second object
     * @param nullsEqual
     *            Whether two null values should be considered equal
     * @param nullsLast
     *            Whether {@code null} objects should come before or after
     *            non-null objects
     * @return A negative integer, 0, or a positive integer if {@code obj1} is
     *         less than, equal to, or greater than {@code obj2}. If both
     *         objects are {@code null} and {@code nullsEqual} is {@code false},
     *         then the outcome of this method in theory is undefined but in
     *         practice is a negative integer to preserve the original ordering.
     * @throws ClassCastException
     *             if {@code obj1} or {@code obj2} are not comparable (i.e.
     *             {@code obj1.compareTo} throws a {@code ClassCastException}).
     *             This might happen if the two objects share a common base
     *             class but are of different, incompatible subclasses (e.g.
     *             {@code obj1} is a {@code java.lang.Float} and {@code obj2} is
     *             a {@code java.lang.Integer}).
     */
    public static <T extends Comparable<T>> int cmp(T obj1, T obj2, boolean nullsEqual, boolean nullsLast) {
        if (obj1 == null && obj2 == null) {
            return nullsEqual ? 0 : -1;
        } else if (obj1 == null) {
            return nullsLast ? 1 : -1;
        } else if (obj2 == null) {
            return nullsLast ? -1 : 1;
        } else {
            return obj1.compareTo(obj2);
        }
    }

    /**
     * <p>
     * Shortcut for {@code cmp(obj1, obj2, false, true)}
     * </p>
     * 
     * @param <T>
     *            The type of object
     * @param obj1
     *            The first object
     * @param obj2
     *            The second object
     * @return A negative integer, 0, or a positive integer if {@code obj1} is
     *         less than, equal to, or greater than {@code obj2}. If both
     *         objects are {@code null} then the outcome of this method in
     *         theory is undefined but in practice is a negative integer to
     *         preserve the original ordering.
     * @see #cmp(java.lang.Comparable, java.lang.Comparable, boolean, boolean)
     */
    public static <T extends Comparable<T>> int cmp(T obj1, T obj2) {
        return cmp(obj1, obj2, false, true);
    }

    /**
     * Attempts to coerce to object to a long value if it is a number
     * 
     * @param obj
     *            Any subclass of Number
     * @return Long
     */
    public static Long coerceToLong(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof Long) {
            return (Long) obj;
        } else if (obj instanceof Number) {
            return ((Number) obj).longValue();
        } else {
            return new Long(obj.toString());
        }
    }

    /**
     * Attempts to coerce to object to an integer value if it is a number
     * 
     * @param obj
     *            Any subclass of Number
     * @return Integer
     */
    public static Integer coerceToInt(Object obj) {
        if (obj == null) {
            return null;
        } else if (obj instanceof Integer) {
            return (Integer) obj;
        } else if (obj instanceof Number) {
            return ((Number) obj).intValue();
        } else {
            return Integer.valueOf(obj.toString());
        }
    }

    /**
     * Attempts to coerce to object to a Boolean value
     * 
     * @param obj
     *            Boolean, Number (0 = false), String
     * @return Boolean
     */
    public static Boolean coerceToBoolean(Object obj) {
        if (obj == null) {
            return null;
        } else if (obj instanceof Boolean) {
            return (Boolean) obj;
        } else if (obj instanceof Number) {
            return ((Number) obj).intValue() != 0;
        } else {
            return Boolean.valueOf(obj.toString());
        }
    }

    /**
     * Attempts to coerce to object to an Enumerated value
     * 
     * @param <E>
     *            The type of enumeration
     * @param obj
     *            The name of an enumeration
     * @param type
     *            The type of enumeration
     * 
     * @return E An enumerated value of type
     */
    @SuppressWarnings("unchecked")
    public static <E extends Enum<E>> E coerceToEnum(Object obj, Class<E> type) {
        if (obj == null) {
            return null;
        } else if (type.isInstance(obj)) {
            return type.cast(obj);
        } else if(Enum.class.isInstance(obj)) {
            return Enum.valueOf(type, ((Enum)obj).name());
        } else {
            return Enum.valueOf(type, obj.toString());
        }
    }

    /**
     * Returns null if object is null, otherwise returns obj.toString
     * 
     * @param obj
     * @return
     */
    public static String coerceToString(Object obj) {
        if (obj == null) {
            return null;
        } else if (obj instanceof Enum<?>) {
            return ((Enum<?>) obj).name();
        } else {
            return String.valueOf(obj);
        }
    }

    /**
     * Returns null if object is null or not a date, otherwise returns a date
     * 
     * @param obj
     * @return
     */
    public static Date coerceToDate(Object obj) {
        if (obj == null || !(obj instanceof Date)) {
            return null;
        }
        return (Date) obj;
    }

    /**
     * Returns the first non-null, non-empty value from a list of values,
     * similar to the SQL COALESCE function.
     * 
     * @param <T>
     *            The type of values
     * @param values
     *            The list of values
     * 
     * @return the first non-null, non-empty value from a list of values or
     *         {@code null} if all values are null or empty
     */
    public static <T> T coalesce(T... values) {
        for (T value : values) {
            if (!Str.empty(value)) {
                return value;
            }
        }
        return null;
    }
}
