package org.jbalance.directory.core.filter;

import java.util.List;

import org.jbalance.directory.core.util.Obj;
import org.jbalance.directory.core.util.Str;

public enum BooleanFilterType implements FilterType {
    /**
     * <p>Equals or equal-to.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> = <i>rvalue</i></tt></p>
     */
    EQUALS("Equals", ArgumentsExpected.ONE),

    /**
     * <p>Equals or is null.  Equivalent to:</p>
     *
     * <p><tt>(<i>lvalue</i> = <i>rvalue</i> OR </tt><i>lvalue</i> IS NULL)</p>
     */
    EQN("Equals or is null", ArgumentsExpected.ONE);

    private final String description;
    private final ArgumentsExpected argumentsExpected;

    /**
     * Returns a user-friendly description of the comparison operator
     *
     * @return a user-friendly description of the comparison operator
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * Returns the number of arguments expected by this filter type
     *
     * @return number of arguments expected by this filter type
     */
    @Override
    public ArgumentsExpected getArgumentsExpected() {
        return argumentsExpected;
    }

    /**
     * Creates a new comparison with the specified description (internal use
     * only)
     *
     * @param description A user-friendly description of the comparison
     * operator
     */
    private BooleanFilterType(String description, ArgumentsExpected argumentsExpected) {
        this.description = description;
        this.argumentsExpected = argumentsExpected;
    }

    /**
     * Returns the user-friendly description of the comparison.  This returns
     * the same value as {@link #getDescription()}
     *
     * @return the user-friendly description of the comparison
     *
     * @see #getDescription()
     */
    @Override
    public String toString() {
        return description;
    }

    @Override
    public void appendFilter(StringBuilder query, FilterParams params, String fieldName, List<?> filterValues) {
        if(query == null) {
            throw new IllegalArgumentException("Query string build is required");
        }

        if(params == null) {
            throw new IllegalArgumentException("Filter parameters are required");
        }

        if(Str.empty(fieldName)) {
            throw new IllegalArgumentException("Field name is required");
        }

        if(filterValues == null || filterValues.size() != 1) {
            throw new IllegalArgumentException(this + " filter type requires exactly one filter value");
        }

        if (this == EQUALS) {
            Object paramValue = filterValues == null || filterValues.isEmpty() ? null : filterValues.get(0);
            query.append(" ");
            query.append(fieldName);
            query.append(" = ");
            query.append(Obj.coerceToString(paramValue));
        } else if (this == EQN) {
            Object paramValue = filterValues == null || filterValues.isEmpty() ? null : filterValues.get(0);
            query.append(" (");
            query.append(fieldName);
            query.append(" = ");
            query.append(Obj.coerceToString(paramValue));
            query.append(" or ");
            query.append(fieldName);
            query.append(" is null)");
        }
    }
}
