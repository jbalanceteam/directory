package org.jbalance.directory.core.model.address;


import java.util.UUID;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class Region extends AddressObject implements IRegion
{

    public Region()
    {
    }

    public Region(UUID uuid)
    {
        super(uuid);
    }

    @Override
    public String toString()
    {
        return this.formalName;
    }
}
