package org.jbalance.directory.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateUtil {
    public static final int DEFAULT_UI_VERSION = 1;
    public static final int MILLIS_PER_SEC = 1000;
    public static final int MILLIS_PER_MIN = 60 * MILLIS_PER_SEC;
    public static final int MILLIS_PER_HR = 60 * MILLIS_PER_MIN;
    public static final int MILLIS_PER_DAY = 24 * MILLIS_PER_HR;
    public static final String DATE_PATTERN = "MM/dd/yyyy";

    public static String format(Date date, String formatPattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(formatPattern);
        return dateFormat.format(date);
    }

    public static String getAge(Date startDate, Date endDate) {
        if (startDate == null) {
            return null;
        }
        if (endDate == null) {
            endDate = new Date();
        }
        long millisDiff = endDate.getTime() - startDate.getTime();
        StringBuilder age = new StringBuilder();
        long[] units = { MILLIS_PER_DAY, MILLIS_PER_HR, MILLIS_PER_MIN };
        char[] unitNames = { 'd', 'h', 'm' };
        int[] values = new int[units.length];
        for (int i = 0; i < units.length; i++) {
            while (millisDiff > units[i]) {
                values[i]++;
                millisDiff -= units[i];
            }
            age.append(values[i]).append(unitNames[i]).append(' ');
        }
        return age.toString().trim();
    }

    public static Date trunc(Date date, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        String str = sdf.format(date);
        try {
            return sdf.parse(str);
        } catch (ParseException pe) {
            // Should not happen
            throw new RuntimeException(pe);
        }
    }

    /**
     * <p>
     * Returns a <tt>Date</tt> corresponding to midnight of the specified
     * <tt>year</tt>, <tt>month</tt>, and <tt>day</tt>.
     * </p>
     *
     * @param year
     *            the 4-digit year
     * @param month
     *            the 1- or 2-digit month (1-12)
     * @param day
     *            the 1- or 2-digit day (1-31)
     * @return a <tt>Date</tt> corresponding to midnight of the specified
     *         <tt>year</tt>, <tt>month</tt>, and <tt>day</tt>.
     */
    public static Date getDate(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.set(year, month - 1, day);
        return cal.getTime();
    }

    public static boolean isSameDay(Date d1, Date d2) {
        if(null == d1 || null == d2){
            return false;
        }
        Calendar c1 = Calendar.getInstance();
        c1.setTime(d1);
        Calendar c2 = Calendar.getInstance();
        c2.setTime(d2);
        return c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH)
                && c1.get(Calendar.DATE) == c2.get(Calendar.DATE);
    }

    public static boolean isWeekend(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int weekDay = cal.get(Calendar.DAY_OF_WEEK);
        return weekDay == Calendar.SATURDAY || weekDay == Calendar.SUNDAY;
    }

    public static boolean isHoliday(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);
        for (Holiday holiday : getHolidaysObserved(year)) {
            if (isSameDay(date, holiday.getDate())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isWorkDay(Date date) {
        return !isWeekend(date) && !isHoliday(date);
    }

    public static Holiday[] getHolidaysObserved() {
        return getHolidaysObserved(Calendar.getInstance().get(Calendar.YEAR));
    }

    public static Holiday[] getHolidaysObserved(int year) {
        return new Holiday[] { new Holiday("New Year's Day", getNewYearsDayObserved(year)),
                new Holiday("Birthday of Martin Luther King, Jr.", getMLKDayObserved(year)),
                new Holiday("President's Day", getPresidentsDayObserved(year)),
                new Holiday("Easter Sunday", getEasterObserved(year)), new Holiday("Memorial Day", getMemorialDayObserved(year)),
                new Holiday("Independence Day", getIndependenceDayObserved(year)),
                new Holiday("Columbus Day", getColumbusDayObserved(year)), new Holiday("Labor Day", getLaborDayObserved(year)),
                new Holiday("Thanksgiving Day", getThanksgivingDayObserved(year)),
                new Holiday("Veterans Day", getVeteransDayObserved(year)),
                new Holiday("Christmas Day", getChristmasDayObserved(year)) };
    }

    public static Date applyObservationRules(Date holiday) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(holiday);
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        switch (dayOfWeek) {
        case Calendar.SATURDAY:
            // Previous Friday
            cal.add(Calendar.DATE, -1);
            break;
        case Calendar.SUNDAY:
            // Following Monday
            cal.add(Calendar.DATE, 1);
            break;
        }
        return cal.getTime();
    }

    /**
     * <p>
     * Returns the <tt>Date</tt> of the <tt>n</tt><sup>th</sup> occurrence of
     * the specified day of the week in the specified <tt>year</tt> and
     * <tt>month</tt>.
     * </p>
     *
     * @param year
     *            the 4-digit year
     * @param month
     *            the 1- or 2-digit month (1-12)
     * @param n
     *            the number of occurrences. Positive values are measured from
     *            the beginning of the month, and negative values are measured
     *            from the end of the month
     * @param dayOfWeek
     *            the day of the week (see <tt>Calendar.SUNDAY</tt>, etc.)
     * @return the <tt>Date</tt> of the <tt>n</tt><sup>th</sup> occurrence of the
     *         specified day of the week in the specified <tt>year</tt> and
     *         <tt>month</tt>.
     */
    public static Date getNthWeekDayOfMonth(int year, int month, int n, int dayOfWeek) {
        Calendar cal = Calendar.getInstance();
        cal.clear();
        int increment = 1;
        if (n < 0) {
            cal.set(year, month - 1, 1);
            cal.set(Calendar.DAY_OF_MONTH, cal.getMaximum(Calendar.DAY_OF_MONTH));
            increment = -1;
            n *= -1;
        } else {
            cal.set(year, month - 1, 1);
        }
        int count = 0;
        while (count < n) {
            if (cal.get(Calendar.DAY_OF_WEEK) == dayOfWeek) {
                count++;
                if (count >= n) {
                    break;
                }
            }
            cal.add(Calendar.DATE, increment);
        }
        return cal.getTime();
    }

    public static int getYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }

    public static Date getNewYearsDayObserved(int year) {
        return applyObservationRules(getDate(year, 1, 1));
    }

    public static Date getMLKDayObserved(int year) {
        return getNthWeekDayOfMonth(year, 1, 3, Calendar.MONDAY);
    }

    public static Date getPresidentsDayObserved(int year) {
        return getNthWeekDayOfMonth(year, 2, 3, Calendar.MONDAY);
    }

    public static Date getEasterObserved(int year) {
        int a = year % 19;
        int b = year / 100;
        int c = year % 100;
        int d = b / 4;
        int e = b % 4;
        int f = (b + 8) / 25;
        int g = (b - f + 1) / 3;
        int h = (19 * a + b - d - g + 15) % 30;
        int i = c / 4;
        int k = c % 4;
        int l = (32 + 2 * e + 2 * i - h - k) % 7;
        int m = (a + 11 * h + 22 * l) / 451;
        int p = (h + l - 7 * m + 114) % 31;
        int month = (h + l - 7 * m + 114) / 31;
        int day = p + 1;
        return getDate(year, month, day);
    }

    public static Date getMemorialDayObserved(int year) {
        return getNthWeekDayOfMonth(year, 5, -1, Calendar.MONDAY);
    }

    public static Date getIndependenceDayObserved(int year) {
        return applyObservationRules(getDate(year, 7, 4));
    }

    public static Date getLaborDayObserved(int year) {
        return getNthWeekDayOfMonth(year, 9, 1, Calendar.MONDAY);
    }

    public static Date getColumbusDayObserved(int year) {
        return getNthWeekDayOfMonth(year, 10, 2, Calendar.MONDAY);
    }

    public static Date getVeteransDayObserved(int year) {
        return applyObservationRules(getDate(year, 11, 11));
    }

    public static Date getThanksgivingDayObserved(int year) {
        return getNthWeekDayOfMonth(year, 11, 4, Calendar.THURSDAY);
    }

    public static Date getChristmasDayObserved(int year) {
        return applyObservationRules(getDate(year, 12, 25));
    }

    public static int sameCompare(Date d1, Date d2) {
        return sameCompare(d1, d2, false);
    }

    public static int sameCompare(Date d1, Date d2, boolean nullsEqual) {
        if ((d1 == null || d2 == null) && !nullsEqual) {
            return -1;
        } else if (d1 == d2) {
            return 0;
        } else if (d1 == null || d2 == null) {
            return -1;
        } else {
            return d1.compareTo(d2);
        }
    }

    public static class Holiday {
        private String name;
        private Date date;

        public String getName() {
            return name;
        }

        public Date getDate() {
            return date;
        }

        public Holiday(String name, Date date) {
            this.name = name;
            this.date = date;
        }
    }

    /**
     * Convienence method, gets duration between startDate and now
     *
     * @param startDate
     * @return {@link DateUtil#duration(java.util.Date, java.util.Date)}
     */
    public static String duration(java.util.Date startDate) {
        return duration(startDate, null);
    }

    @SuppressWarnings("boxing")
    private static String duration(long startTime, long endTime) {
        long diff = endTime - startTime;
        long days = TimeUnit.MILLISECONDS.toDays(diff);
        diff = diff - TimeUnit.DAYS.toMillis(days);
        long hours = TimeUnit.MILLISECONDS.toHours(diff);
        diff = diff - TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);
        if (days > 0) {
            if (hours >= 18) {
                return String.format("%.2f days", days + .75);
            } else if (hours >= 12) {
                return String.format("%.1f days", days + .5);
            } else if (hours >= 6) {
                return String.format("%.2f days", days + .25);
            } else {
                if (days == 1) {
                    return String.format("%d day", days);
                }
                return String.format("%d days", days);
            }
        }
        if (hours > 0) {
            if (minutes >= 45) {
                return String.format("%.2f hrs", hours + .75);
            } else if (hours >= 30) {
                return String.format("%.1f hrs", hours + .5);
            } else if (hours >= 15) {
                return String.format("%.2f hrs", hours + .25);
            } else {
                if (hours == 1) {
                    return String.format("%d hr", hours);
                }
                return String.format("%d hrs", hours);
            }
        }
        if (minutes == 1) {
            return String.format("%d min", minutes);
        }
        return String.format("%d mins", minutes);
    }

    /**
     * Returns a duration string. startTime is required, endTime is optional. if
     * endTime is null, the current system time is used Samples: 1.5 days - if
     * duration has days and hours between 12 and 18 23.25 hours - if duration
     * has 0 days 14 mins - if no hours or days
     *
     * @param startDate
     * @param endDate
     * @return friendly duration string or Unknown if startTime is null
     */
    public static String duration(java.util.Date startDate, java.util.Date endDate) {
        if (null == startDate) {
            return "Unknown";
        }
        return duration(startDate.getTime(), null == endDate ? System.currentTimeMillis() : endDate.getTime());
    }

    /**
     * Gets the month and year from date.
     *
     * @param date
     *            the date
     * @return example 1105
     */
    public static String getMonthYearFromDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return String.valueOf(cal.get(Calendar.MONTH)) + String.valueOf(cal.get(Calendar.YEAR));
    }

    public static long currentTimeMillis() {
        return System.currentTimeMillis();
    }
    
    public static String convertToString(Date date) {
        String dateString = "";
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        dateString = String.format("%d/%d/%d %d:%02d:%02d %s", cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH),
                cal.get(Calendar.YEAR), cal.get(Calendar.HOUR), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND),
                cal.get(Calendar.AM_PM) == Calendar.PM ? "pm" : "am");
        
        return dateString;
    }

    public static String getFormattedDate(Date date) {
        String result = null;
        if (date != null) {
            result = format(date, DATE_PATTERN);
        }
        return result;
    }
}
