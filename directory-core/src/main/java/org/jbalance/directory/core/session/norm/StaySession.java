package org.jbalance.directory.core.session.norm;

import java.util.List;
import org.jbalance.directory.core.model.norm.Stay;
import org.jbalance.directory.core.query.helper.norm.GetStayRequest;
import org.jbalance.directory.core.session.GenericSession;

/**
 *
 * @author Mihail
 */
public interface StaySession extends GenericSession<Stay, GetStayRequest>{
    public List<Stay> getAll();
}
