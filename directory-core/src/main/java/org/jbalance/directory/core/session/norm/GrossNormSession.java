package org.jbalance.directory.core.session.norm;

import org.jbalance.directory.core.model.norm.GrossNorm;
import org.jbalance.directory.core.query.helper.norm.GetGrossNormsRequest;
import org.jbalance.directory.core.session.GenericSession;

/**
 *
 * @author Mihail
 */
public interface GrossNormSession extends GenericSession<GrossNorm, GetGrossNormsRequest>{

}
