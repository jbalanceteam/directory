
package org.jbalance.directory.core.query.helper.product;

import org.jbalance.directory.core.model.product.ProductPacking;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class ProductPackingQueryBuilder extends AbstractQueryBuilder<ProductPackingQueryBuilder>
{

    public ProductPackingQueryBuilder()
    {
        super(null, ProductPacking.class, "productPacking");
    }

    @Override
    public StringBuilder buildJoins()
    {
        StringBuilder joins = new StringBuilder();
//                .append("LEFT JOIN packing.product product ");
        return joins;
    }

    @Override
    protected ProductPackingQueryBuilder get()
    {
        return this;
    }

}
