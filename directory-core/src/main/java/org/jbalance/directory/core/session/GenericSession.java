
package org.jbalance.directory.core.session;

import java.util.Date;
import java.util.List;
import org.jbalance.directory.core.model.BaseEntity;
import org.jbalance.directory.core.query.GetRequest;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 * @param <T> - Реализация класса справочника, наследующая от {@link BaseEntity}
 * @param <S> - Реализация {@link GetRequest}, для конкретного класса справочника <tt>T</tt>
 */
public interface GenericSession<T extends BaseEntity, S extends GetRequest<?>>
{
    
    public T add(T bean);

    public T delete(Long id);

    public T getById(Long id);

    public T edit(T bean);

    public List<T> getUpdates(Date date);

    /**
     * Метод получения списка объектов из справочника по заданному фильтру.
     * @param request Фильтр
     * @return Список объектов справочника.
     * @see GetRequest
     */
    public List<T> list(S request);
}
