package org.jbalance.directory.core.query.helper.flowchart;

import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.query.GetRequest;

/**
 *
 * @author Mihail
 */
public class GetPackingFlowChartPacksRequest extends GetRequest<GetPackingFlowChartPacksResult>{
	
	private LongFilter idFilter;
	private LongFilter packingFlowChartFilter;
	
	@Override
    public GetPackingFlowChartPacksResult createResult(){
        return new GetPackingFlowChartPacksResult(this);
    }
	
	public LongFilter getIdFilter(){
        return idFilter;
    }

    public void setIdFilter(LongFilter idFilter){
        this.idFilter = idFilter;
    }
	
	public LongFilter getPackingFlowChartFilter(){
        return packingFlowChartFilter;
    }

    public void setPackingFlowChartFilter(LongFilter packingFlowChartFilter){
        this.packingFlowChartFilter = packingFlowChartFilter;
    }
}
