package org.jbalance.directory.core.query.helper.norm;

import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.query.GetRequest;

/**
 *
 * @author Mihail
 */
public class GetNormGroupRequest extends GetRequest<GetNormGroupResult>{
    private static final long serialVersionUID = 1L;

    private LongFilter idFilter;

    @Override
    public GetNormGroupResult createResult()
    {
        return new GetNormGroupResult(this);
    }

    public LongFilter getIdFilter() {
        return idFilter;
    }

    public void setIdFilter(LongFilter idFilter) {
        this.idFilter = idFilter;
    }
}
