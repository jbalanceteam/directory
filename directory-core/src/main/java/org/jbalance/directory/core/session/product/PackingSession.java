package org.jbalance.directory.core.session.product;

import org.jbalance.directory.core.model.product.Packing;
import org.jbalance.directory.core.query.helper.product.GetPackingsRequest;
import org.jbalance.directory.core.session.GenericSession;

/**
 *
 * @author Mihail
 */
public interface PackingSession  extends GenericSession<Packing, GetPackingsRequest>{
	
}
