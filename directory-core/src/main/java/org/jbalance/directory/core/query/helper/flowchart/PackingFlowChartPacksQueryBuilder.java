package org.jbalance.directory.core.query.helper.flowchart;

import org.jbalance.directory.core.model.flowchart.PackingFlowChartPacks;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;

/**
 *
 * @author Mihail
 */
public class PackingFlowChartPacksQueryBuilder extends AbstractQueryBuilder<PackingFlowChartPacksQueryBuilder>{
    public PackingFlowChartPacksQueryBuilder()
    {
        super(null, PackingFlowChartPacks.class, "packingFlowChartPacks");
    }


    @Override
    public StringBuilder buildJoins()
    {
        StringBuilder joins = new StringBuilder();
        return joins;
    }

    @Override
    protected PackingFlowChartPacksQueryBuilder get()
    {
        return this;
    }
	
}
