package org.jbalance.directory.core.query.helper.norm;

import org.jbalance.directory.core.model.norm.Stay;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;

/**
 *
 * @author Mihail
 */
public class StayQueryBuilder extends AbstractQueryBuilder<StayQueryBuilder>{

    public StayQueryBuilder() {
        super(null, Stay.class, "stay");
    }
    
    @Override
    protected StayQueryBuilder get() {
        return this;
    }
    
}
