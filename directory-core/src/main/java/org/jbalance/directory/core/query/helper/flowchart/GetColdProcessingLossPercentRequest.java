package org.jbalance.directory.core.query.helper.flowchart;

import org.jbalance.directory.core.filter.DateFilter;
import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.model.flowchart.ColdProcessingLossPercent;
import org.jbalance.directory.core.query.GetRequest;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class GetColdProcessingLossPercentRequest extends GetRequest<GetColdProcessingLossPercentResult>
{

    private static final long serialVersionUID = 1L;

    private LongFilter idFilter;

    public LongFilter getIdFilter()
    {
        return idFilter;
    }

    public void setIdFilter(LongFilter idFilter)
    {
        this.idFilter = idFilter;
    }

    public DateFilter getModifiedFilter()
    {
        return modifiedFilter;
    }

    public void setModifiedFilter(DateFilter modifiedFilter)
    {
        this.modifiedFilter = modifiedFilter;
    }
    private DateFilter modifiedFilter;

    @Override
    public GetColdProcessingLossPercentResult createResult()
    {
        return new GetColdProcessingLossPercentResult(this);
    }

}
