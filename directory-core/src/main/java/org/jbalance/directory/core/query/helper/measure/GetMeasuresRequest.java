package org.jbalance.directory.core.query.helper.measure;

import javax.xml.bind.annotation.XmlType;
import org.jbalance.directory.core.filter.DateFilter;
import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.query.GetRequest;


@XmlType(propOrder ={
    "idFilter",
    "measureNameFilter",
    "groupFilter",
    "modifiedFilter"
})
public class GetMeasuresRequest extends GetRequest<GetMeasuresResult>
{

    private static final long serialVersionUID = 1L;

    private LongFilter idFilter;
    private StringFilter measureNameFilter;
	private StringFilter groupFilter;
	private DateFilter modifiedFilter;
	
    @Override
    public GetMeasuresResult createResult()
    {
        return new GetMeasuresResult(this);
    }

    public LongFilter getIdFilter()
    {
        return idFilter;
    }

    public void setIdFilter(LongFilter idFilter)
    {
        this.idFilter = idFilter;
    }

    public StringFilter getMeasureNameFilter() {
        return measureNameFilter;
    }

    public void setMeasureNameFilter(StringFilter measureNameFilter) {
        this.measureNameFilter = measureNameFilter;
    }
	
	public StringFilter getGroupFilter()
    {
        return groupFilter;
    }

    public void setGroupFilter(StringFilter groupFilter)
    {
        this.groupFilter = groupFilter;
    }
	

    public DateFilter getModifiedFilter()
    {
        return modifiedFilter;
    }

    public void setModifiedFilter(DateFilter modifiedFilter)
    {
        this.modifiedFilter = modifiedFilter;
    }
}
