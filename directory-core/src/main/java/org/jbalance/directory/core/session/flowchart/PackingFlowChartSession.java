package org.jbalance.directory.core.session.flowchart;

import org.jbalance.directory.core.model.flowchart.PackingFlowChart;
import org.jbalance.directory.core.query.helper.flowchart.GetPackingFlowChartRequest;
import org.jbalance.directory.core.session.GenericSession;

/**
 *
 * @author Mihail
 */
public interface PackingFlowChartSession extends GenericSession<PackingFlowChart, GetPackingFlowChartRequest>{
	
}
