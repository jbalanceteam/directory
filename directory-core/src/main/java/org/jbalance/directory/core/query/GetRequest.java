package org.jbalance.directory.core.query;

import java.util.Date;
import javax.xml.bind.annotation.XmlElement;
import org.jbalance.directory.core.filter.DateFilter;
import org.jbalance.directory.core.filter.DateFilterType;

/**
 * <p>
 * The base GetRequest that provides fields common to all GetRequests.
 * </p>
 *
 * <p>
 * If createdDateFilter, deletedDatefilter, and effectiveDate are all null, then
 * effectiveDate is initialized to new Date(). This is the only case where
 * effectiveDate is initialized for the caller.
 * </p>
 *
 * <p>
 * Setting effectiveDate results in these filters being applied:
 * <ul>
 * <li>add createdDateFilter of type Da (less than or equal) with value
 * effectiveDate.</li>
 * <li>add deletedDateFilter of type GTN (greater than or null) with value
 * effectiveDate.</li>
 * </ul>
 * </p>
 *
 * <p>
 * This equates to
 * <code>createdDate &lt;= effectiveDate and (deletedDate &gt; effectiveDate or deletedDate is null)</code>
 * in the where clause.
 * </p>
 *
 * <p>
 * If the caller specifies a createDateFilter or a deletedDateFilter and no
 * effectiveDate, then no effectiveDate filters are applied.
 * </p>
 * <p>
 * If the caller specifies an effectiveDate, a deletedDateFilter, and a
 * createdDateFilter, then all 4 filters are applied.
 * </p>
 *
 * <p>
 * If uuid is not empty it will be ANDed to the filter chain. uuid is useful for
 * external/public access apps that need to retrieve a specific record while
 * hiding the numeric ids that may be sequential (security risk)
 * </p>
 *
 * @param <R> the type of GetResult this request can create
 */
public abstract class GetRequest<R extends GetResult> extends Request<R>
{

    private static final long serialVersionUID = 1L;
    private Integer firstRecord;
    private Integer maxRecords;
    private Date effectiveDate;
    private DateFilter createdDateFilter;
    private DateFilter deletedDateFilter = new DateFilter(DateFilterType.NULL);
    private Long knownTotalRecordCount;
    private String uuid;

    private boolean countOnly;

//    private LongFilter idFilter;
//    private StringFilter nameFilter;
//    private DateFilter modifiedFilter;
//
//    public LongFilter getIdFilter()
//    {
//        return idFilter;
//    }
//
//    public void setIdFilter(LongFilter idFilter)
//    {
//        this.idFilter = idFilter;
//    }
//
//    public StringFilter getNameFilter()
//    {
//        return nameFilter;
//    }
//
//    public void setNameFilter(StringFilter nameFilter)
//    {
//        this.nameFilter = nameFilter;
//    }
//
//    public DateFilter getModifiedFilter()
//    {
//        return modifiedFilter;
//    }
//
//    public void setModifiedFilter(DateFilter modifiedFilter)
//    {
//        this.modifiedFilter = modifiedFilter;
//    }
    public Integer getMaxRecords()
    {
        return maxRecords;
    }

    public void setMaxRecords(Integer maxRecords)
    {
        if (maxRecords != null && maxRecords <= 0)
        {
            throw new IllegalArgumentException("Max records must be a positive integer");
        }
        this.maxRecords = maxRecords;
    }

    public void setFirstRecord(Integer firstRecord)
    {
        if (firstRecord != null && firstRecord < 0)
        {
            throw new IllegalArgumentException("First record must be nonnegative");
        }
        this.firstRecord = firstRecord;
    }

    public Integer getFirstRecord()
    {
        return firstRecord;
    }

    public DateFilter getCreatedDateFilter()
    {
        return createdDateFilter;
    }

    public void setCreatedDateFilter(DateFilter createdDateFilter)
    {
        this.createdDateFilter = createdDateFilter;
    }

    public Date getEffectiveDate()
    {
        return effectiveDate;
    }

    /**
     * EffectiveDate defaults to <code>new Date()</code> unless
     * createdDateFilter is set. Setting effectiveDate causes a
     * createdDateFilter to be created of type {@link DateFilterType#LTE} and a
     * deletedDateFilter of type {@link DateFilterType#GTN}.
     */
    public void setEffectiveDate(Date effectiveDate)
    {
        this.effectiveDate = effectiveDate;
    }

    public GetRequest()
    {
    }

    public GetRequest(GetRequest<?> template)
    {
        this.createdDateFilter = template.getCreatedDateFilter();
        this.effectiveDate = template.getEffectiveDate();
        this.firstRecord = template.getFirstRecord();
        this.maxRecords = template.getMaxRecords();
        this.uuid = template.getUuid();
        this.deletedDateFilter = template.getDeletedDateFilter();
    }

    @XmlElement(required = false)
    public boolean isCountOnly()
    {
        return countOnly;
    }

    public void setCountOnly(boolean countOnly)
    {
        this.countOnly = countOnly;
    }

    public String getUuid()
    {
        return uuid;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public Long getKnownTotalRecordCount()
    {
        return knownTotalRecordCount;
    }

    public void setKnownTotalRecordCount(Long knownTotalRecordCount)
    {
        this.knownTotalRecordCount = knownTotalRecordCount;
    }

    public DateFilter getDeletedDateFilter()
    {
        return deletedDateFilter;
    }

    public void setDeletedDateFilter(DateFilter deletedDateFilter)
    {
        this.deletedDateFilter = deletedDateFilter;
    }
}
