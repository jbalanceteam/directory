package org.jbalance.directory.core.query.helper.norm;

import java.util.List;

import javax.persistence.EntityManager;

import org.jbalance.directory.core.model.norm.AgeBracket;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;
import org.jbalance.directory.core.query.helper.GetRequestHelper;
import org.jbalance.directory.core.util.CollectionUtil;

/**
 *
 * @author Mihail
 */
public class GetAgeBracketRequestHelper extends GetRequestHelper<GetAgeBracketRequest, GetAgeBracketResult>{

    public GetAgeBracketRequestHelper(GetAgeBracketRequest request) {
        super(request);
    }

    @Override
    protected AbstractQueryBuilder<?> initQuery() {
        AgeBracketQueryBuilder аgeBracketQueryBuilder = new AgeBracketQueryBuilder();
        аgeBracketQueryBuilder.appendFilter(request.getIdFilter(), "ageBracket.id");
        аgeBracketQueryBuilder.orderBy().append("order by ageBracket.enteredDate desc");
        return аgeBracketQueryBuilder;
    }


    @Override
    protected GetAgeBracketResult createResult(GetAgeBracketResult result, List<?> records, EntityManager em) {
        @SuppressWarnings("unchecked")
		List<AgeBracket> аgeBrackets = (List<AgeBracket>) records;
        аgeBrackets = CollectionUtil.filterDuplicates(аgeBrackets);
        result.setAgeBrackets(аgeBrackets);
        return result;
    }

}
