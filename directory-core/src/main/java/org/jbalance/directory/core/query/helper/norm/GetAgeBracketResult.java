package org.jbalance.directory.core.query.helper.norm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import org.jbalance.directory.core.model.norm.AgeBracket;
import org.jbalance.directory.core.query.GetResult;

/**
 *
 * @author Mihail
 */
public class GetAgeBracketResult extends GetResult{
    private static final long serialVersionUID = 1L;

    private List<AgeBracket> ageBrackets;

    public GetAgeBracketResult()
    {
    }

    @XmlElement(name = "ageBrackets")
    public List<AgeBracket> getAgeBrackets()
    {
        if (ageBrackets == null)
        {
            ageBrackets = new ArrayList<AgeBracket>();
        }
        return ageBrackets;
    }

    public void setAgeBrackets(List<AgeBracket> ageBrackets)
    {
        this.ageBrackets = ageBrackets;
    }

    public GetAgeBracketResult(GetAgeBracketRequest request)
    {
        super(request);
    }
}