package org.jbalance.directory.core.session.flowchart;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import org.jbalance.directory.core.model.flowchart.PackingFlowChartPacks;
import org.jbalance.directory.core.query.helper.flowchart.GetPackingFlowChartPacksRequest;
import org.jbalance.directory.core.query.helper.flowchart.GetPackingFlowChartPacksRequestHelper;
import org.jbalance.directory.core.query.helper.flowchart.GetPackingFlowChartPacksResult;
import org.jbalance.directory.core.session.GenericSessionBean;

/**
 *
 * @author Mihail
 */
@Stateless
public class PackingFlowChartPacksSessionBean extends GenericSessionBean<PackingFlowChartPacks, GetPackingFlowChartPacksRequest> implements PackingFlowChartPacksSession {

    @Override
    public List<PackingFlowChartPacks> list(GetPackingFlowChartPacksRequest request) {
        
        GetPackingFlowChartPacksResult result = new GetPackingFlowChartPacksRequestHelper(request).createResult(manager);
                
        return result.getPackingFlowChartPacksList();
    }

    @Override
    @PostConstruct
    public void init() {
        beanClass = PackingFlowChartPacks.class;
    }
}
