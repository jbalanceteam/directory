package org.jbalance.directory.core.query.helper.flowchart;

import javax.xml.bind.annotation.XmlType;
import org.jbalance.directory.core.filter.DateFilter;
import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.query.GetRequest;


@XmlType(propOrder ={
    "idFilter",
    "flowChartImageNameFilter",
    "flowChartImageOkpFilter",
    "modifiedFilter"
})
public class GetFlowChartImagesRequest extends GetRequest<GetFlowChartImagesResult>
{

    private static final long serialVersionUID = 1L;

    private LongFilter idFilter;
    private StringFilter flowChartImageNameFilter;
    private StringFilter flowChartImageOkpFilter;
    private DateFilter modifiedFilter;

    @Override
    public GetFlowChartImagesResult createResult()
    {
        return new GetFlowChartImagesResult(this);
    }

    public LongFilter getIdFilter()
    {
        return idFilter;
    }

    public void setIdFilter(LongFilter idFilter)
    {
        this.idFilter = idFilter;
    }

    public StringFilter getFlowChartImageNameFilter() {
        return flowChartImageNameFilter;
    }

    public void setFlowChartImageNameFilter(StringFilter flowChartImageNameFilter) {
        this.flowChartImageNameFilter = flowChartImageNameFilter;
    }

    public StringFilter getFlowChartImageOkpFilter() {
        return flowChartImageOkpFilter;
    }

    public void setFlowChartImageOkpFilter(StringFilter flowChartImageOkpFilter) {
        this.flowChartImageOkpFilter = flowChartImageOkpFilter;
    }

    public DateFilter getModifiedFilter()
    {
        return modifiedFilter;
    }

    public void setModifiedFilter(DateFilter modifiedFilter)
    {
        this.modifiedFilter = modifiedFilter;
    }
}
