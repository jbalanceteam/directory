package org.jbalance.directory.core.query.helper.product;

import java.util.List;
import javax.persistence.EntityManager;
import org.jbalance.directory.core.model.product.ProductImage;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;
import org.jbalance.directory.core.query.helper.GetRequestHelper;
import org.jbalance.directory.core.util.CollectionUtil;

/**
 * @author Komov Roman
 */
public class GetProductImagesRequestHelper
        extends GetRequestHelper<GetProductImagesRequest, GetProductImagesResult>
{

    public GetProductImagesRequestHelper(GetProductImagesRequest request)
    {
        super(request);
    }

    @Override
    protected AbstractQueryBuilder<?> initQuery()
    {
        ProductImageQueryBuilder productImageQueryBuilder = new ProductImageQueryBuilder();
        productImageQueryBuilder.appendFilter(request.getIdFilter(), "productimage.id");
        productImageQueryBuilder.appendFilter(request.getProductImageOkpFilter(), "productimage.okp");        
        productImageQueryBuilder.appendFilter(request.getNameFilter(), "productimage.name");
        
        productImageQueryBuilder.orderBy().append("order by productimage.enteredDate desc");
        return productImageQueryBuilder;
    }

    @Override
    protected GetProductImagesResult createResult(GetProductImagesResult result, List<?> records, EntityManager em)
    {
        List<ProductImage> productImages = (List<ProductImage>) records;
        productImages = CollectionUtil.filterDuplicates(productImages);
        result.setProductImages(productImages);
        return result;
    }
}
