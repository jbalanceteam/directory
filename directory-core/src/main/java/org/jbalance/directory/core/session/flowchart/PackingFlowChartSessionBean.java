package org.jbalance.directory.core.session.flowchart;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import org.jbalance.directory.core.model.flowchart.PackingFlowChart;
import org.jbalance.directory.core.query.helper.flowchart.GetPackingFlowChartRequest;
import org.jbalance.directory.core.query.helper.flowchart.GetPackingFlowChartRequestHelper;
import org.jbalance.directory.core.query.helper.flowchart.GetPackingFlowChartResult;
import org.jbalance.directory.core.session.GenericSessionBean;

/**
 *
 * @author Mihail
 */
@Stateless
public class PackingFlowChartSessionBean extends GenericSessionBean<PackingFlowChart, GetPackingFlowChartRequest> implements PackingFlowChartSession {

    @Override
    public List<PackingFlowChart> list(GetPackingFlowChartRequest request) {
        
        GetPackingFlowChartResult result = new GetPackingFlowChartRequestHelper(request).createResult(manager);
                
        return result.getPackingFlowCharts();
    }

    @Override
    @PostConstruct
    public void init() {
        beanClass = PackingFlowChart.class;
    }
}
