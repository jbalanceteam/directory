package org.jbalance.directory.core.query.helper.measure;

import org.jbalance.directory.core.model.measure.Measure;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class MeasureQueryBuilder extends AbstractQueryBuilder<MeasureQueryBuilder>{
    public MeasureQueryBuilder()
    {
        super(null, Measure.class, "measure");
    }


    @Override
    public StringBuilder buildJoins()
    {
        StringBuilder joins = new StringBuilder();
        return joins;
    }

    @Override
    protected MeasureQueryBuilder get()
    {
        return this;
    }
}