package org.jbalance.directory.core.filter;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;

public class BooleanFilter extends Filter<BooleanFilterType, Boolean> {
    private static final long serialVersionUID = 1L;

    private BooleanFilterType filterType;
    private boolean filterValue;

    public BooleanFilter() {}

    public BooleanFilter(BooleanFilterType filterType, List<Boolean> filterValues) {
        setFilterType(filterType);
        setFilterValues(filterValues);
    }

    public BooleanFilter(BooleanFilterType filterType, boolean filterValue) {
        setFilterType(filterType);
        setFilterValue(filterValue);
    }

    @XmlAttribute(required=true)
    @Override
    public BooleanFilterType getFilterType() {
        return filterType;
    }

    @Override
    public void setFilterType(BooleanFilterType filterType) {
        this.filterType = filterType;
    }

    public boolean isFilterValue() {
        return filterValue;
    }

    public void setFilterValue(boolean filterValue) {
        this.filterValue = filterValue;
    }

    @XmlTransient
    @Override
    public List<Boolean> getFilterValues() {
        List<Boolean> filterValues = new ArrayList<Boolean>();
        filterValues.add(filterValue);
        return filterValues;
    }

    @Override
    public void setFilterValues(List<Boolean> filterValues) {
        if(filterValues != null && !filterValues.isEmpty()) {
            filterValue = filterValues.get(0);
        } else {
            filterValue = false;
        }
    }
}
