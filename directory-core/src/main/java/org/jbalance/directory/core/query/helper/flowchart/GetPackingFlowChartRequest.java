package org.jbalance.directory.core.query.helper.flowchart;

import javax.xml.bind.annotation.XmlType;
import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.query.GetRequest;

/**
 *
 * @author Mihail
 */
@XmlType(propOrder ={
    "idFilter",
    "packingFilter",
    "imageFilter",
    "nameFilter"
})
public class GetPackingFlowChartRequest extends GetRequest<GetPackingFlowChartResult>{
	
	private LongFilter idFilter;
	private LongFilter packingFilter;
	private LongFilter imageFilter;
	private StringFilter nameFilter;

	@Override
    public GetPackingFlowChartResult createResult(){
        return new GetPackingFlowChartResult(this);
    }
	
	public LongFilter getIdFilter(){
        return idFilter;
    }

    public void setIdFilter(LongFilter idFilter){
        this.idFilter = idFilter;
    }
	
	public LongFilter getPackingFilter(){
        return packingFilter;
    }

    public void setPackingFilter(LongFilter packingFilter){
        this.packingFilter = packingFilter;
    }

    public StringFilter getNameFilter() {
        return nameFilter;
    }

    public void setNameFilter(StringFilter nameFilter) {
        this.nameFilter = nameFilter;
    }

    public LongFilter getImageFilter() {
        return imageFilter;
    }

    public void setImageFilter(LongFilter imageFilter) {
        this.imageFilter = imageFilter;
    }
}
