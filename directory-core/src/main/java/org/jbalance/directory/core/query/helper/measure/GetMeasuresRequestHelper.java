package org.jbalance.directory.core.query.helper.measure;

import java.util.List;
import javax.persistence.EntityManager;
import org.jbalance.directory.core.model.measure.Measure;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;
import org.jbalance.directory.core.query.helper.GetRequestHelper;
import org.jbalance.directory.core.util.CollectionUtil;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class GetMeasuresRequestHelper extends GetRequestHelper<GetMeasuresRequest, GetMeasuresResult>
{

    public GetMeasuresRequestHelper(GetMeasuresRequest request)
    {
        super(request);
    }

    @Override
    protected AbstractQueryBuilder<?> initQuery()
    {
        MeasureQueryBuilder measureQueryBuilder = new MeasureQueryBuilder();
        measureQueryBuilder.appendFilter(request.getIdFilter(), "measure.id");      
        measureQueryBuilder.appendFilter(request.getMeasureNameFilter(), "measure.name");
        measureQueryBuilder.appendFilter(request.getGroupFilter(), "measure.measureGroup.name");
        measureQueryBuilder.appendFilter(request.getModifiedFilter(), "measure.modifiedDate");
        measureQueryBuilder.orderBy().append("order by measure.measureGroup.name asc");
        return measureQueryBuilder;
    }

    @Override
    protected GetMeasuresResult createResult(GetMeasuresResult result, List<?> records, EntityManager em)
    {
        List<Measure> l = (List<Measure>) records;
        l = CollectionUtil.filterDuplicates(l);
        result.getMeasures().addAll(l);
        return result;
    }
}