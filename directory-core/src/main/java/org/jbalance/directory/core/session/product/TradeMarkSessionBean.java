package org.jbalance.directory.core.session.product;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import org.jbalance.directory.core.model.product.TradeMark;
import org.jbalance.directory.core.query.helper.product.GetTradeMarkRequest;
import org.jbalance.directory.core.session.GenericSessionBean;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@Stateless
public class TradeMarkSessionBean extends GenericSessionBean<TradeMark, GetTradeMarkRequest>
{

    @Override
    public List<TradeMark> list(GetTradeMarkRequest request)
    {
        //TODO: Implement!
        return null;
    }

    @Override
    @PostConstruct
    public void init() {
        beanClass = TradeMark.class;
    }
}
