package org.jbalance.directory.core.session.juridical;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import org.jbalance.directory.core.model.Juridical;
import org.jbalance.directory.core.query.helper.juridical.GetJuridicalsRequest;
import org.jbalance.directory.core.query.helper.juridical.GetJuridicalsRequestHelper;
import org.jbalance.directory.core.query.helper.juridical.GetJuridicalsResult;
import org.jbalance.directory.core.session.GenericSessionBean;

/**
 *
 * @author Petr Aleksandrov apv@jbalance.org
 */
@Stateless
public class JuridicalSessionBean extends GenericSessionBean<Juridical, GetJuridicalsRequest>
        implements JuridicalSession, Serializable{

    @Override
    public List<Juridical> list(GetJuridicalsRequest request) {
        GetJuridicalsResult result = new GetJuridicalsRequestHelper(request).createResult(manager);
        return result.getJuridicals();
    }

    @Override
    @PostConstruct
    public void init() {
        beanClass = Juridical.class;
    }
}
