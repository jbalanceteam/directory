package org.jbalance.directory.core.session.norm;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import org.jbalance.directory.core.model.norm.NormGroup;
import org.jbalance.directory.core.query.helper.norm.GetNormGroupRequestHelper;
import org.jbalance.directory.core.query.helper.norm.GetNormGroupResult;
import org.jbalance.directory.core.query.helper.norm.GetNormGroupRequest;
import org.jbalance.directory.core.session.GenericSessionBean;

/**
 *
 * @author Mihail
 */
@Stateless
public class NormGroupSessionBean extends GenericSessionBean <NormGroup, GetNormGroupRequest> implements NormGroupSession{
    
    @Override
    public List<NormGroup> list(GetNormGroupRequest request)
    {
        GetNormGroupResult result = new GetNormGroupRequestHelper(request).createResult(manager);
        return result.getNormGroups();
    }

    @Override
    @PostConstruct
    public void init() {
        beanClass = NormGroup.class;
    }
}
