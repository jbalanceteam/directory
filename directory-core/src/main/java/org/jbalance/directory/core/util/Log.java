package org.jbalance.directory.core.util;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Proprietary logging utility class patterned after the Seam logger that is
 * designed to:
 * </p>
 * <ul>
 * <li>Reduce the amount of code needed to log messages by eliminating the
 * {@code ifXXXEnabled} checks while maintaining the performance advantage of
 * performing the check before performing string concatenation</li>
 * <li>Enable and enforce resource bundle-based log messages</li>
 * <li>Enable the use of both {@code java.text.MessageFormat} and {@code
 * String.format} patterns in log messages, whether they be in raw message
 * pattern strings or pattern strings defined in resource bundles.</li>
 * <li>Provide a Seam-like logging environment without imposing a Seam
 * dependency in the core and DAO level.</li>
 * </ul>
 * <p>
 * All methods accept an argument called {@code messagePattern} that can be one
 * of two things:
 * </p>
 * <ul>
 * <li>A resource bundle key; or</li>
 * <li>A raw message pattern that can contain a mixture of {@code String.format}
 * and {@code java.text.MessageFormat} markup.</li>
 * </ul>
 * <p>
 * When a log method is invoked, the very first thing that is done is a check to
 * see if the specified log level is enabled. If not, the call returns without
 * any further processing.
 * </p>
 * <p>
 * If the specified log leve is enabled, a check is done to see if there is a
 * key in the resource bundle(s) matching the specified {@code messagePattern}
 * string exactly. If so, the {@code messagePattern} string is replaced with the
 * resource corresponding to that key in the bundle. If not, the {@code
 * messagePattern} string is taken to be a raw format string.
 * </p>
 * <p>
 * Next, the {@code messagePattern} string and the specified arguments array are
 * passed through
 * {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[])}
 * The resulting string and the argument array are then passed through
 * {@link java.lang.String#format(java.lang.String, java.lang.Object[])} and the
 * result is written to the log.
 * </p>
 * 
 */
public class Log implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final String CLASS_NAME = Log.class.getName();
    private Logger log;

    static final String THROWING_MESSAGE = "{0} throwing";
    static final String CATCHING_MESSAGE = "{0} catching";
    
    static final String EXIT_MESSAGE_0 = "exit {0}";
    static final String EXIT_MESSAGE_1 = "exit {0} with {1}";
    
    static final String ENTRY_MESSAGE_0 = "enter {0}";
    static final String ENTRY_MESSAGE_1 = "enter {0}({1})";
    static final String ENTRY_MESSAGE_2 = "enter {0}({1}, {2})";
    static final String ENTRY_MESSAGE_3 = "enter {0}({1}, {2}, {3})";
    static final String ENTRY_MESSAGE_4 = "enter {0}({1}, {2}, {3}, {4})";
    static int ENTRY_MESSAGE_ARRAY_LEN = 5;
    static String[] ENTRY_MESSAGE_ARRAY = new String[ENTRY_MESSAGE_ARRAY_LEN];
    static {
      ENTRY_MESSAGE_ARRAY[0] = ENTRY_MESSAGE_0;
      ENTRY_MESSAGE_ARRAY[1] = ENTRY_MESSAGE_1;
      ENTRY_MESSAGE_ARRAY[2] = ENTRY_MESSAGE_2;
      ENTRY_MESSAGE_ARRAY[3] = ENTRY_MESSAGE_3;
      ENTRY_MESSAGE_ARRAY[4] = ENTRY_MESSAGE_4;
    }
    
    /**
     * Instantiates a new log.
     * 
     * @param category
     *            the category
     */
    public Log(String category) {
        if (Str.empty(category)) {
            throw new IllegalArgumentException("Category is required");
        }
        log = LoggerFactory.getLogger(category);
    }

    /**
     * Instantiates a new log.
     * 
     * @param clazz
     *            the clazz
     */
    public Log(Class<?> clazz) {
        if (Str.empty(clazz)) {
            throw new IllegalArgumentException("Class is required");
        }
        log = LoggerFactory.getLogger(clazz);
    }

    /**
     * <p>
     * Format and log a message at the {@code TRACE} level if trace level is
     * enabled.
     * </p>
     * 
     * @param messagePattern
     *            A string consisting of either a resource bundle key or a
     *            message pattern consisting of
     *            {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     *            and/or
     *            {@link String#format(java.lang.String, java.lang.Object[]) }
     *            markup
     * @param arguments
     *            Substitution values passed in to
     *            {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     *            and
     *            {@link String#format(java.lang.String, java.lang.Object[]) }
     *            when generating the final log message
     */
    public void trace(String messagePattern, Object... arguments) {
        log(Level.TRACE, messagePattern, (Throwable) null, arguments);
    }

    /**
     * <p>
     * Format and log a message at the {@code TRACE} level if trace level is
     * enabled.
     * </p>
     * 
     * @param messagePattern
     *            A string consisting of either a resource bundle key or a
     *            message pattern consisting of
     *            {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     *            and/or
     *            {@link String#format(java.lang.String, java.lang.Object[]) }
     *            markup
     * @param throwable
     *            The exception/stack trace to log with this message
     * @param arguments
     *            Substitution values passed in to
     *            {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     *            and
     *            {@link String#format(java.lang.String, java.lang.Object[]) }
     *            when generating the final log message
     */
    public void trace(String messagePattern, Throwable throwable, Object... arguments) {
        log(Level.TRACE, messagePattern, throwable, arguments);
    }

    /**
     * <p>
     * Log a {@link Throwable} at the {@code TRACE} level if trace level is
     * enabled.
     * </p>
     * 
     * @param throwable
     */
    public void trace(Throwable throwable) {
        log(Level.TRACE, null, throwable);
    }

    /**
     * <p>
     * Format and log a message at the {@code DEBUG} level if debug level is
     * enabled.
     * </p>
     * 
     * @param messagePattern
     *            A string consisting of either a resource bundle key or a
     *            message pattern consisting of
     *            {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     *            and/or
     *            {@link String#format(java.lang.String, java.lang.Object[]) }
     *            markup
     * @param arguments
     *            Substitution values passed in to
     *            {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     *            and
     *            {@link String#format(java.lang.String, java.lang.Object[]) }
     *            when generating the final log message
     */
    public void debug(String messagePattern, Object... arguments) {
        log(Level.DEBUG, messagePattern, (Throwable) null, arguments);
    }

    /**
     * <p>
     * Format and log a message at the {@code DEBUG} level if debug level is
     * enabled.
     * </p>
     * 
     * @param messagePattern
     *            A string consisting of either a resource bundle key or a
     *            message pattern consisting of
     *            {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     *            and/or
     *            {@link String#format(java.lang.String, java.lang.Object[]) }
     *            markup
     * @param throwable
     *            The exception/stack trace to log with this message
     * @param arguments
     *            Substitution values passed in to
     *            {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     *            and
     *            {@link String#format(java.lang.String, java.lang.Object[]) }
     *            when generating the final log message
     */
    public void debug(String messagePattern, Throwable throwable, Object... arguments) {
        log(Level.DEBUG, messagePattern, throwable, arguments);
    }

    /**
     * <p>
     * Log a {@link Throwable} at the {@code DEBUG} level if debug level is
     * enabled.
     * </p>
     * 
     * @param throwable
     */
    public void debug(Throwable throwable) {
        log(Level.DEBUG, null, throwable);
    }

    /**
     * <p>
     * Format and log a message at the {@code INFO} level if info level is
     * enabled.
     * </p>
     * 
     * @param messagePattern
     *            A string consisting of either a resource bundle key or a
     *            message pattern consisting of
     *            {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     *            and/or
     *            {@link String#format(java.lang.String, java.lang.Object[]) }
     *            markup
     * @param arguments
     *            Substitution values passed in to
     *            {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     *            and
     *            {@link String#format(java.lang.String, java.lang.Object[]) }
     *            when generating the final log message
     */
    public void info(String messagePattern, Object... arguments) {
        log(Level.INFO, messagePattern, (Throwable) null, arguments);
    }

    /**
     * <p>
     * Format and log a message at the {@code INFO} level if info level is
     * enabled.
     * </p>
     * 
     * @param messagePattern
     *            A string consisting of either a resource bundle key or a
     *            message pattern consisting of
     *            {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     *            and/or
     *            {@link String#format(java.lang.String, java.lang.Object[]) }
     *            markup
     * @param throwable
     *            The exception/stack trace to log with this message
     * @param arguments
     *            Substitution values passed in to
     *            {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     *            and
     *            {@link String#format(java.lang.String, java.lang.Object[]) }
     *            when generating the final log message
     */
    public void info(String messagePattern, Throwable throwable, Object... arguments) {
        log(Level.INFO, messagePattern, throwable, arguments);
    }

    /**
     * <p>
     * Log a {@link Throwable} at the {@code INFO} level if info level is
     * enabled.
     * </p>
     * 
     * @param throwable
     */
    public void info(Throwable throwable) {
        log(Level.INFO, null, throwable);
    }

    /**
     * <p>
     * Format and log a message at the {@code WARN} level if warn level is
     * enabled.
     * </p>
     * 
     * @param messagePattern
     *            A string consisting of either a resource bundle key or a
     *            message pattern consisting of
     *            {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     *            and/or
     *            {@link String#format(java.lang.String, java.lang.Object[]) }
     *            markup
     * @param arguments
     *            Substitution values passed in to
     *            {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     *            and
     *            {@link String#format(java.lang.String, java.lang.Object[]) }
     *            when generating the final log message
     */
    public void warn(String messagePattern, Object... arguments) {
        log(Level.WARN, messagePattern, (Throwable) null, arguments);
    }

    /**
     * <p>
     * Format and log a message at the {@code WARN} level if warn level is
     * enabled.
     * </p>
     * 
     * @param messagePattern
     *            A string consisting of either a resource bundle key or a
     *            message pattern consisting of
     *            {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     *            and/or
     *            {@link String#format(java.lang.String, java.lang.Object[]) }
     *            markup
     * @param throwable
     *            The exception/stack trace to log with this message
     * @param arguments
     *            Substitution values passed in to
     *            {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     *            and
     *            {@link String#format(java.lang.String, java.lang.Object[]) }
     *            when generating the final log message
     */
    public void warn(String messagePattern, Throwable throwable, Object... arguments) {
        log(Level.WARN, messagePattern, throwable, arguments);
    }

    /**
     * <p>
     * Log a {@link Throwable} at the {@code WARN} level if warn level is
     * enabled.
     * </p>
     * 
     * @param throwable
     */
    public void warn(Throwable throwable) {
        log(Level.WARN, null, throwable);
    }

    /**
     * <p>
     * Format and log a message at the {@code ERROR} level if error level is
     * enabled.
     * </p>
     * 
     * @param messagePattern
     *            A string consisting of either a resource bundle key or a
     *            message pattern consisting of
     *            {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     *            and/or
     *            {@link String#format(java.lang.String, java.lang.Object[]) }
     *            markup
     * @param arguments
     *            Substitution values passed in to
     *            {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     *            and
     *            {@link String#format(java.lang.String, java.lang.Object[]) }
     *            when generating the final log message
     */
    public void error(String messagePattern, Object... arguments) {
        log(Level.ERROR, messagePattern, (Throwable) null, arguments);
    }

    /**
     * <p>
     * Format and log a message at the {@code ERROR} level if error level is
     * enabled.
     * </p>
     * 
     * @param messagePattern
     *            A string consisting of either a resource bundle key or a
     *            message pattern consisting of
     *            {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     *            and/or
     *            {@link String#format(java.lang.String, java.lang.Object[]) }
     *            markup
     * @param throwable
     *            The exception/stack trace to log with this message
     * @param arguments
     *            Substitution values passed in to
     *            {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     *            and
     *            {@link String#format(java.lang.String, java.lang.Object[]) }
     *            when generating the final log message
     */
    public void error(String messagePattern, Throwable throwable, Object... arguments) {
        log(Level.ERROR, messagePattern, throwable, arguments);
    }

    /**
     * <p>
     * Log a {@link Throwable} at the {@code ERROR} level if error level is
     * enabled.
     * </p>
     * 
     * @param throwable
     */
    public void error(Throwable throwable) {
        log(Level.ERROR, null, throwable);
    }

    /**
     * Checks if is enabled.
     * 
     * @param level
     *            the level
     * @return true, if is enabled
     */
    public boolean isEnabled(Level level) {
        switch (level) {
        case TRACE:
            return log.isTraceEnabled();
        case DEBUG:
            return log.isDebugEnabled();
        case INFO:
            return log.isInfoEnabled();
        case WARN:
            return log.isWarnEnabled();
        case ERROR:
            return log.isErrorEnabled();
        }
        return false;
    }

    /**
     * <p>
     * Format and log a message at the specified level if that level is enabled.
     * </p>
     * 
     * @param level
     *            The desired log level
     * @param messagePattern
     *            A string consisting of either a resource bundle key or a
     *            message pattern consisting of
     *            {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     *            and/or
     *            {@link String#format(java.lang.String, java.lang.Object[]) }
     *            markup
     * @param throwable
     *            The exception/stack trace to log with this message
     * @param arguments
     *            Substitution values passed in to
     *            {@link java.text.MessageFormat#format(java.lang.String, java.lang.Object[]) }
     *            and
     *            {@link String#format(java.lang.String, java.lang.Object[]) }
     *            when generating the final log message
     */
    public void log(Level level, String messagePattern, Throwable throwable, Object... arguments) {
        if (!isEnabled(level)) {
            return;
        }

        if(arguments!=null){
            for(int i = 0;i<arguments.length;i++){
                if(Replacement.METHOD_NAME.equals(arguments[i])){
                    arguments[i] = getMethodName(); 
                }
            }
        }

        if (messagePattern != null) {
            messagePattern = Str.format(messagePattern, arguments);
        } else if (throwable != null) {
            messagePattern = throwable.getMessage();
        }

        switch (level) {
            case TRACE:
                log.trace(messagePattern, throwable);
                break;
            case DEBUG:
                log.debug(messagePattern, throwable);
                break;
            case INFO:
                log.info(messagePattern, throwable);
                break;
            case WARN:
                log.warn(messagePattern, throwable);
                break;
            case ERROR:
                log.error(messagePattern, throwable);
                break;
        }
    }

    /**
     * Log method entry. Defaults to the TRACE level. 
     * 
     * @param argArray
     *            supplied parameters
     */
    public void entry(Object... argArray) {
      if (log.isTraceEnabled()) {
        String messagePattern = null;
        if (argArray.length < ENTRY_MESSAGE_ARRAY_LEN) {
          messagePattern = ENTRY_MESSAGE_ARRAY[argArray.length];
        } else {
          messagePattern = buildMessagePattern(argArray.length);
        }
        Object[] newArgArray = new Object[argArray.length+1];
        newArgArray[0] = Replacement.METHOD_NAME;
        for (int i=1; i < newArgArray.length; i++) {
            newArgArray[i] = argArray[i-1];
        }
        log(Level.TRACE, messagePattern, null, newArgArray);
      }
    }

    /**
     * Log method exit.  Defaults to the TRACE level. Relies on the logging
     * system to lookup the method name. To show the method name in the log,
     * alter the logging pattern if the logger supports (for log4j it's %M).
     */
    public void exit() {
      if (log.isTraceEnabled()) {
          log(Level.TRACE, EXIT_MESSAGE_0, null, Replacement.METHOD_NAME);
      }
    }

    /**
     * Log method exit. Defaults to the TRACE level.
     * 
     * @param result
     *            The result of the method being exited
     * @return Returns the {@code result} that was passed into this
     *         method to be logged
     */
    public <T> T exit(T result) {
        if (log.isTraceEnabled()) {
            log(Level.TRACE, EXIT_MESSAGE_1, null, Replacement.METHOD_NAME, result);
        }
        return result;
    }

    /**
     * Log an exception being thrown. The generated log event uses Level ERROR.
     * 
     * @param throwable
     *            the exception being caught.
     * @return Returns the {@code throwable} that was passed into this
     *         method to be logged
     */
    public <T extends Throwable> T throwing(T throwable) {
        log(Level.ERROR, THROWING_MESSAGE, throwable, Replacement.METHOD_NAME);
        return throwable;
    }

    /**
     * Log an exception being thrown allowing the log level to be specified.
     * 
     * @param level
     *            the logging level to use.
     * @param throwable
     *            the exception being caught.
     * @return Returns the {@code throwable} that was passed into this
     *         method to be logged
     */
    public <T extends Throwable> T throwing(Level level, T throwable) {
        log(level, THROWING_MESSAGE, throwable, Replacement.METHOD_NAME);
        return throwable;
    }

    /**
     * Log an exception being caught. The generated log event uses Level ERROR.
     * 
     * @param throwable
     *            the exception being caught.
     */
    public void catching(Throwable throwable) {
        log(Level.ERROR, CATCHING_MESSAGE, throwable, Replacement.METHOD_NAME);
    }

    /**
     * Log an exception being caught allowing the log level to be specified.
     * 
     * @param level
     *            the logging level to use.
     * @param throwable
     *            the exception being caught.
     */
    public void catching(Level level, Throwable throwable) {
        log(level, CATCHING_MESSAGE, throwable, Replacement.METHOD_NAME);
    }

    /**
     * Dynamically build the entry log statement. This method uses
     * MessageFormatter since it's more efficient than the string formatter.
     * 
     * @param len
     * @return
     */
    private static String buildMessagePattern(int len) {
        StringBuilder sb = new StringBuilder();
        sb.append("enter {0}(");
        for (int i = 0; i < len; i++) {
            sb.append('{').append(i+1).append('}');
            if (i != len - 1)
                sb.append(", ");
        }
        sb.append(')');
        return sb.toString();
    }
    
    /**
     * <p>
     * Returns the method name by instantiating a new exception which is 
     * cheaper than Thread.currentThread().getStackTrace() because Exception
     * already knows which thread it is in and ultimately calls 
     * new Exception().getStackTrace();
     * 
     * @return the string
     */
    private String getMethodName() {
        StackTraceElement[] elements = new RuntimeException().getStackTrace();
        // element 0 is Thread, 1 is this, 2 is a log method in this class
        for(int i = 3;i<elements.length;i++){
            // using contains because some class reloaders (JRebel)
            // add characters to the end of the class name
            if(!elements[i].getClassName().equals(CLASS_NAME)){
                return elements[i].getMethodName();
                // Could also include + ":" + elements[i].getLineNumber();
            }
        }
        return "";
    }

    /**
     * used to replace the enum with another value
     * @author jgerlach
     *
     */
    public enum Replacement {
        /** Replaces METHOD_NAME with current method name */
        METHOD_NAME;
    }
    
    /**
     * Enumerates the valid logging levels
     */
    public enum Level {
        /**
         * <p>
         * Extremely low-level and verbose information used to follow execution
         * paths and logic to troubleshoot errors when the use of a debugger is
         * not practical (i.e. in production). This information is not of any
         * immediately value on a routine basis, but can be turned on as needed
         * to facilitate troubleshooting and diagnosis of production issues.
         * </p>
         * <p>
         * Examples:
         * </p>
         * <ul>
         * <li>Method entry and exit with arguments and return values</li>
         * <li>Indication of the path taking on a conditional branch</li>
         * <li>Denoting loop iterations and counter values</li>
         * <li>Outputing the result of a method call or in-line data conversion</li>
         * <li>Indicating cache hit success/failure</li>
         * </ul>
         * <p>
         * Trace level logs are usually turned off in all environments
         * </p>
         */
        TRACE,
        /**
         * <p>
         * Low-level information that is not quite as verbose as trace level.
         * Debug log statements capture information that is known to be useful
         * but is too verbose to log on a routine basis. Examples include
         * notifications that JMS messages.
         * </p>
         * <p>
         * Examples:
         * </p>
         * <ul>
         * <li>Logging successful integration hand-offs (e.g. delivery or
         * receipt of a JMS message)</li>
         * <li>Recording defaults, overrides, or other implicit business logic
         * of a routine nature made on behalf of the user</li>
         * <li>Echoing UI validation errors</li>
         * </ul>
         * <p>
         * Debug level logs are usually enabled in non-production environments
         * and disabled in production environments.
         * </p>
         */
        DEBUG,
        /**
         * <p>
         * Intermediate level logs that are not verbose and are of immediate
         * interest to tier 2 support staff. These rarely if ever include a
         * throwable/stack trace.
         * </p>
         * <p>
         * Examples:
         * </p>
         * <ul>
         * <li>Notification of start or completion of large batch jobs</li>
         * <li>Providing integration or batch processing summary data such as
         * the number of records processed</li>
         * <li>Recording when configuration files are read from the file system,
         * (initial load or cache expiration)</li>
         * </ul>
         * <p>
         * Info level logs are usually enabled in non-production environments
         * and disabled in production environments.
         * </p>
         */
        INFO,
        /**
         * <p>
         * Records non-critical failures that do not immediately impact normal
         * operation but may result in undefined or undesirable behavior. This
         * may be used when a user-specified value is missing or invalid and is
         * automatically replaced with a questionable default, or when an
         * exception is caught resulting in the partial completion of an
         * operation. Often warn level logs will include a throwable/stack
         * trace, although this isn't always the case.
         * </p>
         * <p>
         * Examples:
         * </p>
         * <ul>
         * <li>Failure to query APS to see if an item has already been
         * provisioned, resulting in the assumption that it has not</li>
         * <li>Failure to delivery low-priority E-mails or other notifications</li>
         * <li>Failure to automatically pre-populate data upon some system
         * event, resulting in the user's having to enter the data manually</li>
         * <li>Failure to automatically route a task or assign a due date,
         * resulting in the user's having to search for it</li>
         * <li>Caught exceptions (e.g. NumberFormatException,
         * IndexOutOfBoundsException, MalformedURLException, etc.) based on
         * implicit conversion of non-critical user input</li>
         * </ul>
         * <p>
         * Warn level logs are usually enabled in all environments.
         * </p>
         */
        WARN,
        /**
         * <p>
         * Critical errors that inhibit normal operation. These errors are
         * unanticipated and/or cannot be handled automatically. Examples
         * include I/O failures, database constraint violations, uncaught
         * runtime exceptions, failure to deliver JMS messages or make web
         * service calls, etc. Generally, error level logs will always include a
         * throwable/stack trace.
         * </p>
         * <p>
         * Error level logs are usually enabled in all environments.
         * </p>
         */
        ERROR
    }
}
