
package org.jbalance.directory.core.query.helper.product;


import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.jbalance.directory.core.model.product.Product;
import org.jbalance.directory.core.query.GetResult;

@XmlRootElement
public class GetProductsResult extends GetResult
{
    private static final long serialVersionUID = 1L;

    private List<Product> products;

    public GetProductsResult()
    {
    }

    @XmlElement(name = "products")
    public List<Product> getProducts()
    {
        if (products == null)
        {
            products = new ArrayList<Product>();
        }
        return products;
    }


    public GetProductsResult(GetProductsRequest request)
    {
        super(request);
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
