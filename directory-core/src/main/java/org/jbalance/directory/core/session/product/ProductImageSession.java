package org.jbalance.directory.core.session.product;

import org.jbalance.directory.core.model.product.ProductImage;
import org.jbalance.directory.core.query.helper.product.GetProductImagesRequest;
import org.jbalance.directory.core.session.GenericSession;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public interface ProductImageSession extends GenericSession<ProductImage, GetProductImagesRequest>
{
}
