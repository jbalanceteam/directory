package org.jbalance.directory.core.session.product;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import org.jbalance.directory.core.model.product.ProductPacking;
import org.jbalance.directory.core.query.helper.product.GetProductPackingsRequest;
import org.jbalance.directory.core.query.helper.product.GetProductPackingsRequestHelper;
import org.jbalance.directory.core.query.helper.product.GetProductPackingsResult;
import org.jbalance.directory.core.session.GenericSessionBean;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@Stateless
public class ProductPackingSessionBean extends GenericSessionBean<ProductPacking, GetProductPackingsRequest>
        implements ProductPackingSession {

    @Override
    public List<ProductPacking> list(GetProductPackingsRequest request) {
        
        GetProductPackingsResult result = new GetProductPackingsRequestHelper(request).createResult(manager);
                
        return result.getPackings();
    }

    @Override
    @PostConstruct
    public void init() {
        beanClass = ProductPacking.class;
    }
	
	/**
	 * 
	 * @param packing
	 * @return packing or null if Packing image and Product image are not the same
	 */
	@Override
	public ProductPacking add(ProductPacking productPacking){
		if (productPacking.getProduct() != null && 
				productPacking.getPacking() != null &&
				!productPacking.getProduct().getImage().equals(productPacking.getPacking().getImage())){
			return null;
		}
		return super.add(productPacking);		
	}
	
	
	/**
	 * 
	 * @param packing
	 * @return packing or null if Packing image and Product image are not the same
	 */
	@Override
	public ProductPacking edit(ProductPacking productPacking){
		if (productPacking.getProduct() != null && 
				productPacking.getPacking()!= null &&
				!productPacking.getProduct().getImage().equals(productPacking.getPacking().getImage())){
			return null;
		}
		return super.edit(productPacking);		
	}

}
