
package org.jbalance.directory.core.query.helper.flowchart;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.jbalance.directory.core.model.flowchart.FlowChart;
import org.jbalance.directory.core.query.GetResult;

@XmlRootElement
public class GetFlowChartResult extends GetResult
{
    private static final long serialVersionUID = 1L;

    private List<FlowChart> flowCharts;

    public GetFlowChartResult()
    {
    }

    @XmlElement(name = "flowCharts")
    public List<FlowChart> getFlowCharts()
    {
        if (flowCharts == null)
        {
            setFlowCharts(new ArrayList<FlowChart>());
        }
        return flowCharts;
    }


    public GetFlowChartResult(GetFlowChartRequest request)
    {
        super(request);
    }

    public void setFlowCharts(List<FlowChart> flowCharts) {
        this.flowCharts= flowCharts;
    }
}
