package org.jbalance.directory.core.model.product;

import java.util.Date;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import org.jbalance.directory.core.model.BaseEntity;
import org.jbalance.directory.core.model.flowchart.ColdProcessingLossPercent;
import org.jbalance.directory.core.model.measure.Measure;
import org.jbalance.directory.core.model.norm.ProductCategory;

/**
 * Образ продукта
 */
@Entity
public class ProductImage extends BaseEntity {

    /**
     *
     * Название
     */
    @Column(unique = true)
    private String name;

    /**
     * Код ОКП
     */
    @Column
    private String okp;

    @ManyToOne
    private Measure baseMeasure;

    @ManyToOne
    private ProductCategory productCategory;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER,mappedBy = "image")
    @OrderBy("periodEnd ASC")
    private SortedSet<ColdProcessingLossPercent> losts = new TreeSet<>();
//	
//	@OneToMany(mappedBy = "Packing")
//	private List<Packing> packings;
//	

    public ProductImage() {
    }

    public ProductImage(Long id, Date enteredDate, Date deletedDate, String okp) {
        super(id, enteredDate, deletedDate);
        this.okp = okp;
    }

    /**
     * Set the value of okp
     *
     * @param newVar the new value of okp
     */
    public void setOkpCode(String newVar) {
        okp = newVar;
    }

    /**
     * Get the value of okp
     *
     * @return the value of okp
     */
    public String getOkpCode() {
        return okp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Measure getBaseMeasure() {
        return baseMeasure;
    }

    public void setBaseMeasure(Measure measure) {
        this.baseMeasure = measure;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

//	public List<Packing> getPackings() {
//		
//        return packings;
//    }
//	
    @Override
    public String toString() {
        return String.valueOf(this.getName());
    }

    public String getOkp() {
        return okp;
    }

    public void setOkp(String okp) {
        this.okp = okp;
    }

    public SortedSet<ColdProcessingLossPercent> getLosts() {
        return losts;
    }

    public void setLosts(SortedSet<ColdProcessingLossPercent> losts) {
        this.losts = losts;
    }

    public int getColdProcessingPeriodStart(ColdProcessingLossPercent lost) {

        ColdProcessingLossPercent l = null;
        try {
            l = losts.headSet(lost).last();
        } catch (Exception e) {
            return 0;
        }
        return l == null ? 0 : l.getPeriodEnd();
    }
}
