package org.jbalance.directory.core.session.norm;

import java.util.List;
import org.jbalance.directory.core.model.norm.ProductCategory;
import org.jbalance.directory.core.query.helper.norm.GetProductCategoryRequest;
import org.jbalance.directory.core.session.GenericSession;

/**
 *
 * @author Mihail
 */
public interface ProductCategorySession extends GenericSession<ProductCategory, GetProductCategoryRequest>{

    public List<ProductCategory> getAll();
    
}
