package org.jbalance.directory.core.query.helper.product;

import java.util.List;
import javax.persistence.EntityManager;
import org.jbalance.directory.core.model.product.Product;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;
import org.jbalance.directory.core.query.helper.GetRequestHelper;
import org.jbalance.directory.core.util.CollectionUtil;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class GetProductsRequestHelper  extends GetRequestHelper<GetProductsRequest, GetProductsResult>
{

    public GetProductsRequestHelper(GetProductsRequest request)
    {
        super(request);
    }

    @Override
    protected AbstractQueryBuilder<?> initQuery()
    {
        ProductQueryBuilder qb = new ProductQueryBuilder();
        qb.appendFilter(request.getIdFilter(), "product.id");      
        qb.appendFilter(request.getProductNameFilter(), "product.name");
        
        qb.orderBy().append("order by product.enteredDate desc");
        return qb;
    }

    @Override
    protected GetProductsResult createResult(GetProductsResult result, List<?> records, EntityManager em)
    {
        List<Product> productImages = (List<Product>) records;
        productImages = CollectionUtil.filterDuplicates(productImages);
        result.setProducts(productImages);
        return result;
    }
}

