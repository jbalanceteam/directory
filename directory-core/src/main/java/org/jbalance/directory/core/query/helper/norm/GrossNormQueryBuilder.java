package org.jbalance.directory.core.query.helper.norm;

import org.jbalance.directory.core.model.norm.GrossNorm;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;

/**
 *
 * @author Mihail
 */
public class GrossNormQueryBuilder extends AbstractQueryBuilder<GrossNormQueryBuilder>{
    public GrossNormQueryBuilder() {
        super(null, GrossNorm.class, "grossNorm");
    }

    @Override
    public StringBuilder buildJoins() {
        StringBuilder joins = new StringBuilder();
        return joins;
    }

    @Override
    protected GrossNormQueryBuilder get() {
        return this;
    }
}