package org.jbalance.directory.core.query.helper.product;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import org.jbalance.directory.core.model.product.Packing;
import org.jbalance.directory.core.query.GetResult;

/**
 *
 * @author Mihail
 */
public class GetPackingsResult extends GetResult{
	private static final long serialVersionUID = 1L;

    private List<Packing> productImagePackings;

    public GetPackingsResult()
    {
    }

    @XmlElement(name = "productImagePackings")
    public List<Packing> getProductImagePackings()
    {
        if (productImagePackings == null)
        {
            setProductImagePackings(new ArrayList<Packing>());
        }
        return productImagePackings;
    }


    public GetPackingsResult(GetPackingsRequest request)
    {
        super(request);
    }

    public void setProductImagePackings(List<Packing> productImagePackings) {
        this.productImagePackings = productImagePackings;
    }
}
