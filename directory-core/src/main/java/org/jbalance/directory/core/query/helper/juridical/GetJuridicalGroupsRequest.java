package org.jbalance.directory.core.query.helper.juridical;

import javax.xml.bind.annotation.XmlType;
import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.query.GetRequest;

/**
 * @author Alexandr Chubenko
 */
@XmlType(propOrder
        = {
            "idFilter",
            "juridicalNameFilter",
            "juridicalInnFilter"
        })
public class GetJuridicalGroupsRequest extends GetRequest<GetJuridicalGroupsResult> {

    private static final long serialVersionUID = 1L;

    private LongFilter idFilter;
    private StringFilter juridicalIdFilter;

    @Override
    public GetJuridicalGroupsResult createResult() {
        return new GetJuridicalGroupsResult(this);
    }

    public StringFilter getJuridicalIdFilter() {
        return juridicalIdFilter;
    }

    public void setJuridicalIdFilter(StringFilter juridicalIdFilter) {
        this.juridicalIdFilter = juridicalIdFilter;
    }

    public LongFilter getIdFilter() {
        return idFilter;
    }

    public void setIdFilter(LongFilter idFilter) {
        this.idFilter = idFilter;
    }
}
