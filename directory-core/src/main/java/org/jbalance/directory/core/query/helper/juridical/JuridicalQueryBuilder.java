package org.jbalance.directory.core.query.helper.juridical;

import org.jbalance.directory.core.model.Juridical;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;

/**
 * Построитель запросов для {@link Juridical}
 *
 * @author Alexandr Chubenko
 */
public class JuridicalQueryBuilder extends AbstractQueryBuilder<JuridicalQueryBuilder> {

    public JuridicalQueryBuilder() {
        super(null, Juridical.class, "juridical");
    }

//  закоментировал чтобы не перечислять аттрибуты 
//    @Override
//    public StringBuilder buildSelectArgs() {
//        StringBuilder sb = new StringBuilder()
//                .append(primaryAlias()).append(".id, ")
//                .append(primaryAlias()).append(".enteredDate, ")
//                .append(primaryAlias()).append(".deletedDate, ")
//                .append(primaryAlias()).append(".name, ")
//                .append(primaryAlias()).append(".inn, ")
//                .append(primaryAlias()).append(".mailingAdress, ")
//                .append(primaryAlias()).append(".contactInfo, ")
//                .append(primaryAlias()).append(".phone, ")
//                .append(primaryAlias()).append(".eMail, ")
//                .append(primaryAlias()).append(".registeredOffice, ")
//                .append(primaryAlias()).append(".okonh, ")
//                .append(primaryAlias()).append(".okpo");
//        return sb;
//    }

    @Override
    public StringBuilder buildJoins() {
        StringBuilder joins = new StringBuilder();
        return joins;
    }

    @Override
    protected JuridicalQueryBuilder get() {
        return this;
    }

}
