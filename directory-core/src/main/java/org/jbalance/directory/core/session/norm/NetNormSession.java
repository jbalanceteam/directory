package org.jbalance.directory.core.session.norm;

import org.jbalance.directory.core.model.norm.NetNorm;
import org.jbalance.directory.core.query.helper.norm.GetNetNormsRequest;
import org.jbalance.directory.core.session.GenericSession;

/**
 *
 * @author mihail
 */
public interface NetNormSession extends GenericSession<NetNorm, GetNetNormsRequest>{
    
}
