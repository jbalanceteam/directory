package org.jbalance.directory.core.model.address;


import java.util.UUID;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class Street extends AddressObject implements IStreet
{

    public static final int ADDRESS_LEVEL_ID = 7;

    protected City city;

    public Street()
    {
    }

    public Street(UUID uuid)
    {
        super(uuid);
    }

    public void setCity(City city)
    {
        this.city = city;
    }

    public City getCity()
    {
        return city;
    }

    @Override
    public String toString()
    {
        return this.formalName + " " + this.aoGuid + " " + this.parentGuid + " " + this.regionCode + " " + this.actStatus;
    }
}
