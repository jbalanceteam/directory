package org.jbalance.directory.core.query.helper.norm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import org.jbalance.directory.core.model.norm.ProductCategory;
import org.jbalance.directory.core.query.GetResult;

/**
 *
 * @author Mihail
 */
public class GetProductCategoryResult extends GetResult{
    private static final long serialVersionUID = 1L;

    private List<ProductCategory> productCategories;

    public GetProductCategoryResult()
    {
    }

    @XmlElement(name = "productCategories")
    public List<ProductCategory> getProductCategories()
    {
        if (productCategories == null)
        {
            productCategories = new ArrayList<ProductCategory>();
        }
        return productCategories;
    }

    public void setProductCategories(List<ProductCategory> productCategories)
    {
        this.productCategories = productCategories;
    }

    public GetProductCategoryResult(GetProductCategoryRequest request)
    {
        super(request);
    }
}