package org.jbalance.directory.core.model.address;

import java.util.UUID;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public interface IHouse
{

    UUID getAoGuid();

    String getBuildNum();

    Integer getCounter();

    Integer getEstStatus();

    UUID getHouseGuid();

    UUID getHouseId();

    String getHouseNum();

    int getRegionCode();

    Integer getStatStatus();

    Integer getStrStatus();

    String getStructNum();

    String getPostalCode();

    void setPostalCode(String postalCode);
    
    void setAoGuid(UUID aoGuid);

    void setBuildNum(String buildNum);

    void setCounter(Integer counter);

    void setEstStatus(Integer estStatus);

    void setHouseGuid(UUID houseGuid);

    void setHouseId(UUID houseId);

    void setHouseNum(String houseNum);

    void setRegionCode(int regionCode);

    void setStatStatus(Integer statStatus);

    void setStrStatus(Integer strStatus);

    void setStructNum(String structNum);

    String toString();

}
