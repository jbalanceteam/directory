package org.jbalance.directory.core.model.address;


import java.util.UUID;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */

public class HouseInterval implements IHouseInterval
{

    /**
     * Значение начала интервала
     */
    private Integer intStart;

    /**
     * Значение окончания интервала
     */
    private Integer intEnd;

    /**
     * Иидентификатор записи интервала домов
     */
    private UUID houseIntId;

    /**
     * Глобальный уникальный идентификатор интервала домов
     */
    private UUID intGuid;

    /**
     * Статус интервала (обычный, четный, нечетный)
     *
     */
    private Integer intStatus;

    /**
     * Guid записи родительского объекта (улицы, города, населенного пункта и
     * т.п.)
     */
    protected UUID aoGuid;

    public HouseInterval()
    {
    }

    public HouseInterval(UUID INTGUID)
    {
        this.houseIntId = INTGUID;
    }

    @Override
    public Integer getIntStart()
    {
        return intStart;
    }

    @Override
    public Integer getIntEnd()
    {
        return intEnd;
    }

    @Override
    public UUID getHouseIntId()
    {
        return houseIntId;
    }

    @Override
    public UUID getIntGuid()
    {
        return intGuid;
    }

    @Override
    public Integer getIntStatus()
    {
        return intStatus;
    }

    @Override
    public UUID getAoGuid()
    {
        return aoGuid;
    }

    @Override
    public void setIntStart(Integer intStart)
    {
        this.intStart = intStart;
    }

    @Override
    public void setIntEnd(Integer intEnd)
    {
        this.intEnd = intEnd;
    }

    @Override
    public void setHouseIntId(UUID houseIntId)
    {
        this.houseIntId = houseIntId;
    }

    @Override
    public void setHouseIntId(String houseIntId)
    {
        this.houseIntId = UUID.fromString(houseIntId);
    }

    @Override
    public void setIntGuid(UUID intGuid)
    {
        this.intGuid = intGuid;
    }

    @Override
    public void setIntGuid(String intGuid)
    {
        this.intGuid = UUID.fromString(intGuid);
    }

    @Override
    public void setIntStatus(Integer intStatus)
    {
        this.intStatus = intStatus;
    }

    @Override
    public void setAoGuid(UUID aoGuid)
    {
        this.aoGuid = aoGuid;
    }

    @Override
    public void setAoGuid(String aoGuid)
    {
        this.aoGuid = UUID.fromString(aoGuid);
    }

    @Override
    public String toString()
    {
        return intStart + " " + intEnd + " " + intStatus;
    }
}
