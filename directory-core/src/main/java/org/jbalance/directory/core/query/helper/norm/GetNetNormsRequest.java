package org.jbalance.directory.core.query.helper.norm;

import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.query.GetRequest;

/**
 *
 * @author mihail
 */
public class GetNetNormsRequest extends GetRequest<GetNetNormsResult>{
    private static final long serialVersionUID = 1L;

    private LongFilter idFilter;
    private LongFilter normGroupFilter;

    @Override
    public GetNetNormsResult createResult()
    {
        return new GetNetNormsResult(this);
    }

    public LongFilter getIdFilter() {
        return idFilter;
    }

    public void setIdFilter(LongFilter idFilter) {
        this.idFilter = idFilter;
    }

    public LongFilter getNormGroupFilter() {
        return normGroupFilter;
    }

    public void setNormGroupFilter(LongFilter normGroupFilter) {
        this.normGroupFilter = normGroupFilter;
    }
    
}
