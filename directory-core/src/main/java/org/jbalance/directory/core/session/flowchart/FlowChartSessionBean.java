package org.jbalance.directory.core.session.flowchart;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import org.jbalance.directory.core.model.flowchart.FlowChart;
import org.jbalance.directory.core.query.helper.flowchart.GetFlowChartRequest;
import org.jbalance.directory.core.session.GenericSessionBean;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@Stateless
public class FlowChartSessionBean extends GenericSessionBean<FlowChart, GetFlowChartRequest>
{

    @Override
    public List<FlowChart> list(GetFlowChartRequest request)
    {
        return null;
    }

    @Override
    @PostConstruct
    public void init() {
        beanClass = FlowChart.class;
    }
}
