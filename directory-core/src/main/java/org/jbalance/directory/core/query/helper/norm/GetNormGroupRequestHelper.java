package org.jbalance.directory.core.query.helper.norm;

import java.util.List;
import javax.persistence.EntityManager;
import org.jbalance.directory.core.model.norm.NormGroup;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;
import org.jbalance.directory.core.query.helper.GetRequestHelper;
import org.jbalance.directory.core.util.CollectionUtil;

/**
 *
 * @author Mihail
 */
public class GetNormGroupRequestHelper extends GetRequestHelper<GetNormGroupRequest, GetNormGroupResult>{

    public GetNormGroupRequestHelper(GetNormGroupRequest request) {
        super(request);
    }
    
    @Override
    protected GetNormGroupResult createResult(GetNormGroupResult result, List<?> records, EntityManager em) {
        List<NormGroup> normGroups = (List<NormGroup>) records;
        normGroups = CollectionUtil.filterDuplicates(normGroups);
        result.setNormGroups(normGroups);
        return result;
    }

    @Override
    protected AbstractQueryBuilder<?> initQuery() {
        NormGroupQueryBuilder normGroupQueryBuilder = new NormGroupQueryBuilder();
        normGroupQueryBuilder.appendFilter(request.getIdFilter(), "normGroup.id");
        normGroupQueryBuilder.orderBy().append("order by normGroup.enteredDate desc");
        return normGroupQueryBuilder;
    }
    
}
