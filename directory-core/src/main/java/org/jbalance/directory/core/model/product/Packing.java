package org.jbalance.directory.core.model.product;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import org.jbalance.directory.core.model.BaseEntity;
import org.jbalance.directory.core.model.measure.Measure;

/**
 *
 * Сущность предстваляет упаковку в которой может находится образ продукта
 * 
 * @author Komov Roman <komov.r@gmail.com>
 */
@Entity
public class Packing extends BaseEntity {

    /**
     *
     * Название
     */
    @Column
    private String name;

	@Column
	private Float quantity;
    /**
     * Образ продукта
     */
    @ManyToOne(fetch = FetchType.EAGER)
    private ProductImage image;
	
	@Column
	private Boolean isBasePacking;
	
	@ManyToOne
	private Measure measure;
	
	private String description;

    private Long containsBaseUnits;

    public Packing() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
	
    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    public ProductImage getImage() {
        return image;
    }

    public void setImage(ProductImage image) {
        this.image = image;
    }    

	public Boolean getIsBasePacking() {
        return isBasePacking;
    }

    public void setIsBasePacking(Boolean isBasePacking) {
        this.isBasePacking = isBasePacking;
    }  
	
	public Measure getMeasure() {
        return measure;
    }

    public void setMeasure(Measure measure) {
        this.measure = measure;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getContainsBaseUnits() {
        return containsBaseUnits;
    }

    public void setContainsBaseUnits(Long containsBaseUnits) {
        this.containsBaseUnits = containsBaseUnits;
    }
}
