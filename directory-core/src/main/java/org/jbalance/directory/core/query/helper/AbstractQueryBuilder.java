package org.jbalance.directory.core.query.helper;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.jbalance.directory.core.filter.DateFilter;
import org.jbalance.directory.core.filter.DateFilterType;
import org.jbalance.directory.core.filter.Filter;
import org.jbalance.directory.core.filter.FilterParams;
import org.jbalance.directory.core.query.Sort;
import org.jbalance.directory.core.util.Str;

/**
 * Similar to {@link QueryHelper}, {@link AbstractQueryBuilder} assists in building
 * dynamic query strings. Where they differ is that {@link AbstractQueryBuilder} only
 * contains logic for building the query string. QueryBuilders may be used to
 * build partial queryStrings that are intended to be included in other
 * queryStrings. For example, using select new to instantiate a new info object
 * may require concatenating the arguments for several info objects into one
 * constructor. {@link AccountInfoQueryBuilder} is a good example of this since
 * constructing a new {@link AccountInfo} requires the constructor arguments for
 * {@link ContactInfo} and {@link AddressInfo}.
 *
 * <code>
 * if (!argumentsOnly) {
 *     select.append("select new ").append(AccountInfo.class.getName()).append("(");
 * }
 * StringBuilder billingAddress = new AddressInfoQueryBuilder()
 *     .argumentsOnly(true)
 *     .addressAlias(billingAddressAlias)
 *     .countryAlias(billingAddressCountryAlias)
 *     .select();
 *
 * StringBuilder contact = new ContactInfoQueryBuilder()
 *     .argumentsOnly(true)
 *     .contactAlias(primaryContactAlias)
 *     .addressAlias(primaryContactAddressAlias)
 *     .countryAlias(primaryContactCountryAlias)
 *     .select();
 *
 * select.append(accountAlias).append(".id, ")
 *     .append(accountAlias).append(".createdDate, ")
 *     .append(accountAlias).append(".deletedDate, ")
 *     .append(accountAlias).append(".billingAccountNumber, ")
 *     .append(accountAlias).append(".name, ")
 *     .append(accountAlias).append(".status, ")
 *     .append(accountTypeAlias ).append(".code, ")
 *     .append(billingAddress)
 *     .append(",")
 *     .append(contact);
 * if (!argumentsOnly) {
 *     select.append(")");
 * }
 *
 * </code>
 *
 *
 * @param <B>
 *            - concrete type of builder
 */
public abstract class AbstractQueryBuilder<B extends AbstractQueryBuilder<B>> {
    private final StringBuilder select = new StringBuilder();
    private final StringBuilder from = new StringBuilder();
    private final StringBuilder where = new StringBuilder();
    private final StringBuilder groupBy = new StringBuilder();
    private final StringBuilder orderBy = new StringBuilder();
    private final FilterParams params = new FilterParams();
    private String primaryAlias;
    private Class<?> fromClass;
    private Class<?> selectClass;
    private StringBuilder selectArgs = new StringBuilder();
    private StringBuilder joins = new StringBuilder();
    private boolean selectPrimaryAlias = true;
    private boolean selectCount = false;
    private boolean paginatedRequest = false;
    private Map<Enum<?>, String> sortablesMap;

    public AbstractQueryBuilder() {

    }

    public AbstractQueryBuilder(String primaryAlias) {
        this.primaryAlias = primaryAlias;
        this.selectPrimaryAlias = true;
    }

    public AbstractQueryBuilder(Class<?> fromClass, String primaryAlias) {
        this(primaryAlias);
        this.fromClass = fromClass;
        this.selectPrimaryAlias = true;
    }

    public AbstractQueryBuilder(Class<?> selectClass,
            Class<?> fromClass, String primaryAlias) {
        this(fromClass, primaryAlias);
        this.selectClass = selectClass;
    }

    protected boolean isDistinct() {
    	return false;
    }

    public StringBuilder select() {
        if (select.length() == 0) {
            if (selectCount) {
                select.append("select count(*)");
            } else {
                if (selectClass != null) {
                    select.append("select " + (isDistinct()?" distinct ":"") + "new ").append(selectClass.getName()).append("(");
                    select.append(selectArgs());
                    select.append(")");
                } else if (!Str.empty(primaryAlias)) {
                    select.append("select ");
                    if (selectPrimaryAlias) {
                        select.append(primaryAlias);
                    }
                } else {
                    select.append("select ");
                }
            }
        }
        return select;
    }

    public StringBuilder selectArgs() {
        if (selectArgs == null || selectArgs.length() == 0) {
            selectArgs = buildSelectArgs();
        }
        return selectArgs;
    }

    public StringBuilder buildSelectArgs() {
        return new StringBuilder();
    }

    public StringBuilder from() {
        if (from.length() == 0 && fromClass != null) {
            from.append("from ");
            from.append(fromClass.getSimpleName());
            from.append(" ").append(primaryAlias());
            StringBuilder joins = joins();
            if (joins != null && joins.length() > 0) {
                from.append(" ").append(joins);
            }
        }
        return from;
    }

    public StringBuilder joins() {
        if (joins == null || joins.length() == 0) {
            joins = buildJoins();
        }
        return joins;
    }

    public StringBuilder buildJoins() {
        return new StringBuilder();
    }

    public StringBuilder where() {
        return where;
    }

    public StringBuilder groupBy() {
        return groupBy;
    }

    public StringBuilder orderBy() {
        return orderBy;
    }

    public FilterParams params() {
        return params;
    }

    /**
     * @return the full SQL query
     */
    @Override
    public String toString() {
        StringBuilder queryString = new StringBuilder();
        String selectString = select().toString();
        queryString.append(selectString);
        queryString.append(' ').append(from().toString());
        if(where().length() != 0) {
            queryString.append(' ').append(where());
        }
        if(groupBy().length() != 0) {
            queryString.append(' ').append(groupBy());
        }
        if (!selectCount) {
            //append ORDER BY clause for a correct pagination
            if (paginatedRequest && !Str.empty(primaryAlias) && selectString.indexOf(primaryAlias+".id") > -1) {
                if (orderBy().length() != 0 && orderBy().indexOf(primaryAlias+".id") < 0) {
                    orderBy().append(", " + primaryAlias + ".id");
                } else if (orderBy().length() == 0) {
                    orderBy().append("order by ").append(primaryAlias + ".id");
                }
            }
            if (orderBy().length() != 0) {
                queryString.append(' ').append(orderBy());
            }
        }

        return queryString.toString();
    }

    public B and() {
        if (where().length() == 0) {
            where().append(" where");
        } else {
            where().append(" and");
        }
        return get();
    }

    public B or() {
        if (where().length() == 0) {
            where().append(" where");
        } else {
            where().append(" or");
        }
        return get();
    }

    public B appendFilter(Filter<?,?> filter, String... names) {
        if (filter != null) {
            and();
            if(names.length>1){
                where().append(" (");
            }
            for(int i = 0;i<names.length;i++){
                filter.appendFilter(where(), params, names[i]);
                if(i<names.length-1){
                    or();
                }
            }
            if(names.length>1){
                where().append(')');
            }
        }
        return get();
    }

    public void appendEffectiveDateFilters(Date effectiveDate, String alias) {
        if (effectiveDate != null && !Str.empty(alias)) {
            DateFilter enteredDateFilter = new DateFilter(DateFilterType.LTE, effectiveDate);
            appendFilter(enteredDateFilter, alias+".enteredDate");
        }
    }

    public String primaryAlias() {
        return primaryAlias;
    }

    public Class<?> selectClass() {
        return selectClass;
    }

    public Class<?> fromClass() {
        return fromClass;
    }

    public boolean isSelectPrimaryAlias() {
        return selectPrimaryAlias;
    }

    public void selectPrimaryAlias(boolean selectPrimaryAlias) {
        this.selectPrimaryAlias = selectPrimaryAlias;
    }

    public boolean isSelectCount() {
        return selectCount;
    }

    public void setSelectCount(boolean selectCount) {
        this.selectCount = selectCount;
    }

    public boolean isPaginatedRequest() {
        return paginatedRequest;
    }

    public void setPaginatedRequest(boolean paginatedRequest) {
        this.paginatedRequest = paginatedRequest;
    }

    /**
     * Returns a reference to the concrete builder. This method governs what
     * builder will be returned by methods like uuid(String)
     *
     * @return
     */
    protected abstract B get();

    private Map<Enum<?>, String> getSortablesMapping() {
        if (sortablesMap == null) {
            sortablesMap = new LinkedHashMap<Enum<?>, String>();
        }
        return sortablesMap;
    }

    /**
     * QueryBuilders and {@link GetRequestHelper}s should use this method to map
     * a {@link SortableField} to the corresponding query field. The mapping is
     * used to build the {@link #orderBy} clause.
     *
     * @param sortableField
     *            the API-defined sortable field
     * @param name
     *            the query field that corresponds to the sortableField
     */
    public void mapSortable(Enum<?> sortableField, String name) {
        getSortablesMapping().put(sortableField, name);
    }

    public void appendSortables(List<? extends Sort> sort) {
        if (sort == null || sort.isEmpty() || selectCount) {
            return;
        }
        orderBy.setLength(0);
        orderBy.append("order by ");
        String clausePart = Joiner.on(", ").join(Lists.transform(sort, new OrderByFunction()));
        orderBy.append(clausePart);
    }

    private class OrderByFunction implements Function<Sort, String> {

        @Override
        public String apply(Sort input) {
            StringBuilder output = new StringBuilder();
            Map<Enum<?>, String> sortablesMapping = getSortablesMapping();
            Enum<?> sortable = input.getSortableField();
            String orderByFieldName = sortablesMapping.get(sortable);
            if (!Str.empty(orderByFieldName)) {
                output.append(orderByFieldName);
                if (input.getSortDirection() != null) {
                    output.append(" ").append(input.getSortDirection().name());
                }
            }
            return output.toString();
        }

    }

}
