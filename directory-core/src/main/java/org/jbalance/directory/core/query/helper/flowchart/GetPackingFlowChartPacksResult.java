package org.jbalance.directory.core.query.helper.flowchart;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import org.jbalance.directory.core.model.flowchart.PackingFlowChartPacks;
import org.jbalance.directory.core.query.GetResult;

/**
 *
 * @author Mihail
 */
public class GetPackingFlowChartPacksResult extends GetResult
{
    private static final long serialVersionUID = 1L;

    private List<PackingFlowChartPacks> packingFlowChartPacksList;

    public GetPackingFlowChartPacksResult()
    {
    }

    @XmlElement(name = "packingFlowChartPacksList")
    public List<PackingFlowChartPacks> getPackingFlowChartPacksList()
    {
        if (packingFlowChartPacksList == null)
        {
            packingFlowChartPacksList = new ArrayList<PackingFlowChartPacks>();
        }
        return packingFlowChartPacksList;
    }


    public GetPackingFlowChartPacksResult(GetPackingFlowChartPacksRequest request)
    {
        super(request);
    }
}
