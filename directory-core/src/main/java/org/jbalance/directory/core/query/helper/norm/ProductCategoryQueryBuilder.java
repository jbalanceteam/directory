package org.jbalance.directory.core.query.helper.norm;


import org.jbalance.directory.core.model.norm.ProductCategory;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;

/**
 *
 * @author Mihail
 */
public class ProductCategoryQueryBuilder extends AbstractQueryBuilder<ProductCategoryQueryBuilder> {

    public ProductCategoryQueryBuilder() {
        super(null, ProductCategory.class, "productCategory");
    }

    @Override
    public StringBuilder buildJoins() {
        StringBuilder joins = new StringBuilder();
        return joins;
    }

    @Override
    protected ProductCategoryQueryBuilder get() {
        return this;
    }

}