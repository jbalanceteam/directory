
package org.jbalance.directory.core.query.helper.product;


import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.jbalance.directory.core.model.product.ProductPacking;
import org.jbalance.directory.core.query.GetResult;

@XmlRootElement
public class GetProductPackingsResult extends GetResult
{
    private static final long serialVersionUID = 1L;

    private List<ProductPacking> packings;

    public GetProductPackingsResult()
    {
    }

    @XmlElement(name = "packings")
    public List<ProductPacking> getPackings()
    {
        if (packings == null)
        {
            setPackings(new ArrayList<ProductPacking>());
        }
        return packings;
    }


    public GetProductPackingsResult(GetProductPackingsRequest request)
    {
        super(request);
    }

    public void setPackings(List<ProductPacking> packings) {
        this.packings = packings;
    }
}
