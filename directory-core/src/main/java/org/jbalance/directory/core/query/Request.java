package org.jbalance.directory.core.query;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import org.jbalance.directory.core.util.Obj;


public abstract class Request<R extends Result> implements Serializable {

    private static final long serialVersionUID = 1L;
    private String correlationId;
    private String actorId;

    @XmlAttribute
    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public String getActorId() {
        return actorId;
    }

    public void setActorId(String actorId) {
        this.actorId = actorId;
    }
    
    public abstract R createResult();

    @XmlTransient
    protected Map<String, Object> getToStringMap() {
        Map<String, Object> toStringMap = new LinkedHashMap<String, Object>();
        toStringMap.put("correlationId", correlationId);
        toStringMap.put("actorId", actorId);
        return toStringMap;
    }
    
    @Override
    public String toString() {
        SimpleDateFormat shortDateTime = new SimpleDateFormat("yyyyMMddHHmmss.S");
        StringBuilder string = new StringBuilder();
        string.append(getClass().getSimpleName());
        string.append("[");
        Map<String, Object> toStringMap = getToStringMap();
        if(null != toStringMap && !toStringMap.isEmpty()) {
            Iterator<Entry<String, Object>> i = getToStringMap().entrySet().iterator();
            while(i.hasNext()) {
                Entry<String, Object> entry = i.next();
                Object value = entry.getValue();
                if(value instanceof Date) {
                    value = shortDateTime.format((Date)value);
                }
                string.append(entry.getKey()).append('=').append(Obj.coerceToString(value));
                if(i.hasNext()) {
                    string.append(", ");
                }
            }
        }
        string.append("]");
        return string.toString();
    }
}
