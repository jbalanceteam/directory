package org.jbalance.directory.core.session.address;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.sql.DataSource;

import org.jbalance.directory.core.model.address.*;
import org.jbalance.directory.core.util.Log;
import org.jbalance.directory.core.util.Log.Replacement;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@ApplicationScoped
public class AddressFinder implements IAddressFinder {

    @Resource(mappedName = "java:jboss/datasources/fias_db")
    DataSource ds;

    private Log log = new Log(getClass());
    /**
     * Ограничение размера выборки Если 0 - ограничений нет
     */
    private int limitSelectRegion = DEFAULT_LIMIT_SELECT_REGION;
    private int limitSelectCity = DEFAULT_LIMIT_SELECT_CITY;
    private int limitSelectStreet = DEFAULT_LIMIT_SELECT_STREET;
    private int limitSelectHouse = DEFAULT_LIMIT_SELECT_HOUSE;

    @Override
    public List<Region> findRegion(String term) {
        List<Region> result = new ArrayList<>();
        String sql = "SELECT * FROM ADDROBJ where lower(FORMALNAME) like lower(?) "
                + "AND AOLEVEL IN (?) and ACTSTATUS=? and SHORTNAME <> ?" + produceLimitCondition(limitSelectCity);// usual sql query
        try {
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, term + "%");

//            AOLEVEL - Уровень адресного объекта
//            уровень региона
            String regionLevel = "1";

            preparedStatement.setString(2, regionLevel);
            String activeObject = "1";
            preparedStatement.setString(3, activeObject);
            String cityObject = "г";
            preparedStatement.setString(4, cityObject);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Region region = (Region) mapAddressObject(resultSet, new Region());

                result.add(region);
            }

            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            log.error(e);
        }
        return result;
    }

    @Override
    public Region findRegion(UUID uuid) {
        Region region = null;

        String sql = "SELECT * FROM ADDROBJ where AOGUID = ? AND ACTSTATUS = ? ";
        sql += produceLimitCondition(limitSelectRegion);
        try {
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, uuid.toString());
            String actstatus = "1";
            preparedStatement.setString(2, actstatus);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                region = (Region) mapAddressObject(resultSet, new Region());
            }

            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            log.error(e);
        }
        return region;
    }

    @Override
    public List<City> findCity(String term, Integer region) {
        List<City> result = new ArrayList<>();

        StringBuilder sqlBuilder = new StringBuilder("SELECT * FROM ADDROBJ where lower(FORMALNAME) like lower(?) "
                + "AND (AOLEVEL IN ('4', '6') or (AOLEVEL IN ('1') AND SHORTNAME='г')) and ACTSTATUS=? ");
        if (region != null){
            sqlBuilder.append(" AND REGIONCODE = '" + region + "' ");
        }

        sqlBuilder.append("ORDER BY AOLEVEL, FORMALNAME, SHORTNAME, REGIONCODE" + produceLimitCondition(limitSelectCity));// usual sql query
        try {
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sqlBuilder.toString());
            preparedStatement.setString(1, "%" + term + "%");

//            AOLEVEL - Уровень адресного объекта
//            уровень города
            String cityLevel = "4";
//			 уровень населенного пункта
            String locationLevel = "6";

//            preparedStatement.setString(2, cityLevel);
            String activeObject = "1";
            preparedStatement.setString(2, activeObject);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                City c = (City) mapAddressObject(resultSet, new City());

                result.add(c);
            }

            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            log.error(e);
        }
        return result;
    }

    @Override
    public City findCity(UUID uuid) {
        City c = null;

        String sql = "SELECT * FROM ADDROBJ where AOGUID = ?  ";
        sql += produceLimitCondition(limitSelectCity);
        try {
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, uuid.toString());

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                c = (City) mapAddressObject(resultSet, new City());
            }

            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            log.error(e);
        }
        return c;
    }

    @Override
    public List<Street> findStreet(String term, UUID parent) {
        log.entry(term, parent);
        List<Street> result = new ArrayList<>();

        String sql = "SELECT * FROM ADDROBJ where lower(FORMALNAME) like lower(?) AND PARENTGUID=?  AND  ACTSTATUS=? ";
        sql += produceLimitCondition(limitSelectStreet);

        log.trace("{0} sql: {1}", Replacement.METHOD_NAME, sql);

        try {
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            //search term
            preparedStatement.setString(1, "%" + term + "%");

            //parent
            preparedStatement.setString(2, parent.toString());

            //actual status
            preparedStatement.setString(3, "1");

            //limit
            //preparedStatement.setInt(3, AddressFinder.STREET_SELECT_LIMIT);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Street s = (Street) mapAddressObject(resultSet, new Street());
                result.add(s);
            }
            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            log.error(e);
        }
        return result;
    }

    @Override
    public Street findStreet(UUID uuid) {
        Street s = null;

        String sql = "SELECT * FROM ADDROBJ where AOGUID = ?  ";
        sql += produceLimitCondition(limitSelectStreet);
        try {
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, uuid.toString());

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                s = (Street) mapAddressObject(resultSet, new Street());
            }
            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            log.error(e);
        }
        return s;
    }

    @Override
    public List<House> findHouse(String term, UUID parent) {
        log.entry(term, parent);
        List<House> result = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        IStreet street = this.findStreet(parent);
        String tableName = getHouseTableName(street.getRegionCode());

        String sql = "SELECT * FROM " + tableName + " where lower(HOUSENUM) like lower(?) AND AOGUID=? and 	ENDDATE>NOW() order by length(HOUSENUM), HOUSENUM desc";
        sql += produceLimitCondition(limitSelectHouse);
        log.trace("{0} sql: {1}", Replacement.METHOD_NAME, sql);
        try {

            Connection connection = getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, term + "%");
            preparedStatement.setString(2, parent.toString());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                House house = new House(UUID.fromString(resultSet.getString("HOUSEGUID")), street.getRegionCode());
                house.setHouseId(UUID.fromString(resultSet.getString("HOUSEID")));
                house.setHouseNum(resultSet.getString("HOUSENUM"));
                house.setAoGuid(UUID.fromString(resultSet.getString("AOGUID")));
                house.setPostalCode(resultSet.getString("POSTALCODE"));
                result.add(house);
            }
            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            log.error(e);
        }
        if (result.size() == 0){
            House house = new House();
            house.setAoGuid(parent);
            house.setHouseNum(term);
            result.add(house);
        }
        return result;
    }

    public String getFullAddressForHouse(UUID uuid, int regionCode) {
        StringBuilder sb = new StringBuilder();
        String tableName = getHouseTableName(regionCode);

        String sql = "SELECT * FROM " + tableName + " where HOUSEGUID = ?";

        try {
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, uuid.toString());

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                sb.append(resultSet.getString("HOUSENUM")).append(", ");
                sb.append(getFullAddressForAddressObject(UUID.fromString(resultSet.getString("AOGUID"))));
            }
            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            log.error(e);
        }
        return sb.toString();
    }

    public String getFullAddressForHouseInterval(UUID uuid, String houseNum) {
        StringBuilder sb = new StringBuilder();

        String sql = "SELECT * FROM HOUSEINT where HOUSEINTID = ?";

        try {
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, uuid.toString());

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                sb.append(houseNum).append(", ");
                sb.append(getFullAddressForAddressObject(UUID.fromString(resultSet.getString("AOGUID"))));
            }
            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            log.error(e);
        }
        return sb.toString();
    }

//    @Override
//    public String getFullAddressForAddressObject(UUID uuid) {
//        StringBuilder sb = new StringBuilder();
//        PreparedStatement preparedStatement = null;
//
//        String sql = "SELECT * FROM ADDROBJ where AOGUID = ?  ";
//        try {
//            Connection connection = getConnection();
//            while (true) {
//                preparedStatement = connection.prepareStatement(sql);
//                preparedStatement.setString(1, uuid.toString());
//
//                ResultSet resultSet = preparedStatement.executeQuery();
//                if (resultSet.next()) {
//                    sb
//                            .append(resultSet.getString("SHORTNAME"))
//                            .append(" ")
//                            .append(resultSet.getString("FORMALNAME"))
//                            .append(" ");
//                    String _parentGuid = resultSet.getString("PARENTGUID");
//                    if (!_parentGuid.equals("")) {
//                        uuid = UUID.fromString(resultSet.getString("PARENTGUID"));
//                        continue;
//                    }
//                    break;
//                }
//                preparedStatement.close();
//            }
//            connection.close();
//        } catch (Exception e) {
//            log.error(e);
//        }
//
//        return sb.toString();
//    }

    @Override
    public List<HouseInterval> findHouseInterval(UUID parent) {
        List<HouseInterval> result = new ArrayList<>();

        String sql = "SELECT * FROM HOUSEINT where AOGUID = ? and ENDDATE<NOW()";// usual sql query
        try {
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setString(1, parent.toString());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                HouseInterval s = new HouseInterval(UUID.fromString(resultSet.getString("INTGUID")));
                s.setIntStart(resultSet.getInt("INTSTART"));
                s.setIntEnd(resultSet.getInt("INTEND"));
                s.setIntStatus(resultSet.getInt("INTSTATUS"));
                s.setAoGuid(UUID.fromString(resultSet.getString("AOGUID")));

                result.add(s);
            }
            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            log.error(e);
        }
        return result;
    }

    protected String getHouseTableName(int regionCode) {
        return "HOUSE" + String.valueOf(regionCode);
    }

    protected IAddressObject mapAddressObject(ResultSet r, AddressObject o) throws SQLException {

        //TODO: use  mapping lib
        o.setAoGuid(UUID.fromString(r.getString("AOGUID")));
        o.setAoId(UUID.fromString(r.getString("AOID")));
        o.setFormalName(r.getString("FORMALNAME"));
        if (r.getString("PARENTGUID") != null && !r.getString("PARENTGUID").isEmpty()) {
            o.setParentGuid(UUID.fromString(r.getString("PARENTGUID")));
        }
        o.setRegionCode(r.getInt("REGIONCODE"));
        o.setActStatus(r.getInt("ACTSTATUS"));
        o.setShortName(r.getString("SHORTNAME"));
        o.setFullAddressString(getFullAddressForAddressObject(UUID.fromString(r.getString("AOGUID"))));
        return o;
    }

    @Override
    public void setLimitSelectRegion(int recordsAmountLimit) {
        this.limitSelectRegion = recordsAmountLimit;
    }

    @Override
    public void setLimitSelectCity(int recordsAmountLimit) {
        this.limitSelectCity = recordsAmountLimit;
    }

    @Override
    public void setLimitSelectStreet(int recordsAmountLimit) {
        this.limitSelectStreet = recordsAmountLimit;
    }

    @Override
    public void setLimitSelectHouse(int recordsAmountLimit) {
        this.limitSelectHouse = recordsAmountLimit;
    }

    private String produceLimitCondition(int recordsAmountLimit) {
        return " limit " + recordsAmountLimit;
    }

    private Connection getConnection() throws SQLException {
        return ds.getConnection();
    }

    public String getFullAddressForAddressObject(UUID uuid){
        StringBuilder sb = new StringBuilder();
        List <AddressObject> parents = new ArrayList<>();
        String sql = "WITH RECURSIVE child_to_parents AS (" +
                "  SELECT addrobj.* FROM addrobj" +
                "      WHERE aoguid = ? and actstatus = ?" +
                "  UNION ALL" +
                "  SELECT addrobj.* FROM addrobj, child_to_parents" +
                "      WHERE addrobj.aoguid = child_to_parents.parentguid" +
                "        AND addrobj.actstatus = ?" +
                ")" +
                "SELECT * FROM child_to_parents ORDER BY aolevel";
        try {
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, uuid.toString());

            String activeObject = "1";
            preparedStatement.setString(2, activeObject);
            preparedStatement.setString(3, activeObject);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                sb.append(resultSet.getString("FORMALNAME")).append(" ");
                sb.append(resultSet.getString("SHORTNAME"));
                sb.append(", ");
            }
            sb.setLength(sb.length() > 0 ? sb.length() - 2 : sb.length());
            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            log.error(e);
        }
        return sb.toString();
    }
}
