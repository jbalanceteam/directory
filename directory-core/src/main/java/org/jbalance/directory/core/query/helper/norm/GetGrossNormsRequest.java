package org.jbalance.directory.core.query.helper.norm;

import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.query.GetRequest;

/**
 *
 * @author Mihail
 */
public class GetGrossNormsRequest extends GetRequest<GetGrossNormsResult>{
    private static final long serialVersionUID = 1L;

    private LongFilter idFilter;
    private LongFilter netNormFilter;

    @Override
    public GetGrossNormsResult createResult()
    {
        return new GetGrossNormsResult(this);
    }

    public LongFilter getIdFilter() {
        return idFilter;
    }

    public void setIdFilter(LongFilter idFilter) {
        this.idFilter = idFilter;
    }
    
    public LongFilter getNetNormFilter() {
        return netNormFilter;
    }

    public void setNetNormFilter(LongFilter netNormFilter) {
        this.netNormFilter = netNormFilter;
    }
    
    public void setProductFilter(){
        
    }
}
