package org.jbalance.directory.core.model.flowchart;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import org.jbalance.directory.core.model.BaseEntity;
import org.jbalance.directory.core.model.measure.Measure;
import org.jbalance.directory.core.model.product.ProductImage;

/**
 * Компонент в технологической карте
 */
@Entity
public class FlowChartImage extends BaseEntity
{

    @ManyToOne
    private ProductImage image;

    @ManyToOne
    private Measure measure;

    @OneToMany
    private List<ColdProcessingLossPercent> proccessingLosses;

    @Column
    private float quantiy;

    public FlowChartImage()
    {
    }

    /**
     * Set the value of image
     *
     * @param newVar the new value of image
     */
    public void setImage(ProductImage newVar)
    {
        image = newVar;
    }

    /**
     * Get the value of image
     *
     * @return the value of image
     */
    public ProductImage getImage()
    {
        return image;
    }

    /**
     * Set the value of measure
     *
     * @param newVar the new value of measure
     */
    public void setMeasure(Measure newVar)
    {
        measure = newVar;
    }

    /**
     * Get the value of measure
     *
     * @return the value of measure
     */
    public Measure getMeasure()
    {
        return measure;
    }

    /**
     * Set the value of quantiy
     *
     * @param newVar the new value of quantiy
     */
    public void setQuantiy(float newVar)
    {
        quantiy = newVar;
    }

    /**
     * Get the value of quantiy
     *
     * @return the value of quantiy
     */
    private float getQuantiy()
    {
        return quantiy;
    }
}
