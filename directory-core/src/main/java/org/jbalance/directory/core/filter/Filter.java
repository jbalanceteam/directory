package org.jbalance.directory.core.filter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;

import org.jbalance.directory.core.util.Str;

/**
 *
 * @param <F>
 * @param <V>
 * 
 */
public abstract class Filter<F extends FilterType, V> implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    private boolean negated;

    @XmlTransient
    public abstract F getFilterType();

    public abstract void setFilterType(F filterType);

    @XmlTransient
    public abstract List<V> getFilterValues();

    public abstract void setFilterValues(List<V> filterValues);

    @XmlTransient
    public V getFilterValue() {
        return getFilterValues().isEmpty() ? null : getFilterValues().get(0);
    }

    public void setFilterValue(V filterValue) {
        List<V> filterValues = new ArrayList<V>();
        filterValues.add(filterValue);
        setFilterValues(filterValues);
    }

    @XmlAttribute(required = false)
    public boolean isNegated() {
        return negated;
    }

    public void setNegated(boolean negated) {
        this.negated = negated;
    }

    public void appendFilter(StringBuilder query, FilterParams params, String fieldName) {
        if(query == null) {
            throw new IllegalArgumentException("Query string builder is required");
        }

        if(params == null) {
            throw new IllegalArgumentException("Filter parameters are required");
        }

        if(Str.empty(fieldName)) {
            throw new IllegalArgumentException("Field name is required");
        }

        if(isNegated()) {
            query.append(" not");
        }

        F filterType = getFilterType();
        if(filterType == null) {
            throw new IllegalStateException("Filter must have a filter type");
        }

        filterType.appendFilter(query, params, fieldName, getFilterValues());
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder(getClass().getSimpleName());
        string.append("(");
        string.append("filterType=").append(getFilterType());
        string.append(", filterValues={").append(Str.list(getFilterValues(), null, false)).append("}");
        string.append(", negated=").append(negated);
        return string.toString();
    }

    public static <F extends Filter<?, ?>> F not(F filter) {
        try {
            @SuppressWarnings("unchecked")
            F clone = (F)filter.clone();
            clone.setNegated(!filter.isNegated());
            return clone;
        } catch(CloneNotSupportedException cnse) {
            throw new RuntimeException(cnse);
        }
    }
}
