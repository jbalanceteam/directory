package org.jbalance.directory.core.model.flowchart;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import org.jbalance.directory.core.model.BaseEntity;
import org.jbalance.directory.core.model.product.Packing;

/**
 *
 * Объект представляет технологическую карту созания фасовки
 * 
 * 
 * @author Komov Roman <komov.r@gmail.com>
 */
@Entity
public class PackingFlowChart extends BaseEntity {

	private String name;

    @Column(length = 5000)
    private String description;
    
    @ManyToOne
    private Packing targetPacking;

    public PackingFlowChart() {
    }

	public String getName(){
        return name;
    }
    
    public void setName(String name){
        this.name = name;
    }

	public String getDescription(){
        return description;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public Packing getTargetPacking() {
        return targetPacking;
    }

    public void setTargetPacking(Packing targetPacking) {
        this.targetPacking = targetPacking;
    }
}
