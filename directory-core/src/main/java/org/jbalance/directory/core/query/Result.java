package org.jbalance.directory.core.query;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;

/*
 * This was named Response, but that led to conflicts with JAX-WS's default response naming convention of methodName + "Response" 
 * for the children. That can be overridden for each method but rather than do that I just renamed the Response to Result 
 * for simpler maintenance.
 */
public abstract class Result implements Serializable {

    private static final long serialVersionUID = 1L;
    private String correlationId;

    @XmlAttribute
    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }
    
    public Result() {
    }

    public Result(Request<?> request) {
        this.correlationId = request.getCorrelationId();
    }
}