
package org.jbalance.directory.core.query.helper.flowchart;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.jbalance.directory.core.model.flowchart.FlowChartProductImage;
import org.jbalance.directory.core.query.GetResult;

@XmlRootElement
public class GetFlowChartImagesResult extends GetResult
{
    private static final long serialVersionUID = 1L;

    private List<FlowChartProductImage> flowChartImages;

    public GetFlowChartImagesResult()
    {
    }

    @XmlElement(name = "flowChartImages")
    public List<FlowChartProductImage> getFlowChartImages()
    {
        if (flowChartImages == null)
        {
            setFlowChartImages(new ArrayList<FlowChartProductImage>());
        }
        return flowChartImages;
    }


    public GetFlowChartImagesResult(GetFlowChartImagesRequest request)
    {
        super(request);
    }

    public void setFlowChartImages(List<FlowChartProductImage> flowChartImages) {
        this.flowChartImages = flowChartImages;
    }
}
