package org.jbalance.directory.core.session.norm;

import org.jbalance.directory.core.model.norm.NormGroup;
import org.jbalance.directory.core.query.helper.norm.GetNormGroupRequest;
import org.jbalance.directory.core.session.GenericSession;

/**
 *
 * @author Mihail
 */
public interface NormGroupSession extends GenericSession<NormGroup, GetNormGroupRequest>{
    
}
