package org.jbalance.directory.core.session.norm;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import org.jbalance.directory.core.model.norm.GrossNorm;
import org.jbalance.directory.core.query.helper.norm.GetGrossNormsRequest;
import org.jbalance.directory.core.query.helper.norm.GetGrossNormsRequestHelper;
import org.jbalance.directory.core.query.helper.norm.GetGrossNormsResult;
import org.jbalance.directory.core.session.GenericSessionBean;

/**
 *
 * @author Mihail
 */
@Stateless
public class GrossNormSessionBean extends GenericSessionBean<GrossNorm,GetGrossNormsRequest> implements GrossNormSession{
    @Override
    public List<GrossNorm> list(GetGrossNormsRequest request)
    {
        GetGrossNormsResult result = new GetGrossNormsRequestHelper(request).createResult(manager);
        return result.getGrossNorms();
    }

    @Override
    @PostConstruct
    public void init() {
        beanClass = GrossNorm.class;
    }
}
