package org.jbalance.directory.core.query.helper.norm;

import org.jbalance.directory.core.model.norm.NetNorm;
import org.jbalance.directory.core.query.helper.AbstractQueryBuilder;

/**
 *
 * @author Mihail
 */
public class NetNormQueryBuilder extends AbstractQueryBuilder<NetNormQueryBuilder>{
    public NetNormQueryBuilder() {
        super(null, NetNorm.class, "netNorm");
    }

    @Override
    public StringBuilder buildJoins() {
        StringBuilder joins = new StringBuilder();
        return joins;
    }

    @Override
    protected NetNormQueryBuilder get() {
        return this;
    }
}
