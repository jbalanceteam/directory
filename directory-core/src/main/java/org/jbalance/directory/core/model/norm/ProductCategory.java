package org.jbalance.directory.core.model.norm;

import javax.persistence.Column;
import javax.persistence.Entity;
import org.jbalance.directory.core.model.BaseEntity;

/**
 * @author Mihail
 * описывает категории продуктов
 */
@Entity
public class ProductCategory extends BaseEntity{
    
    @Column(nullable = false)
    private String name;
    
    private String description;
    
    public String getName(){
        return name;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public String getDescription(){
        return description;
    }
    
    public void setDescription(String description){
        this.description = description;
    }
    
    public ProductCategory(){}
    
}
