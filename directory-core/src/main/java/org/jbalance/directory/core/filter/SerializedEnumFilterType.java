package org.jbalance.directory.core.filter;

import java.util.List;

import org.jbalance.directory.core.util.Str;

public enum SerializedEnumFilterType implements FilterType {
    /**
     * <p>Is null.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> IS NULL}.</tt></p>
     */
    NULL("Is Null", ArgumentsExpected.NONE),
    /**
     * <p>In.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> IN (<i>rvalue</i>[0],
     * <i>rvalue</i>[1], ...)</tt></p>
     */
    IN("In", ArgumentsExpected.ONE_OR_MORE);

    private final String description;
    private final ArgumentsExpected argumentsExpected;

    /**
     * Returns a user-friendly description of the comparison operator
     *
     * @return a user-friendly description of the comparison operator
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * Returns the number of arguments expected by this filter type
     *
     * @return number of arguments expected by this filter type
     */
    @Override
    public ArgumentsExpected getArgumentsExpected() {
        return argumentsExpected;
    }

    /**
     * Creates a new comparison with the specified description (internal use
     * only)
     *
     * @param description A user-friendly description of the comparison
     * operator
     */
    private SerializedEnumFilterType(String description, ArgumentsExpected argumentsExpected) {
        this.description = description;
        this.argumentsExpected = argumentsExpected;
    }

    /**
     * Returns the user-friendly description of the comparison.  This returns
     * the same value as {@link #getDescription()}
     *
     * @return the user-friendly description of the comparison
     *
     * @see #getDescription()
     */
    @Override
    public String toString() {
        return description;
    }

    @Override
    public void appendFilter(StringBuilder query, FilterParams params, String fieldName, List<?> filterValues) {
        if(query == null) {
            throw new IllegalArgumentException("Query string builder is required");
        }

        if(params == null) {
            throw new IllegalArgumentException("Filter parameters are required");
        }

        if(Str.empty(fieldName)) {
            throw new IllegalArgumentException("Field name is required");
        }

        String paramName = null;
        if(this == NULL) {
            query.append(" ");
            query.append(fieldName);
            query.append(" is null");
        } else if(this == IN) {
            if(filterValues == null || filterValues.isEmpty()) {
                // IN (empty list) will always evaluate to false, but we will
                // get a query syntax exception if the parameter list is
                // missing.  To avoid this, substitute with an expression that
                // will always evaluate to false.
                query.append(" 1=0");
            } else {
                paramName = params.addParam(filterValues);
                query.append(" ");
                query.append(fieldName);
                query.append(" in (:");
                query.append(paramName);
                query.append(")");
            }
        } else {
            throw new UnsupportedOperationException("Unsupported filter type: " + this);
        }
    }

    public StringFilterType toStringFilterType() {
        return StringFilterType.valueOf(this.name());
    }
}
