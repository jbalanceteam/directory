package org.jbalance.directory.core.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class SecurityUtil {
    public static byte[] computeHash(String cleartext) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("MD5");
        digest.update(cleartext.getBytes());
        return digest.digest();
    }
}
