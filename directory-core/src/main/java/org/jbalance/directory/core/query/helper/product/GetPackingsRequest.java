package org.jbalance.directory.core.query.helper.product;

import javax.xml.bind.annotation.XmlType;
import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.query.GetRequest;

/**
 *
 * @author Mihail
 */
@XmlType(propOrder
        = {
            "idFilter",
            "nameFilter",
            "imageFilter"
        })
public class GetPackingsRequest extends GetRequest<GetPackingsResult>{
	private static final long serialVersionUID = 1L;

    private LongFilter idFilter;

    private StringFilter nameFilter;
	
    private LongFilter imageFilter;

    @Override
    public GetPackingsResult createResult() {
        return new GetPackingsResult(this);
    }

    public LongFilter getIdFilter() {
        return idFilter;
    }

    public void setIdFilter(LongFilter idFilter) {
        this.idFilter = idFilter;
    }

    public StringFilter getNameFilter() {
        return nameFilter;
    }

    public void setNameFilter(StringFilter nameFilter) {
        this.nameFilter = nameFilter;
    }

    public LongFilter getImageFilter() {
        return imageFilter;
    }

    public void setImageFilter(LongFilter imageFilter) {
        this.imageFilter = imageFilter;
    }


}
