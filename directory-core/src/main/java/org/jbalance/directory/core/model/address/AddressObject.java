package org.jbalance.directory.core.model.address;

import java.io.Serializable;

import java.util.UUID;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class AddressObject implements IAddressObject, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Глобальный уникальный идентификатор адресного объекта
     */
    protected UUID aoGuid;

    /**
     * Формализованное наименование
     */
    protected String formalName;

    /**
     * Код региона
     */
    protected int regionCode;

    /**
     * Код автономии
     */
    protected String autoCode;

    /**
     * Код района
     */
    protected String areaCode;

    /**
     * Код города
     */
    protected String cityCode;

    /**
     * Код внутригородского района
     */
    protected String ctarCode;

    /**
     * Код насленного пункта
     */
    protected String placeCode;

    /**
     * Код улицы
     */
    protected String streetCode;

    /**
     * Код дополнительного адресообразующего элемента
     */
    protected String extrCode;

    /**
     * Код подчиненного дополнительного адресообразующего элемента
     */
    protected String sextCode;

    /**
     * Официальное наименование
     */
    protected String offName;

    /**
     * Краткое наименование типа объекта
     */
    protected String shortName;

    /**
     * Уровень адресного объекта
     */
    protected Integer aoLevel;

    /**
     * Идентификатор объекта родительского объекта
     */
    protected UUID parentGuid;

    /**
     * Уникальный идентификатор записи. Ключевое поле
     *
     *
     */
    protected UUID aoId;

    /**
     * Идентификатор записи связывания с предыдушей исторической записью
     */
    protected UUID prevId;

    /**
     * Идентификатор записи связывания с последующей исторической записью
     */
    protected UUID nextId;

    /**
     * Код адресного объекта одной строкой с признаком актуальности из КЛАДР 4.0
     */
    protected String code;

    /**
     * "Код адресного объекта из КЛАДР 4.0 одной строкой без признака
     * актуальности (последних двух цифр)")
     */
    protected String plainCode;

    /**
     * Статус актуальности адресного объекта ФИАС
     */
    protected Integer actStatus;

    /**
     * Статус центра
     */
    protected Integer centStatus;
    /**
     * "Статус действия над записью – причина появления записи"
     */

    protected Integer operStatus;

    /**
     * Статус актуальности КЛАДР 4 (последние две цифры в коде)
     */
    protected Integer currStatus;

    /**
     * Признак действующего адресного объекта
     */
    protected Byte liveStatus;

    protected String fullAddressString;

    public AddressObject()
    {
    }

    public AddressObject(UUID aoGuid)
    {
        this.aoGuid = aoGuid;
    }

    @Override
    public UUID getAoGuid()
    {
        return aoGuid;
    }

    @Override
    public void setAoGuid(UUID aoGuid)
    {
        this.aoGuid = aoGuid;
    }

    @Override
    public String getFormalName()
    {
        return formalName;
    }

    @Override
    public void setFormalName(String formalName)
    {
        this.formalName = formalName;
    }

    @Override
    public int getRegionCode()
    {
        return regionCode;
    }

    @Override
    public void setRegionCode(int regionCode)
    {
        this.regionCode = regionCode;
    }

    @Override
    public String getAutoCode()
    {
        return autoCode;
    }

    @Override
    public void setAutoCode(String autoCode)
    {
        this.autoCode = autoCode;
    }

    @Override
    public String getAreaCode()
    {
        return areaCode;
    }

    @Override
    public void setAreaCode(String areaCode)
    {
        this.areaCode = areaCode;
    }

    @Override
    public String getCityCode()
    {
        return cityCode;
    }

    @Override
    public void setCityCode(String cityCode)
    {
        this.cityCode = cityCode;
    }

    @Override
    public String getCtarCode()
    {
        return ctarCode;
    }

    @Override
    public void setCtarCode(String ctarCode)
    {
        this.ctarCode = ctarCode;
    }

    @Override
    public String getPlaceCode()
    {
        return placeCode;
    }

    @Override
    public void setPlaceCode(String placeCode)
    {
        this.placeCode = placeCode;
    }

    @Override
    public String getStreetCode()
    {
        return streetCode;
    }

    @Override
    public void setStreetCode(String streetCode)
    {
        this.streetCode = streetCode;
    }

    @Override
    public String getExtrCode()
    {
        return extrCode;
    }

    @Override
    public void setExtrCode(String extrCode)
    {
        this.extrCode = extrCode;
    }

    @Override
    public String getSextCode()
    {
        return sextCode;
    }

    @Override
    public void setSextCode(String sextCode)
    {
        this.sextCode = sextCode;
    }

    @Override
    public String getOffName()
    {
        return offName;
    }

    @Override
    public void setOffName(String offName)
    {
        this.offName = offName;
    }

    @Override
    public String getShortName()
    {
        return shortName;
    }

    @Override
    public void setShortName(String shortName)
    {
        this.shortName = shortName;
    }

    @Override
    public Integer getAoLevel()
    {
        return aoLevel;
    }

    @Override
    public void setAoLevel(Integer aoLevel)
    {
        this.aoLevel = aoLevel;
    }

    @Override
    public UUID getParentGuid()
    {
        return parentGuid;
    }

    @Override
    public void setParentGuid(UUID parentGuid)
    {
        this.parentGuid = parentGuid;
    }

    @Override
    public UUID getAoId()
    {
        return aoId;
    }

    @Override
    public void setAoId(UUID aoId)
    {
        this.aoId = aoId;
    }

    @Override
    public UUID getPrevId()
    {
        return prevId;
    }

    @Override
    public void setPrevId(UUID prevId)
    {
        this.prevId = prevId;
    }

    @Override
    public UUID getNextId()
    {
        return nextId;
    }

    @Override
    public void setNextId(UUID nextId)
    {
        this.nextId = nextId;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public void setCode(String code)
    {
        this.code = code;
    }

    @Override
    public String getPlainCode()
    {
        return plainCode;
    }

    @Override
    public void setPlainCode(String plainCode)
    {
        this.plainCode = plainCode;
    }

    @Override
    public Integer getActStatus()
    {
        return actStatus;
    }

    @Override
    public void setActStatus(Integer actStatus)
    {
        this.actStatus = actStatus;
    }

    @Override
    public Integer getCentStatus()
    {
        return centStatus;
    }

    @Override
    public void setCentStatus(Integer centStatus)
    {
        this.centStatus = centStatus;
    }

    @Override
    public Integer getOperStatus()
    {
        return operStatus;
    }

    @Override
    public void setOperStatus(Integer operStatus)
    {
        this.operStatus = operStatus;
    }

    @Override
    public Integer getCurrStatus()
    {
        return currStatus;
    }

    @Override
    public void setCurrStatus(Integer currStatus)
    {
        this.currStatus = currStatus;
    }

    @Override
    public Byte getLiveStatus()
    {
        return liveStatus;
    }

    @Override
    public void setLiveStatus(Byte liveStatus)
    {
        this.liveStatus = liveStatus;
    }

    @Override
    public String toString()
    {
        return this.formalName + " " + this.aoGuid + " " + this.parentGuid + " " + this.actStatus;
    }

    public String getFullAddressString() {
        return fullAddressString;
    }

    public void setFullAddressString(String fullAddressString) {
        this.fullAddressString = fullAddressString;
    }
}
