package org.jbalance.directory.core.model.measure;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.UniqueConstraint;
import org.jbalance.directory.core.model.BaseEntity;

/**
 * Единица измерения
 */
@Entity
public class Measure extends BaseEntity{

	@Column(unique = true)
    private String name;
	
	@Column(name = "rus_name1")
	private String rusShortName1;
	
	@Column(name = "rus_name2")
	private String rusShortName2;
	
	@Column(name = "eng_name1")
	private String engShortName1;
	
	@Column(name = "eng_name2")
	private String engShortName2;
	
	@Column(name = "number_code")
	private String numberCode;
	
	@Enumerated(EnumType.STRING)
	private MeasureGroup measureGroup;
	
	@Enumerated(EnumType.STRING)
	private MeasureType measureType;
    
    public String getName(){
        return name;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
	public String getRusShortName1(){
        return rusShortName1;
    }
    
    public void setRusShortName1(String rusShortName1){
        this.rusShortName1 = rusShortName1;
    }
	
	public String getRusShortName2(){
        return rusShortName2;
    }
    
    public void setRusShortName2(String rusShortName2){
        this.rusShortName2 = rusShortName2;
    }
	
	public String getEngShortName1(){
        return engShortName1;
    }
    
    public void setEngShortName1(String engShortName1){
        this.engShortName1 = engShortName1;
    }
	
	public String getEngShortName2(){
        return engShortName2;
    }
    
    public void setEngShortName2(String engShortName2){
        this.engShortName2 = engShortName2;
    }
	
	public String getNumberCode(){
        return numberCode;
    }
    
    public void setNumberCode(String numberCode){
        this.numberCode = numberCode;
    }
    
	public MeasureGroup getMeasureGroup(){
		return measureGroup;
	}
	
	public void setMeasureGroup(MeasureGroup measureGroup){
		this.measureGroup = measureGroup;
	}
	
	public MeasureType getMeasureType(){
		return measureType;
	}
	
	public void setMeasureType(MeasureType measureType){
		this.measureType = measureType;
	}
}
