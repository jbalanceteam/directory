package org.jbalance.directory.core.filter;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author jsweetland
 */
public class IntegerFilter extends Filter<NumberFilterType, Integer> {
    private static final long serialVersionUID = 1L;

    private NumberFilterType filterType;
    private List<Integer> filterValues;

    public IntegerFilter() {}

    public IntegerFilter(NumberFilterType filterType, List<Integer> filterValues) {
        this.filterType = filterType;
        this.filterValues = filterValues;
    }

    public IntegerFilter(NumberFilterType filterType, Integer filterValue) {
        setFilterType(filterType);
        setFilterValue(filterValue);
    }

    @XmlAttribute(required=true)
    @Override
    public NumberFilterType getFilterType() {
        return filterType;
    }

    @Override
    public void setFilterType(NumberFilterType filterType) {
        this.filterType = filterType;
    }

    @XmlElement(name="filterValue")
    @Override
    public List<Integer> getFilterValues() {
        if (filterValues == null) {
            filterValues = new ArrayList<Integer>();
        }
        return filterValues;
    }

    @Override
    public void setFilterValues(List<Integer> filterValues) {
        this.filterValues = filterValues;
    }
}
