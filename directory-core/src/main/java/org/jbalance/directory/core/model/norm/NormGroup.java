package org.jbalance.directory.core.model.norm;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import org.jbalance.directory.core.model.BaseEntity;

/**
 *
 * @author Mihail
 */
@Entity
public class NormGroup extends BaseEntity{
	
	/**
	 * название группы норм
	 */
	private String name;
	
	/**
	 * Дата начала действия
	 */
	private Date openDate;
	
	/**
	 * Дата окончания действия
	 */
	private Date closeDate;
    
    /**
     * указывает, для какой возрастной группы данная норма
     */
    @ManyToOne
    private AgeBracket ageBracket;
    
     /**
     * режим пребывания в заведении
     */
    @ManyToOne
    private Stay stay;
    
    @OneToMany(mappedBy = "normGroup")
    private Set<NetNorm> netNorms;
    
	public String getName(){
        return name;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
	public Date getOpenDate(){
        return openDate;
    }
    
	public String getFormattedOpenDate(){
		String result = null;
		if (openDate != null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			result = sdf.format(openDate);
		}
        return result;
    }
    
    public void setOpenDate(Date openDate){
        this.openDate = openDate;
    }
    
	public Date getCloseDate(){
        return closeDate;
    }
    
	public String getFormattedCloseDate(){
		String result = null;
		if (closeDate != null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			result = sdf.format(closeDate);
		}
        return result;
    }
    
    public void setCloseDate(Date closeDate){
        this.closeDate = closeDate;
    }
	
    public AgeBracket getAgeBracket(){
        return ageBracket;
    }
    
    public void setAgeBracket(AgeBracket ageBracket){
        this.ageBracket = ageBracket;
    }
    
    public Stay getStay(){
        return stay;
    }
    
    public void setStay(Stay stay){
        this.stay = stay;
    }
    
    public Set<NetNorm> getNetNorms(){
        return netNorms;
    }
}
