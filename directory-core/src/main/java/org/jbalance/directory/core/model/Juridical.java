
package org.jbalance.directory.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 * Сущность описывающая контрагентов
 */
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public class Juridical extends Contractor {
    
	private static final long serialVersionUID = 1L;

	/**
     * Юридический адрес
     */
    @Column 
    private String registeredOffice;
    
    /**
     * основной государственный регистрационный номер
     */
    @Column 
    private String ogrn;
    
    /**
     * Общесоюзный классификатор отраслей народного хозяйства
     */
    @Column
    private String okonh;
    
    /**
     * Общероссийский классификатор предприятий и организаций
     */
    @Column
    private String okpo;
        
    public Juridical() {
    	contractorType = ContractorType.JURIDICAL;
    }  

    public String getRegisteredOffice() {
        return registeredOffice;
    }

    public void setRegisteredOffice(String registeredOffice) {
        this.registeredOffice = registeredOffice;
    }
    
    public String getOgrn() {
        return ogrn;
    }

    public void setOgrn(String ogrn) {
        this.ogrn = ogrn;
    }
    
    public String getOkonh() {
        return okonh;
    }

    public void setOkonh(String okonh) {
        this.okonh = okonh;
    }
    
    public String getOkpo() {
        return okpo;
    }

    public void setOkpo(String okpo) {
        this.okpo = okpo;
    }
       
}
