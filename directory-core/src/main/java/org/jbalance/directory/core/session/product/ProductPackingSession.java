package org.jbalance.directory.core.session.product;

import org.jbalance.directory.core.model.product.ProductPacking;
import org.jbalance.directory.core.query.helper.product.GetProductPackingsRequest;
import org.jbalance.directory.core.session.GenericSession;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public interface ProductPackingSession extends GenericSession<ProductPacking, GetProductPackingsRequest>
{
}
