PATH_TO_EXTRACTED_FILES="/var/lib/pgsql/fias_update/extracted_dbfs";
PATH_TO_TMP_FILES="/var/lib/pgsql/fias_update/renamed_dbfs";
CURR_PATH="/var/lib/pgsql/fias_update";
ST_FILE="$CURR_PATH/st.sql";
RAR_FILE="$CURR_PATH/fias_delta_dbf.rar";

echo "delete old files";
rm -f "$PATH_TO_EXTRACTED_FILES"/*;
rm -f "$PATH_TO_TMP_FILES"/*;
rm -f "$ST_FILE";
unrar e "$RAR_FILE" "$PATH_TO_EXTRACTED_FILES" || exit "$?";
echo "copy dbf-s to tmp dir";
cp "$PATH_TO_EXTRACTED_FILES"/* "$PATH_TO_TMP_FILES/" || exit "$?";
echo "update tables:";

echo "BEGIN;" >> "$ST_FILE";

ls "$PATH_TO_TMP_FILES" |  grep '^ADDROBJ.DBF$\|^HOUSEINT.DBF$\|^LANDMARK.DBF$\|^HOUSE[0-9][0-9].DBF$' | while read i;
do
	tmp_name="${i/.DBF/_TMP.DBF}";
	table_name="${i/.DBF/}";
	tmp_table_name="${tmp_name/.DBF/}";
	memofile="${i/.DBF/.DBT}";
	mv "$PATH_TO_TMP_FILES/$i" "$PATH_TO_TMP_FILES/$tmp_name";
	echo "import $tmp_name to pg";
	
	if [ -e "$PATH_TO_TMP_FILES/$memofile" ]; then
		pgdbf -m "$PATH_TO_TMP_FILES/$memofile" "$PATH_TO_TMP_FILES/$tmp_name" | iconv -c -f cp866 -t UTF-8 | psql -U postgres -d fias_db || exit "$?";
	else
		pgdbf "$PATH_TO_TMP_FILES/$tmp_name" | iconv -c -f cp866 -t UTF-8 | psql -U postgres -d fias_db || exit "$?";
	fi;
	id_field=0;
	case "$i" in
		ADDROBJ.DBF) id_field="aoid";;
   		HOUSEINT.DBF) id_field="houseintid";;
		LANDMARK.DBF) id_field="landid";;
		HOUSE[0-9][0-9].DBF) id_field="houseid";;
	esac;
	echo "
		delete from $table_name where $id_field IN (select $id_field from $tmp_table_name);
		insert into $table_name	(select * from $tmp_table_name);
		TRUNCATE TABLE $tmp_table_name;
	" >> "$ST_FILE";
done;

ls "$PATH_TO_TMP_FILES" | grep '^DADDROBJ.DBF$\|^DHOUSINT.DBF$\|^DHOUSE.DBF$' | while read i;
do
	echo "import $i to pg";
	pgdbf "$PATH_TO_TMP_FILES/$i" | iconv -c -f cp866 -t UTF-8 | psql -U postgres -d fias_db || exit "$?";
done;

echo "
	delete from addrobj where aoid in (select aoid from DADDROBJ);
	delete from houseint where houseintid in (select houseintid from DHOUSINT);
" >> "$ST_FILE";

psql -U postgres -d fias_db -c "SELECT table_name FROM information_schema.tables WHERE table_schema='public'"|
	grep "^ house[0-9][0-9]$" | while read i;
	do
		echo "delete from $i where houseid in (select houseid from dhouse);">>"$ST_FILE";
	done;
echo "commit;" >> "$ST_FILE";
echo "updating database:";
psql -U postgres -d fias_db -f "$ST_FILE" || exit "$?";

ls "$PATH_TO_TMP_FILES" | 
	grep '^SOCRBASE.DBF$\|^CURENTST.DBF$\|^ACTSTAT.DBF$\|^OPERSTAT.DBF$\|^CENTERST.DBF$\|^INTVSTAT.DBF$\|^HSTSTAT.DBF$\|^ESTSTAT.DBF$\|^STRSTAT.DBF$'|
	while read i;
	do
		echo "import $i to pg";
		pgdbf "$PATH_TO_TMP_FILES/$i" | iconv -c -f cp866 -t UTF-8 | psql -U postgres -d fias_db || exit "$?";
	done;
	
date -r fias_delta_dbf.rar >> "$CURR_PATH/updates_history";

rm -f "$PATH_TO_EXTRACTED_FILES"/*;
rm -f "$PATH_TO_TMP_FILES"/*;
rm -f "$ST_FILE";

echo "done";