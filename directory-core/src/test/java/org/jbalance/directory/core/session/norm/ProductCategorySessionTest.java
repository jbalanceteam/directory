package org.jbalance.directory.core.session.norm;

import java.util.List;
import javax.inject.Inject;
import junit.framework.Assert;
import org.jbalance.directory.core.model.norm.ProductCategory;
import org.jbalance.directory.core.session.BaseTest;
import org.junit.Test;

/**
 *
 * @author Mihail
 */
public class ProductCategorySessionTest extends BaseTest{
    
    @Inject
    ProductCategorySession session;
    
    @Test
    public void testProductCategorySessionDeployed(){
        org.junit.Assert.assertNotNull(session);
    }
    
    /**
     * Test of add method
     */
    @Test
    public void testAdd() {
        final ProductCategory productCategory = createTestRecord();

        final ProductCategory saved = session.add(productCategory);
        
        System.out.println(session.getById(productCategory.getId()).getName());
        Assert.assertNotNull(saved);
//        ID должен быть установлен автоматически
        Assert.assertNotNull(saved.getId());
//      Дата создания должна быть установлена автоматически
        Assert.assertNotNull(saved.getEnteredDate());
        Assert.assertEquals(productCategory.getName(), saved.getName());
    }

    private ProductCategory createTestRecord() {
        final ProductCategory productCategory = new ProductCategory();
        productCategory.setName("some name");
        return productCategory;
    }
//    /**
//     * Test of get method
//     */
    @Test
    public void testGet() {
    	final ProductCategory productCategory = createTestRecord();
    	
        session.add(productCategory);
        final ProductCategory result = session.getById(productCategory.getId());
        Assert.assertNotNull(result);
//        ID должен быть установлен автоматически
        Assert.assertNotNull(result.getId());
//        Имена должены совпадать
        Assert.assertEquals(productCategory.getName(), result.getName());
    }

    @Test
    public void testDelete() {
    	final ProductCategory productCategory = createTestRecord();
        session.add(productCategory);
        final ProductCategory deleted = session.delete(productCategory.getId());
        Assert.assertTrue(deleted.isDeleted());
    }

    @Test
    public void testEdit() {
    	final ProductCategory productCategory = createTestRecord();
        session.add(productCategory);
        final ProductCategory eBeforeEdit = session.getById(productCategory.getId());
        productCategory.setName("other name");
        final ProductCategory eAfterUpdate = session.edit(productCategory);
//        должны существовать
        Assert.assertNotNull(eBeforeEdit);
        Assert.assertNotNull(eAfterUpdate);
//        имена должны совпадать
        Assert.assertEquals(eBeforeEdit.getId(), eAfterUpdate.getId());
        Assert.assertNotSame(eBeforeEdit.getName(), eAfterUpdate.getName());
    }
    /**
     * Test of getAll method.
     */
    @Test
    public void testGetAll() {
    	final ProductCategory productCategory = createTestRecord();
        session.add(productCategory);
        List<ProductCategory> productCategories = session.getAll();
        Assert.assertFalse(productCategories.isEmpty());
    }
}
