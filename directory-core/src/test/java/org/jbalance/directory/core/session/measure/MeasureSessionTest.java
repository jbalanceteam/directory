package org.jbalance.directory.core.session.measure;

import java.util.List;
import javax.inject.Inject;
import junit.framework.Assert;
import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.NumberFilterType;
import org.jbalance.directory.core.model.measure.Measure;
import org.jbalance.directory.core.query.helper.measure.GetMeasuresRequest;
import org.jbalance.directory.core.session.BaseTest;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Mihail
 */
public class MeasureSessionTest extends BaseTest{
	
	public MeasureSessionTest() {
    }
    
    @Inject
    protected MeasureSession measureSession;

    
    @Test
    public void testMeasureSessionDeployed(){
      org.junit.Assert.assertNotNull(measureSession);
    }

	/**
     * Test of add method
     */
    @Test
    public void testAdd() {
        final Measure measure = new Measure();
        measure.setName("mes1");

        final Measure saved = measureSession.add(measure);
        Assert.assertNotNull(saved);
//        ID должен быть установлен автоматически
        Assert.assertNotNull(saved.getId());
//      Дата создания должна быть установлена автоматически
        Assert.assertNotNull(saved.getEnteredDate());
//        Имена должны совпадать 
        Assert.assertEquals(measure.getName(), saved.getName());
    }
//    /**
//     * Test of get method
//     */
    @Test
    public void testGet() {
        final Measure measure = new Measure();
        measure.setName("mes2");
        measureSession.add(measure);
        final Measure result = measureSession.getById(measure.getId());
        Assert.assertNotNull(result);
//        ID должен быть установлен автоматически
        Assert.assertNotNull(result.getId());
//        Имена должены совпадать
        Assert.assertEquals(measure.getName(), result.getName());
    }
    /**
     * Test of delete method
     */
    @Test
    public void testDelete() {
        final Measure measure = new Measure();
        measure.setName("name");
        measureSession.add(measure);
        final Measure deleted = measureSession.delete(measure.getId());
//        Имена должены совпадать
        Assert.assertEquals(measure.getName(), deleted.getName());
//        должен быть помечен, как удаленный
        Assert.assertTrue(deleted.isDeleted());
    }
//
//    /**
//     * Test of update method
//     */
    @Test
    public void testEdit() {
        final Measure measure = new Measure();
        measure.setName("name2");
        measureSession.add(measure);
        final Measure eBeforeEdit = measureSession.getById(measure.getId());
        measure.setName("name3");
        final Measure eAfterUpdate = measureSession.edit(measure);
//        должны существовать
        Assert.assertNotNull(eBeforeEdit);
        Assert.assertNotNull(eAfterUpdate);
        Assert.assertEquals(eBeforeEdit.getId(), eAfterUpdate.getId());
        Assert.assertTrue(eBeforeEdit.getName().equals("name2"));
        Assert.assertTrue(eAfterUpdate.getName().equals("name3"));
    }
    
    @Ignore
    public void testGetMeasureList() {
        for (int i = 10; i < 20; i++)
        {
            final Measure measure = new Measure();
            measureSession.add(measure);
        }
        GetMeasuresRequest request = new GetMeasuresRequest();
        request.setIdFilter(new LongFilter(NumberFilterType.GT, new Long("0")));
        request.setFirstRecord(3);
        request.setMaxRecords(5);
        List<Measure> measures = measureSession.list(request);
//        должны существовать
        Assert.assertFalse(measures.isEmpty());
        
        Assert.assertTrue(measures.size()==5);
    }
  
}
