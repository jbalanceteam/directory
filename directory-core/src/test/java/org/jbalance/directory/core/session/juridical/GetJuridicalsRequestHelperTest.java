package org.jbalance.directory.core.session.juridical;

import javax.inject.Inject;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.filter.StringFilterType;
import org.jbalance.directory.core.model.Juridical;
import org.jbalance.directory.core.query.helper.juridical.GetJuridicalsRequest;
import org.jbalance.directory.core.query.helper.juridical.GetJuridicalsRequestHelper;
import org.jbalance.directory.core.query.helper.juridical.GetJuridicalsResult;
import org.jbalance.directory.core.session.BaseTest;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Тест хелпера юр.лиц
 * @author Alexandr Chubenko
 */
public class GetJuridicalsRequestHelperTest extends BaseTest{
    
    @Inject
    protected JuridicalSession juridicalSession;
    
    @Before
    public void setup(){
        final Juridical juridical = new Juridical();
        juridical.setName("name1");
        juridicalSession.add(juridical);
    }

    @Test
    public void getJuridicalByNameTest(){
        GetJuridicalsRequest request = new GetJuridicalsRequest();
        request.setJuridicalNameFilter(new StringFilter(StringFilterType.EQUALS, "name1"));
        GetJuridicalsResult result = new GetJuridicalsRequestHelper(request).createResult(em);
        assertNotNull(result);
        assertNotNull(result.getJuridicals());
        assertTrue(result.getJuridicals().size()==1);
        System.out.println("Juridical by name: "+result.getJuridicals().get(0).getName());
    }
}
