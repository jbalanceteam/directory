package org.jbalance.directory.core.session.norm;

import java.util.List;
import javax.inject.Inject;
import junit.framework.Assert;
import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.NumberFilterType;
import org.jbalance.directory.core.model.norm.NetNorm;
import org.jbalance.directory.core.model.norm.NormGroup;
import org.jbalance.directory.core.query.helper.norm.GetNetNormsRequest;
import org.jbalance.directory.core.session.BaseTest;
import org.junit.Test;

/**
 *
 * @author Mihail
 */
public class NetNormSessionTest extends BaseTest{
    
    public NetNormSessionTest() {
    }
    
    @Inject
    protected NetNormSession netNormSession;
    
    @Inject
    protected NormGroupSession normGroupSession;

    
    
    @Test
    public void testNetNormSessionDeployed(){
      org.junit.Assert.assertNotNull(netNormSession);
    }
    
    /**
     * Test of add method
     */
    @Test
    public void testAdd() {
        final NetNorm netNorm = new NetNorm();
        netNorm.setQuantity(new Float(10));

        final NetNorm saved = netNormSession.add(netNorm);
        Assert.assertNotNull(saved);
//        ID должен быть установлен автоматически
        Assert.assertNotNull(saved.getId());
//      Дата создания должна быть установлена автоматически
        Assert.assertNotNull(saved.getEnteredDate());
//        Имена должны совпадать 
        Assert.assertEquals(netNorm.getQuantity(), saved.getQuantity());
    }
//    /**
//     * Test of get method
//     */
    @Test
    public void testGet() {
        final NetNorm netNorm = new NetNorm();
        netNorm.setQuantity(new Float(10));
        netNormSession.add(netNorm);
        final NetNorm result = netNormSession.getById(netNorm.getId());
        Assert.assertNotNull(result);
//        ID должен быть установлен автоматически
        Assert.assertNotNull(result.getId());
//        Имена должены совпадать
        Assert.assertEquals(netNorm.getQuantity(), result.getQuantity());
    }
    /**
     * Test of delete method
     */
    @Test
    public void testDelete() {
        final NetNorm netNorm = new NetNorm();
        netNorm.setQuantity(new Float(10));
        netNormSession.add(netNorm);
        final NetNorm deleted = netNormSession.delete(netNorm.getId());
//        Имена должены совпадать
        Assert.assertEquals(netNorm.getQuantity(), deleted.getQuantity());
//        должен быть помечен, как удаленный
        Assert.assertTrue(deleted.isDeleted());
    }
//
//    /**
//     * Test of update method
//     */
    @Test
    public void testEdit() {
        final NetNorm netNorm = new NetNorm();
        netNorm.setQuantity(new Float(0));
        netNormSession.add(netNorm);
        final NetNorm eBeforeEdit = netNormSession.getById(netNorm.getId());
        netNorm.setQuantity(new Float(10));
        final NetNorm eAfterUpdate = netNormSession.edit(netNorm);
//        должны существовать
        Assert.assertNotNull(eBeforeEdit);
        Assert.assertNotNull(eAfterUpdate);
        Assert.assertEquals(eBeforeEdit.getId(), eAfterUpdate.getId());
        Assert.assertTrue(eBeforeEdit.getQuantity() == 0);
        Assert.assertTrue(eAfterUpdate.getQuantity() == 10);
    }
    
    
//    @Test
//    public void testGetUpdatesAfterDate() {
//        final NetNorm netNorm = new NetNorm();
//        netNorm.setQuantity(10);
//        netNormSession.add(netNorm);
//        Date date = new Date();
//        date.setTime(date.getTime() - 1000*60*60*24);
//        List<NetNorm> netNorms = netNormSession.getUpdates(date);
////        должны существовать
//        Assert.assertFalse(netNorms.isEmpty());
//    }
    
    @Test
    public void testGetGrossNormsList() {
        for (int i = 10; i < 20; i++)
        {
            final NetNorm netNorm = new NetNorm();
            netNorm.setQuantity(new Float(10 + i));
            netNormSession.add(netNorm);
        }
        GetNetNormsRequest request = new GetNetNormsRequest();
        request.setIdFilter(new LongFilter(NumberFilterType.GT, new Long("0")));
        request.setFirstRecord(3);
        request.setMaxRecords(5);
        List<NetNorm> netNorms = netNormSession.list(request);
//        должны существовать
        Assert.assertFalse(netNorms.isEmpty());
        
        Assert.assertTrue(netNorms.size()==5);
    }
    
    @Test
    public void testNormGroupFilter(){
        NormGroup normGroup = new NormGroup();
        normGroup = normGroupSession.add(normGroup);
        
        NetNorm netNorm = new NetNorm();
        netNorm.setNormGroup(normGroup);
        netNormSession.add(netNorm);
        
        GetNetNormsRequest getNetNormsRequest = new GetNetNormsRequest();
        getNetNormsRequest.setNormGroupFilter(new LongFilter(NumberFilterType.EQUALS, normGroup.getId()));
        List<NetNorm> list = netNormSession.list(getNetNormsRequest);
        Assert.assertFalse(list.isEmpty());
    }
}
