package org.jbalance.directory.core.session.flowchart;

import java.util.List;
import javax.inject.Inject;
import junit.framework.Assert;
import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.NumberFilterType;
import org.jbalance.directory.core.model.flowchart.PackingFlowChartPacks;
import org.jbalance.directory.core.query.helper.flowchart.GetPackingFlowChartPacksRequest;
import org.jbalance.directory.core.session.BaseTest;
import org.junit.Test;

/**
 *
 * @author Mihail
 */
public class PackingFlowChartPacksSessionTest extends BaseTest{
	
	public PackingFlowChartPacksSessionTest() {
    }
    
    @Inject
    protected PackingFlowChartPacksSession packingFlowChartPacksSession;

    
    @Test
    public void testPackingFlowChartPacksSessionDeployed(){
      Assert.assertNotNull(packingFlowChartPacksSession);
    }

	/**
     * Test of add method
     */
    @Test
    public void testAdd() {
        final PackingFlowChartPacks packingFlowChartPacks = new PackingFlowChartPacks();
        packingFlowChartPacks.setQuantity((float)10);

        final PackingFlowChartPacks saved = packingFlowChartPacksSession.add(packingFlowChartPacks);
        Assert.assertNotNull(saved);
//        ID должен быть установлен автоматически
        Assert.assertNotNull(saved.getId());
//      Дата создания должна быть установлена автоматически
        Assert.assertNotNull(saved.getEnteredDate());
//        Имена должны совпадать 
        Assert.assertEquals(packingFlowChartPacks.getQuantity(), saved.getQuantity());
    }
//    /**
//     * Test of get method
//     */
    @Test
    public void testGet() {
        final PackingFlowChartPacks packingFlowChartPacks = new PackingFlowChartPacks();
        packingFlowChartPacks.setQuantity((float)10);
        packingFlowChartPacksSession.add(packingFlowChartPacks);
        final PackingFlowChartPacks result = packingFlowChartPacksSession.getById(packingFlowChartPacks.getId());
        Assert.assertNotNull(result);
//        ID должен быть установлен автоматически
        Assert.assertNotNull(result.getId());
//        Имена должены совпадать
        Assert.assertEquals(packingFlowChartPacks.getQuantity(), result.getQuantity());
    }
    /**
     * Test of delete method
     */
    @Test
    public void testDelete() {
        final PackingFlowChartPacks packingFlowChartPacks = new PackingFlowChartPacks();
        packingFlowChartPacks.setQuantity((float)10);
        packingFlowChartPacksSession.add(packingFlowChartPacks);
        final PackingFlowChartPacks deleted = packingFlowChartPacksSession.delete(packingFlowChartPacks.getId());
//        Имена должены совпадать
        Assert.assertEquals(packingFlowChartPacks.getQuantity(), deleted.getQuantity());
//        должен быть помечен, как удаленный
        Assert.assertTrue(deleted.isDeleted());
    }
//
//    /**
//     * Test of update method
//     */
    @Test
    public void testEdit() {
        final PackingFlowChartPacks packingFlowChartPacks = new PackingFlowChartPacks();
        packingFlowChartPacks.setQuantity((float)10);
        packingFlowChartPacksSession.add(packingFlowChartPacks);
        final PackingFlowChartPacks eBeforeEdit = packingFlowChartPacksSession.getById(packingFlowChartPacks.getId());
        packingFlowChartPacks.setQuantity((float)20);
        final PackingFlowChartPacks eAfterUpdate = packingFlowChartPacksSession.edit(packingFlowChartPacks);
//        должны существовать
        Assert.assertNotNull(eBeforeEdit);
        Assert.assertNotNull(eAfterUpdate);
        Assert.assertEquals(eBeforeEdit.getId(), eAfterUpdate.getId());
        Assert.assertTrue(eBeforeEdit.getQuantity().equals((float)10));
        Assert.assertTrue(eAfterUpdate.getQuantity().equals((float)20));
    }
    
    @Test
    public void testGetPackingFlowChartPacksList() {
        for (int i = 10; i < 20; i++)
        {
            final PackingFlowChartPacks packingFlowChartPacks = new PackingFlowChartPacks();
            packingFlowChartPacksSession.add(packingFlowChartPacks);
        }
        GetPackingFlowChartPacksRequest request = new GetPackingFlowChartPacksRequest();
        request.setIdFilter(new LongFilter(NumberFilterType.GT, new Long("0")));
        request.setFirstRecord(3);
        request.setMaxRecords(5);
        List<PackingFlowChartPacks> packingFlowChartPackss = packingFlowChartPacksSession.list(request);
//        должны существовать
        Assert.assertFalse(packingFlowChartPackss.isEmpty());
        
        Assert.assertTrue(packingFlowChartPackss.size()==5);
    }
}
