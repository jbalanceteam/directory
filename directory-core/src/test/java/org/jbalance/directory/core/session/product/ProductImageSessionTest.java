package org.jbalance.directory.core.session.product;

import org.jbalance.directory.core.session.BaseTest;
import java.util.List;
import javax.inject.Inject;
import junit.framework.Assert;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.model.product.ProductImage;
import org.jbalance.directory.core.query.helper.product.GetProductImagesRequest;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class ProductImageSessionTest extends BaseTest {

    @Inject
    protected ProductImageSession session;

    /**
     * Test of add method
     */
    @Test
    public void testAdd() {
        final ProductImage bean = new ProductImage();
        bean.setOkpCode("name1");
        final ProductImage saved = session.add(bean);

        Assert.assertNotNull(saved);
//        ID должен быть установлен автоматически
        Assert.assertNotNull(saved.getId());
//      Дата создания должна быть установлена автоматически
        Assert.assertNotNull(saved.getEnteredDate());
//        Имена должны совпадать
        Assert.assertEquals(bean.getOkpCode(), saved.getOkpCode());
    }

    /**
     * Test of get method.
     */
    @Test
    public void testGet() {
        final ProductImage bean = new ProductImage();
        bean.setOkpCode("name3");
        session.add(bean);

        session.getById(bean.getId());

//        ID должен быть установлен автоматически
        Assert.assertNotNull(bean.getId());

        final ProductImage saved = session.getById(bean.getId());
        Assert.assertEquals(saved.getOkpCode(), bean.getOkpCode());
    }

    @Test
    public void testFilter() {
        final ProductImage bean = new ProductImage();
        bean.setName("uniquename");
        final ProductImage saved = session.add(bean);

        GetProductImagesRequest r = new GetProductImagesRequest();
        r.setNameFilter(new StringFilter("uniquename"));

        List<ProductImage> result = session.list(r);

        assertNotNull(result);
        assertTrue(result.size() >= 1);
    }

    /**
     * Test of delete method, of class JuridicalService.
     */
    @Test
    public void testDelete() {
        final ProductImage bean = new ProductImage();
        bean.setOkpCode("name2");
        session.add(bean);
        final ProductImage deleted = session.delete(bean.getId());
        //        Имена должены совпадать
        Assert.assertEquals(bean.getOkpCode(), deleted.getOkpCode());
        //        должен быть помечен, как удаленный
        Assert.assertTrue(deleted.isDeleted());
    }

    /**
     * Test of update method
     */
    @Test
    public void testEdit() {
        final ProductImage bean = new ProductImage();
        bean.setOkpCode(null);
        session.add(bean);
        final ProductImage before = session.getById(bean.getId());
        bean.setOkpCode("info");
        final ProductImage after = session.edit(bean);
//        должны существовать
        Assert.assertNotNull(before);
        Assert.assertNotNull(after);
//        не должно быть контактной информации
        Assert.assertNull(before.getOkpCode());
//        должна быть контактная информация
        Assert.assertNotNull(after.getOkpCode());
    }

}
