package org.jbalance.directory.core.session.norm;

import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;
import junit.framework.Assert;
import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.NumberFilterType;
import org.jbalance.directory.core.model.norm.GrossNorm;
import org.jbalance.directory.core.model.norm.NetNorm;
import org.jbalance.directory.core.query.helper.norm.GetGrossNormsRequest;
import org.jbalance.directory.core.session.BaseTest;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Mihail
 */
public class GrossNormSessionTest extends BaseTest{
    
    @Inject
    protected GrossNormSession grossNormSession;
    
    @Inject
    protected NetNormSession netNormSession;
    
    protected NetNorm netNorm;
    
    public GrossNormSessionTest() {
    }
    
    @Before
    public void setupRelations(){
        netNorm = new NetNorm();
        netNormSession.add(netNorm);
    }

    @Test
    public void testGrossNormSessionDeployed(){
      org.junit.Assert.assertNotNull(grossNormSession);
    }
    
    /**
     * Test of add method
     */
    @Test
    public void testAdd() {
        final GrossNorm grossNorm = new GrossNorm();
        grossNorm.setQuantity(new Float(10));
        grossNorm.setNetNorm(netNorm);

        final GrossNorm saved = grossNormSession.add(grossNorm);
        
        System.out.println(grossNormSession.getById(grossNorm.getId()).getQuantity());
        Assert.assertNotNull(saved);
//        ID должен быть установлен автоматически
        Assert.assertNotNull(saved.getId());
//      Дата создания должна быть установлена автоматически
        Assert.assertNotNull(saved.getEnteredDate());
//        Имена должны совпадать 
        Assert.assertEquals(grossNorm.getQuantity(), saved.getQuantity());
    }
//    /**
//     * Test of get method
//     */
    @Test
    public void testGet() {
        final GrossNorm grossNorm = new GrossNorm();
        grossNorm.setNetNorm(netNorm);
        grossNorm.setQuantity(new Float(10));
        grossNormSession.add(grossNorm);
        final GrossNorm result = grossNormSession.getById(grossNorm.getId());
        Assert.assertNotNull(result);
//        ID должен быть установлен автоматически
        Assert.assertNotNull(result.getId());
//        Имена должены совпадать
        Assert.assertEquals(grossNorm.getQuantity(), result.getQuantity());
    }
    /**
     * Test of delete method
     */
    @Test
    public void testDelete() {
        final GrossNorm grossNorm = new GrossNorm();
        grossNorm.setNetNorm(netNorm);
        grossNorm.setQuantity(new Float(10));
        grossNormSession.add(grossNorm);
        final GrossNorm deleted = grossNormSession.delete(grossNorm.getId());
//        Имена должены совпадать
        Assert.assertEquals(grossNorm.getQuantity(), deleted.getQuantity());
//        должен быть помечен, как удаленный
        Assert.assertTrue(deleted.isDeleted());
    }
//
//    /**
//     * Test of update method
//     */
    @Test
    public void testEdit() {
        final GrossNorm grossNorm = new GrossNorm();
        grossNorm.setNetNorm(netNorm);
        grossNorm.setQuantity(new Float(0));
        grossNormSession.add(grossNorm);
        final GrossNorm eBeforeEdit = grossNormSession.getById(grossNorm.getId());
        grossNorm.setQuantity(new Float(10));
        final GrossNorm eAfterUpdate = grossNormSession.edit(grossNorm);
//        должны существовать
        Assert.assertNotNull(eBeforeEdit);
        Assert.assertNotNull(eAfterUpdate);
        Assert.assertTrue(((float)eBeforeEdit.getQuantity()) == 0);
        Assert.assertTrue(((float)eAfterUpdate.getQuantity()) == 10);
    }
    
//    @Test
//    public void testGetUpdatesAfterDate() {
//        final GrossNorm grossNorm = new GrossNorm();
//        grossNorm.setQuantity(10);
//        grossNormSession.add(grossNorm);
//        Date date = new Date();
//        date.setTime(date.getTime() - 1000*60*60*24);
//        List<GrossNorm> grossNorms = grossNormSession.getUpdates(date);
////        должны существовать
//        Assert.assertFalse(grossNorms.isEmpty());
//    }
    
    @Test
    public void testGetGrossNormsList() {
        for (int i = 10; i < 20; i++)
        {
            final GrossNorm grossNorm = new GrossNorm();
            grossNorm.setNetNorm(netNorm);
            grossNorm.setQuantity(new Float(10 + i));
            grossNormSession.add(grossNorm);
        }
        GetGrossNormsRequest request = new GetGrossNormsRequest();
        request.setIdFilter(new LongFilter(NumberFilterType.GT, new Long("0")));
        request.setFirstRecord(3);
        request.setMaxRecords(5);
        List<GrossNorm> grossNorms = grossNormSession.list(request);
//        должны существовать
        Assert.assertFalse(grossNorms.isEmpty());
        
        Assert.assertTrue(grossNorms.size()==5);
    }
    
    @Test
    public void testGrossNormsFilter(){
        NetNorm netNorm1 = new NetNorm();
        netNorm1 = netNormSession.add(netNorm1);
        
        NetNorm netNorm2 = new NetNorm();
        netNorm2 = netNormSession.add(netNorm2);
        
        GrossNorm grossNorm1 = new GrossNorm();
        grossNorm1.setNetNorm(netNorm1);
        grossNorm1.setQuantity(new Float(1));
        grossNormSession.add(grossNorm1);

        
        
        GrossNorm grossNorm2 = new GrossNorm();
        grossNorm2.setNetNorm(netNorm2);
        grossNorm2.setQuantity(new Float(2));
        grossNormSession.add(grossNorm2);
        
                
        GrossNorm grossNorm3 = new GrossNorm();
        grossNorm3.setNetNorm(netNorm1);
        grossNorm3.setQuantity(new Float(3));
        grossNormSession.add(grossNorm3);
        
        
                
        GrossNorm grossNorm4 = new GrossNorm();
        grossNorm4.setNetNorm(netNorm2);
        grossNorm4.setQuantity(new Float(4));
        grossNormSession.add(grossNorm4);
        
        
        GetGrossNormsRequest getGrossNormsRequest = new GetGrossNormsRequest();
        getGrossNormsRequest.setNetNormFilter(new LongFilter(NumberFilterType.EQUALS, netNorm2.getId()));
        List<GrossNorm> list = grossNormSession.list(getGrossNormsRequest);
        Assert.assertFalse(list.isEmpty());
        //Assert.assertTrue(list.size() == 2);
    }
}
