package org.jbalance.directory.core.session.flowchart;

import java.util.List;
import javax.inject.Inject;
import junit.framework.Assert;
import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.NumberFilterType;
import org.jbalance.directory.core.model.flowchart.PackingFlowChart;
import org.jbalance.directory.core.model.product.Packing;
import org.jbalance.directory.core.model.product.ProductImage;
import org.jbalance.directory.core.query.helper.flowchart.GetPackingFlowChartRequest;
import org.jbalance.directory.core.session.BaseTest;
import org.jbalance.directory.core.session.product.PackingSession;
import org.jbalance.directory.core.session.product.ProductImageSession;
import org.junit.Test;

/**
 *
 * @author Mihail
 */
public class PackingFlowChartSessionTest extends BaseTest{
	
	public PackingFlowChartSessionTest() {
    }
    
    @Inject
    protected PackingFlowChartSession packingFlowChartSession;
    
    @Inject
    protected ProductImageSession productImageSession;
    
    @Inject
    protected PackingSession packingSession;

    
    @Test
    public void testPackingFlowChartSessionDeployed(){
      Assert.assertNotNull(packingFlowChartSession);
    }

	/**
     * Test of add method
     */
    @Test
    public void testAdd() {
        final PackingFlowChart packingFlowChart = new PackingFlowChart();
        packingFlowChart.setName("mes1");

        final PackingFlowChart saved = packingFlowChartSession.add(packingFlowChart);
        Assert.assertNotNull(saved);
//        ID должен быть установлен автоматически
        Assert.assertNotNull(saved.getId());
//      Дата создания должна быть установлена автоматически
        Assert.assertNotNull(saved.getEnteredDate());
//        Имена должны совпадать 
        Assert.assertEquals(packingFlowChart.getName(), saved.getName());
    }
//    /**
//     * Test of get method
//     */
    @Test
    public void testGet() {
        final PackingFlowChart packingFlowChart = new PackingFlowChart();
        packingFlowChart.setName("mes2");
        packingFlowChartSession.add(packingFlowChart);
        final PackingFlowChart result = packingFlowChartSession.getById(packingFlowChart.getId());
        Assert.assertNotNull(result);
//        ID должен быть установлен автоматически
        Assert.assertNotNull(result.getId());
//        Имена должены совпадать
        Assert.assertEquals(packingFlowChart.getName(), result.getName());
    }
    /**
     * Test of delete method
     */
    @Test
    public void testDelete() {
        final PackingFlowChart packingFlowChart = new PackingFlowChart();
        packingFlowChart.setName("name");
        packingFlowChartSession.add(packingFlowChart);
        final PackingFlowChart deleted = packingFlowChartSession.delete(packingFlowChart.getId());
//        Имена должены совпадать
        Assert.assertEquals(packingFlowChart.getName(), deleted.getName());
//        должен быть помечен, как удаленный
        Assert.assertTrue(deleted.isDeleted());
    }
//
//    /**
//     * Test of update method
//     */
    @Test
    public void testEdit() {
        final PackingFlowChart packingFlowChart = new PackingFlowChart();
        packingFlowChart.setName("name2");
        packingFlowChartSession.add(packingFlowChart);
        final PackingFlowChart eBeforeEdit = packingFlowChartSession.getById(packingFlowChart.getId());
        packingFlowChart.setName("name3");
        final PackingFlowChart eAfterUpdate = packingFlowChartSession.edit(packingFlowChart);
//        должны существовать
        Assert.assertNotNull(eBeforeEdit);
        Assert.assertNotNull(eAfterUpdate);
        Assert.assertEquals(eBeforeEdit.getId(), eAfterUpdate.getId());
        Assert.assertTrue(eBeforeEdit.getName().equals("name2"));
        Assert.assertTrue(eAfterUpdate.getName().equals("name3"));
    }
    
    @Test
    public void testGetPackingFlowChartList() {
        Packing packing = new Packing();
        packing.setName("name1");
        ProductImage productImage = new ProductImage();
        productImage.setName("name1");
        packing.setImage(productImage);
        productImageSession.add(productImage);
        packingSession.add(packing);
        for (int i = 10; i < 20; i++)
        {
            final PackingFlowChart packingFlowChart = new PackingFlowChart();
            packingFlowChart.setTargetPacking(packing);
            packingFlowChart.setName("name" + i);
            packingFlowChartSession.add(packingFlowChart);
        }
        GetPackingFlowChartRequest request = new GetPackingFlowChartRequest();
        request.setIdFilter(new LongFilter(NumberFilterType.GT, new Long("0")));
        request.setFirstRecord(3);
        request.setMaxRecords(5);
        List<PackingFlowChart> packingFlowCharts = packingFlowChartSession.list(request);
//        должны существовать
        Assert.assertFalse(packingFlowCharts.isEmpty());
        
        Assert.assertTrue(packingFlowCharts.size()==5);
    }
}