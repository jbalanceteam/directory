package org.jbalance.directory.core.session.product;

import java.util.Date;
import javax.inject.Inject;
import junit.framework.Assert;
import org.jbalance.directory.core.model.product.Product;
import org.jbalance.directory.core.model.product.TradeMark;
import org.jbalance.directory.core.session.BaseTest;
import org.junit.Test;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class TradeMarkSessionTest extends BaseTest
{

    @Inject
    protected TradeMarkSessionBean session;

    @Inject
    protected ProductSession productSession;

    /**
     * Test of add method
     */
    @Test
    public void testAdd()
    {
        final TradeMark bean = new TradeMark();

        session.add(bean);

        Assert.assertNotNull(bean);

        Assert.assertNotNull(bean.getId());

        Assert.assertNotNull(bean.getEnteredDate());

        final Product product = new Product();
        product.setTradeMark(bean);
        productSession.add(product);

        final TradeMark saved = session.getById(bean.getId());

        // NO session error here
        //Assert.assertNotNull(bean.getImages());
    }

    /**
     * Test of get method.
     */
    @Test
    public void testGet()
    {
        final TradeMark bean = new TradeMark();
        session.add(bean);

        session.getById(bean.getId());

//        ID должен быть установлен автоматически
        Assert.assertNotNull(bean.getId());

        final TradeMark saved = session.getById(bean.getId());
        Assert.assertEquals(saved.getId(), bean.getId());
    }

    /**
     * Test of delete method, of class JuridicalService.
     */
    @Test
    public void testDelete()
    {
        final TradeMark bean = new TradeMark();
        session.add(bean);
        final TradeMark deleted = session.delete(bean.getId());
        //        Имена должены совпадать
        Assert.assertEquals(bean.getId(), deleted.getId());
        //        должен быть помечен, как удаленный
        Assert.assertTrue(deleted.isDeleted());
    }

    /**
     * Test of update method
     */
    @Test
    public void testEdit()
    {
        final TradeMark bean = new TradeMark();
        session.add(bean);
        final TradeMark before = session.getById(bean.getId());
        bean.setEnteredDate(new Date());
        final TradeMark after = session.edit(bean);

        Assert.assertNotNull(before);
        Assert.assertNotNull(after);

        Assert.assertNotSame(before.getEnteredDate(), after.getEnteredDate());
    }
}
