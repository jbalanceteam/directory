package org.jbalance.directory.core.session.product;

import java.util.List;
import javax.inject.Inject;
import junit.framework.Assert;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.model.product.Packing;
import org.jbalance.directory.core.query.helper.product.GetPackingsRequest;
import org.jbalance.directory.core.session.BaseTest;
import org.junit.Test;

/**
 *
 * @author Mihail
 */
public class PackingSessionTest extends BaseTest {
	@Inject
    protected PackingSession session;

    /**
     * Test of add method
     */
    @Test
    public void testAdd() {
        final Packing bean = new Packing();
        bean.setName("name1");
        final Packing saved = session.add(bean);

        Assert.assertNotNull(saved);
//        ID должен быть установлен автоматически
        Assert.assertNotNull(saved.getId());
//      Дата создания должна быть установлена автоматически
        Assert.assertNotNull(saved.getEnteredDate());
//        Имена должны совпадать
        Assert.assertEquals(bean.getName(), saved.getName());
    }

    /**
     * Test of get method.
     */
    @Test
    public void testGet() {
        final Packing bean = new Packing();
        bean.setName("name3");
        session.add(bean);

        session.getById(bean.getId());

//        ID должен быть установлен автоматически
        Assert.assertNotNull(bean.getId());

        final Packing saved = session.getById(bean.getId());
        Assert.assertEquals(saved.getName(), bean.getName());
    }

    @Test
    public void testFilter() {
        final Packing bean = new Packing();
        bean.setName("uniquename");
        final Packing saved = session.add(bean);

        GetPackingsRequest r = new GetPackingsRequest();
        r.setNameFilter(new StringFilter("uniquename"));

        List<Packing> result = session.list(r);

        Assert.assertNotNull(result);
        Assert.assertTrue(result.size() >= 1);
    }

    /**
     * Test of delete method, of class JuridicalService.
     */
    @Test
    public void testDelete() {
        final Packing bean = new Packing();
        bean.setName("name2");
        session.add(bean);
        final Packing deleted = session.delete(bean.getId());
        //        Имена должены совпадать
        Assert.assertEquals(bean.getName(), deleted.getName());
        //        должен быть помечен, как удаленный
        Assert.assertTrue(deleted.isDeleted());
    }

    /**
     * Test of update method
     */
    @Test
    public void testEdit() {
        final Packing bean = new Packing();
        bean.setName(null);
        session.add(bean);
        final Packing before = session.getById(bean.getId());
        bean.setName("info");
        final Packing after = session.edit(bean);
//        должны существовать
        Assert.assertNotNull(before);
        Assert.assertNotNull(after);
//        не должно быть контактной информации
        Assert.assertNull(before.getName());
//        должна быть контактная информация
        Assert.assertNotNull(after.getName());
    }

}
