package org.jbalance.directory.core.session.product;

import org.jbalance.directory.core.session.BaseTest;
import javax.inject.Inject;
import junit.framework.Assert;
import org.jbalance.directory.core.model.product.ProductImage;
import org.jbalance.directory.core.model.product.ProductPacking;
import org.jbalance.directory.core.model.product.Product;
import org.jbalance.directory.core.model.product.TradeMark;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class ProductSessionTest extends BaseTest
{

    @Inject
    protected ProductSession productsession;

    @Inject
    protected ProductImageSession imageSession;

    @Inject
    protected ProductPackingSession packingSession;

    @Inject
    protected TradeMarkSessionBean tradeMarkSession;

    protected ProductImage image;

    protected ProductPacking packing;

    protected TradeMark tradeMark;

    @Before
    public void setupRelations()
    {
        image = new ProductImage();
        imageSession.add(image);

        packing = new ProductPacking();
        packingSession.add(packing);

        tradeMark = new TradeMark();
        tradeMarkSession.add(tradeMark);
    }

    /**
     * Test of add method
     */
    @Test
    public void testAdd()
    {
        final Product p = new Product();

        p.setImage(image);
        p.setTradeMark(tradeMark);

        productsession.add(p);

        //bean persisted correctly
        Assert.assertNotNull(p);
        Assert.assertNotNull(p.getId());
        Assert.assertNotNull(p.getEnteredDate());

        packing.setProduct(p);
        packingSession.edit(packing);

        final Product saved = productsession.getById(p.getId());

        Assert.assertEquals(saved.getImage().getId(), image.getId());

        //bean relations bound correctly
        Assert.assertEquals(saved.getImage().getId(), image.getId());
        Assert.assertEquals(saved.getTradeMark().getId(), tradeMark.getId());

        //need to solve "no session"  problem for lazy load
        //Assert.assertEquals(saved.getPackings().iterator().next().getName(), packing.getName());
    }

    /**
     * Test of delete method, of class JuridicalService.
     */
    @Test
    public void testDelete()
    {
        final Product bean = new Product();
        productsession.add(bean);
        Product deleted = productsession.delete(bean.getId());

        Assert.assertTrue(deleted.isDeleted());
    }

    @Test
    public void testEdit()
    {
        final Product p = new Product();

        p.setImage(image);
        p.setTradeMark(tradeMark);

        productsession.add(p);

        Assert.assertNotNull(p);
        Assert.assertNotNull(p.getId());

        p.setTradeMark(null);

        Product p1 = productsession.edit(p);

        Assert.assertNull(p1.getTradeMark());
    }
}
