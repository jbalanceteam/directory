package org.jbalance.directory.core.session.juridical;

import java.util.List;
import javax.inject.Inject;
import junit.framework.Assert;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.filter.StringFilterType;
import org.jbalance.directory.core.model.Juridical;
import org.jbalance.directory.core.query.helper.juridical.GetJuridicalsRequest;
import org.jbalance.directory.core.session.BaseTest;
import org.junit.Test;

/**
 * Тестируем JuridicalSession EJB
 * @author Mihail
 */
public class JuridicalSessionTest extends BaseTest{
    
    @Inject
    protected JuridicalSession juridicalSession;
    
    public JuridicalSessionTest() {
    }
    
    @Test
    public void testJuridicalSessionDeployed(){
      org.junit.Assert.assertNotNull(juridicalSession);
    }
    
    /**
     * Test of add method, of class JuridicalSession
     */
    @Test
    public void testAdd() {
        final Juridical juridical = new Juridical();
        juridical.setName("name1");

        final Juridical saved = juridicalSession.add(juridical);
        Assert.assertNotNull(saved);
//        ID должен быть установлен автоматически
        Assert.assertNotNull(saved.getId());
//      Дата создания должна быть установлена автоматически
        Assert.assertNotNull(saved.getEnteredDate());
//        Имена должны совпадать 
        Assert.assertEquals(juridical.getName(), saved.getName());
    }
//    /**
//     * Test of get method, of class JuridicalService.
//     */
    @Test
    public void testGet() {
//    	TODO ...
        final Juridical juridical = new Juridical();
        juridical.setName("name3");
        juridicalSession.add(juridical);
        final Juridical result = juridicalSession.getById(juridical.getId());
        Assert.assertNotNull(result);
//        ID должен быть установлен автоматически
        Assert.assertNotNull(result.getId());
//        Имена должены совпадать
        Assert.assertEquals(juridical.getName(), result.getName());
    }
    /**
     * Test of delete method, of class JuridicalService.
     */
    @Test
    public void testDelete() {
//       TODO написать тест, для проверки "мягкого" (soft) удаления
        final Juridical juridical = new Juridical();
        juridical.setName("name2");
        juridicalSession.add(juridical);
        final Juridical deleted = juridicalSession.delete(juridical.getId());
//        Имена должены совпадать
        Assert.assertEquals(juridical.getName(), deleted.getName());
//        должен быть помечен, как удаленный
        Assert.assertTrue(deleted.isDeleted());
        
        Assert.assertNotNull(deleted.getDeletedDate());
    }
//
//    /**
//     * Test of update method, of class JuridicalService.
//     */
    @Test
    public void testEdit() {
        final Juridical juridical = new Juridical();
        juridical.setName("name4");
        juridical.setContactInfo(null);
        juridicalSession.add(juridical);
        Assert.assertEquals(juridical.getEnteredDate(), juridical.getModifiedDate());
        
        final Juridical jBeforeEdit = juridicalSession.getById(juridical.getId());
        juridical.setContactInfo("info");
        final Juridical jAfterUpdate = juridicalSession.edit(juridical);
//        должны существовать
        Assert.assertNotNull(jBeforeEdit);
        Assert.assertNotNull(jAfterUpdate);
//        имена должны совпадать
        Assert.assertEquals(jBeforeEdit.getName(), jAfterUpdate.getName());
//        не должно быть контактной информации
        Assert.assertNull(jBeforeEdit.getContactInfo());
//        должна быть контактная информация
        Assert.assertNotNull(jAfterUpdate.getContactInfo());
        
        Assert.assertFalse(jAfterUpdate.getEnteredDate().equals(jAfterUpdate.getModifiedDate()));
    }
    
//    @Test
//    public void testGetUpdatesAfterDate() {
//        final Juridical juridical = new Juridical();
//        juridical.setName("name5");
//        juridicalSession.add(juridical);
//        Date date = new Date();
//        date.setTime(date.getTime() - 1000*60*60*24);
//        List<Juridical> juridicals = juridicalSession.getUpdates(date);
////        должны существовать
//        Assert.assertFalse(juridicals.isEmpty());
//    }
    
    @Test
    public void testGetJuridicalsList() {
        for (int i = 10; i < 20; i++)
        {
            final Juridical juridical = new Juridical();
            juridical.setName("name" + i);
            juridicalSession.add(juridical);
        }
        GetJuridicalsRequest request = new GetJuridicalsRequest();
        request.setJuridicalNameFilter(new StringFilter(StringFilterType.STARTS_WITH, "name"));
        request.setFirstRecord(3);
        request.setMaxRecords(5);
        List<Juridical> juridicals = juridicalSession.list(request);
//        должны существовать
        Assert.assertFalse(juridicals.isEmpty());
        
        Assert.assertTrue(juridicals.size()==5);
    }
}
