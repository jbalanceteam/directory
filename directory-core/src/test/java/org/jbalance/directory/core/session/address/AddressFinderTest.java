package org.jbalance.directory.core.session.address;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;
import javax.naming.NamingException;

import org.jbalance.directory.core.model.address.*;
import org.jbalance.directory.core.session.BaseTest;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class AddressFinderTest extends BaseTest {

    @Inject
    AddressFinder adr;
	
//	@Inject
//	private DatasourceDefinitionBean datasourceDefinitionBean;
	
//	@Test
//	public void testDs(){
//		Assert.assertNotNull(datasourceDefinitionBean);
//		Assert.assertNotNull(datasourceDefinitionBean.getFiasDataSource());
//	}

    @Ignore
    @Test
    public void testFind() throws NamingException, SQLException  {
        
        String region = "Ростовская";
        String city = "Ростов";
        String street = "Б";
        String house = "";

//        AddressFinderManager mgr = new AddressFinderManager("address");
//        IAddressFinder adr = mgr.getFinder();
        List<Region> reg = adr.findRegion(region);
        Iterator<Region> regionIterator = reg.iterator();
        while (regionIterator.hasNext()) {
            Region nextRegion = regionIterator.next();
            System.out.println(adr.getFullAddressForAddressObject(nextRegion.getAoGuid()));

            List<City> c = adr.findCity(city, nextRegion.getRegionCode());
            Iterator<City> i = c.iterator();
            while (i.hasNext()) {
                City _c = (City) i.next();
                //System.out.println(_c);
                System.out.println(adr.getFullAddressForAddressObject(_c.getAoGuid()));

                List<Street> s = adr.findStreet(street, _c.getAoGuid());
                Iterator<Street> is = s.iterator();
                while (is.hasNext()) {
                    IStreet _s = is.next();
                    //System.out.println(_s);
                    System.out.println(adr.getFullAddressForAddressObject(_s.getAoGuid()));

                    List<House> h = adr.findHouse(house, _s.getAoGuid());
                    Iterator<House> ih = h.iterator();
                    while (ih.hasNext()) {
                        IHouse __h = ih.next();

                        //System.out.println(__h);
                        System.out.println(adr.getFullAddressForHouse(__h.getHouseGuid(), _s.getRegionCode()));
                    }

                    List<HouseInterval> hi = adr.findHouseInterval(_s.getAoGuid());
                    Iterator<HouseInterval> ihi = hi.iterator();
                    while (ihi.hasNext()) {
                        IHouseInterval __hi = ihi.next();
                        //System.out.println(ihi.next());
                        System.out.println(adr.getFullAddressForHouseInterval(__hi.getHouseIntId(), "1"));
                    }
                }
            }
        }
    }
}
