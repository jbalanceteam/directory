package org.jbalance.directory.core.session.norm;

import java.util.List;
import javax.inject.Inject;
import junit.framework.Assert;
import org.jbalance.directory.core.model.norm.AgeBracket;
import org.jbalance.directory.core.session.BaseTest;
import org.junit.Test;

/**
 *
 * @author Mihail
 */
public class AgeBracketSessionTest extends BaseTest{
    
    @Inject
    AgeBracketSession session;
    

    @Test
    public void testAgeBracketSessionDeployed(){
      org.junit.Assert.assertNotNull(session);
    }
    
    /**
     * Test of add method
     */
    @Test
    public void testAdd() {
        final AgeBracket ageBracket = createTestRecord();

        final AgeBracket saved = session.add(ageBracket);
        
        System.out.println(session.getById(ageBracket.getId()).getMaximumAge());
        Assert.assertNotNull(saved);
//        ID должен быть установлен автоматически
        Assert.assertNotNull(saved.getId());
//      Дата создания должна быть установлена автоматически
        Assert.assertNotNull(saved.getEnteredDate());
        Assert.assertEquals(ageBracket.getName(), saved.getName());
        Assert.assertEquals(ageBracket.getMaximumAge(), saved.getMaximumAge());
        Assert.assertEquals(ageBracket.getMinimumAge(), saved.getMinimumAge());
    }

	private AgeBracket createTestRecord() {
		final AgeBracket ageBracket = new AgeBracket();
        ageBracket.setName("some name");
        ageBracket.setMaximumAge((short) 2);
        ageBracket.setMinimumAge((short) 10);
		return ageBracket;
	}
//    /**
//     * Test of get method
//     */
    @Test
    public void testGet() {
    	final AgeBracket ageBracket = createTestRecord();
    	
        session.add(ageBracket);
        final AgeBracket result = session.getById(ageBracket.getId());
        Assert.assertNotNull(result);
//        ID должен быть установлен автоматически
        Assert.assertNotNull(result.getId());
//        Имена должены совпадать
        Assert.assertEquals(ageBracket.getMaximumAge(), result.getMaximumAge());
    }

    @Test
    public void testDelete() {
    	final AgeBracket ageBracket = createTestRecord();
        session.add(ageBracket);
        final AgeBracket deleted = session.delete(ageBracket.getId());
        Assert.assertTrue(deleted.isDeleted());
    }

    @Test
    public void testEdit() {
    	final AgeBracket ageBracket = createTestRecord();
    	ageBracket.setMaximumAge((short)0);
        session.add(ageBracket);
        final AgeBracket eBeforeEdit = session.getById(ageBracket.getId());
        ageBracket.setMaximumAge((short) 10);
        final AgeBracket eAfterUpdate = session.edit(ageBracket);
//        должны существовать
        Assert.assertNotNull(eBeforeEdit);
        Assert.assertNotNull(eAfterUpdate);
//        имена должны совпадать
        Assert.assertEquals(eBeforeEdit.getId(), eAfterUpdate.getId());
        Assert.assertTrue(eBeforeEdit.getMaximumAge() == 0);
        Assert.assertTrue(eAfterUpdate.getMaximumAge() == 10);
    }
    /**
     * Test of getAll method, of class AgeBracketSessionBean.
     */
    @Test
    public void testGetAll() {
    	final AgeBracket ageBracket = createTestRecord();
        session.add(ageBracket);
        List<AgeBracket> ageBrackets = session.getAll();
        Assert.assertFalse(ageBrackets.isEmpty());
    }
    
}
