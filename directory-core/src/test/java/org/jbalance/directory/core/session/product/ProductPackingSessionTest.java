package org.jbalance.directory.core.session.product;

import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import junit.framework.Assert;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.model.product.ProductPacking;
import org.jbalance.directory.core.model.product.Product;
import org.jbalance.directory.core.query.helper.product.GetProductPackingsRequest;
import org.jbalance.directory.core.session.BaseTest;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class ProductPackingSessionTest extends BaseTest {

    @Inject
    protected ProductPackingSession packingSession;

    @Inject
    protected ProductSession productSession;

    protected Product product;

    @Before
    public void setupRelations() {
        product = new Product();
        product.setName("product");
        product.setEnteredDate(new Date());
        productSession.add(product);
    }

    /**
     * Test of add method
     */
    @Test
    public void testAdd() {

        final ProductPacking bean = new ProductPacking();
        bean.setProduct(product);

        packingSession.add(bean);

        Assert.assertNotNull(bean);
        Assert.assertNotNull(bean.getId());

        final ProductPacking saved = packingSession.getById(bean.getId());

        Assert.assertEquals(saved.getProduct().getId(), product.getId());
    }

    @Test
    public void testFilter() {
        final ProductPacking bean = new ProductPacking();
        bean.setProduct(product);
        final ProductPacking saved = packingSession.add(bean);

        //test with empty request
        {
            GetProductPackingsRequest r = new GetProductPackingsRequest();

            List<ProductPacking> result = packingSession.list(r);

            assertNotNull(result);
            assertTrue(result.size() >= 1);
        }
    }

    @Test
    public void testModifiedDateCascading() {
        final ProductPacking bean = new ProductPacking();
        bean.setProduct(product);
        packingSession.add(bean);
        
        product=productSession.getById(product.getId());
        
        
        Product updateProduct=productSession.edit(product);
        
        final ProductPacking p= packingSession.getById(bean.getId());
        
        Assert.assertEquals(p.getModifiedDate().getTime(), updateProduct.getModifiedDate().getTime());
        
        
    }

    @Test
    public void testDelete() {
        ProductPacking bean = new ProductPacking();
        ProductPacking saved = packingSession.add(bean);
        ProductPacking deleted = packingSession.delete(saved.getId());

        Assert.assertTrue(deleted.isDeleted());
    }

    @Test
    public void testEdit() {
        Product newProduct = new Product();
        productSession.add(newProduct);

        ProductPacking bean = new ProductPacking();
        bean.setProduct(product);

        packingSession.add(bean);
        bean.setProduct(newProduct);

        ProductPacking p1 = packingSession.edit(bean);

        Assert.assertEquals(p1.getProduct().getId(), newProduct.getId());
    }

}
