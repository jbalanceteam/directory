package org.jbalance.directory.core.session.norm;

import java.util.List;
import java.util.Objects;
import javax.inject.Inject;
import junit.framework.Assert;
import org.jbalance.directory.core.model.norm.AgeBracket;
import org.jbalance.directory.core.model.norm.NormGroup;
import org.jbalance.directory.core.query.helper.norm.GetNormGroupRequest;
import org.jbalance.directory.core.session.BaseTest;
import org.junit.Test;

/**
 *
 * @author Mihail
 */
public class NormGroupSessionTest extends BaseTest{
    
    public NormGroupSessionTest() {
    }
    
    @Inject
    protected NormGroupSession normGroupSession;
    
    @Inject
    protected AgeBracketSession ageBracketSession;

    @Inject
    protected NetNormSession netNormSession;
    
    @Test
    public void testNetNormSessionSessionDeployed(){
      org.junit.Assert.assertNotNull(normGroupSession);
    }
    
    /**
     * Test of add method
     */
    @Test
    public void testAdd() {
        final NormGroup normGroup = new NormGroup();

        final NormGroup saved = normGroupSession.add(normGroup);
        
        Assert.assertNotNull(saved);
//        ID должен быть установлен автоматически
        Assert.assertNotNull(saved.getId());
//      Дата создания должна быть установлена автоматически
        Assert.assertNotNull(saved.getEnteredDate());
//        Имена должны совпадать 
        Assert.assertEquals(normGroup.getId(), saved.getId());
    }
//    /**
//     * Test of get method
//     */
    @Test
    public void testGet() {
        final NormGroup normGroup = new NormGroup();
        normGroupSession.add(normGroup);
        final NormGroup result = normGroupSession.getById(normGroup.getId());
        Assert.assertNotNull(result);
//        ID должен быть установлен автоматически
        Assert.assertNotNull(result.getId());
//        Имена должены совпадать
        Assert.assertEquals(normGroup.getId(), result.getId());
    }
    /**
     * Test of delete method
     */
    @Test
    public void testDelete() {
        final NormGroup normGroup = new NormGroup();
        normGroupSession.add(normGroup);
        final NormGroup deleted = normGroupSession.delete(normGroup.getId());
//        Имена должены совпадать
        Assert.assertEquals(normGroup.getId(), deleted.getId());
//        должен быть помечен, как удаленный
        Assert.assertTrue(deleted.isDeleted());
    }
//
//    /**
//     * Test of update method
//     */
    @Test
    public void testEdit() {
        final NormGroup normGroup = new NormGroup();
        final AgeBracket ageBracket = new AgeBracket();
        ageBracket.setName("name1");
        ageBracketSession.add(ageBracket);
        normGroupSession.add(normGroup);
        final NormGroup eBeforeEdit = normGroupSession.getById(normGroup.getId());
        normGroup.setAgeBracket(ageBracket);
        final NormGroup eAfterUpdate = normGroupSession.edit(normGroup);
//        должны существовать
        Assert.assertNotNull(eBeforeEdit);
        Assert.assertNotNull(eAfterUpdate);
        Assert.assertEquals(eBeforeEdit.getId(), eAfterUpdate.getId());
        Assert.assertTrue(eBeforeEdit.getAgeBracket() == null);
        Assert.assertTrue(Objects.equals(eAfterUpdate.getAgeBracket().getName(), ageBracket.getName()));
    }
    
    @Test
    public void testGetGrossNormsList() {
        for (int i = 10; i < 20; i++)
        {
            NormGroup normGroup = new NormGroup();
            normGroupSession.add(normGroup);
        }
        GetNormGroupRequest request = new GetNormGroupRequest();
        request.setFirstRecord(3);
        request.setMaxRecords(5);
        List<NormGroup> normGroups = normGroupSession.list(request);
//        должны существовать
        Assert.assertFalse(normGroups.isEmpty());
        
        Assert.assertTrue(normGroups.size()==5);
    }
}
