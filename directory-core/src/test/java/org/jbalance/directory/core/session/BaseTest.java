package org.jbalance.directory.core.session;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.runner.RunWith;
/**
 * Базовый класс для тестов
 * @author Petr Aleksandrov apv@jbalance.org
 */

@RunWith(Arquillian.class)
public abstract class BaseTest {
	   @Deployment
	   public static WebArchive createDeployment() {
		   WebArchive archive = ShrinkWrap.create(WebArchive.class, "test.war")
	            .addPackages(true, "org.jbalance.directory")
	            
//	            http://blog.ringerc.id.au/2012/06/jbas011440-cant-find-persistence-unit.html
	            .addAsWebInfResource("META-INF/persistence.xml", "classes/META-INF/persistence.xml")
	            .addAsWebInfResource("META-INF/jboss-deployment-structure.xml", "jboss-deployment-structure.xml")
	            .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
//	  		    .addAsWebInfResource("test-ds.xml", "test-ds.xml")
	  		    ;
		   System.err.println("===============");
		   archive.writeTo( System.err, org.jboss.shrinkwrap.api.formatter.Formatters.VERBOSE);
		   return archive;
	   }
           
       @PersistenceContext
       protected EntityManager em;
}