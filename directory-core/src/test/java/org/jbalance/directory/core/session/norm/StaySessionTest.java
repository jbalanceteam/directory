package org.jbalance.directory.core.session.norm;

import java.util.List;
import javax.inject.Inject;
import junit.framework.Assert;
import org.jbalance.directory.core.model.norm.Stay;
import org.jbalance.directory.core.session.BaseTest;
import org.junit.Test;

/**
 *
 * @author Mihail
 */
public class StaySessionTest extends BaseTest{
    @Inject
    StaySession session;
    
    @Test
    public void testProductCategorySessionDeployed(){
        org.junit.Assert.assertNotNull(session);
    }
    
    /**
     * Test of add method
     */
    @Test
    public void testAdd() {
        final Stay stay = createTestRecord();

        final Stay saved = session.add(stay);
        
        System.out.println(session.getById(stay.getId()).getName());
        Assert.assertNotNull(saved);
//        ID должен быть установлен автоматически
        Assert.assertNotNull(saved.getId());
//      Дата создания должна быть установлена автоматически
        Assert.assertNotNull(saved.getEnteredDate());
        Assert.assertEquals(stay.getName(), saved.getName());
    }

    private Stay createTestRecord() {
        final Stay stay = new Stay();
        stay.setName("some name");
        return stay;
    }
//    /**
//     * Test of get method
//     */
    @Test
    public void testGet() {
    	final Stay stay = createTestRecord();
    	
        session.add(stay);
        final Stay result = session.getById(stay.getId());
        Assert.assertNotNull(result);
//        ID должен быть установлен автоматически
        Assert.assertNotNull(result.getId());
//        Имена должены совпадать
        Assert.assertEquals(stay.getName(), result.getName());
    }

    @Test
    public void testDelete() {
    	final Stay stay = createTestRecord();
        session.add(stay);
        final Stay deleted = session.delete(stay.getId());
        Assert.assertTrue(deleted.isDeleted());
    }

    @Test
    public void testEdit() {
    	final Stay stay = createTestRecord();
        session.add(stay);
        final Stay eBeforeEdit = session.getById(stay.getId());
        stay.setName("other name");
        final Stay eAfterUpdate = session.edit(stay);
//        должны существовать
        Assert.assertNotNull(eBeforeEdit);
        Assert.assertNotNull(eAfterUpdate);
//        имена должны совпадать
        Assert.assertEquals(eBeforeEdit.getId(), eAfterUpdate.getId());
        Assert.assertNotSame(eBeforeEdit.getName(), eAfterUpdate.getName());
    }
    /**
     * Test of getAll method.
     */
    @Test
    public void testGetAll() {
    	final Stay stay = createTestRecord();
        session.add(stay);
        List<Stay> stayList = session.getAll();
        Assert.assertFalse(stayList.isEmpty());
    }
}
