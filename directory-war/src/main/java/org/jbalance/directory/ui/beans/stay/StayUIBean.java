package org.jbalance.directory.ui.beans.stay;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jbalance.directory.core.model.norm.Stay;
import org.jbalance.directory.core.query.helper.norm.GetStayRequest;
import org.jbalance.directory.core.session.norm.StaySession;
import org.jbalance.directory.core.util.Log;
import org.jbalance.directory.ui.core.AbstractDirectoryUIBean;

/**
 *
 * @author Mihail
 */

@Named("Stay")
@SessionScoped
public class StayUIBean extends AbstractDirectoryUIBean<Stay, StaySession, GetStayRequest>{

	private static final long serialVersionUID = 1L;

	@Override
    protected void initEntity() {
        entity = new Stay();
    }

    @Override
    protected void initRequest() {
        request = new GetStayRequest();
    }

    @Override
    protected void formFilter(GetStayRequest filter) {
        
    }

    @Override
    public void clearFilterValues() {
        
    }

    @Inject
    @Override
    public void setSessionBean(StaySession sessionBean) {
        log.debug("{0} sessionBean: {1}", Log.Replacement.METHOD_NAME, sessionBean);
        this.sessionBean = sessionBean;
    }

    @Override
    protected boolean validateFilter(GetStayRequest filter) {
        return true;
    }

    @Override
    protected boolean validateEntity() {
        boolean valid = true;
		if(getEntity().getName().length() > 255){
    		addErrorMessage("Поле 'Название' - 255 символов максимум");
            valid  = false;
    	}
    	
    	if(getEntity().getDescription().length() > 255){
    		addErrorMessage("Поле 'Описание' - 255 символов максимум");
            valid = false;
    	}
        return valid;
    }
    
}
