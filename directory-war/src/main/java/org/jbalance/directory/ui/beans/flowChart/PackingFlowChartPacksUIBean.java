package org.jbalance.directory.ui.beans.flowChart;

import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.NumberFilterType;
import org.jbalance.directory.core.model.flowchart.PackingFlowChart;
import org.jbalance.directory.core.model.flowchart.PackingFlowChartPacks;
import org.jbalance.directory.core.model.product.Packing;
import org.jbalance.directory.core.model.product.ProductImage;
import org.jbalance.directory.core.query.helper.flowchart.GetPackingFlowChartPacksRequest;
import org.jbalance.directory.core.query.helper.product.GetPackingsRequest;
import org.jbalance.directory.core.session.flowchart.PackingFlowChartPacksSession;
import org.jbalance.directory.core.session.product.PackingSession;
import org.jbalance.directory.core.util.Log;
import org.jbalance.directory.ui.core.AbstractDirectoryUIBean;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Created by user on 22.12.14.
 */
@SessionScoped
@Named("PackingFlowChartPacksUI")
public class PackingFlowChartPacksUIBean extends AbstractDirectoryUIBean<PackingFlowChartPacks, PackingFlowChartPacksSession, GetPackingFlowChartPacksRequest> {

    private PackingFlowChart packingFlowChart;
    private ProductImage image;
    private Packing packing;

    @Inject
    private PackingSession packingSession;

    @Override
    protected void initEntity() {
        entity = new PackingFlowChartPacks();
        entity.setPacking(getPackingFlowChart());
    }

    @Override
    protected void initRequest() {
        request = new GetPackingFlowChartPacksRequest();
    }

    @Override
    protected void formFilter(GetPackingFlowChartPacksRequest filter) {
        if (getPackingFlowChart()!=null){
            LongFilter packingFlowChartFilter = new LongFilter(NumberFilterType.EQUALS, getPackingFlowChart().getId());
            filter.setPackingFlowChartFilter(packingFlowChartFilter);
        }
    }

    @Override
    public void clearFilterValues() {

    }

    @Inject
    @Override
    public void setSessionBean(PackingFlowChartPacksSession sessionBean) {
        log.debug("{0} sessionBean: {1}", Log.Replacement.METHOD_NAME, sessionBean);
        this.sessionBean = sessionBean;
    }

    @Override
    protected boolean validateFilter(GetPackingFlowChartPacksRequest filter) {
        return true;
    }

    @Override
    protected boolean validateEntity() {
        if (getEntity() == null || getEntity().getPacking() == null || getEntity().getQuantity() == null){
            return false;
        }
        if(getEntity().getQuantity() < 0){
            addErrorMessage("Количество не может быть отрицательным значением");
            return false;
        }
        return true;
    }

    public PackingFlowChart getPackingFlowChart() {
        return packingFlowChart;
    }

    public void setPackingFlowChart(PackingFlowChart packingFlowChart) {
        this.packingFlowChart = packingFlowChart;
    }

    public ProductImage getImage() {
        return image;
    }

    public void setImage(ProductImage image) {
        this.image = image;
        this.packing = null;
    }

    public Packing getPacking() {
        return packing;
    }

    public void setPacking(Packing packing) {
        this.packing = packing;
    }

    public List<Packing> getCurrentPackings(){
        GetPackingsRequest getPackingsRequest = new GetPackingsRequest();
        if (getImage()!=null) {
            LongFilter imageFilter = new LongFilter(NumberFilterType.EQUALS, getImage().getId());
            getPackingsRequest.setImageFilter(imageFilter);
        }
        return packingSession.list(getPackingsRequest);
    }

    @Override
    public void addNotPersistent(){
        if (getNotPersistentList().size() < 1){
            super.addNotPersistent();
        }
    }
}
