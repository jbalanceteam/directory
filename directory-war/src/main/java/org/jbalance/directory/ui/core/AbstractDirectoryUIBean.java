package org.jbalance.directory.ui.core;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.naming.NamingException;

import org.jbalance.directory.core.model.BaseEntity;
import org.jbalance.directory.core.query.GetRequest;
import org.jbalance.directory.core.session.GenericSession;
import org.jbalance.directory.core.util.Log;
import org.jbalance.directory.core.util.Log.Replacement;
import org.jbalance.directory.ui.beans.BaseUIBean;

/**
 * Абстрактный бин - "Справочник".<br/>
 * Является предком для всех реализаций справочников.
 *
 * @author Alexandr Chubenko
 * @param <T> Класс сущности справочника.
 * @param <R> Сессионный бин соответствующий классу.
 * @param <S> Соответствующий {@link GetRequest}
 */
public abstract class AbstractDirectoryUIBean<T extends BaseEntity, R extends GenericSession<T, S>, S extends GetRequest<?>> extends BaseUIBean implements IDirectory<T> {

	private static final long serialVersionUID = 1L;
	
	/**
	 * указывает является ли справочник редактируемым или статичным 
	 */
	protected boolean editable;

	/**
     * Логгер.
     */
    protected Log log = new Log(getClass());

    /**
     * Фильтр.
     */
    protected S request;

    /**
     * Список сущностей для формы списка.
     */
    private List<T> list;
    
    private List<T> notPersistentList;
    
    /**
     * Сессионный бин сущности.
     */
    protected R sessionBean;

    /**
     * Текущий экземпляр сущности.
     */
    protected T entity;
    
    /**
     * Индекс сущности в массиве.
     */
    private Long index;

    public void setEntity(T ent) {
    	log.entry(ent);
    	this.entity = ent;
    }

    public T getEntity() {
        if (entity == null) {
            initEntity();
        }
        return (T) entity;
    }

    public void clear() {
        setEntity(null);
    }

    /**
     * Метод вызывается с формы редактирования/добавления сущности.<br/>
     * Если {@link AbstractDirectoryBean#entity} имеет id - для него вызывается
     * edit(),<br/>
     * иначе - add()
     */
    public void modifyAction() {
        clearMessages();
        if (validateEntity()) {
            if (getEntity() != null) {
                if (getEntity().isPersistent()) {
                    edit(getEntity());
                } else {
                    add(getEntity());
                }
            }
        }
    }

    /**
     * Метод инициализирует объект сущности, сессию и фильтр.
     *
     * @throws NamingException Если не удалось найти сессионный бин.
     */
    @PostConstruct
    protected void init() {
        initEntity();
        initRequest();
		setEditable();
		clearNotPersistentList();
        //Получает записи при открытии страницы
        list();
    }

    /**
     * Инициализирует экземпляр объекта справочника.<br/>
     * В теле метода полю {@link AbstractDirectoryBean#entity} должна
     * присваиваться ссылка на созданный объект сущности.
     */
    protected abstract void initEntity();

    /**
     * Инициализирует фильтр {@link AbstractDirectoryBean#request}.<br/>
     * Очищает поля фильтра конкретного бина.
     */
    protected abstract void initRequest();

    protected R getSessionBean() throws NamingException {
    	log.exit(sessionBean);
        return sessionBean;
    }

    @Override
    public void add(T entity) {
        log.entry();
		if(!isEditable()){
			addInfoMessage("Данный список является статичным (редактирование запрещено)");
			return;
		}
        try {
        	entity = (T) getSessionBean().add(entity);
			if (getNotPersistentList() != null && entity.isPersistent()){
				getNotPersistentList().remove(entity);
			}
            addInfoMessage("Объект успешно сохранен. ID = " + entity.getId());
        } catch (NamingException e) {
            log.error(e);
            throw new RuntimeException(e);
        } catch (Exception e) {
            log.error(e);
            outputErrorMessage(e);
        } 
        log.exit();
    }

    @Override
    public void edit(T entity) {
        log.entry();
		if(!isEditable()){
			addInfoMessage("Данный список является статичным (редактирование запрещено)");
			return;
		}
        try {
        	entity = (T) getSessionBean().edit(entity);
            addInfoMessage("Объект успешно изменен. ID = " + entity.getId());
        } catch (NamingException e) {
            log.error(e);
            throw new RuntimeException(e);
        } catch (Exception e) {
        	log.error(e);
            outputErrorMessage(e);
        } 
        log.exit();
    }

	private void outputErrorMessage(Exception e) {
//		Попробуем вывести детальное сообщение... 
		StringBuilder messageText = new StringBuilder();
		messageText.append("Ошибка при сохранении записи: ");
		Throwable throwable = e;

//		Вывод всех сообщений :
//		do{
//			messageText.append(throwable.getLocalizedMessage());
//			throwable = throwable.getCause();
//		} while (throwable != null);
		
//		Вывод одного последнего сообщения
		while(throwable.getCause() != null){
			throwable = throwable.getCause();
		}
		messageText.append(throwable.getLocalizedMessage());
		addErrorMessage(messageText.toString());
	}
    
    

    public void test(){
    	log.info("{0} entity: {1}", Replacement.METHOD_NAME, entity);
    }
    
    /**
     * Метод выполняет удаление и выполняет перезагрузку списка объектов.
     */
    public void deleteAction(){
    	log.info("{0} entity: {1}", Replacement.METHOD_NAME, entity);
        clearMessages();
        delete(entity);
        entity = null;
        list();
    }
    
    @Override
    public void delete(T entity) {
        log.entry();
		if(!isEditable()){
			addInfoMessage("Данный список является статичным (редактирование запрещено)");
			return;
		}
        try {
            if (entity != null) {
                getSessionBean().delete(entity.getId());
                addInfoMessage("Объект успешно удален.");
            } else {
                throw new IllegalArgumentException("Удаляемая сущность не может иметь значение null.");
            }
        } catch (NamingException ex) {
            log.error(ex);
            throw new RuntimeException(ex);
        } catch (Exception e) {
        	log.error(e);
            outputErrorMessage(e);
        } 
        log.exit();
    }

    public S getRequest() {
        if (request == null) {
            initRequest();
        }
        return request;
    }

    @Override
    public List<T> list() {
        log.entry();
        try {
            S filter = getRequest();
            formFilter(filter);
            if (validateFilter(filter)) {
                list = new ArrayList<T>();
                list = getSessionBean().list(filter);
                log.trace("List was initialized: {0}", list);
            }
            return getList();
        } catch (NamingException ex) {
            log.error(ex);
            throw new RuntimeException(ex);
        } finally {
            log.exit();
        }
    }

    /**
     * Метод формирования фильтра, для метода <code>list()</code>.<br/>
     * В теле метода заполняются поля фильтра в зависимости от заданных значений
     * формы.
     *
     * @param filter Класс фильтра, расширяющий {@link GetRequest}
     */
    protected abstract void formFilter(S filter);

    /**
     * Метод вызывается с формы фильтрации.<br/>
     * Он сбрасывает фильтр и получает список объектов справочника по умолчанию.
     */
    public void clearFilter() {
        clear();
        clearMessages();
        //Очищаем фильтр, чтобы заставить перезагрузиться список сущностей.
        clearFilterValues();
        request = null;
        initRequest();
        list();
    }
    
    /**
     * Очищает значения фильтра.
     */
    public abstract void clearFilterValues();

    public List<T> getList() {
        if (list == null) {
            list();
        }
        return list;
    }

    /**
     * @return список добавленных, но не сохраненных в базу
     */
    public List<T> getNotPersistentList(){
        return notPersistentList;
    }
    
	public void addNotPersistent(){
		initEntity();
		getNotPersistentList().add(entity);
	}
	
	/**
	 * объединяет элементы из базы и из списка еще не сохранённых
	 * @return 
	 */
	public List<T> getEditingList(){
		ArrayList<T> editingList = new ArrayList<T>();
		editingList.addAll(list());
        editingList.addAll(getNotPersistentList());
        return editingList;
    }
    /**
     * удаляет элемент из списка для добавления в базу
     */
    public void removeFromNotPersistent(){
        if (!getEntity().isPersistent()){
            notPersistentList.remove(getEntity());
        }
    }
    
    public void clearNotPersistentList(){
        notPersistentList = new ArrayList<T>();
    }
    /**
     * Метод используется для EJB-инъекции реализации сессионного бина.
     *
     * @param sessionBean Сессия текущего справочника.
     */
    public abstract void setSessionBean(R sessionBean);

    /**
     * Метод обнуляет список сущностей из справочника, что вызывает метод
     * {@link AbstractDirectoryUIBean#list()}.
     */
    public void doSearch() {
        clear();
        clearMessages();
        list = null;
    }

    /**
     * Метод инициализирует новый экземпляр <tt>entity</tt> и выполняет JSF
     * redirect на страницу редактирования/добавления
     *
     * @return Адрес страницы добавления/редактирования сущности справочника
     */
    public String addNewDirectoryObject() {
        initEntity();
        return goToAddPage();
    }
    
    /**
     * Метод осуществляет redirect на страницу редактирования/добавления
     * @return Адрес страницы добавления/редактирования сущности справочника
     */
    public String editDirectoryObject() {
        return goToAddPage();
    }

    /**
     * Выполняет редирект на страницу добавления/редактирования
     *
     * @return Адрес страницы добавления/редактирования сущности справочника
     */
    public String goToAddPage() {
        return "add.xhtml?faces-redirect=true";
    }
    
    /**
     * Перезагружает список и выполняет редирект на страницу списка справочника.
     *
     * @return Адрес страницы добавления/редактирования сущности справочника
     */
    public String backToList() {
        list();
        return "list.xhtml?faces-redirect=true";
    }

    /**
     * Метод валидирует значения фильтра при поиске объектов справочника.
     * Добавляет {@link FacesMessage сообщения JSF} в случае ошибок.
     * @param filter Класс фильтра, расширяющий {@link GetRequest}
     * @return <code>true</code> - если фильтр валиден, <code>false</code> -
     * если имеются ошибки в значения фильтра.
     * @see BaseUIBean#addErrorMessage(java.lang.String)
     */
    protected abstract boolean validateFilter(S filter);

    /**
     * Метод валидирует значения полей добавляемого объекта справочника.
     * Добавляет {@link FacesMessage сообщения JSF} в случае ошибок.
     *
     * @return <code>true</code> - если поля валидны, <code>false</code> - если
     * имеется хотя бы одно не валидное поле.
     * @see BaseUIBean#addErrorMessage(java.lang.String)
     */
    protected abstract boolean validateEntity();

    public Long getIndex() {
        return index;
    }

    public void setIndex(Long index) {
        this.index = index;
    }
	
	public boolean isEditable(){
		return editable;
	}
	
	protected void setEditable(){
		editable = true;
	}
	
	
}
