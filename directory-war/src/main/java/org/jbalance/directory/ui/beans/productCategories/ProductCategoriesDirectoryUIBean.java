package org.jbalance.directory.ui.beans.productCategories;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.filter.StringFilterType;
import org.jbalance.directory.core.model.norm.ProductCategory;
import org.jbalance.directory.core.query.helper.norm.GetProductCategoryRequest;
import org.jbalance.directory.core.session.norm.ProductCategorySession;
import org.jbalance.directory.core.util.Log;
import org.jbalance.directory.core.util.Str;
import org.jbalance.directory.ui.core.AbstractDirectoryUIBean;

/**
 *
 * @author Mihail
 */
@Named("ProductCategories")
@SessionScoped
public class ProductCategoriesDirectoryUIBean extends AbstractDirectoryUIBean<ProductCategory, ProductCategorySession, GetProductCategoryRequest> {

    private String nameFilter;
    
    @Override
    protected void initEntity() {
        entity = new ProductCategory();
    }

    @Override
    protected void initRequest() {
        request = new GetProductCategoryRequest();
        nameFilter = null;
    }

    @Override
    protected void formFilter(GetProductCategoryRequest filter) {
        if (!Str.empty(nameFilter)) {
            filter.setCategoryNameFilter(new StringFilter(StringFilterType.CONTAINS, nameFilter));
        }
    }

    @Override
    public void clearFilterValues() {
        nameFilter=null;
    }

    @Inject
    @Override
    public void setSessionBean(ProductCategorySession sessionBean) {
        log.debug("{0} sessionBean: {1}", Log.Replacement.METHOD_NAME, sessionBean);
        this.sessionBean = sessionBean;
    }

    @Override
    protected boolean validateFilter(GetProductCategoryRequest filter) {
        return true;
    }
    
    public String getNameFilter() {
        return nameFilter;
    }

    public void setNameFilter(String nameFilter) {
        this.nameFilter = nameFilter;
    }

    @Override
    protected boolean validateEntity() {
        boolean valid = true;
    	if(getEntity().getName().length() > 255){
            addErrorMessage("Поле 'Описание' - 255 символов максимум");
            valid = false;
    	}
        return valid;
    }
    
}
