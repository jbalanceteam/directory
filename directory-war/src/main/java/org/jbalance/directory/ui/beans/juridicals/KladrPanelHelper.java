package org.jbalance.directory.ui.beans.juridicals;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.jbalance.directory.core.model.address.*;
import org.jbalance.directory.core.session.address.IAddressFinder;
import org.jbalance.directory.core.util.Log;
import org.jbalance.directory.core.util.Str;

public class KladrPanelHelper {

    protected Log log = new Log(getClass());
    private final IAddressFinder addressFinder;

    private static final String DELIMITER = ", ";
    private static final String DEFAULT_COUNTRY = "Российская Федерация";

    private String country = DEFAULT_COUNTRY;
    private String regionName;

    private List<Region> regionSugessionList;
    private List<City> citySugessionList;
    private List<Street> streetSugessionList;
    private List<House> houseSugessionList;

//	Selected objects
    /**
     * Индекс
     */
    private String zipCode;
    private IRegion region;
    private ICity city;
    private IStreet street;
    private IHouse house;
    /**
     * Квартира
     */
    private String apartments;
    /**
     * Является ли адрес абонентским ящиком?
     */
    private boolean isPostOfficeBox = false;
    private String postOfficeBox;

//	Following fields for search
    private String regionNamePrefix;
    private String cityNamePrefix;
    private String streetNamePrefix;
    private String houseNamePrefix;

    private final KladrListener listener;

    public KladrPanelHelper(IAddressFinder addressFinder, KladrListener listener) {
        super();
        if (listener == null) {
            throw new NullPointerException();
        }
        this.listener = listener;
        this.addressFinder = addressFinder;
        addressFinder.setLimitSelectCity(15);
        addressFinder.setLimitSelectStreet(15);

    }

    private boolean validate() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<KladrPanelHelper>> constraintViolations = validator.validate(this);
        for (ConstraintViolation<KladrPanelHelper> constraintViolation : constraintViolations) {
            log.warn(constraintViolation + "");
        }

        if (isPostOfficeBox) {
            if (Str.empty(postOfficeBox)) {
                log.warn("Заполните номер абонентского ящика!");
            }
        }

        return constraintViolations.isEmpty();
    }

    public String getAddressString() {
        StringBuilder sb = new StringBuilder();
        sb.append(zipCode).append(DELIMITER);
        sb.append(country).append(DELIMITER);
        sb.append(getCity().getFullAddressString()).append(DELIMITER);

        if (isPostOfficeBox) {
            sb.append("а/я").append(postOfficeBox);
        } else {
            sb.append(street.getFormalName()).append(DELIMITER);
            sb.append(house.getHouseNum());
            if (!Str.empty(apartments)) {
                sb.append(DELIMITER).append("кв. ").append(apartments);
            }
        }
        return sb.toString();
    }

    public void selectRegion(IRegion region) {
//		log.entry(city);
        this.region = region;
        regionSugessionList = null;
        regionNamePrefix = region.getFormalName();
        regionName = region.getRegionCode() + "";
    }

    public boolean isRegionSelected() {
        return region != null;
    }

    public void unselectRegion() {
//		log.entry();
        unselectCity();
        region = null;
        regionNamePrefix = null;
        regionName = null;
        regionSugessionList = null;
    }

    public List<Region> getRegionSugessions() {
//		log.entry(cityNamePrefix);

        if (isRegionSelected()) {
            return Collections.emptyList();
        }

        if (Str.empty(regionNamePrefix) || regionNamePrefix.length() < 3) {
            return Collections.emptyList();
        }

        regionSugessionList = addressFinder.findRegion(regionNamePrefix);
        log.exit(regionSugessionList.size());
        return regionSugessionList;
    }

    public void selectCity(ICity city) {
//		log.entry(city);
        this.city = city;
        citySugessionList = null;
        cityNamePrefix = city.getFormalName();
        regionName = city.getRegionCode() + "";
    }

    public boolean isCitySelected() {
        return city != null;
    }

    public void unselectCity() {
//		log.entry();
        unselectStreet();
        city = null;
        cityNamePrefix = null;
        regionName = null;
        citySugessionList = null;
    }

    public List<City> getCitySugessions() {
//		log.entry(cityNamePrefix);

        if (isCitySelected()) {
            return Collections.emptyList();
        }

        if (Str.empty(cityNamePrefix) || cityNamePrefix.length() < 3) {
            return Collections.emptyList();
        }

        citySugessionList = addressFinder.findCity(cityNamePrefix, isRegionSelected() ? region.getRegionCode() : null);
        log.exit(citySugessionList.size());
        return citySugessionList;
    }

    public void selectStreet(IStreet street) {
        this.street = street;
        streetSugessionList = null;
        streetNamePrefix = street.getFormalName();
    }

    public boolean isStreetSelected() {
        return street != null;
    }

    public void unselectStreet() {
        log.entry();
        unselectHouse();
        street = null;
        streetNamePrefix = null;
        streetSugessionList = null;
    }

    public List<Street> getStreetSugessions() {
//		log.entry(streetNamePrefix);

        if (isStreetSelected()) {
            return Collections.emptyList();
        }

        if (isPostOfficeBox) {
            return Collections.emptyList();
        }

        if (city == null || Str.empty(streetNamePrefix) || streetNamePrefix.length() < 3) {
            return Collections.emptyList();
        }

        streetSugessionList = addressFinder.findStreet(streetNamePrefix, city.getAoGuid());
        log.exit(streetSugessionList.size());
        return streetSugessionList;
    }

    public void selectHouse(IHouse house) {
        this.house = house;
        houseSugessionList = null;
        houseNamePrefix = house.getHouseNum();
        zipCode = house.getPostalCode();
    }

    public boolean isHouseSelected() {
        return house != null;
    }

    public void unselectHouse() {
        house = null;
        houseNamePrefix = null;
        apartments = null;
        zipCode = null;
        houseSugessionList = null;
    }

    public List<House> getHouseSugessions() {
        if (isHouseSelected()) {
            return Collections.emptyList();
        }

        if (isPostOfficeBox) {
            return Collections.emptyList();
        }

        if (city == null || street == null || Str.empty(houseNamePrefix)) {
            return Collections.emptyList();
        }

        houseSugessionList = addressFinder.findHouse(houseNamePrefix, street.getAoGuid());
        log.exit(houseSugessionList.size());
        return houseSugessionList;
    }

    public String getCountryName() {
        return country;
    }

    public void setCountryName(String country) {
        this.country = country;
    }

    public String getCityNamePrefix() {
        return cityNamePrefix;
    }

    public void setCityNamePrefix(String cityNamePrefix) {
        this.cityNamePrefix = cityNamePrefix;
        getCitySugessions();
    }

    public String getStreetNamePrefix() {
        return streetNamePrefix;
    }

    public void setStreetNamePrefix(String streetNamePrefix) {
        this.streetNamePrefix = streetNamePrefix;
        getStreetSugessions();
    }

    public String getCountry() {
        return country;
    }

    public String getRegionName() {
        return regionName;
    }

    public IRegion getRegion() {
        return region;
    }

    @NotNull
    public ICity getCity() {
        return city;
    }

    public IStreet getStreet() {
        return street;
    }

    public IHouse getHouse() {
        return house;
    }

    public String getHouseNamePrefix() {
        return houseNamePrefix;
    }

    public void setHouseNamePrefix(String houseNamePrefix) {
        this.houseNamePrefix = houseNamePrefix;
        getHouseSugessions();
    }

    private void cleanForm() {
        region = null;
        city = null;
        street = null;
        house = null;

        regionNamePrefix = null;
        cityNamePrefix = null;
        streetNamePrefix = null;
        houseNamePrefix = null;

        regionSugessionList = null;
        citySugessionList = null;
        streetSugessionList = null;
        houseSugessionList = null;
    }

    public void save() {
        log.entry();
        if (validate()) {
            listener.saveAddress(getAddressString());
        }
        cleanForm();
    }

    public void cancel() {
        log.entry();
        listener.cancel();;
        cleanForm();
    }

    @Size(min = 6, max = 6)
    @NotBlank
    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public boolean isIsPostOfficeBox() {
        return isPostOfficeBox;
    }

    public void setIsPostOfficeBox(boolean isPostOfficeBox) {
        this.isPostOfficeBox = isPostOfficeBox;
    }

    public String getApartments() {
        return apartments;
    }

    public void setApartments(String apartments) {
        this.apartments = apartments;
    }

    public void isPostOfficeBoxChanged() {
        log.entry();
        unselectStreet();
    }

    public String getPostOfficeBox() {
        return postOfficeBox;
    }

    public void setPostOfficeBox(String postOfficeBox) {
        this.postOfficeBox = postOfficeBox;
    }

    public List<Region> getRegionSugessionList() {
        return regionSugessionList;
    }

    public void setRegionSugessionList(List<Region> regionSugessionList) {
        this.regionSugessionList = regionSugessionList;
    }

    public List<City> getCitySugessionList() {
        return citySugessionList;
    }

    public void setCitySugessionList(List<City> citySugessionList) {
        this.citySugessionList = citySugessionList;
    }

    public List<Street> getStreetSugessionList() {
        return streetSugessionList;
    }

    public void setStreetSugessionList(List<Street> streetSugessionList) {
        this.streetSugessionList = streetSugessionList;
    }

    public List<House> getHouseSugessionList() {
        return houseSugessionList;
    }

    public void setHouseSugessionList(List<House> houseSugessionList) {
        this.houseSugessionList = houseSugessionList;
    }

    public String getRegionNamePrefix() {
        return regionNamePrefix;
    }

    public void setRegionNamePrefix(String regionNamePrefix) {
        this.regionNamePrefix = regionNamePrefix;
        getRegionSugessions();
    }

}
