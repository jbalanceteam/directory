package org.jbalance.directory.ui.beans;

import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.jbalance.directory.core.model.BaseEntity;
import org.jbalance.directory.core.util.Log;


@ViewScoped
@FacesConverter("JbalanceEntityConverter")
public class EntityConverter implements Converter, Serializable {
    
//	Injection doesn't work properly here
//	@Inject
    private EntityStore entityStore;

	private Log log = new Log(getClass());
	
    @Override
    public Object getAsObject(FacesContext arg0, UIComponent arg1, String uuid) {
        BaseEntity result = getEntityStore().get(uuid);
        log.trace("getAsObject({0}) uid: {1}", uuid , result);
		return result;
    }

    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object entity) {
        String uid = getEntityStore().put((BaseEntity) entity);
        log.trace("getAsString({0}) uid: {1}", entity , uid);
		return uid;
    }

    public EntityStore getEntityStore() {
        if (entityStore == null) {
        	entityStore = EntityStore.instance();
        }
        return entityStore;
    }
}
