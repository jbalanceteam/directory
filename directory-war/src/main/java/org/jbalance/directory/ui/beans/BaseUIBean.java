package org.jbalance.directory.ui.beans;

import java.io.Serializable;
import java.util.Iterator;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * An ancestor of all UI beans. Provides basic UI methods, access to the client,
 * etc
 *
 * @author Alexandr Chubenko
 */
public class BaseUIBean implements Serializable {

    private static final long serialVersionUID = -1L;
    /**
     * Gets current instance of JSF {@link javax.faces.context.FacesContext} 
     *
     * @return
     */
    protected FacesContext getFacesContext() {
        return FacesContext.getCurrentInstance();
    }

    /**
     * Adds a message with a given severity
     *
     * @param severity Message severity
     * @param message Message to be shown
     */
    private void addFacesMessage(FacesMessage.Severity severity, String message) {
        FacesMessage facesMessage = new FacesMessage(severity, message, null);
        getFacesContext().addMessage(null, facesMessage);
    }

    /**
     * Adds a message with a given severity
     *
     * @param severity Message severity
     * @param message Message to be shown
     * @param controlId h:messages or rich:messages component ID
     */
    private void addFacesMessageToControl(String controlId, FacesMessage.Severity severity, String message) {
        FacesMessage facesMessage = new FacesMessage(severity, message, null);
        getFacesContext().addMessage(controlId, facesMessage);
    }

    /**
     * Adds a message with info severity to a control
     *
     * @param message Message to be shown
     * @param controlId h:messages or rich:messages component ID
     */
    protected void addInfoMessageToControl(String controlId, String message) {
        addFacesMessageToControl(controlId, FacesMessage.SEVERITY_INFO, message);
    }

    /**
     * Adds a message with error severity to a control
     *
     * @param message Message to be shown
     * @param controlId h:messages or rich:messages component ID
     */
    protected void addErrorMessageToControl(String controlId, String message) {
        addFacesMessageToControl(controlId, FacesMessage.SEVERITY_ERROR, message);
    }

    /**
     * Adds a faces message with info severity
     *
     * @param message Message to be shown
     */
    protected void addInfoMessage(String message) {
        addFacesMessage(FacesMessage.SEVERITY_INFO, message);
    }

    /**
     * Adds a faces message with error severity
     *
     * @param message Message to be shown
     */
    protected void addErrorMessage(String message) {
        addFacesMessage(FacesMessage.SEVERITY_ERROR, message);
    }

    /**
     * Clears the faces messages list
     */
    protected void clearMessages() {
        Iterator<FacesMessage> msgIterator = getFacesContext().getMessages();
        while (msgIterator.hasNext()) {
            msgIterator.next();
            msgIterator.remove();
        }
    }
}
