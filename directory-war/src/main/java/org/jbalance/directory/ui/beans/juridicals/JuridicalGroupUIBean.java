package org.jbalance.directory.ui.beans.juridicals;

import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.jbalance.directory.core.model.Juridical;
import org.jbalance.directory.core.model.JuridicalGroup;
import org.jbalance.directory.core.query.helper.juridical.GetJuridicalGroupsRequest;
import org.jbalance.directory.core.query.helper.juridical.GetJuridicalsRequest;
import org.jbalance.directory.core.session.juridical.JuridicalGroupSession;
import org.jbalance.directory.core.session.juridical.JuridicalSession;
import org.jbalance.directory.ui.core.AbstractDirectoryUIBean;

/**
 *
 * @author roman
 */
@Named("JuridicalGroups")
@SessionScoped
public class JuridicalGroupUIBean extends AbstractDirectoryUIBean<JuridicalGroup, JuridicalGroupSession, GetJuridicalGroupsRequest> {

    private static final long serialVersionUID = 1L;

    @Inject
    JuridicalSession juridicalSession;

    List<Juridical> juridicals;

    @Override
    public void initEntity() {
        entity = new JuridicalGroup();
    }

    @Override
    protected void initRequest() {
        request = new GetJuridicalGroupsRequest();
    }

    @Override
    public void setEntity(JuridicalGroup ent) {

        if (ent != null && ent.getId() != null) {
            ent = sessionBean.getById(ent.getId());
        }

        super.setEntity(ent);
        juridicals = null;

    }

    @Override
    protected void formFilter(GetJuridicalGroupsRequest filter) {
    }

    @Inject
    @Override
    public void setSessionBean(JuridicalGroupSession sessionBean) {
        this.sessionBean = sessionBean;
    }

    @Override
    protected boolean validateEntity() {
        boolean valid = true;

        return valid;
    }

    @Override
    protected boolean validateFilter(GetJuridicalGroupsRequest filter) {
        return true;
    }

    @Override
    public void clearFilterValues() {
    }

    @Override
    public boolean isEditable() {
        return true;
    }

    public void addJuridical(Juridical j) {
        juridicals.remove(j);
        getEntity().getJuridicals().add(j);
    }

    public void removeJuridical(Juridical j) {
        getEntity().getJuridicals().remove(j);
        juridicals.remove(j);
    }

    public List<Juridical> getJuridicals() {

        if (juridicals == null) {
            juridicals = juridicalSession.list(new GetJuridicalsRequest());
            juridicals.removeAll(getEntity().getJuridicals());
        }

        return juridicals;
    }

    public void setJuridicals(List<Juridical> juridicals) {
        this.juridicals = juridicals;
    }

}
