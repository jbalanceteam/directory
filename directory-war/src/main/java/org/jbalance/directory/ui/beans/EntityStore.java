package org.jbalance.directory.ui.beans;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.jbalance.directory.core.model.BaseEntity;
import org.jbalance.directory.core.util.Log;

@ViewScoped
@Named("EntityStore")
public class EntityStore implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    /** The store. */
    private Map<String, BaseEntity> store;

    private Log log = new Log(getClass());
    
    @PostConstruct
    public void create() {
    	log.entry();
        store = new HashMap<String, BaseEntity>();
    }

    public String put(BaseEntity entity) {
        if (entity != null) {
            store.put(entity.getGuid(), entity);
            return entity.getGuid();
        }
        return null;
    }

    public BaseEntity get(String uuid) {
        return store.get(uuid);
    }

    public static EntityStore instance() {
    	FacesContext context = FacesContext.getCurrentInstance();
        return  context.getApplication().evaluateExpressionGet(context, "#{EntityStore}", EntityStore.class);
    }
}
