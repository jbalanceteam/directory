package org.jbalance.directory.ui.beans.product;

import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.NamingException;

import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.NumberFilterType;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.filter.StringFilterType;
import org.jbalance.directory.core.model.product.Product;
import org.jbalance.directory.core.model.product.ProductImage;
import org.jbalance.directory.core.model.product.ProductPacking;
import org.jbalance.directory.core.query.helper.product.GetProductImagesRequest;
import org.jbalance.directory.core.query.helper.product.GetProductPackingsRequest;
import org.jbalance.directory.core.query.helper.product.GetProductsRequest;
import org.jbalance.directory.core.session.product.ProductImageSession;
import org.jbalance.directory.core.session.product.ProductSession;
import org.jbalance.directory.core.util.Log;
import org.jbalance.directory.ui.core.AbstractDirectoryUIBean;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@Named("Products")
@SessionScoped
public class ProductUIBean extends AbstractDirectoryUIBean<Product, ProductSession, GetProductsRequest> {

    private static final long serialVersionUID = 1L;

    private String nameFilter = "";

	@Inject
	protected ProductPackingUIBean productPackingUIBean;
	
    @Inject
    protected ProductImageSession imageSession;

    private List<ProductImage> images;

    @Override
    protected void initRequest() {
        request = new GetProductsRequest();
    }

    @Override
    protected void formFilter(GetProductsRequest filter) {
        if (!"".equals(nameFilter)) {
            request.setProductNameFilter(new StringFilter(StringFilterType.STARTS_WITH, nameFilter));
        } else {
            request.setProductNameFilter(null);
        }
    }

    public List<ProductImage> getImages() {
        images = imageSession.list(new GetProductImagesRequest());
        return images;
    }

    @Override
    public void clearFilterValues() {
        nameFilter = "";
    }

    @Inject
    @Override
    public void setSessionBean(ProductSession sessionBean) {
        log.debug("{0} sessionBean: {1}", Log.Replacement.METHOD_NAME, sessionBean);
        this.sessionBean = sessionBean;
    }

    @Override
    protected boolean validateFilter(GetProductsRequest filter) {
        return true;
    }

    @Override
    protected boolean validateEntity() {
        boolean valid = true;
        if (getEntity().getName().length() > 255) {
            addErrorMessage("Поле 'Название' - 255 символов максимум");
            valid = false;
        }

        return valid;
    }

    @Override
    protected void initEntity() {
        entity = new Product();
    }

    public String getNameFilter() {
        return nameFilter;
    }

    public void setNameFilter(String nameFilter) {
        this.nameFilter = nameFilter;
    }

    public void setImage(ProductImage i) {
        this.entity.setImage(i);
    }

    public void unSetImage() {
        this.entity.setImage(null);
    }
	
	@Override
	public String goToAddPage(){
		productPackingUIBean.setProduct(getEntity());
		return super.goToAddPage();
	}

    @Override
    public void modifyAction(){
        try {
            if (getEntity().isPersistent() &&
                    !getSessionBean()
                            .getById(getEntity().getId())
                            .getImage()
                            .equals(getEntity().getImage())
                    ){

                productPackingUIBean.setProduct(getEntity());
                for (ProductPacking productPacking:productPackingUIBean.list()){
                    productPackingUIBean.delete(productPacking);
                    productPackingUIBean.list();
                }
            }
            super.modifyAction();
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }
}
