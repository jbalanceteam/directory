package org.jbalance.directory.ui.beans.juridicals;

public interface  KladrListener {
	void saveAddress(String address);
	void cancel();
}
