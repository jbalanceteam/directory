package org.jbalance.directory.ui.beans.product;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.filter.StringFilterType;

import org.jbalance.directory.core.model.product.ProductImage;
import org.jbalance.directory.core.query.helper.product.GetProductImagesRequest;
import org.jbalance.directory.core.session.product.ProductImageSession;

import java.io.Serializable;

/**
 *
 * @author Mihail
 */
@ViewScoped
@FacesConverter("ProductImageConverter")
public class ProductImageConverter implements Converter,Serializable {

    @Inject
    protected ProductImageSession imageSession;

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String s) {
        if(s==null)
            return null;
		GetProductImagesRequest getProductImagesRequest = new GetProductImagesRequest();
		StringFilter productImageNameFilter = new StringFilter(StringFilterType.EQUALS, s);
		getProductImagesRequest.setNameFilter(productImageNameFilter);
        return imageSession.list(getProductImagesRequest).get(0);

    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object s) {
        return s != null ? ((ProductImage) s).getName() : "";
    }
}
