package org.jbalance.directory.ui.beans.ageBrackets;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jbalance.directory.core.model.norm.AgeBracket;
import org.jbalance.directory.core.query.helper.norm.GetAgeBracketRequest;
import org.jbalance.directory.core.session.norm.AgeBracketSession;
import org.jbalance.directory.core.util.Log.Replacement;
import org.jbalance.directory.ui.core.AbstractDirectoryUIBean;

/**
 * 
 * @author Petr Aleksandrov apv@jbalance.org
 *
 */
@Named("AgeBrackets")
@SessionScoped
public class AgeBracketsUIBean extends AbstractDirectoryUIBean<AgeBracket, AgeBracketSession, GetAgeBracketRequest> {

	private static final long serialVersionUID = 1L;

	@Override
    public void initEntity() {
        entity = new AgeBracket();
    }

    @Override
    protected void initRequest() {
        request = new GetAgeBracketRequest();
    }

    @Override
    protected void formFilter(GetAgeBracketRequest filter) {
    	
    }
    
    @Inject
    @Override
    public void setSessionBean(AgeBracketSession sessionBean) {
    	log.debug("{0} sessionBean: {1}", Replacement.METHOD_NAME, sessionBean);
        this.sessionBean = sessionBean;
    }

    @Override
    protected boolean validateEntity() {
    	boolean valid = true;
		if(getEntity().getName().length() > 255){
    		addErrorMessage("Поле 'Название' - 255 символов максимум");
            valid  = false;
    	}
    	
    	if(getEntity().getDescription().length() > 255){
    		addErrorMessage("Поле 'Описание' - 255 символов максимум");
            valid = false;
    	}
        
        if(getEntity().getMinimumAge() >= getEntity().getMaximumAge()){
    		addErrorMessage("Нижний порог вознаста не может быть больше верхнего");
            valid = false;
    	}
        
        return valid;
    }

    @Override
    protected boolean validateFilter(GetAgeBracketRequest filter) {
        return true;
    }

    @Override
    public void clearFilterValues() {
    	
    }
}
