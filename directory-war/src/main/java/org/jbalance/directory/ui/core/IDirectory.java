package org.jbalance.directory.ui.core;

import java.util.List;


/**
 * Интерфейс справочника.
 *
 * @author Alexandr Chubenko
 * @param <T> Сущность справочника
 */
public interface IDirectory<T> {

    /**
     * Добавление элемента справочника.
     *
     * @param entity
     */
    public void add(T entity);

    /**
     * Редактирование элемента справочника.
     *
     * @param entity
     */
    public void edit(T entity);

    /**
     * Удаление элемента справочника.
     *
     * @param entity
     */
    public void delete(T entity);

    /**
     * Возвращает список объектов заданного типа.<br/>
     * Используется в list view для отрисовки таблицы сущностей.
     *
     * @return
     */
    public List<T> list();
}
