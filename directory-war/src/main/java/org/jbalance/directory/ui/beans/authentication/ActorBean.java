package org.jbalance.directory.ui.beans.authentication;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 * Actor managed session bean. Provides user info.
 * @author Alexandr Chubenko
 */
@Named("actor")
@SessionScoped
public class ActorBean implements Serializable {
    
    private static final long serialVersionUID = -1L;
    
}
