package org.jbalance.directory.ui.beans.norm;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.NumberFilterType;
import org.jbalance.directory.core.model.norm.GrossNorm;
import org.jbalance.directory.core.model.norm.NetNorm;
import org.jbalance.directory.core.query.helper.norm.GetGrossNormsRequest;
import org.jbalance.directory.core.session.norm.GrossNormSession;
import org.jbalance.directory.core.util.Log;
import org.jbalance.directory.ui.core.AbstractDirectoryUIBean;

/**
 *
 * @author Mihail
 */
@SessionScoped
@Named("GrossNorms")
public class GrossNormsDirectoryUIBean extends AbstractDirectoryUIBean<GrossNorm, GrossNormSession, GetGrossNormsRequest> {

    protected NetNorm netNorm = null;
	
	private boolean editingGrossNorms = false;
    
    @Inject
    NetNormsDirectoryUIBean netNorms;
    
    @Override
    protected void initEntity() {
        entity = new GrossNorm();
        entity.setNetNorm(getNetNorm());
    }

    @Override
    protected void initRequest() {
        request = new GetGrossNormsRequest();
    }

    @Override
    protected void formFilter(GetGrossNormsRequest filter) {
        if (getNetNorm() != null){
            LongFilter netNormFilter = new LongFilter(NumberFilterType.EQUALS, getNetNorm().getId());
            filter.setNetNormFilter(netNormFilter);
        }
    }

    @Override
    public void clearFilterValues() {
    }

    @Inject
    @Override
    public void setSessionBean(GrossNormSession sessionBean) {
        log.debug("{0} sessionBean: {1}", Log.Replacement.METHOD_NAME, sessionBean);
        this.sessionBean = sessionBean;
    }

    @Override
    protected boolean validateFilter(GetGrossNormsRequest filter) {
        boolean valid = true;
        //if (filter.getNetNormFilter() == null) valid = false;
        return valid;
    }

    @Override
    protected boolean validateEntity() {
		if (getEntity() == null || getEntity().getStartMonth() == null || getEntity().getEndMonth() == null || getEntity().getQuantity() == null){
            return false;
        }
        boolean valid = true;
		if(getEntity().getNetNorm() == null){
    		addErrorMessage("Не установлена норма нетто");
            valid = false;
    	}
    	if(getEntity().getQuantity() < 0){
    		addErrorMessage("Количество не может быть отрицательным значением");
            valid = false;
    	}
    	if(getEntity().getStartMonth() > getEntity().getEndMonth()){
    		addErrorMessage("Начало действия нормы не может быть позже окончания(некорректно указаны месяцы)");
            valid = false;
    	}
		
//        Iterator<GrossNorm> i = getList().iterator();
//        while (i.hasNext()){
//            GrossNorm next = i.next();
//            if (!next.equals(getEntity()) && (next.getEndMonth() <= getEntity().getStartMonth() || next.getStartMonth() >= getEntity().getEndMonth())){
//                addErrorMessage("Пересечение месяцев действия норм");
//                valid = false;
//                break;
//            }
//        }
        return valid;
    }
    
    public void setNetNorm(NetNorm netNorm){
    	this.netNorm = netNorm;
        clearNotPersistentList();
        list();
    }
    
    public NetNorm getNetNorm(){
    	return netNorm;
    }
    
    public void clearNetNormValue(){
        netNorm = null;
    }
    
    //TODO: попробовать обойтись без переопределения
	@Override
    public List<GrossNorm> getEditingList(){
        if (getNetNorm() == null || !getNetNorm().equals(netNorms.getEntity())){
            setNetNorm(netNorms.getEntity());
        }
		return super.getEditingList();
    }
	
    public String getListAsString(){
        Iterator<GrossNorm> i = getList().iterator();
        StringBuilder sb = new StringBuilder();
        GrossNorm next;
        while (i.hasNext()){
            next = i.next();
            sb.append(next.getStartMonth()).append("-").append(next.getEndMonth()).append(" мес: ")
                    .append(next.getQuantity())
                    .append(next.getNetNorm().getMeasure().getRusShortName1())
                    .append(i.hasNext()?"<br/>":"");
        }
        return sb.toString();
    }
	
	public void startEditingGrossNorms(){
		setNetNorm(netNorms.getEntity());
		editingGrossNorms = true;
	}
	
	public void finishEditGrossNorms(){
		editingGrossNorms = false;
	}
	
	public boolean getEditState(){
		return editingGrossNorms;
	}
	
	public List<SelectItem> getMonthList(){
		ArrayList<SelectItem> monthList = new ArrayList<SelectItem>();
		monthList.add(new SelectItem(new Short("1"), "Январь"));
		monthList.add(new SelectItem(new Short("2"), "Февраль"));
		monthList.add(new SelectItem(new Short("3"), "Март"));
		monthList.add(new SelectItem(new Short("4"), "Апрель"));
		monthList.add(new SelectItem(new Short("5"), "Май"));
		monthList.add(new SelectItem(new Short("6"), "Июнь"));
		monthList.add(new SelectItem(new Short("7"), "Июль"));
		monthList.add(new SelectItem(new Short("8"), "Август"));
		monthList.add(new SelectItem(new Short("9"), "Сентябрь"));
		monthList.add(new SelectItem(new Short("10"), "Октябрь"));
		monthList.add(new SelectItem(new Short("11"), "Ноябрь"));
		monthList.add(new SelectItem(new Short("12"), "Декабрь"));
		return monthList;
	}
}
