package org.jbalance.directory.ui.beans.measure;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.NamingException;

import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.filter.StringFilterType;
import org.jbalance.directory.core.model.measure.Measure;
import org.jbalance.directory.core.query.helper.measure.GetMeasuresRequest;
import org.jbalance.directory.core.session.measure.MeasureSession;
import org.jbalance.directory.core.util.Log;
import org.jbalance.directory.ui.core.AbstractDirectoryUIBean;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@Named("Measures")
@SessionScoped
public class MeasureUIBean extends  AbstractDirectoryUIBean<Measure, MeasureSession, GetMeasuresRequest> {

	 private List<Measure> allMeasures;
	
    private String nameFilter = "";
	
	@Inject
    private MeasureSession measureSession;

    @Override
    protected void initRequest() {
        request = new GetMeasuresRequest();
    }

	@Override
	protected void setEditable(){
		editable = false;
	}
	
    @Override
    protected void formFilter(GetMeasuresRequest filter) {
        if (!"".equals(nameFilter)) {
			StringFilter nameStrFilter = new StringFilter(StringFilterType.CONTAINS, nameFilter);
            filter.setMeasureNameFilter(nameStrFilter);
        } else {
            filter.setMeasureNameFilter(null);
        }
    }

    @Override
    public void clearFilterValues() {
        nameFilter = "";
    }

    @Inject
    @Override
    public void setSessionBean(MeasureSession sessionBean) {
        log.debug("{0} sessionBean: {1}", Log.Replacement.METHOD_NAME, sessionBean);
        this.sessionBean = sessionBean;
    }

    @Override
    protected boolean validateFilter(GetMeasuresRequest filter) {
        return true;
    }

    @Override
    protected boolean validateEntity() {
        boolean valid = true;
        if (getEntity().getName().length() > 255) {
            addErrorMessage("Поле 'Название' - 255 символов максимум");
            valid = false;
        }

        return valid;
    }

    @Override
    protected void initEntity() {
        entity = new Measure();
    }

    public String getNameFilter() {
        return nameFilter;
    }

    public void setNameFilter(String nameFilter) {
        this.nameFilter = nameFilter;
    }

	public List<Measure> autocomplete(String s){
		GetMeasuresRequest measuresRequest = new GetMeasuresRequest();
		StringFilter filter = new StringFilter(StringFilterType.CONTAINS, s);
		measuresRequest.setMeasureNameFilter(filter);
		List<Measure> measures = null;
		try {
			measures = getSessionBean().list(measuresRequest);
		} catch (NamingException ex) {
			Logger.getLogger(MeasureUIBean.class.getName()).log(Level.SEVERE, null, ex);
		}
		return measures;
	}
	
	public List<Measure> getAllMeasures(){
    	if(allMeasures == null){
            GetMeasuresRequest measuresRequest = new GetMeasuresRequest();
            allMeasures = measureSession.list(measuresRequest);
    	}
	return allMeasures;
    }
}

