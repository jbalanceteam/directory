package org.jbalance.directory.ui.beans.norm;

import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import org.jbalance.directory.core.model.norm.AgeBracket;
import org.jbalance.directory.core.model.norm.NormGroup;
import org.jbalance.directory.core.model.norm.Stay;
import org.jbalance.directory.core.query.helper.norm.GetAgeBracketRequest;
import org.jbalance.directory.core.query.helper.norm.GetNormGroupRequest;
import org.jbalance.directory.core.query.helper.norm.GetStayRequest;
import org.jbalance.directory.core.session.norm.AgeBracketSession;
import org.jbalance.directory.core.session.norm.NormGroupSession;
import org.jbalance.directory.core.session.norm.StaySession;
import org.jbalance.directory.core.util.Log;
import org.jbalance.directory.ui.core.AbstractDirectoryUIBean;

/**
 *
 * @author Mihail
 */
@SessionScoped
@Named("NormGroups")
public class NormGroupDirectoryUIBean extends AbstractDirectoryUIBean<NormGroup, NormGroupSession, GetNormGroupRequest>{
    
    private List<AgeBracket> allAgeBrackets;
    private List<Stay> allStay;
    
    @Inject
    private StaySession staySession;
    
    @Inject
    private AgeBracketSession ageBracketSession;
	
	@Inject
	private NetNormsDirectoryUIBean netNorms;
    
    @Override
    protected void initEntity() {
        entity = new NormGroup();
    }

    @Override
    protected void initRequest() {
        request = new GetNormGroupRequest();
    }

    @Override
    protected void formFilter(GetNormGroupRequest filter) {
    }

    @Override
    public void clearFilterValues() {
        request = new GetNormGroupRequest();
    }

    @Inject
    @Override
    public void setSessionBean(NormGroupSession sessionBean) {
        log.debug("{0} sessionBean: {1}", Log.Replacement.METHOD_NAME, sessionBean);
        this.sessionBean = sessionBean;
    }

    @Override
    protected boolean validateFilter(GetNormGroupRequest filter) {
        return true;
    }

    @Override
    protected boolean validateEntity() {
        boolean valid = true;
    	if(getEntity().getAgeBracket() == null){
    		addErrorMessage("Не указана возрастная группа");
            valid = false;
    	}
        if(getEntity().getStay() == null){
    		addErrorMessage("Не указан режим пребывания");
            valid = false;
    	}
        return valid;
    }
    
    public List<AgeBracket> getAllAgeBrackets(){
    	if(allAgeBrackets == null){
            GetAgeBracketRequest ageBracketRequest = new GetAgeBracketRequest();
            allAgeBrackets = ageBracketSession.list(ageBracketRequest);
    	}
	return allAgeBrackets;
    }
        
    public List<Stay> getAllStay(){
    	if(allStay == null){
            GetStayRequest stayRequest = new GetStayRequest();
            allStay = staySession.list(stayRequest);
    	}
		
	return allStay;
    }
	
	@Override
	public String editDirectoryObject(){
		netNorms.setNormGroup(this.getEntity());
		return super.editDirectoryObject();
	}
}
