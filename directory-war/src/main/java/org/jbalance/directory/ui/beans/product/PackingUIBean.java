package org.jbalance.directory.ui.beans.product;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.NamingException;

import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.NumberFilterType;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.filter.StringFilterType;
import org.jbalance.directory.core.model.product.ProductImage;
import org.jbalance.directory.core.model.product.Packing;
import org.jbalance.directory.core.query.helper.product.GetPackingsRequest;
import org.jbalance.directory.core.session.product.PackingSession;
import org.jbalance.directory.core.util.Log;
import org.jbalance.directory.ui.core.AbstractDirectoryUIBean;

/**
 *
 * @author Mihail
 */
@SessionScoped
@Named("PackingUIBean")
public class PackingUIBean extends AbstractDirectoryUIBean<Packing, PackingSession, GetPackingsRequest>{

	private ProductImage productImage;
	
	@Inject
	private ProductImageUIBean productImages;
	
	@Override
	protected void initEntity() {
		entity = new Packing();
	}

	@Override
	protected void initRequest() {
		request = new GetPackingsRequest();
	}

	@Override
	protected void formFilter(GetPackingsRequest filter) {
		if (getProductImage() != null){
            LongFilter productImageFilter = new LongFilter(NumberFilterType.EQUALS, getProductImage().getId());
            filter.setImageFilter(productImageFilter);
        }
	}

	@Override
	public void clearFilterValues() {
		productImage = null;
	}
	
	@Inject
	@Override
	public void setSessionBean(PackingSession sessionBean) {
        log.debug("{0} sessionBean: {1}", Log.Replacement.METHOD_NAME, sessionBean);
        this.sessionBean = sessionBean;
	}

	@Override
	protected boolean validateFilter(GetPackingsRequest filter) {
		return true;
	}

	@Override
	protected boolean validateEntity() {
		if (getEntity() == null || getEntity().getImage() == null || getEntity().getMeasure() == null || getEntity().getName() == null){
            return false;
        }

        return true;
	}
	
	public ProductImage getProductImage(){
        return productImage;
    }
    
    public void setProductImage(ProductImage productImage){
        this.productImage = productImage;
        clearNotPersistentList();
        list();
    }
	
	@Override
    public List<Packing> getEditingList(){
        if (getProductImage()== null || !getProductImage().equals(productImages.getEntity())){
            setProductImage(productImages.getEntity());
        }
        return super.getEditingList();
    }
    
	@Override
    public void addNotPersistent(){
        initEntity();
        getEntity().setImage(getProductImage());
        getNotPersistentList().add(entity);
    }
	
	public List<Packing> getList(ProductImage image){
		setProductImage(image);
		return getList();
	}


	public List<Packing> autocomplete(String s){
		GetPackingsRequest getPackingsRequest = new GetPackingsRequest();
		if (getProductImage() != null) {
			LongFilter productImageFilter = new LongFilter(NumberFilterType.EQUALS, getProductImage().getId());
			getPackingsRequest.setImageFilter(productImageFilter);
		}
		StringFilter packingNameFilter = new StringFilter(StringFilterType.STARTS_WITH, s);
		getPackingsRequest.setNameFilter(packingNameFilter);

		try {
			return getSessionBean().list(getPackingsRequest);
		} catch (NamingException ex) {
			Logger.getLogger(ProductImageUIBean.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}
}
