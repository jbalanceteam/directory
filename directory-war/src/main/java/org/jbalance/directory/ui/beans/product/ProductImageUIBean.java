package org.jbalance.directory.ui.beans.product;

import java.math.BigDecimal;
import java.text.DateFormatSymbols;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.NamingException;

import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.filter.StringFilterType;
import org.jbalance.directory.core.model.flowchart.ColdProcessingLossPercent;
import org.jbalance.directory.core.model.product.ProductImage;
import org.jbalance.directory.core.query.helper.product.GetProductImagesRequest;
import org.jbalance.directory.core.session.product.ProductImageSession;
import org.jbalance.directory.core.util.Log;
import org.jbalance.directory.ui.core.AbstractDirectoryUIBean;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@Named("ProductImages")
@SessionScoped
public class ProductImageUIBean extends AbstractDirectoryUIBean<ProductImage, ProductImageSession, GetProductImagesRequest> {

    private String nameFilter = "";

    private ColdProcessingLossPercent lost = new ColdProcessingLossPercent();

    @Override
    protected void initEntity() {
        entity = new ProductImage();
    }

    @Override
    protected void initRequest() {
        request = new GetProductImagesRequest();
    }

    @Override
    protected void formFilter(GetProductImagesRequest filter) {
        if (!"".equals(nameFilter)) {
            request.setNameFilter(new StringFilter(StringFilterType.STARTS_WITH, nameFilter));
        } else {
            request.setNameFilter(null);
        }
    }

    @Override
    public void clearFilterValues() {
        nameFilter = "";
    }

    @Inject
    @Override
    public void setSessionBean(ProductImageSession sessionBean) {
        log.debug("{0} sessionBean: {1}", Log.Replacement.METHOD_NAME, sessionBean);
        this.sessionBean = sessionBean;
    }

    @Override
    protected boolean validateFilter(GetProductImagesRequest filter) {
        return true;
    }

    @Override
    protected boolean validateEntity() {
        boolean valid = true;
        if (getEntity().getName().length() > 255) {
            addErrorMessage("Поле 'Название' - 255 символов максимум");
            valid = false;
        }

        return valid;
    }

    public String getNameFilter() {
        return nameFilter;
    }

    public void setNameFilter(String nameFilter) {
        this.nameFilter = nameFilter;
    }

    public List<ProductImage> autocomplete(String s) {
        GetProductImagesRequest getProductImagesRequest = new GetProductImagesRequest();
        StringFilter productImageNameFilter = new StringFilter(StringFilterType.STARTS_WITH, s);
        getProductImagesRequest.setNameFilter(productImageNameFilter);
        try {
            return getSessionBean().list(getProductImagesRequest);
        } catch (NamingException ex) {
            Logger.getLogger(ProductImageUIBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void addLost() {
        lost.setImage(this.entity);
        this.entity.getLosts().add(lost);
        lost = new ColdProcessingLossPercent();
    }

    public void removeLost(ColdProcessingLossPercent l) {
        this.entity.getLosts().remove(l);
    }

    public String getMonth(int month) {
        return DateFormatSymbols.getInstance(Locale.forLanguageTag("ru")).getMonths()[month ];        
    }

    public int getMonthIndex(String month) {
        return Arrays.asList(DateFormatSymbols.getInstance(Locale.forLanguageTag("ru")).getMonths()).indexOf(month);
    }

    public String[] getMonths() {
        return DateFormatSymbols.getInstance(Locale.forLanguageTag("ru")).getMonths();
    }

    public ColdProcessingLossPercent getLost() {
        return lost;
    }

    public void setLost(ColdProcessingLossPercent lost) {
        this.lost = lost;
    }

}
