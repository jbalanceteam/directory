package org.jbalance.directory.ui.beans.product;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.NumberFilterType;
import org.jbalance.directory.core.model.product.ProductImage;
import org.jbalance.directory.core.model.product.ProductPacking;
import org.jbalance.directory.core.model.product.Product;
import org.jbalance.directory.core.model.product.Packing;
import org.jbalance.directory.core.query.helper.product.GetProductPackingsRequest;
import org.jbalance.directory.core.query.helper.product.GetPackingsRequest;
import org.jbalance.directory.core.session.product.ProductSession;
import org.jbalance.directory.core.session.product.ProductPackingSession;
import org.jbalance.directory.core.session.product.PackingSession;
import org.jbalance.directory.core.util.Log;
import org.jbalance.directory.ui.core.AbstractDirectoryUIBean;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@Named("ProductPackings")
@SessionScoped
public class ProductPackingUIBean extends AbstractDirectoryUIBean<ProductPacking, ProductPackingSession, GetProductPackingsRequest> {

    private static final long serialVersionUID = 1L;

	private Product product = null;

	private Packing packing = null;

    @Inject
    protected PackingSession packingSession;

    @Inject
    private ProductUIBean products;
	
    @Inject
    protected ProductSession productSession;

    @Inject
    protected ProductPackingSession productPackingSession;

//    private List<Product> products;

    @Override
    protected void initRequest() {
        request = new GetProductPackingsRequest();
    }

    @Override
    protected void formFilter(GetProductPackingsRequest filter) {
		if (getProduct()!= null){
            LongFilter productFilter = new LongFilter(NumberFilterType.EQUALS, getProduct().getId());
            filter.setProductFilter(productFilter);
        } 
    }

    @Override
    public void clearFilterValues() {
    }

    @Inject
    @Override
    public void setSessionBean(ProductPackingSession sessionBean) {
        log.debug("{0} sessionBean: {1}", Log.Replacement.METHOD_NAME, sessionBean);
        this.sessionBean = sessionBean;
    }

    @Override
    protected boolean validateFilter(GetProductPackingsRequest filter) {
        return true;
    }

    @Override
    protected boolean validateEntity() {
        boolean valid = true;

        return valid;
    }

    @Override
    protected void initEntity() {
        entity = new ProductPacking();
		entity.setProduct(getProduct());
		entity.setPacking(getPacking());
    }
		
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
        clearNotPersistentList();
        list();
    }

    public Packing getPacking() {
        return packing;
    }

    public void setPacking(Packing packing) {
        this.packing = packing;
    }
//
//	@Override
//    public List<ProductPacking> getEditingList(){
//        if (getProduct() == null || !getProduct().equals(products.getEntity())){
//            setProduct(products.getEntity());
//        }
//        return super.getEditingList();
//    }

    public void createAndSaveNewEntity(){
        initEntity();
        modifyAction();
    }

	public List<Packing> getAvailablePackings(){
		List<Packing> availablePackings = null;
		if (getProduct()!=null && getProduct().getImage()!=null){
			Iterator<ProductPacking> productPackingIterator = getList().iterator();
			LinkedList<Long> addedPacksIdList = new LinkedList<Long>();
			while (productPackingIterator.hasNext()){
				addedPacksIdList.add(productPackingIterator.next().getPacking().getId());
			}
			GetPackingsRequest getPackingsRequest = new GetPackingsRequest();
			LongFilter imageFilter = new LongFilter(NumberFilterType.EQUALS, getProduct().getImage().getId());

			LongFilter packingIdFilter = new LongFilter(NumberFilterType.IN, addedPacksIdList);
			packingIdFilter.setNegated(true);
			getPackingsRequest.setIdFilter(packingIdFilter);
			getPackingsRequest.setImageFilter(imageFilter);
			availablePackings = packingSession.list(getPackingsRequest);
		}
		return availablePackings;
	}

	protected boolean validateProduct(){
		if (getProduct() != null){
			return true;
		}
		setProduct(products.getEntity());
		if (getProduct() != null){
			return true;
		}
		return false;
	}

	public List<ProductPacking> getList(Product product){
		setProduct(product);
		return getList();
	}
}
