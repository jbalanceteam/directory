package org.jbalance.directory.ui.beans;

import java.io.Serializable;



import javax.enterprise.context.SessionScoped;
//import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.jbalance.directory.core.util.Log;

//@Named("TestBean")
@Named("TestBean")
@SessionScoped
public class TestBean implements Serializable {

	
	private static final long serialVersionUID = 1L;

	Log log = new Log(getClass()); 
	
	private Boolean panelVisability;

	public Boolean getPanelVisability() {
		log.entry(panelVisability);
		return panelVisability;
	}

	public void setPanelVisability(Boolean panelVisability) {
		log.entry(panelVisability);
		this.panelVisability = panelVisability;
	}
	
	public void changePanelVisability(){
		log.entry();
		if(panelVisability == null ){
			panelVisability = true;
		} else {
			panelVisability = !panelVisability;
		}
	}

	
}
