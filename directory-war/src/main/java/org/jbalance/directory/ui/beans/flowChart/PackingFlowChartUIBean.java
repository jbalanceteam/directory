package org.jbalance.directory.ui.beans.flowChart;

import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.NumberFilterType;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.filter.StringFilterType;
import org.jbalance.directory.core.model.flowchart.PackingFlowChart;
import org.jbalance.directory.core.model.product.Packing;
import org.jbalance.directory.core.model.product.ProductImage;
import org.jbalance.directory.core.query.helper.flowchart.GetPackingFlowChartRequest;
import org.jbalance.directory.core.query.helper.product.GetPackingsRequest;
import org.jbalance.directory.core.session.flowchart.PackingFlowChartSession;
import org.jbalance.directory.core.session.product.PackingSession;
import org.jbalance.directory.core.util.Log;
import org.jbalance.directory.ui.core.AbstractDirectoryUIBean;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Created by Mihail on 19.12.14.
 */
@SessionScoped
@Named("PackingFlowChartUI")
public class PackingFlowChartUIBean extends AbstractDirectoryUIBean<PackingFlowChart, PackingFlowChartSession, GetPackingFlowChartRequest> {

    private ProductImage imageFilterValue;
    private ProductImage image;
    private Packing packing;
    private String nameFilter;

    @Inject
    PackingSession packingSession;

    @Inject
    PackingFlowChartPacksUIBean packingFlowChartPacksUIBean;

    @Override
    protected void initEntity() {
        entity = new PackingFlowChart();
        entity.setTargetPacking(getPacking());
    }

    @Override
    protected void initRequest() {
        request = new GetPackingFlowChartRequest();
    }

    @Override
    protected void formFilter(GetPackingFlowChartRequest filter) {
        if (getImageFilterValue() != null){
            LongFilter imageFilter = new LongFilter(NumberFilterType.EQUALS, getImageFilterValue().getId());
            filter.setImageFilter(imageFilter);
        }
        if (getPacking() != null){
            LongFilter packingFilter = new LongFilter(NumberFilterType.EQUALS, getPacking().getId());
            filter.setPackingFilter(packingFilter);
        }
        if (getNameFilter() != null){
            StringFilter nameFilter1 = new StringFilter(StringFilterType.CONTAINS, nameFilter);
            filter.setNameFilter(nameFilter1);
        }
    }

    @Override
    public void clearFilterValues() {
        imageFilterValue = null;
        packing = null;
        nameFilter = null;

    }

    @Inject
    @Override
    public void setSessionBean(PackingFlowChartSession sessionBean) {
        log.debug("{0} sessionBean: {1}", Log.Replacement.METHOD_NAME, sessionBean);
        this.sessionBean = sessionBean;
    }

    @Override
    protected boolean validateFilter(GetPackingFlowChartRequest filter) {
        return true;
    }

    @Override
    protected boolean validateEntity() {
        return true;
    }

    public Packing getPacking(){
        return packing;
    }

    public void setPacking(Packing packing){
        this.packing = packing;
        clearNotPersistentList();
        list();
    }

    public String getNameFilter() {
        return nameFilter;
    }

    public void setNameFilter(String nameFilter) {
        this.nameFilter = nameFilter;
    }

    public ProductImage getImage() {
        return image;
    }

    public void setImage(ProductImage image) {
        this.image = image;
    }

    public List<Packing> getCurrentPackings(){
        GetPackingsRequest getPackingsRequest = new GetPackingsRequest();
        if (getImage()!=null) {
            LongFilter imageFilter = new LongFilter(NumberFilterType.EQUALS, getImage().getId());
            getPackingsRequest.setImageFilter(imageFilter);
        }
        return packingSession.list(getPackingsRequest);
    }

    @Override
    public String goToAddPage(){
        packingFlowChartPacksUIBean.setPackingFlowChart(getEntity());
        if (getEntity() != null && getEntity().getTargetPacking() != null && getEntity().getTargetPacking().getImage() != null) {
            setImage(getEntity().getTargetPacking().getImage());
        }else{
            setImage(null);
        }
        return super.goToAddPage();
    }

    public ProductImage getImageFilterValue() {
        return imageFilterValue;
    }

    public void setImageFilterValue(ProductImage imageFilterValue) {
        this.imageFilterValue = imageFilterValue;
        this.packing = null;
    }
}
