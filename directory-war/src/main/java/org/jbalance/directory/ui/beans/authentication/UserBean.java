package org.jbalance.directory.ui.beans.authentication;

import java.io.Serializable;

import javax.enterprise.inject.Produces;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;

import org.jbalance.directory.ui.beans.BaseUIBean;




@ViewScoped
@Named("user")
public class UserBean extends BaseUIBean implements Serializable,Cloneable {

    private static final long serialVersionUID = -1L;
//    @ManagedProperty(value = "#{actor}")
    @Produces
    private ActorBean actor;
    private String username;
    private String password;

    /**
     * Performs user login. Uses username and password fields
     */
    public void login() {
        try {
            clearMessages();
            addInfoMessage("Login successful.");
            reset();
        } catch (Exception ex) {
            clearMessages();
            ex.printStackTrace();
            addErrorMessage("Login failed. Invalid user name or password.");
        }
        //unload password from memory
        password = null;
    }

    public void signUp() {
        try {
            addInfoMessage("Registration successful. Use your credentials to login.");
            reset();
        } catch (Exception ex) {

            clearMessages();
            addErrorMessage("Could not sign you up due to internal service error.");
        }
    }

    public void reset() {
        username = null;
        password = null;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ActorBean getActor() {
        return actor;
    }

    public void setActor(ActorBean actor) {
        this.actor = actor;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); 
    }
}
