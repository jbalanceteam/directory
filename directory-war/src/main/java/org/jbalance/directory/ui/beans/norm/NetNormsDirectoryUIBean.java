package org.jbalance.directory.ui.beans.norm;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.NumberFilterType;
import org.jbalance.directory.core.model.norm.GrossNorm;

import org.jbalance.directory.core.model.norm.NetNorm;
import org.jbalance.directory.core.model.norm.NormGroup;
import org.jbalance.directory.core.model.norm.ProductCategory;
import org.jbalance.directory.core.query.helper.norm.GetGrossNormsRequest;
import org.jbalance.directory.core.query.helper.norm.GetNetNormsRequest;
import org.jbalance.directory.core.query.helper.norm.GetProductCategoryRequest;
import org.jbalance.directory.core.session.norm.GrossNormSession;
import org.jbalance.directory.core.session.norm.NetNormSession;
import org.jbalance.directory.core.session.norm.ProductCategorySession;
import org.jbalance.directory.core.util.Log;
import org.jbalance.directory.ui.core.AbstractDirectoryUIBean;

/**
 *
 * @author Mihail
 */
@SessionScoped
@Named("NetNorms")
public class NetNormsDirectoryUIBean extends AbstractDirectoryUIBean<NetNorm, NetNormSession, GetNetNormsRequest> {

    
    private NormGroup normGroup = null;
	
	private ProductCategory productCategory = null;
	
    @Inject
    private NormGroupDirectoryUIBean normGroups;
    
    @Inject
    private GrossNormsDirectoryUIBean grossNorms;
    
    @Inject
    private ProductCategorySession productCategorySession;
    
    @Inject
    private GrossNormSession grossNormSession;
	
    @Override
    protected void initEntity() {
        entity = new NetNorm();
        entity.setNormGroup(getNormGroup());
        entity.setProductCategory(getProductCategory());
    }

    @Override
    protected void initRequest() {
        request = new GetNetNormsRequest();
    }

    @Override
    protected void formFilter(GetNetNormsRequest filter) {
        if (getNormGroup() != null){
            LongFilter normGroupFilter = new LongFilter(NumberFilterType.EQUALS, getNormGroup().getId());
            filter.setNormGroupFilter(normGroupFilter);
        }
    }

    @Override
    public void clearFilterValues() {
        
    }
    
    @Inject
    @Override
    public void setSessionBean(NetNormSession sessionBean) {
        log.debug("{0} sessionBean: {1}", Log.Replacement.METHOD_NAME, sessionBean);
        this.sessionBean = sessionBean;
    }

    @Override
    protected boolean validateFilter(GetNetNormsRequest filter) {
        return true;
    }

    @Override
    protected boolean validateEntity() {
        if (getEntity() == null || getEntity().getProductCategory() == null || getEntity().getQuantity() == null || getEntity().getMeasure() == null){
            return false;
        }
    	if(getEntity().getQuantity() < 0){
            addErrorMessage("Количество не может быть отрицательным значением");
            return false;
    	}
        return true;
    }

	//TODO: попробовать обойтись без переопределения
	@Override
    public List<NetNorm> getEditingList(){
        if (getNormGroup() == null || !getNormGroup().equals(normGroups.getEntity())){
            setNormGroup(normGroups.getEntity());
        }
        return super.getEditingList();
    }

    /**
     * 
     * @return список категорий продуктов, для которых нет записи нормы нетто в текущей группе
     */
    public List<ProductCategory> getProductCategories(){
        Iterator<NetNorm> netNormsIterator = getEditingList().iterator();
		LinkedList<Long> addedProdCatIdList = new LinkedList<Long>();
        while (netNormsIterator.hasNext()){
			addedProdCatIdList.add(netNormsIterator.next().getProductCategory().getId());
        }
		GetProductCategoryRequest getProductCategoryRequest = new GetProductCategoryRequest();
		LongFilter prodCatIdFilter = new LongFilter(NumberFilterType.IN, addedProdCatIdList);
		prodCatIdFilter.setNegated(true);
		getProductCategoryRequest.setIdFilter(prodCatIdFilter);
		List<ProductCategory> productCategories = productCategorySession.list(getProductCategoryRequest);
	return productCategories;
    }

    public NormGroup getNormGroup(){
        return normGroup;
    }
    
    public void setNormGroup(NormGroup normGroup){
		grossNorms.finishEditGrossNorms();
        this.normGroup = normGroup;
        clearNotPersistentList();
        list();
    }
	
	public ProductCategory getProductCategory(){
        return productCategory;
    }
    
    public void setProductCategory(ProductCategory productCategory){
        this.productCategory = productCategory;
    }
    
    public List<GrossNorm> getGrossNorms(){
        GetGrossNormsRequest getGrossNormsRequest = new GetGrossNormsRequest();
        getGrossNormsRequest.setNetNormFilter(new LongFilter(NumberFilterType.EQUALS, getEntity().getId()));
        return grossNormSession.list(getGrossNormsRequest);
    }
    
    public String getGrossNormsAsString(NetNorm netNorm){
        if (!netNorm.isPersistent()) return null;
        grossNorms.setNetNorm(netNorm);
        return grossNorms.getListAsString();
    }
    
    public boolean renderGrossNormTable(NetNorm netNorm){
        return netNorm.equals(getEntity());
    }
}
