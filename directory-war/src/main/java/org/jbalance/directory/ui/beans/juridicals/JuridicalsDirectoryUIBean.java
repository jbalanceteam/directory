package org.jbalance.directory.ui.beans.juridicals;

import java.sql.SQLException;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.NamingException;

import org.jbalance.directory.core.filter.LongFilter;
import org.jbalance.directory.core.filter.NumberFilterType;
import org.jbalance.directory.core.filter.StringFilter;
import org.jbalance.directory.core.filter.StringFilterType;
import org.jbalance.directory.core.model.Juridical;
import org.jbalance.directory.core.model.educationalInstitution.EducationalInstitution;
import org.jbalance.directory.core.model.educationalInstitution.EducationalInstitutionType;
import org.jbalance.directory.core.query.helper.juridical.GetJuridicalsRequest;
import org.jbalance.directory.core.session.address.IAddressFinder;
import org.jbalance.directory.core.session.juridical.JuridicalSession;
import org.jbalance.directory.core.util.Str;
import org.jbalance.directory.ui.core.AbstractDirectoryUIBean;

/**
 * Бин представления контрагентов.
 *
 * @author Alexandr Chubenko
 */
@Named("Juridicals")
@SessionScoped
public class JuridicalsDirectoryUIBean extends AbstractDirectoryUIBean<Juridical, JuridicalSession, GetJuridicalsRequest> {

    private static final long serialVersionUID = 1L;

//	@Resource(mappedName = "java:jboss/datasources/fias_db", type=DataSource.class)
//	private DataSource addressDataSource;
////	@Resource(mappedName = "java:jboss/datasources/fias_db", type=DataSource.class)
//	public void setAddressDataSource(DataSource addressDataSource){
//		this.addressDataSource = addressDataSource;
//	}
//	
//	@PostConstruct
//	public void init(){
//		try {
//			addressDataSource = (DataSource) new InitialContext().lookup("java:jboss/datasources/fias_db");
////			addressDataSource = (DataSource) new InitialContext().lookup("java:jboss/datasources/directory");
//			
//		} catch (NamingException e) {
//			throw new RuntimeException(e);
//		}
//	}
    @Inject
    private IAddressFinder addressFinder;

    private String nameFilter;
    private Long innFilter;

    private Boolean isEducationalInstitution = false;
    /**
     * Показывать ли панель редактирования почтового адреса
     */
    private Boolean mailingAddressPanelVisible = false;
    private Boolean registeredOfficePanelVisible = false;

    private KladrPanelHelper mailingAddressHelper;
    private KladrPanelHelper registeredOfficeAddressHelper;

    private boolean registeredOfficeAddressTheSameAsMailing = false;

    @Override
    public void initEntity() {
        entity = new Juridical();
        isEducationalInstitution = false;
    }

    @Override
    protected void initRequest() {
        request = new GetJuridicalsRequest();
        nameFilter = null;
        innFilter = null;
    }

    @Override
    protected void formFilter(GetJuridicalsRequest filter) {
        if (!Str.empty(nameFilter)) {
            filter.setJuridicalNameFilter(new StringFilter(StringFilterType.CONTAINS, nameFilter));
        }
        if (innFilter != null) {
            filter.setJuridicalInnFilter(new LongFilter(NumberFilterType.EQUALS, innFilter));
        }
    }

    @Inject
    @Override
    public void setSessionBean(JuridicalSession sessionBean) {
        this.sessionBean = sessionBean;
    }

    public Long getInnFilter() {
        return innFilter;
    }

    public void setInnFilter(Long innFilter) {
        this.innFilter = innFilter;
    }

    public String getNameFilter() {
        return nameFilter;
    }

    public void setNameFilter(String nameFilter) {
        this.nameFilter = nameFilter;
    }

    private void convertJuridicalToEducationalInstitution() {
        if (getEntity().isPersistent()) {
            throw new RuntimeException("We can't modify object's class for persistent objects");
        }
        EducationalInstitution newEntity = new EducationalInstitution();
        if (entity != null) {
            copyFields(entity, newEntity);
        }
        entity = newEntity;
    }

    public void changeObjectType() {
        log.entry();
        if (isEducationalInstitution) {
            log.debug("Now it's Educational Institution");
            convertJuridicalToEducationalInstitution();
        } else {
            log.debug("Now it's Juridical");
            convertEducationalInstitutionToJuridical();
        }
    }

    private void convertEducationalInstitutionToJuridical() {
        if (getEntity().isPersistent()) {
            throw new RuntimeException("We can't modify object's class for persistent objects");
        }
        Juridical newEntity = new Juridical();
        if (entity != null) {
            copyFields(entity, newEntity);
        }
        entity = newEntity;
    }

    private void copyFields(Juridical source, Juridical destinition) {
        destinition.setRegisteredOffice(source.getRegisteredOffice());
        destinition.setOgrn(source.getOgrn());
        destinition.setName(source.getName());
        destinition.setInn(source.getInn());
        destinition.setMailingAdress(source.getMailingAdress());
        destinition.setContactInfo(source.getContactInfo());
        destinition.setPhone(source.getPhone());
        destinition.setEmail(source.getEmail());
    }

    public EducationalInstitutionType[] getAllEducationalInstitutionTypes() {
        return EducationalInstitutionType.values();
    }

    @Override
    protected boolean validateEntity() {
        boolean valid = true;
        valid = validateInn(valid);
//        valid = validateAddresses(valid);
        return valid;
    }

    private boolean validateAddresses(boolean valid) {
        if (Str.empty(getEntity().getRegisteredOffice())) {
            addErrorMessage("Поле 'Адрес регистрации' обязательно к заполнению");
            valid = false;
        }
        if (Str.empty(getEntity().getMailingAdress())) {
            addErrorMessage("Поле 'Почтовый адрес' обязательно к заполнению");
            valid = false;
        }
        return valid;
    }

    private boolean validateInn(boolean valid) {
        //фильтр ИНН
        if (getEntity().getInn() != null) {
            //Конвертим в строку, чтобы проверить длину ИНН (10 или 12 цифр).
            String sInn = getEntity().getInn().toString();
            if (sInn.length() != 10 && sInn.length() != 12) {
                addErrorMessage("Поле 'ИНН' должно содержать 10 или 12 цифр.");
                valid = false;
            } else {
                valid = validateInnDuplicates(valid);
            }
        }
        return valid;
    }

    private boolean validateInnDuplicates(boolean valid) {
        //Проверяем на дубликаты ИНН
        GetJuridicalsRequest testDuplicates = new GetJuridicalsRequest();
        testDuplicates.setJuridicalInnFilter(new LongFilter(getEntity().getInn()));
        try {
            List<Juridical> list = getSessionBean().list(testDuplicates);
            if (list.isEmpty()) {
                return true;
            }

            if (list.size() == 1) {
                if (list.get(0).equals(getEntity())) {
                    return true;
                }
            }

            addErrorMessage("Контрагент с таким ИНН уже существует.");
            valid = false;
        } catch (NamingException ex) {
            log.error(ex);
            addErrorMessage("Ошибка! Не удалось проверить уникальность ИНН.");
            valid = false;
        }
        return valid;
    }

    @Override
    protected boolean validateFilter(GetJuridicalsRequest filter) {
        return true;
    }

    @Override
    public void clearFilterValues() {
        innFilter = null;
        nameFilter = null;
    }

    public Boolean getIsEducationalInstitution() {
        return isEducationalInstitution;
    }

    public void setIsEducationalInstitution(Boolean isEducationalInstitution) {
        this.isEducationalInstitution = isEducationalInstitution;
    }

    public Boolean getMailingAddressPanelVisible() {
        return mailingAddressPanelVisible;
    }

    public void setMailingAddressPanelVisible(Boolean mailingAddressPanelVisible) {
        this.mailingAddressPanelVisible = mailingAddressPanelVisible;
    }

    public void changeMailingAddressPanelVisibility() {
        if (mailingAddressPanelVisible == null) {
            mailingAddressPanelVisible = true;
        } else {
            mailingAddressPanelVisible = !mailingAddressPanelVisible;
        }
        log.trace("mailingAddressPanelVisible: {0}", mailingAddressPanelVisible);
    }

    public Boolean getRegisteredOfficePanelVisible() {
        return registeredOfficePanelVisible;
    }

    public void setRegisteredOfficePanelVisible(Boolean registeredOfficePanelVisible) {
        this.registeredOfficePanelVisible = registeredOfficePanelVisible;
    }

    public void changeRegisteredOfficePanelVisibility() {
        if (registeredOfficePanelVisible == null) {
            registeredOfficePanelVisible = true;
        } else {
            registeredOfficePanelVisible = !registeredOfficePanelVisible;
        }
    }

    public KladrPanelHelper getRegisteredOfficeAddressHelper() throws NamingException, SQLException {
        if (registeredOfficeAddressHelper == null) {
            IAddressFinder addressFinder = getAddressFinder();
            KladrListener listener = new KladrListener() {
                @Override
                public void saveAddress(String address) {
                    getEntity().setRegisteredOffice(address);
                    registeredOfficePanelVisible = false;
                }

                @Override
                public void cancel() {
                    registeredOfficePanelVisible = false;
                }
            };
            registeredOfficeAddressHelper = new KladrPanelHelper(addressFinder, listener);
        }
        return registeredOfficeAddressHelper;
    }

    private IAddressFinder getAddressFinder() throws SQLException {
        if (addressFinder == null) {
//			Connection connection = addressDataSource.getConnection();
//			addressFinder = new AddressFinder(connection);
        }
        return addressFinder;
    }

    public KladrPanelHelper getMailingAddressHelper() throws NamingException, SQLException {
        if (mailingAddressHelper == null) {
            IAddressFinder addressFinder = getAddressFinder();
            KladrListener listener = new KladrListener() {
                @Override
                public void saveAddress(String address) {
                    getEntity().setMailingAdress(address);
                    mailingAddressPanelVisible = false;
                }

                @Override
                public void cancel() {
                    mailingAddressPanelVisible = false;
                }
            };
            mailingAddressHelper = new KladrPanelHelper(addressFinder, listener);
        }
        return mailingAddressHelper;
    }

    public boolean isRegisteredOfficeAddressTheSameAsMailing() {
        return registeredOfficeAddressTheSameAsMailing;
    }

    public void setRegisteredOfficeAddressTheSameAsMailing(
            boolean registeredOfficeAddressTheSameAsMailing) {
        this.registeredOfficeAddressTheSameAsMailing = registeredOfficeAddressTheSameAsMailing;
    }

    public void changeRegisteredOfficeAddressTheSameAsMailing() {
        if (registeredOfficeAddressTheSameAsMailing) {
            getEntity().setRegisteredOffice(getEntity().getMailingAdress());
        } else {
            getEntity().setRegisteredOffice("");
        }
    }

    public void copyMailingAddressToRegistredOffice() {
        getEntity().setRegisteredOffice(getEntity().getMailingAdress());
    }
    public void copyRegistredOfficeToMailingAddress() {
        getEntity().setMailingAdress(getEntity().getRegisteredOffice());
    }

    public void clearRegistredOfficeField(){
        if (entity != null) {
            entity.setRegisteredOffice(null);
        }
    }

    public void clearMailingAddressField(){
        if (entity != null){
            entity.setMailingAdress(null);
        }
    }

}
