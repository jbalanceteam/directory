package org.jbalance.directory.ui.beans.measure;

import java.io.Serializable;
import java.util.Iterator;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import org.jbalance.directory.core.model.measure.Measure;
import org.jbalance.directory.core.util.Log;

/**
 *
 * @author Mihail
 */
@ViewScoped
@FacesConverter("MeasureConverter")
public class MeasureConverter implements Converter, Serializable {
	
	@Inject
	private MeasureUIBean measures;

	private Log log = new Log(getClass());
	
    @Override
    public Object getAsObject(FacesContext arg0, UIComponent arg1, String name) {
		if (name == null || name.equals("")){return null; }
        Iterator<Measure> measureIterator = measures.getAllMeasures().iterator();
		while(measureIterator.hasNext()){
			Measure next = measureIterator.next();
			if (next.getName().equals(name)){
				log.trace("getAsObject({0}) name: {1}", name , next);
				return next;
			}
		}
		return null;
    }

    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object entity) {
        String name = null;
		if (entity!=null){
			name = ((Measure) entity).getName();
			log.trace("getAsString({0}) name: {1}", entity , name);
		}
		return name;
    }
	
}
