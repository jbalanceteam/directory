package org.jbalance.directory.client.query.helper.product;

import org.jbalance.core.query.GetResult;
import org.jbalance.directory.client.model.product.ProductImage;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author roman
 */
public class GetProductImageResult extends GetResult {

    private static final long serialVersionUID = 1L;

    private List<ProductImage> records;

    public GetProductImageResult() {
    }

    public GetProductImageResult(GetProductImageRequest request) {
        super(request);
    }

    @XmlElement(name = "packings")
    public List<ProductImage> getRecords() {
        if (records == null) {
            setRecords(new ArrayList<ProductImage>());
        }
        return records;
    }

    public void setRecords(List<ProductImage> packings) {
        this.records = packings;
    }
}
