package org.jbalance.directory.client.util.builder.product;

import org.jbalance.directory.client.model.product.Packing;
import org.jbalance.directory.client.util.builder.BaseDirectoryBeanBuilder;
import org.jbalance.directory.client.util.builder.DirectoryBeanBuilder;
import org.jbalance.directory.client.util.builder.general.MeasureBuilder;

/**
 * Created by user on 05.02.15.
 */
public class PackingBuilder extends BaseDirectoryBeanBuilder<Packing>
        implements DirectoryBeanBuilder<Packing> {

    public PackingBuilder() {
        proxyClassName = "Packing";
    }

    public PackingBuilder(Object proxy) {
        proxyClassName = "Packing";
        setProxy(proxy);
    }

    @Override
    public Packing buildEntity() {
        if (proxy == null) {
            return null;
        }
        entity = new Packing();
        setBaseParams();
        entity.setQuantity((Float) getProxyParam("quantity"));
        entity.setName((String) getProxyParam("name"));
        entity.setMeasure((new MeasureBuilder(getProxyParam("measure"))).buildEntity());
        entity.setDescription((String) getProxyParam("description"));
        entity.setImage(new ProductImageBuilder(getProxyParam("image")).buildEntity());

        entity.setCompositeName(entity.getImage().getName() + " " + entity.getName());
        return entity;
    }

    @Override
    public org.jbalance.directory.client.packing.Packing buildProxy() {
        return null;
    }

}
