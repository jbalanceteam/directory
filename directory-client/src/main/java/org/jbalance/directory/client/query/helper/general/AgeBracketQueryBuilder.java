package org.jbalance.directory.client.query.helper.general;

import org.jbalance.core.query.helper.AbstractQueryBuilder;
import org.jbalance.directory.client.model.general.AgeBracket;

/**
 * Created by Mihail on 24.05.2015.
 */
public class AgeBracketQueryBuilder extends AbstractQueryBuilder<AgeBracketQueryBuilder> {

    public AgeBracketQueryBuilder()
    {
        super(null, AgeBracket.class, "ageBracket");
    }

    @Override
    public StringBuilder buildJoins()
    {
        StringBuilder joins = new StringBuilder()
                .append("LEFT JOIN FETCH ageBracket.juridicals juridicals ");
        return joins;
    }

    @Override
    protected AgeBracketQueryBuilder get() {
        return this;
    }
}
