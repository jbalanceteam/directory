package org.jbalance.directory.client.wsclient;

import org.jbalance.directory.client.product.Products;
import org.jbalance.directory.client.product.Products_Service;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@Stateless
public class ProductClient extends BaseDirectoryClient
{

    public ProductClient()
    {
    }

    @PostConstruct
    public void init()
    {
        wsdlLocation = getWsdlLocation(Products.class.getSimpleName());
    }

    public Products getPort()
    {
        Products_Service s = null;

        if (wsdlLocation != null)
        {
            s = new Products_Service(wsdlLocation);
        } else
        {
            s = new Products_Service();
        }

        return s.getProductsPort();
    }
}
