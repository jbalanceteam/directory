package org.jbalance.directory.client.util.builder.product;

import org.jbalance.directory.client.model.product.ProductPacking;
import org.jbalance.directory.client.util.builder.BaseDirectoryBeanBuilder;
import org.jbalance.directory.client.util.builder.DirectoryBeanBuilder;
import org.jbalance.directory.client.util.builder.product.PackingBuilder;
import org.jbalance.directory.client.util.builder.product.ProductBuilder;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class ProductPackingBuilder extends BaseDirectoryBeanBuilder<ProductPacking>
        implements DirectoryBeanBuilder<ProductPacking> {

    public ProductPackingBuilder() {
        proxyClassName = "ProductPacking";
    }

    public ProductPackingBuilder(Object proxy) {
        proxyClassName = "ProductPacking";
        setProxy(proxy);
    }

    @Override
    public ProductPacking buildEntity() {
        if (proxy == null) {
            return null;
        }
        entity = new ProductPacking();
        setBaseParams();
        entity.setPacking((new PackingBuilder(getProxyParam("packing"))).buildEntity());
        entity.setProduct((new ProductBuilder(getProxyParam("product"))).buildEntity());
        entity.setCompositeName((entity.getProduct() != null ? entity.getProduct().getName() : null) + " " + (entity.getPacking() != null ? entity.getPacking().getName() : null));

        return entity;
    }

    @Override
    public org.jbalance.directory.client.productpacking.ProductPacking buildProxy() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
