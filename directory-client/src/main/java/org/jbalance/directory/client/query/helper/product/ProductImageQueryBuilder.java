package org.jbalance.directory.client.query.helper.product;

import org.jbalance.core.query.helper.AbstractQueryBuilder;
import org.jbalance.directory.client.model.product.ProductImage;

/**
 *
 * @author roman
 */
public class ProductImageQueryBuilder extends AbstractQueryBuilder<ProductImageQueryBuilder> {

    public ProductImageQueryBuilder() {
        super(null, ProductImage.class, "image");
    }

    @Override
    public StringBuilder buildJoins() {
        StringBuilder joins = new StringBuilder()
                .append("LEFT JOIN  image.juridicals juridicals ");
        return joins;
    }

    @Override
    protected ProductImageQueryBuilder get() {
        return this;
    }

}
