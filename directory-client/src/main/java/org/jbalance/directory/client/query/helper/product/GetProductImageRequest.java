package org.jbalance.directory.client.query.helper.product;

import org.jbalance.core.filter.DateFilter;
import org.jbalance.core.filter.LongFilter;
import org.jbalance.core.filter.StringFilter;
import org.jbalance.core.query.GetRequest;

/**
 *
 * @author roman
 */
public class GetProductImageRequest extends GetRequest<GetProductImageResult> {

    private static final long serialVersionUID = 1L;

    private LongFilter idFilter;

    private StringFilter productNameFilter;

    private DateFilter modifiedFilter;

    private LongFilter juridicalFilter;
    

    
    @Override
    public GetProductImageResult createResult() {
        return new GetProductImageResult(this);
    }

    public LongFilter getIdFilter() {
        return idFilter;
    }

    public void setIdFilter(LongFilter idFilter) {
        this.idFilter = idFilter;
    }

    public StringFilter getProductNameFilter() {
        return productNameFilter;
    }

    public void setProductNameFilter(StringFilter name) {
        this.productNameFilter = name;
    }

    public DateFilter getModifiedFilter() {
        return modifiedFilter;
    }

    public void setModifiedFilter(DateFilter modifiedFilter) {
        this.modifiedFilter = modifiedFilter;
    }

    public LongFilter getJuridicalFilter() {
        return juridicalFilter;
    }

    public void setJuridicalFilter(LongFilter juridicalFilter) {
        this.juridicalFilter = juridicalFilter;
    }
}
