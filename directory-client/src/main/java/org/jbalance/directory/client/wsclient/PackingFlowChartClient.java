package org.jbalance.directory.client.wsclient;

import org.jbalance.directory.client.packingflowchart.PackingFlowChartService;
import org.jbalance.directory.client.packingflowchart.PackingFlowChartService_Service;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

/**
 * Created by Mihail on 27.01.15.
 */
@Stateless
public class PackingFlowChartClient extends BaseDirectoryClient {

	public PackingFlowChartClient() {
	}

	@PostConstruct
	public void init() {
		wsdlLocation = getWsdlLocation(PackingFlowChartService.class.getSimpleName());
	}

	public PackingFlowChartService getPort() {
		PackingFlowChartService_Service s = null;

		if (wsdlLocation != null) {
			s = new PackingFlowChartService_Service(wsdlLocation);
		} else {
			s = new PackingFlowChartService_Service();
		}

		return s.getPackingFlowChartServicePort();
	}
}