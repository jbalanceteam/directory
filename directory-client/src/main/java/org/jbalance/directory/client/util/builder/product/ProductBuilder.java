package org.jbalance.directory.client.util.builder.product;

import org.jbalance.directory.client.model.product.Product;
import org.jbalance.directory.client.util.builder.BaseDirectoryBeanBuilder;
import org.jbalance.directory.client.util.builder.DirectoryBeanBuilder;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class ProductBuilder extends BaseDirectoryBeanBuilder<Product> implements DirectoryBeanBuilder<Product> {

    public ProductBuilder() {
        proxyClassName = "Product";
    }

    public ProductBuilder(Object proxy) {
        proxyClassName = "Product";
        setProxy(proxy);
    }

    @Override
    public Product buildEntity() {
        if (proxy == null) {
            return null;
        }
        entity = new Product();
        setBaseParams();
        entity.setName((String) getProxyParam("name"));
        entity.setTradeMark((new TradeMarkBuilder(getProxyParam("tradeMark"))).buildEntity());
        entity.setImage((new ProductImageBuilder(getProxyParam("image"))).buildEntity());
        return entity;
    }

    @Override
    public org.jbalance.directory.client.product.Product buildProxy() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
