package org.jbalance.directory.client.util.builder.product;

import org.jbalance.directory.client.model.product.TradeMark;
import org.jbalance.directory.client.util.builder.BaseDirectoryBeanBuilder;
import org.jbalance.directory.client.util.builder.DirectoryBeanBuilder;

import javax.annotation.PostConstruct;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class TradeMarkBuilder extends BaseDirectoryBeanBuilder<TradeMark>
        implements DirectoryBeanBuilder<TradeMark> {

    public TradeMarkBuilder() {
        proxyClassName = "TradeMark";
    }

    public TradeMarkBuilder(Object proxy) {
        proxyClassName = "TradeMark";
        setProxy(proxy);
    }

    @Override
    public TradeMark buildEntity() {
        if (proxy == null) {
            return null;
        }
        entity = new TradeMark();
        setBaseParams();
        entity.setName((String)getProxyParam("name"));
        return entity;
    }

    @Override
    public org.jbalance.directory.client.trademark.TradeMark buildProxy() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
