package org.jbalance.directory.client.wsclient;

import org.jbalance.directory.client.addresses.Address;
import org.jbalance.directory.client.addresses.Addresses;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@Stateless
public class AddressClient extends BaseDirectoryClient
{

    public AddressClient()
    {
    }

    @PostConstruct
    public void init()
    {
        wsdlLocation = getWsdlLocation(Addresses.class.getSimpleName());
    }

    public Address getPort()
    {
        Addresses s = null;

        if (wsdlLocation != null)
        {
            s = new Addresses(wsdlLocation);
        } else
        {
            s = new Addresses();
        }

        return s.getAddressPort();
    }
}
