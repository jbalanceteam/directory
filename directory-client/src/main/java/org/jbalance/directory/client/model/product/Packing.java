package org.jbalance.directory.client.model.product;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import org.jbalance.core.model.BaseDirectoryEntity;
import org.jbalance.directory.client.model.general.Measure;

/**
 *
 * @author roman
 */
@Entity
@NamedQuery(name = "Packing.lastUpdate", query = "SELECT e.syncDate FROM Packing e ORDER By e.syncDate ASC")
public class Packing extends BaseDirectoryEntity {

    private String name;

    private Float quantity;

    private String compositeName;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private ProductImage image;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Measure measure;

    private String description;

    public Packing() {
    }

    public Packing(String name, Float quantity, Measure measure) {
        this.name = name;
        this.quantity = quantity;
        this.measure = measure;
    }

    public Packing(Long id, Date enteredDate, Date deleted, String name, Float quantity, Measure measure) {
        super(id, enteredDate, deleted);
        this.name = name;
        this.quantity = quantity;
        this.measure = measure;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    public Measure getMeasure() {
        return measure;
    }

    public void setMeasure(Measure measure) {
        this.measure = measure;
    }

    public ProductImage getImage() {
        return image;
    }

    public void setImage(ProductImage image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCompositeName() {
        return compositeName;
    }

    public void setCompositeName(String compositeName) {
        this.compositeName = compositeName;
    }

    @Override
    public String toString() {
        return getCompositeName() + " " + String.valueOf(getQuantity()) + " " + getMeasure().getName();
    }

}
