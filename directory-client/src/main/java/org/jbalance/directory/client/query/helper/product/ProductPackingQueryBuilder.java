package org.jbalance.directory.client.query.helper.product;

import org.jbalance.core.query.helper.AbstractQueryBuilder;
import org.jbalance.directory.client.model.product.ProductPacking;


/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class ProductPackingQueryBuilder extends AbstractQueryBuilder<ProductPackingQueryBuilder> {

    public ProductPackingQueryBuilder() {
        super(null, ProductPacking.class, "packing");
    }

    @Override
    public StringBuilder buildJoins() {
        StringBuilder joins = new StringBuilder()
                .append(" LEFT JOIN FETCH packing.product product ")
                .append(" LEFT JOIN FETCH product.image image ")
                .append(" LEFT JOIN FETCH packing.packing p")
                .append(" LEFT JOIN FETCH p.measure")
                .append(" LEFT JOIN  packing.juridicals juridicals ");
        return joins;
    }

    @Override
    protected ProductPackingQueryBuilder get() {
        return this;
    }

}
