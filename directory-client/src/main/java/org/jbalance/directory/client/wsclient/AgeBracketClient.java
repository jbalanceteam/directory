package org.jbalance.directory.client.wsclient;

import org.jbalance.directory.client.agebrackets.AgeBrackets;
import org.jbalance.directory.client.agebrackets.AgeBrackets_Service;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

/**
 * Created by Mihail on 26.05.2015.
 */
@Stateless
public class AgeBracketClient extends BaseDirectoryClient{
    public AgeBracketClient() {
    }

    @PostConstruct
    public void init() {
        wsdlLocation = getWsdlLocation(AgeBrackets.class.getSimpleName());
    }

    public AgeBrackets getPort() {
        AgeBrackets_Service s = null;

        if (wsdlLocation != null) {
            s = new AgeBrackets_Service(wsdlLocation);
        } else {
            s = new AgeBrackets_Service();
        }

        return s.getAgeBracketsPort();
    }
}
