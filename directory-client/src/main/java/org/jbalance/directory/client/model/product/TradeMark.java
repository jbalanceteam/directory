package org.jbalance.directory.client.model.product;

import java.util.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import org.jbalance.core.model.BaseDirectoryEntity;

/**
 * Торговая марка
 */
@Entity
@NamedQuery(name = "TradeMark.lastUpdate",query = "SELECT e.syncDate FROM TradeMark e ORDER By e.syncDate ASC")
public class TradeMark extends BaseDirectoryEntity {

    @Column
    private String name;

    public TradeMark() {
    }

    public TradeMark(Long id, Date enteredDate, Date deleted,String name) {
        super(id, enteredDate, deleted);
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
