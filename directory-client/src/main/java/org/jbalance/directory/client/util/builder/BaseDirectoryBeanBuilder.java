package org.jbalance.directory.client.util.builder;

import org.jbalance.core.model.BaseDirectoryEntity;
import org.jbalance.directory.client.packingflowchart.PackingFlowChart;

import javax.annotation.PostConstruct;
import javax.xml.datatype.XMLGregorianCalendar;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;

/**
 *
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public abstract class BaseDirectoryBeanBuilder<E extends BaseDirectoryEntity>
{
    protected String proxyClassName;
    protected Object proxy;
    protected E entity;

    public BaseDirectoryBeanBuilder()
    {
    }

    public BaseDirectoryBeanBuilder(Object p)
    {
        setProxy(p);
    }

    public void setProxy(Object p)
    {
        if (p != null && p.getClass().getSimpleName().equals(proxyClassName)) {
            proxy = p;
        }
    }

    public void setEntity(E e)
    {
        entity = e;
    }

    public abstract E buildEntity();

    public abstract Object buildProxy();

    protected void setBaseParams(){
        if (entity != null && proxy != null){
            entity.setId((Long)getProxyParam("id"));
            entity.setGuid((String)getProxyParam("guid"));
            entity.setSyncDate(new Date());
            if (getProxyParam("enteredDate") != null)
                entity.setEnteredDate(((XMLGregorianCalendar)getProxyParam("enteredDate")).toGregorianCalendar().getTime());
            
            if (getProxyParam("modifiedDate") != null)
                entity.setModifiedDate(((XMLGregorianCalendar)getProxyParam("modifiedDate")).toGregorianCalendar().getTime());
            
            if (getProxyParam("deletedDate") != null)
                entity.setDeleted(((XMLGregorianCalendar)getProxyParam("deletedDate")).toGregorianCalendar().getTime());
        }
    }
    //TODO: написать обработчики исключений
    public Object getProxyParam(String paramName){
        if (proxy != null && paramName.length() > 1) {
            String methodName = "get" + paramName.substring(0,1).toUpperCase() + paramName.substring(1,paramName.length());
            try {
                return proxy.getClass().getMethod(methodName).invoke(proxy);
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
}
