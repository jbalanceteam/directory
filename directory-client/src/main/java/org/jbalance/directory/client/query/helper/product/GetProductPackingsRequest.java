package org.jbalance.directory.client.query.helper.product;

import org.jbalance.core.filter.DateFilter;
import org.jbalance.core.filter.LongFilter;
import org.jbalance.core.filter.StringFilter;
import org.jbalance.core.query.GetRequest;

import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{
    "idFilter",
    "packingNameFilter",
    "modifiedFilter"
})
public class GetProductPackingsRequest extends GetRequest<GetProductPackingsResult>
{

    private static final long serialVersionUID = 1L;

    private LongFilter idFilter;
    
    private StringFilter productNameFilter;

    private DateFilter modifiedFilter;
    
    private LongFilter juridicalFilter;

    private LongFilter imageFilter;
    
    @Override
    public GetProductPackingsResult createResult()
    {
        return new GetProductPackingsResult(this);
    }

    public LongFilter getIdFilter()
    {
        return idFilter;
    }

    public void setIdFilter(LongFilter idFilter)
    {
        this.idFilter = idFilter;
    }

    public StringFilter getProductNameFilter()
    {
        return productNameFilter;
    }

    public void setProductNameFilter(StringFilter name)
    {
        this.productNameFilter = name;
    }

    public DateFilter getModifiedFilter()
    {
        return modifiedFilter;
    }

    public void setModifiedFilter(DateFilter modifiedFilter)
    {
        this.modifiedFilter = modifiedFilter;
    }

    public LongFilter getJuridicalFilter() {
        return juridicalFilter;
    }

    public void setJuridicalFilter(LongFilter juridicalFilter) {
        this.juridicalFilter = juridicalFilter;
    }    

    public LongFilter getImageFilter() {
        return imageFilter;
    }

    public void setImageFilter(LongFilter imageFilter) {
        this.imageFilter = imageFilter;
    }
}
