package org.jbalance.directory.client.model.product;

import java.util.Date;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import org.jbalance.core.model.BaseDirectoryEntity;

/**
 * Образ продукта
 */
@Entity
@NamedQuery(name = "ProductImage.lastUpdate",query = "SELECT e.syncDate FROM ProductImage e ORDER By e.syncDate ASC")
public class ProductImage extends BaseDirectoryEntity
{

     @Column
    private String name;
     
    /**
     * Код ОКП
     */
    @Column
    private String okp;

    
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @OrderBy("periodEnd ASC")
    private SortedSet<ColdProcessingLossPercent> losts = new TreeSet<ColdProcessingLossPercent>();
    
    public ProductImage()
    {
    }

    public ProductImage(Long id, Date enteredDate, Date deleted,String name,  String okp)
    {
        super(id, enteredDate, deleted);
        this.name=name;
        this.okp = okp;
    }

    /**
     * Set the value of okp
     *
     * @param newVar the new value of okp
     */
    public void setOkpCode(String newVar)
    {
        okp = newVar;
    }

    /**
     * Get the value of okp
     *
     * @return the value of okp
     */
    public String getOkpCode()
    {
        return okp;
    }
    
     public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SortedSet<ColdProcessingLossPercent> getLosts() {
        return losts;
    }

    public void setLosts(SortedSet<ColdProcessingLossPercent> losts) {
        this.losts = losts;
    }
    
    

}
