package org.jbalance.directory.client.util.builder;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public interface DirectoryBeanBuilder<E>
{

    public void setProxy(Object p);

    public E buildEntity();
}
