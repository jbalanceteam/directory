package org.jbalance.directory.client.util.builder.general;

import org.jbalance.core.model.o.Juridical;
import org.jbalance.directory.client.util.builder.BaseDirectoryBeanBuilder;
import org.jbalance.directory.client.util.builder.DirectoryBeanBuilder;

import javax.annotation.PostConstruct;

/**
 *
 * @author Mihail
 */
public class JuridicalBuilder extends BaseDirectoryBeanBuilder<Juridical>
        implements DirectoryBeanBuilder<Juridical>
{

    public JuridicalBuilder(){
        proxyClassName = "Juridical";
    }

    @Override
    public Juridical buildEntity() {
        if (proxy == null) {
            return null;
        }

        entity = new Juridical();
        setBaseParams();
        entity.setName((String)getProxyParam("name"));
        entity.setInn((Long)getProxyParam("inn"));
//        juridical.setOkonh(proxy.getOkonh());
//        juridical.setOkpo(proxy.getOkpo());
        entity.setPhone((String)getProxyParam("phone"));
        entity.setContactInfo((String)getProxyParam("contactInfo"));
        entity.setpAddress((String)getProxyParam("mailingAdress"));
//        juridical.seteMail(proxy.getEMail());
//        juridical.setDescription(proxy.getDescription());
        entity.setjAddress((String)getProxyParam("registeredOffice"));
        return entity;
    }
    @Override
    public org.jbalance.directory.client.juridical.Juridical buildProxy()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
