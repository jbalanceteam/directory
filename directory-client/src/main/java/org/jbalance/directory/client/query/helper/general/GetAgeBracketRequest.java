package org.jbalance.directory.client.query.helper.general;

import org.jbalance.core.filter.LongFilter;
import org.jbalance.core.filter.StringFilter;
import org.jbalance.core.query.GetRequest;

import javax.xml.bind.annotation.XmlType;

/**
 * Created by Mihail on 24.05.2015.
 */
@XmlType(propOrder ={
        "idFilter",
        "nameFilter",
        "juridicalFilter"
})
public class GetAgeBracketRequest extends GetRequest<GetAgeBracketResult> {

    private LongFilter idFilter;
    private StringFilter nameFilter;
    private LongFilter juridicalFilter;

    @Override
    public GetAgeBracketResult createResult() {
        return new GetAgeBracketResult(this);
    }

    @Override
    public LongFilter getIdFilter() {
        return idFilter;
    }

    @Override
    public void setIdFilter(LongFilter idFilter) {
        this.idFilter = idFilter;
    }

    public StringFilter getNameFilter() {
        return nameFilter;
    }

    public void setNameFilter(StringFilter nameFilter) {
        this.nameFilter = nameFilter;
    }

    public LongFilter getJuridicalFilter() {
        return juridicalFilter;
    }

    public void setJuridicalFilter(LongFilter juridicalFilter) {
        this.juridicalFilter = juridicalFilter;
    }
}
