package org.jbalance.directory.client.util.builder.general;

import org.jbalance.directory.client.model.general.AgeBracket;
import org.jbalance.directory.client.util.builder.BaseDirectoryBeanBuilder;
import org.jbalance.directory.client.util.builder.DirectoryBeanBuilder;


/**
 * Created by Mihail on 24.05.2015.
 */
public class AgeBracketBuilder extends BaseDirectoryBeanBuilder<AgeBracket>
implements DirectoryBeanBuilder<AgeBracket> {

    public AgeBracketBuilder(){
        proxyClassName = "AgeBracket";
    }

    @Override
    public AgeBracket buildEntity() {
        if (proxy == null) {
            return null;
        }

        entity = new AgeBracket();
        setBaseParams();
        entity.setName((String) getProxyParam("name"));
        entity.setDescription((String) getProxyParam("description"));
        entity.setMinimumAge((Short)getProxyParam("minimumAge"));
        entity.setMinimumAge((Short)getProxyParam("maximumAge"));
        return entity;
    }

    @Override
    public Object buildProxy() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
