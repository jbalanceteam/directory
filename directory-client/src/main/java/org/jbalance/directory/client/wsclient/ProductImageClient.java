package org.jbalance.directory.client.wsclient;

import org.jbalance.directory.client.productimage.ProductImages;
import org.jbalance.directory.client.productimage.ProductImages_Service;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@Stateless
public class ProductImageClient extends BaseDirectoryClient
{

    public ProductImageClient()
    {
    }

    @PostConstruct
    public void init()
    {
        wsdlLocation = getWsdlLocation(ProductImages.class.getSimpleName());
    }

    public ProductImages getPort()
    {
        ProductImages_Service s = null;

        if (wsdlLocation != null)
        {
            s = new ProductImages_Service(wsdlLocation);
        } else
        {
            s = new ProductImages_Service();
        }

        return s.getProductImagesPort();
    }
}
