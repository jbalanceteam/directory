package org.jbalance.directory.client.util.builder.general;

import org.jbalance.directory.client.model.general.Measure;
import org.jbalance.directory.client.util.builder.BaseDirectoryBeanBuilder;
import org.jbalance.directory.client.util.builder.DirectoryBeanBuilder;


/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class MeasureBuilder extends BaseDirectoryBeanBuilder<Measure>
        implements DirectoryBeanBuilder<Measure>
{

    public MeasureBuilder(){
        proxyClassName = "Measure";
    }

    public MeasureBuilder(Object proxy){
        proxyClassName = "Measure";
        setProxy(proxy);
    }

    @Override
    public Measure buildEntity()
    {
        if (proxy == null)
        {
            return null;
        }
        entity = new Measure();
        setBaseParams();
        entity.setName((String) getProxyParam("name"));
        entity.setEngShortName1((String) getProxyParam("engShortName1"));
        entity.setEngShortName2((String) getProxyParam("engShortName2"));
        entity.setRusShortName1((String) getProxyParam("rusShortName1"));
        entity.setRusShortName2((String) getProxyParam("rusShortName2"));
        entity.setNumberCode((String) getProxyParam("numberCode"));
//        entity.setMeasureGroup(getProxyParam("measureGroup"));
//        entity.setMeasureType();

        return entity;
    }

    @Override
    public org.jbalance.directory.client.measure.Measure buildProxy()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
