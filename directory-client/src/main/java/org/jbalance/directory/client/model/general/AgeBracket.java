package org.jbalance.directory.client.model.general;

import org.jbalance.core.model.BaseDirectoryEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Created by Mihail on 24.05.2015.
 */
@Entity
public class AgeBracket extends BaseDirectoryEntity {
    private static final long serialVersionUID = 1L;

    @Column(nullable = false)
    protected String name;

    @Column
    protected String description;

    /**
     * минимальный возраст для возрастной группы в месяцах
     */
    @Column(nullable=false)
    private short minimumAge;

    /**
     * максимальный возраст для возрастной группы в месяцах
     */
    @Column(nullable=false)
    private short maximumAge;

    public short getMinimumAge(){
        return minimumAge;
    }

    public float getMinimumAgeInYears(){
        float ageInYears = (float)(minimumAge * 10 / 12);
        ageInYears /= 10;
        return ageInYears;
    }

    public void setMinimumAge(short minimumAge){
        this.minimumAge = minimumAge;
    }

    public short getMaximumAge(){
        return maximumAge;
    }

    public float getMaximumAgeInYears(){
        float ageInYears = maximumAge * 10 / 12;
        ageInYears /= 10;
        return ageInYears;
    }

    public void setMaximumAge(short maximumAge){
        this.maximumAge = maximumAge;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
