package org.jbalance.directory.client.wsclient;

import org.jbalance.directory.client.trademark.TradeMarks;
import org.jbalance.directory.client.trademark.TradeMarks_Service;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@Stateless
public class TradeMarkClient extends BaseDirectoryClient
{

    public TradeMarkClient()
    {
    }

    @PostConstruct
    public void init()
    {
        wsdlLocation = getWsdlLocation(TradeMarks.class.getSimpleName());
    }

    public TradeMarks getPort()
    {
        TradeMarks_Service s = null;

        if (wsdlLocation != null)
        {
            s = new TradeMarks_Service(wsdlLocation);
        } else
        {
            s = new TradeMarks_Service();
        }

        return s.getTradeMarksPort();
    }
}
