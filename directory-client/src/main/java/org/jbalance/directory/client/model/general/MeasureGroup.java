package org.jbalance.directory.client.model.general;

/**
 *
 * @author Mihail
 */
public enum MeasureGroup {
	Time, Length, Weight, Volume, Area, Engineering, Economic;
	public String getStringValue(){
		String val = null;
		switch(this){
			case Time:
				val="Единицы времени";
				break;
			case Length:
				val="Единицы длины";
				break;
			case Weight:
				val="Единицы массы";
				break;
			case Volume:
				val="Единицы объема";
				break;
			case Area:
				val="Единицы площади";
				break;
			case Engineering:
				val="Технические единицы";
				break;
			case Economic:
				val="Экономические единицы";
				break;
		}
		return val;
	}
}
