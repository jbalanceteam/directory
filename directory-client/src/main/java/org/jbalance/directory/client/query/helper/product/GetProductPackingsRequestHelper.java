package org.jbalance.directory.client.query.helper.product;

import org.jbalance.core.query.helper.AbstractQueryBuilder;
import org.jbalance.core.query.helper.GetRequestHelper;
import org.jbalance.core.utils.CollectionUtil;
import org.jbalance.directory.client.model.product.ProductPacking;

import javax.persistence.EntityManager;
import java.util.List;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class GetProductPackingsRequestHelper extends GetRequestHelper<GetProductPackingsRequest, GetProductPackingsResult> {

    public GetProductPackingsRequestHelper(GetProductPackingsRequest request) {
        super(request);
    }

    @Override
    protected AbstractQueryBuilder<?> initQuery() {
        ProductPackingQueryBuilder qb = new ProductPackingQueryBuilder();
        qb.appendFilter(request.getIdFilter(), "packing.id");
        qb.appendFilter(request.getProductNameFilter(), "product.name");
        qb.appendFilter(request.getModifiedFilter(), "packing.modifiedDate");

        qb.appendFilter(request.getJuridicalFilter(), "juridicals.id");
        qb.appendFilter(request.getImageFilter(), "image.id");

        qb.orderBy().append("order by packing.enteredDate desc");
        return qb;
    }

    @Override
    protected GetProductPackingsResult createResult(GetProductPackingsResult result, List<?> records, EntityManager em) {
        List<ProductPacking> productImages = (List<ProductPacking>) records;
        productImages = CollectionUtil.filterDuplicates(productImages);
        result.setPackings(productImages);
        return result;
    }
}
