package org.jbalance.directory.client.wsclient;

import org.jbalance.core.wsclient.BaseDirectoryClient;
import org.jbalance.directory.client.juridical.Juridicals;
import org.jbalance.directory.client.juridical.Juridicals_Service;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

/**
 *
 * @author Mihail
 */
@Stateless
public class JuridicalClient extends BaseDirectoryClient{


    public JuridicalClient()
    {
    }

    @PostConstruct
    public void init()
    {
        wsdlLocation = getWsdlLocation(Juridicals.class.getSimpleName());
    }

    public Juridicals getPort()
    {
        Juridicals_Service s;

        if (wsdlLocation != null)
        {
            s = new Juridicals_Service(wsdlLocation);
        } else
        {
            s = new Juridicals_Service();
        }

        return s.getJuridicalsPort();
    }
}
