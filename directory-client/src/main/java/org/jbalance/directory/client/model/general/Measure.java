package org.jbalance.directory.client.model.general;

import java.util.Date;
import javax.persistence.*;

import org.jbalance.core.model.BaseDirectoryEntity;

/**
 * Единица измерения
 */
@Entity
@NamedQuery(name = "Measure.lastUpdate",query = "SELECT e.syncDate FROM Measure e ORDER By e.syncDate ASC")
public class Measure extends BaseDirectoryEntity
{

    @Column(unique = true)
    private String name;

    @Column(name = "rus_name1")
    private String rusShortName1;

    @Column(name = "rus_name2")
    private String rusShortName2;

    @Column(name = "eng_name1")
    private String engShortName1;

    @Column(name = "eng_name2")
    private String engShortName2;

    @Column(name = "number_code")
    private String numberCode;

    @Enumerated(EnumType.STRING)
    private MeasureGroup measureGroup;

    @Enumerated(EnumType.STRING)
    private MeasureType measureType;

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getRusShortName1(){
        return rusShortName1;
    }

    public void setRusShortName1(String rusShortName1){
        this.rusShortName1 = rusShortName1;
    }

    public String getRusShortName2(){
        return rusShortName2;
    }

    public void setRusShortName2(String rusShortName2){
        this.rusShortName2 = rusShortName2;
    }

    public String getEngShortName1(){
        return engShortName1;
    }

    public void setEngShortName1(String engShortName1){
        this.engShortName1 = engShortName1;
    }

    public String getEngShortName2(){
        return engShortName2;
    }

    public void setEngShortName2(String engShortName2){
        this.engShortName2 = engShortName2;
    }

    public String getNumberCode(){
        return numberCode;
    }

    public void setNumberCode(String numberCode){
        this.numberCode = numberCode;
    }

    public MeasureGroup getMeasureGroup(){
        return measureGroup;
    }

    public void setMeasureGroup(MeasureGroup measureGroup){
        this.measureGroup = measureGroup;
    }

    public MeasureType getMeasureType(){
        return measureType;
    }

    public void setMeasureType(MeasureType measureType){
        this.measureType = measureType;
    }

    public Measure()
    {
    }
    
    public Measure(Long id, Date enteredDate, Date deleted,String name) {
        super(id, enteredDate, deleted);
        this.name=name;
    }
}
