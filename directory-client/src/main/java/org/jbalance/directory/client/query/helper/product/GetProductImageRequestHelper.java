package org.jbalance.directory.client.query.helper.product;

import org.jbalance.core.query.helper.AbstractQueryBuilder;
import org.jbalance.core.query.helper.GetRequestHelper;
import org.jbalance.core.utils.CollectionUtil;
import org.jbalance.directory.client.model.product.ProductImage;

import javax.persistence.EntityManager;
import java.util.List;

/**
 *
 * @author roman
 */
public class GetProductImageRequestHelper extends GetRequestHelper<GetProductImageRequest, GetProductImageResult> {

    public GetProductImageRequestHelper(GetProductImageRequest request) {
        super(request);
    }

    @Override
    protected AbstractQueryBuilder<?> initQuery() {
        ProductImageQueryBuilder qb = new ProductImageQueryBuilder();
        qb.appendFilter(request.getIdFilter(), "image.id");
        qb.appendFilter(request.getProductNameFilter(), "image.name");
        qb.appendFilter(request.getModifiedFilter(), "image.modifiedDate");

        qb.appendFilter(request.getJuridicalFilter(), "juridicals.id");

        qb.orderBy().append("order by image.enteredDate desc");
        return qb;
    }

    @Override
    protected GetProductImageResult createResult(GetProductImageResult result, List<?> records, EntityManager em) {
        List<ProductImage> productImages = (List<ProductImage>) records;
        productImages = CollectionUtil.filterDuplicates(productImages);
        result.setRecords(productImages);
        return result;
    }
}
