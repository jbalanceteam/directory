package org.jbalance.directory.client.query.helper.general;

import org.jbalance.core.query.GetResult;
import org.jbalance.directory.client.model.general.AgeBracket;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mihail on 24.05.2015.
 */
public class GetAgeBracketResult extends GetResult {
    private static final long serialVersionUID = 1L;

    private List<AgeBracket> ageBrackets;

    public GetAgeBracketResult()
    {
    }

    @XmlElement(name = "ageBrackets")
    public List<AgeBracket> getAgeBrackets()
    {
        if (ageBrackets == null)
        {
            ageBrackets = new ArrayList<AgeBracket>();
        }
        return ageBrackets;
    }


    public GetAgeBracketResult(GetAgeBracketRequest request)
    {
        super(request);
    }
}
