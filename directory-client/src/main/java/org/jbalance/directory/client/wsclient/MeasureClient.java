package org.jbalance.directory.client.wsclient;

import org.jbalance.directory.client.measure.Measures;
import org.jbalance.directory.client.measure.Measures_Service;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@Stateless
public class MeasureClient extends BaseDirectoryClient
{

    public MeasureClient()
    {
    }

    @PostConstruct
    public void init()
    {
        wsdlLocation = getWsdlLocation(Measures.class.getSimpleName());
    }

    public Measures getPort()
    {
        Measures_Service s = null;

        if (wsdlLocation != null)
        {
            s = new Measures_Service(wsdlLocation);
        } else
        {
            s = new Measures_Service();
        }

        return s.getMeasuresPort();
    }
}
