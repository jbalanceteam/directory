package org.jbalance.directory.client.wsclient;

import org.jbalance.directory.client.packingflowchartpacks.PackingFlowChartPacksService;
import org.jbalance.directory.client.packingflowchartpacks.PackingFlowChartPacksService_Service;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

/**
 * Created by Mihail on 27.01.15.
 */
@Stateless
public class PackingFlowChartPacksClient extends BaseDirectoryClient {

	public PackingFlowChartPacksClient() {
	}

	@PostConstruct
	public void init() {
		wsdlLocation = getWsdlLocation(PackingFlowChartPacksService.class.getSimpleName());
	}

	public PackingFlowChartPacksService getPort() {
		PackingFlowChartPacksService_Service s = null;

		if (wsdlLocation != null) {
			s = new PackingFlowChartPacksService_Service(wsdlLocation);
		} else {
			s = new PackingFlowChartPacksService_Service();
		}

		return s.getPackingFlowChartPacksServicePort();
	}
}