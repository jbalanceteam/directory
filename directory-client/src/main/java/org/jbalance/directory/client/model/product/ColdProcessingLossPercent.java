
package org.jbalance.directory.client.model.product;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlTransient;
import org.jbalance.core.model.BaseDirectoryEntity;

/**
 * Класс представляет процент потерь при холодной обработке продукта.
 *
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@Entity
public class ColdProcessingLossPercent extends BaseDirectoryEntity implements Comparable<ColdProcessingLossPercent> {

    @ManyToOne
    private ProductImage image;

    /**
     * Процент потерь при обработке
     */
    private BigDecimal percent;

    /**
     * Дата, определяющая когда закончится период, представляемый данным классом
     */
    private Integer periodEnd;

    @XmlTransient
    public ProductImage getImage() {
        return image;
    }

    public void setImage(ProductImage image) {
        this.image = image;
    }

    public BigDecimal getPercent() {
        return percent;
    }

    public void setPercent(BigDecimal precent) {
        this.percent = precent;
    }

    public Integer getPeriodEnd() {
        return periodEnd;
    }

    public void setPeriodEnd(Integer periodEnd) {
        this.periodEnd = periodEnd;
    }

    @Override
    public int compareTo(ColdProcessingLossPercent o) {
        if (o == null || this.getPeriodEnd() == null || o.getPeriodEnd() == null) {
            return 0;
        }
        if (this.getPeriodEnd() == null) {
            return -1;
        }
        if (o.getPeriodEnd() == null) {
            return 1;
        }
        return this.getPeriodEnd().compareTo(o.getPeriodEnd());
    }
}