package org.jbalance.directory.client.model.product;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import org.jbalance.core.model.BaseDirectoryEntity;

/**
 * Конкретный продукт в упакове
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "ProductPacking.lastUpdate", query = "SELECT e.syncDate FROM ProductPacking e ORDER By e.syncDate ASC")
})
public class ProductPacking extends BaseDirectoryEntity  implements Comparable<ProductPacking>{

    private String compositeName;

    /**
     * Продукт
     */
    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private Product product;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Packing packing;

    public ProductPacking() {
    }

    public ProductPacking(Long id, Date enteredDate, Date deleted) {
        super(id, enteredDate, deleted);
    }

    /**
     * Set the value of product
     *
     * @param newVar the new value of product
     */
    public void setProduct(Product newVar) {
        product = newVar;
    }

    /**
     * Get the value of product
     *
     * @return the value of product
     */
    public Product getProduct() {
        return product;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    public Packing getPacking() {
        return packing;
    }

    public void setPacking(Packing packing) {
        this.packing = packing;
    }

    public String getCompositeName() {
        return compositeName;
    }

    public void setCompositeName(String compositeName) {
        this.compositeName = compositeName;
    }

    public String getNameWithPacking() {
        return compositeName;
    }

    @Override
    public int compareTo(ProductPacking o) {
        return compositeName.compareToIgnoreCase(o.getCompositeName());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return String.valueOf(product);
    }

}
