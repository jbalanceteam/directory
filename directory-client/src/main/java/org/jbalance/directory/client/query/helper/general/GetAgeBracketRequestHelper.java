package org.jbalance.directory.client.query.helper.general;

import org.jbalance.core.query.helper.AbstractQueryBuilder;
import org.jbalance.core.query.helper.GetRequestHelper;
import org.jbalance.core.utils.CollectionUtil;
import org.jbalance.directory.client.model.general.AgeBracket;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by Mihail on 24.05.2015.
 */
public class GetAgeBracketRequestHelper extends GetRequestHelper<GetAgeBracketRequest, GetAgeBracketResult> {
    public GetAgeBracketRequestHelper(GetAgeBracketRequest request)
    {
        super(request);
    }

    @Override
    protected AbstractQueryBuilder<?> initQuery()
    {
        AgeBracketQueryBuilder ageBracketQueryBuilder = new AgeBracketQueryBuilder();
        ageBracketQueryBuilder.appendFilter(request.getIdFilter(), "ageBracket.id");
        ageBracketQueryBuilder.appendFilter(request.getNameFilter(), "ageBracket.name");
        ageBracketQueryBuilder.appendFilter(request.getJuridicalFilter(), "juridicals.id");
        ageBracketQueryBuilder.orderBy().append("order by ageBracket.name asc");
        return ageBracketQueryBuilder;
    }

    @Override
    protected GetAgeBracketResult createResult(GetAgeBracketResult result, List<?> records, EntityManager em)
    {
        List<AgeBracket> l = (List<AgeBracket>) records;
        l = CollectionUtil.filterDuplicates(l);
        result.getAgeBrackets().addAll(l);
        return result;
    }

}
