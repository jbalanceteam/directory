package org.jbalance.directory.client.util.builder.product;

import java.math.BigDecimal;
import java.util.List;
import org.jbalance.directory.client.model.product.ColdProcessingLossPercent;
import org.jbalance.directory.client.model.product.ProductImage;
import org.jbalance.directory.client.util.builder.BaseDirectoryBeanBuilder;
import org.jbalance.directory.client.util.builder.DirectoryBeanBuilder;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class ProductImageBuilder
        extends BaseDirectoryBeanBuilder<ProductImage>
        implements DirectoryBeanBuilder<ProductImage> {

    public ProductImageBuilder() {
        proxyClassName = "ProductImage";
    }

    public ProductImageBuilder(Object proxy) {
        proxyClassName = "ProductImage";
        setProxy(proxy);
    }

    @Override
    public ProductImage buildEntity() {
        if (proxy == null) {
            return null;
        }
        entity = new ProductImage();
        setBaseParams();
        entity.setName((String) getProxyParam("name"));
        entity.setOkpCode((String) getProxyParam("okp"));

        List<Object> loss = (List<Object>) getProxyParam("losts");

        ColdProcessingLossPercentBuilder lb = new ColdProcessingLossPercentBuilder();
        for (Object o : loss) {
            lb.setProxy(o);
            entity.getLosts().add(lb.buildEntity());
        }
        return entity;
    }

    @Override
    public org.jbalance.directory.client.productimage.ProductImage buildProxy() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

class ColdProcessingLossPercentBuilder
        extends BaseDirectoryBeanBuilder<ColdProcessingLossPercent>
        implements DirectoryBeanBuilder<ColdProcessingLossPercent> {

    public ColdProcessingLossPercentBuilder() {
        proxyClassName = "ColdProcessingLossPercent";
    }

    public ColdProcessingLossPercentBuilder(Object proxy) {
        proxyClassName = "ColdProcessingLossPercent";
        setProxy(proxy);
    }

    @Override
    public ColdProcessingLossPercent buildEntity() {
        if (proxy == null) {
            return null;
        }
        entity = new ColdProcessingLossPercent();
        setBaseParams();
        entity.setPeriodEnd((Integer) getProxyParam("periodEnd"));
        entity.setPercent((BigDecimal) getProxyParam("percent"));

        return entity;
    }

    @Override
    public Object buildProxy() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
