package org.jbalance.directory.client.wsclient;

import org.jbalance.directory.client.productpacking.ProductPackings;
import org.jbalance.directory.client.productpacking.ProductPackings_Service;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */

@Stateless
public class ProductPackingClient extends BaseDirectoryClient
{

    public ProductPackingClient()
    {
    }

    @PostConstruct
    public void init()
    {
        wsdlLocation = getWsdlLocation(ProductPackings.class.getSimpleName());
    }

    public ProductPackings getPort()
    {
        ProductPackings_Service s = null;

        if (wsdlLocation != null)
        {
            s = new ProductPackings_Service(wsdlLocation);
        } else
        {
            s = new ProductPackings_Service();
        }

        return s.getProductPackingsPort();
    }
}
