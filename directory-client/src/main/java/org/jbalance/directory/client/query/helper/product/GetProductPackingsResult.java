package org.jbalance.directory.client.query.helper.product;

import org.jbalance.core.query.GetResult;
import org.jbalance.directory.client.model.product.ProductPacking;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class GetProductPackingsResult extends GetResult {

    private static final long serialVersionUID = 1L;

    private List<ProductPacking> packings;

    public GetProductPackingsResult() {
    }

    public GetProductPackingsResult(GetProductPackingsRequest request) {
        super(request);
    }

    @XmlElement(name = "packings")
    public List<ProductPacking> getPackings() {
        if (packings == null) {
            setPackings(new ArrayList<ProductPacking>());
        }
        return packings;
    }

    public void setPackings(List<ProductPacking> packings) {
        this.packings = packings;
    }
}
