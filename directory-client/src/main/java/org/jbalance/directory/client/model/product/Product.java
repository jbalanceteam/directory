package org.jbalance.directory.client.model.product;

import java.util.*;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlTransient;
import org.jbalance.core.model.BaseDirectoryEntity;

/**
 * Продукт
 *
 */
@Entity
@NamedQuery(name = "Product.lastUpdate",query = "SELECT e.syncDate FROM Product e ORDER By e.syncDate ASC")
public class Product extends BaseDirectoryEntity {

    @Column
    private String name;

    /**
     * Торговая марка
     */
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
    private TradeMark tradeMark;

    /**
     *
     */
    @XmlTransient
    @OneToMany(mappedBy = "product")
    private Set<ProductPacking> packings;

    /**
     * Образ продукта
     */
    @ManyToOne(fetch = FetchType.EAGER)
    private ProductImage image;

    public Product() {
    }

    public Product(Long id, Date enteredDate, Date deleted, String name) {
        super(id, enteredDate, deleted);
        this.name = name;
    }

    /**
     * Set the value of tradeMark
     *
     * @param newVar the new value of tradeMark
     */
    public void setTradeMark(TradeMark newVar) {
        tradeMark = newVar;
    }

    /**
     * Get the value of tradeMark
     *
     * @return the value of tradeMark
     */
    public TradeMark getTradeMark() {
        return tradeMark;
    }

    /**
     * Set the value of packings
     *
     * @param newVar the new value of packings
     */
    public void setPackings(Set<ProductPacking> newVar) {
        packings = newVar;
    }

    /**
     * Get the value of packings
     *
     * @return the value of packings
     */
    @XmlTransient
    public Set<ProductPacking> getPackings() {
        return packings;
    }

    /**
     * Set the value of image
     *
     * @param newVar the new value of image
     */
    public void setImage(ProductImage newVar) {
        image = newVar;
    }

    /**
     * Get the value of image
     *
     * @return the value of image
     */
    public ProductImage getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String toString()
    {
        return name;
    }
}
