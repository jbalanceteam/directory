package org.jbalance.directory.service.product;

import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jbalance.directory.core.model.product.ProductPacking;
import org.jbalance.directory.core.query.helper.product.GetProductPackingsRequest;
import org.jbalance.directory.core.session.product.ProductPackingSession;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@WebService(serviceName = "ProductPackings")
@Stateless
public class ProductPackings
{

    @EJB
    ProductPackingSession session;

    @WebMethod(operationName = "getById")
    public ProductPacking getById(@WebParam(name = "id") Long id)
    {
        return session.getById(id);
    }

    /**
     *
     *
     * @param date
     * @return
     */
    @WebMethod(operationName = "getUpdated")
    public List<ProductPacking> getUpdated(@WebParam(name = "date") Date date)
    {
        return session.getUpdates(date);
    }

    /**
     * Get list filtered of objects
     *
     * @param  request
     * @return
     */
    @WebMethod(operationName = "getFilteredList")
    public List<ProductPacking> getFilteredList(@WebParam(name = "request") GetProductPackingsRequest request)
    {
        return session.list(request);
    }
}
