package org.jbalance.directory.service.address;

import java.util.List;
import java.util.UUID;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.jbalance.directory.core.model.address.City;
import org.jbalance.directory.core.model.address.House;
import org.jbalance.directory.core.model.address.HouseInterval;
import org.jbalance.directory.core.model.address.Street;
import org.jbalance.directory.core.session.address.IAddressFinder;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@WebService(serviceName = "Addresses")
@Stateless
public class Address {

    @Inject
    IAddressFinder finder;

    @WebMethod(operationName = "findCityByTerm")
    public List<City> findCity(@WebParam(name = "term") String term) {
        return finder.findCity(term, null);
    }

    @WebMethod(operationName = "findCityByUUID")
    public City findCity(@WebParam(name = "uuid")  UUID uuid) {
        return finder.findCity(uuid);
    }

    @WebMethod(operationName = "findStreetByTerm")
    public List<Street> findStreet(@WebParam(name = "term") String term,@WebParam(name = "cityUUID") UUID cityUUID) {
        return finder.findStreet(term, cityUUID);
    }

    @WebMethod(operationName = "findStreetByUUID")
    public Street findStreet(@WebParam(name = "uuid") UUID uuid) {
        return finder.findStreet(uuid);
    }

    @WebMethod(operationName = "findHouse")
    public List<House> findHouse(@WebParam(name = "term") String term,
           @WebParam(name = "streetUUID") UUID streetUUID) {
        IAddressFinder af = finder;

        return af.findHouse(term, streetUUID);
    }

     @WebMethod(operationName = "findHouseInterval")
    public List<HouseInterval> findHouseInterval( @WebParam(name = "parentUUID") UUID parentUUID) {
        IAddressFinder af = finder;

        return af.findHouseInterval(parentUUID);
    }

    @WebMethod(operationName = "getFullAddressForAddressObject")
    public String getFullAddressForAddressObject(@WebParam(name = "uuid") UUID uuid) {
        IAddressFinder af = finder;

        return af.getFullAddressForAddressObject(uuid);
    }

    public String getFullAddressForHouse(UUID uuid, int regionCode) {
        IAddressFinder af = finder;

        return af.getFullAddressForHouse(uuid, regionCode);
    }

    public String getFullAddressForHouseInterval(UUID uuid, String houseNum) {
        IAddressFinder af = finder;

        return af.getFullAddressForHouseInterval(uuid, houseNum);
    }

}
