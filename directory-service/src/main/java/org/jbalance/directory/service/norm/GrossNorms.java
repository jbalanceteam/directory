package org.jbalance.directory.service.norm;

import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import org.jbalance.directory.core.model.norm.GrossNorm;
import org.jbalance.directory.core.query.helper.norm.GetGrossNormsRequest;
import org.jbalance.directory.core.session.norm.GrossNormSession;

/**
 *
 * @author Mihail
 */
@WebService(serviceName = "GrossNorms")
@Stateless
public class GrossNorms {

    @EJB
    GrossNormSession session;

    @WebMethod(operationName = "getById")
    public GrossNorm getById(@WebParam(name = "id") Long id)
    {
        return session.getById(id);
    }

    /**
     * @param date
     * @return
     */
    @WebMethod(operationName = "getUpdatedGrossNorms")
    public List<GrossNorm> getUpdatedGrossNorms(@WebParam(name = "date") Date date)
    {

        return session.getUpdates(date);
    }

    /**
     * @param request
     * @return
     */
    @WebMethod(operationName = "getFilteredGrossNormList")
    public List<GrossNorm> getFilteredGrossNormList(@WebParam(name = "request") GetGrossNormsRequest request)
    {

        return session.list(request);
    }
}
