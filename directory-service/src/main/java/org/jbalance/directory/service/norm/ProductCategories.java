package org.jbalance.directory.service.norm;

import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import org.jbalance.directory.core.model.norm.ProductCategory;
import org.jbalance.directory.core.query.helper.norm.GetProductCategoryRequest;
import org.jbalance.directory.core.session.norm.ProductCategorySession;

/**
 *
 * @author Mihail
 */
@WebService(serviceName = "ProductCategories")
@Stateless
public class ProductCategories {

    @EJB
    ProductCategorySession session;

    @WebMethod(operationName = "getById")
    public ProductCategory getById(@WebParam(name = "id") Long id)
    {
        return session.getById(id);
    }

    /**
     *
     * @param date
     * @return
     */
    @WebMethod(operationName = "getUpdatedProductCategories")
    public List<ProductCategory> getUpdatedProductCategories(@WebParam(name = "date") Date date)
    {

        return session.getUpdates(date);
    }

    /**
     * @param request
     * @return
     */
    @WebMethod(operationName = "getFilteredProductCategoryList")
    public List<ProductCategory> getFilteredProductCategoryList(@WebParam(name = "request") GetProductCategoryRequest request)
    {

        return session.list(request);
    }
}
