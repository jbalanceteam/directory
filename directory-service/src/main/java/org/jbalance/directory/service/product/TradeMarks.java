package org.jbalance.directory.service.product;

import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.jbalance.directory.core.model.product.TradeMark;
import org.jbalance.directory.core.query.helper.product.GetTradeMarkRequest;
import org.jbalance.directory.core.session.product.TradeMarkSessionBean;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@WebService(serviceName = "TradeMarks")
@Stateless
public class TradeMarks
{

    @EJB
    TradeMarkSessionBean session;

    @WebMethod(operationName = "getById")
    public TradeMark getById(@WebParam(name = "id") Long id)
    {
        return session.getById(id);
    }

    /**
     *
     * @param date
     * @return
     */
    @WebMethod(operationName = "getUpdated")
    public List<TradeMark> getUpdated(@WebParam(name = "date") Date date)
    {
        return session.getUpdates(date);
    }

    /**
     * Get list filtered of objects
     *
     * @param request
     * @return
     */
    @WebMethod(operationName = "getFilteredList")
    public List<TradeMark> getFilteredList(@WebParam(name = "request") GetTradeMarkRequest request)
    {
        return session.list(request);
    }
}
