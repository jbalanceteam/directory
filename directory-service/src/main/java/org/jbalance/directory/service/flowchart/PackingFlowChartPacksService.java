package org.jbalance.directory.service.flowchart;

import org.jbalance.directory.core.model.flowchart.PackingFlowChartPacks;
import org.jbalance.directory.core.query.helper.flowchart.GetPackingFlowChartPacksRequest;
import org.jbalance.directory.core.session.flowchart.PackingFlowChartPacksSession;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

/**
 * Created by Mihail on 23.01.15.
 */
@WebService(serviceName = "PackingFlowChartPacksService")
@Stateless
public class PackingFlowChartPacksService {

	@EJB
	PackingFlowChartPacksSession session;

	@WebMethod(operationName = "getById")
	public PackingFlowChartPacks getById(@WebParam(name = "id") Long id)
	{
		return session.getById(id);
	}

	/**
	 *
	 *
	 * @param date
	 * @return
	 */
	@WebMethod(operationName = "getUpdated")
	public List<PackingFlowChartPacks> getUpdated(@WebParam(name = "date") Date date)
	{
		return session.getUpdates(date);
	}

	/**
	 * Get list filtered of objects
	 *
	 * @param  request
	 * @return
	 */
	@WebMethod(operationName = "getFilteredList")
	public List<PackingFlowChartPacks> getFilteredList(@WebParam(name = "request") GetPackingFlowChartPacksRequest request)
	{
		return session.list(request);
	}
}
