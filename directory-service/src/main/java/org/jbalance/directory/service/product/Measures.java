package org.jbalance.directory.service.product;

import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.jbalance.directory.core.model.measure.Measure;
import org.jbalance.directory.core.query.helper.measure.GetMeasuresRequest;
import org.jbalance.directory.core.session.measure.MeasureSession;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@WebService(serviceName = "Measures")
@Stateless
public class Measures
{

    @EJB
    MeasureSession session;

    @WebMethod(operationName = "getById")
    public Measure getById(@WebParam(name = "id") Long id)
    {
        return session.getById(id);
    }

    /**
     *
     * @param date
     * @return
     */
    @WebMethod(operationName = "getUpdated")
    public List<Measure> getUpdated(@WebParam(name = "date") Date date)
    {
        return session.getUpdates(date);
    }

    /**
     * Get list filtered of objects
     *
     * @param request
     * @return
     */
    @WebMethod(operationName = "getFilteredList")
    public List<Measure> getFilteredList(@WebParam(name = "request") GetMeasuresRequest request)
    {
        return session.list(request);
    }
}
