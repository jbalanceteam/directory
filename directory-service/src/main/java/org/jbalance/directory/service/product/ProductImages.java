package org.jbalance.directory.service.product;

import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import org.jbalance.directory.core.model.product.ProductImage;
import org.jbalance.directory.core.query.helper.product.GetProductImagesRequest;
import org.jbalance.directory.core.session.product.ProductImageSession;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@WebService(serviceName = "ProductImages")
@Stateless
public class ProductImages
{
    @EJB
    ProductImageSession session;

    @WebMethod(operationName = "getById")
    public ProductImage getById(@WebParam(name = "id") Long id)
    {
        return session.getById(id);
    }

    /**
     *
     * @param date
     * @return
     */
    @WebMethod(operationName = "getUpdated")
    public List<ProductImage> getUpdated(@WebParam(name = "date") Date date)
    {
        return session.getUpdates(date);
    }

    /**
     * Get list filtered of objects
     *
     * @param request
     * @return
     */
    @WebMethod(operationName = "getFilteredList")
    public List<ProductImage> getFilteredList(@WebParam(name = "request") GetProductImagesRequest request)
    {
        return session.list(request);
    }
}
