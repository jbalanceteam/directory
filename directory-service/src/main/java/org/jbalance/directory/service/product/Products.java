package org.jbalance.directory.service.product;

import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.jbalance.directory.core.model.product.Product;
import org.jbalance.directory.core.query.helper.product.GetProductsRequest;
import org.jbalance.directory.core.session.product.ProductSession;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@WebService(serviceName = "Products")
@Stateless
public class Products
{

    @EJB
    ProductSession session;

    @WebMethod(operationName = "getById")
    public Product getById(@WebParam(name = "id") Long id)
    {
        return session.getById(id);
    }

    /**
     *
     * @param date
     * @return
     */
    @WebMethod(operationName = "getUpdated")
    public List<Product> getUpdated(@WebParam(name = "date") Date date)
    {
        return session.getUpdates(date);
    }

    /**
     * Get list filtered of objects
     *
     * @param request
     * @return
     */
    @WebMethod(operationName = "getFilteredList")
    public List<Product> getFilteredList(@WebParam(name = "request") GetProductsRequest request)
    {
        return session.list(request);
    }
}
