package org.jbalance.directory.service.norm;

import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.jbalance.directory.core.model.norm.AgeBracket;
import org.jbalance.directory.core.query.helper.norm.GetAgeBracketRequest;
import org.jbalance.directory.core.session.norm.AgeBracketSession;

/**
 *
 * @author Mihail
 */
@WebService(serviceName = "AgeBrackets")
@Stateless
public class AgeBrackets {

    @EJB
    AgeBracketSession session;
    
    @WebMethod(operationName = "getById")
    public AgeBracket getById(@WebParam(name = "id") Long id)
    {
        return session.getById(id);
    }
    
        /**
     *
     * @param date
     * @return
     */
    @WebMethod(operationName = "getUpdated")
    public List<AgeBracket> getUpdated(@WebParam(name = "date") Date date)
    {

        return session.getUpdates(date);
    }

    /**
     * @param request
     * @return
     */
    @WebMethod(operationName = "getFilteredList")
    public List<AgeBracket> getFilteredList(@WebParam(name = "request") GetAgeBracketRequest request)
    {

        return session.list(request);
    }
}
