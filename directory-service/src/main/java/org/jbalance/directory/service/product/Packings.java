
package org.jbalance.directory.service.product;

import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.jbalance.directory.core.model.product.Packing;
import org.jbalance.directory.core.query.helper.product.GetPackingsRequest;
import org.jbalance.directory.core.session.product.PackingSession;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@WebService(serviceName = "Packings")
@Stateless
public class Packings {
    
     @EJB
    PackingSession session;

    @WebMethod(operationName = "getById")
    public Packing getById(@WebParam(name = "id") Long id)
    {
        return session.getById(id);
    }

    /**
     *
     *
     * @param date
     * @return
     */
    @WebMethod(operationName = "getUpdated")
    public List<Packing> getUpdated(@WebParam(name = "date") Date date)
    {
        return session.getUpdates(date);
    }

    /**
     * Get list filtered of objects
     *
     * @param  request
     * @return
     */
    @WebMethod(operationName = "getFilteredList")
    public List<Packing> getFilteredList(@WebParam(name = "request") GetPackingsRequest request)
    {
        return session.list(request);
    }
    
}
