package org.jbalance.directory.service.norm;

import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.jbalance.directory.core.model.norm.NetNorm;
import org.jbalance.directory.core.query.helper.norm.GetNetNormsRequest;
import org.jbalance.directory.core.session.norm.NetNormSession;

/**
 *
 * @author Mihail
 */
@WebService(serviceName = "NetNorms")
@Stateless
public class NetNorms {

    @EJB
    NetNormSession session;

    @WebMethod(operationName = "getById")
    public NetNorm getById(@WebParam(name = "id") Long id)
    {
        return session.getById(id);
    }

    /**
     * This is a sample web service operation
     *
     * @param date
     * @return
     */
    @WebMethod(operationName = "getUpdatedNetNorms")
    public List<NetNorm> getUpdatedNetNorms(@WebParam(name = "date") Date date)
    {

        return session.getUpdates(date);
    }

    /**
     * @param request
     * @return
     */
    @WebMethod(operationName = "getFilteredNetNormList")
    public List<NetNorm> getFilteredNetNormList(@WebParam(name = "request") GetNetNormsRequest request)
    {

        return session.list(request);
    }
}
