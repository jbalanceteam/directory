package org.jbalance.directory.service.flowchart;

import org.jbalance.directory.core.model.flowchart.PackingFlowChart;
import org.jbalance.directory.core.query.helper.flowchart.GetPackingFlowChartRequest;
import org.jbalance.directory.core.session.flowchart.PackingFlowChartSession;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

/**
 * Created by Mihail on 23.01.15.
 */
@WebService(serviceName = "PackingFlowChartService")
@Stateless
public class PackingFlowChartService {

	@EJB
	PackingFlowChartSession session;

	@WebMethod(operationName = "getById")
	public PackingFlowChart getById(@WebParam(name = "id") Long id)
	{
		return session.getById(id);
	}

	/**
	 *
	 *
	 * @param date
	 * @return
	 */
	@WebMethod(operationName = "getUpdated")
	public List<PackingFlowChart> getUpdated(@WebParam(name = "date") Date date)
	{
		return session.getUpdates(date);
	}

	/**
	 * Get list filtered of objects
	 *
	 * @param  request
	 * @return
	 */
	@WebMethod(operationName = "getFilteredList")
	public List<PackingFlowChart> getFilteredList(@WebParam(name = "request") GetPackingFlowChartRequest request)
	{
		return session.list(request);
	}
}
