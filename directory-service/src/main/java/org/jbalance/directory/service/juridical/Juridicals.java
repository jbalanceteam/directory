package org.jbalance.directory.service.juridical;

import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.jbalance.directory.core.model.Juridical;
import org.jbalance.directory.core.query.helper.juridical.GetJuridicalsRequest;
import org.jbalance.directory.core.session.juridical.JuridicalSession;

/**
 *
 *
 *
 * @author Mihail
 */
@WebService(serviceName = "Juridicals")
@Stateless
public class Juridicals
{

//TODO: change to @Inject. Figure out why it isn't working
    @EJB
    JuridicalSession session;

    @WebMethod(operationName = "getById")
    public Juridical getById(@WebParam(name = "id") Long id)
    {
        return session.getById(id);
    }

    /**
     * This is a sample web service operation
     *
     * @param date
     * @return
     */
    @WebMethod(operationName = "getUpdatedJuridicals")
    public List<Juridical> getUpdatedJuridicals(@WebParam(name = "date") Date date)
    {

        return session.getUpdates(date);
    }

    /**
     * This is a sample web service operation
     *
     * @param request
     * @return
     */
    @WebMethod(operationName = "getFilteredJuridicalList")
    public List<Juridical> getFilteredJuridicalList(@WebParam(name = "request") GetJuridicalsRequest request)
    {

        return session.list(request);
    }
}
