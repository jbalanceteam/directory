package org.jbalance.directory.service.norm;

import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import org.jbalance.directory.core.model.norm.Stay;
import org.jbalance.directory.core.query.helper.norm.GetStayRequest;
import org.jbalance.directory.core.session.norm.StaySession;

/**
 *
 * @author Mihail
 */
@WebService(serviceName = "Stay")
public class StayService {

    @EJB
    StaySession session;

    @WebMethod(operationName = "getById")
    public Stay getById(@WebParam(name = "id") Long id)
    {
        return session.getById(id);
    }

    /**
     *
     * @param date
     * @return
     */
    @WebMethod(operationName = "getUpdatedStayList")
    public List<Stay> getUpdatedStayList(@WebParam(name = "date") Date date)
    {

        return session.getUpdates(date);
    }

    /**
     * @param request
     * @return
     */
    @WebMethod(operationName = "getFilteredStayList")
    public List<Stay> getFilteredStayList(@WebParam(name = "request") GetStayRequest request)
    {

        return session.list(request);
    }
}
